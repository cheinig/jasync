package de.chdev.jasync.usecase.register;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class OptionsRequest {

    @Autowired
    TestRestTemplate restTemplate;

    @Before
    public void setup(){
        BasicAuthorizationInterceptor bai = new BasicAuthorizationInterceptor("test", "test");
        restTemplate.getRestTemplate().getInterceptors().add(bai);
    }

    @Test
    public void firstOptionsRequest() {
        ResponseEntity<Object> exchange = restTemplate.exchange("/Microsoft-Server-ActiveSync", HttpMethod.OPTIONS, new HttpEntity<>(new HttpHeaders()), Object.class);
        Assert.assertThat(exchange.getStatusCode(), is(HttpStatus.OK));
        Assert.assertThat(exchange, Matchers.notNullValue());
        Assert.assertThat(exchange.getHeaders(), IsMapContaining.hasKey("ms-asprotocolcommands"));
    }
}
