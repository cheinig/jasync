package de.chdev.jasync;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.model.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JasyncApplicationTests {

	@Before
	public void setup(){
		BackendRegistry.getInstance().removeBackend("dummy");
	}

	@Test
	public void contextLoads() {
		assertThat(Configuration.getInstance(), notNullValue());
		assertThat(Configuration.getInstance().getGreeting(), is("Hello"));
		assertThat(BackendRegistry.getInstance().getBackendHandlerList().size(), is(1));
	}

}
