package de.chdev.jasync.backend.carddav;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class CardDavRESTFunctionsMock extends CardDavRESTFunctions {

    Boolean autodiscoverUserDavUriCalled = false;
    Boolean autodiscoverUserHomeURICalled = false;

    @Override
    public List<Addressbook> getAddressbookList(URI uri, String username, String password) {
        List<Addressbook> addressbookList = new ArrayList<>();

        if (username.equalsIgnoreCase("test"))
            addressbookList.addAll(getAddressbooksForUserTest());

        return addressbookList;
    }

    protected List<Addressbook> getAddressbooksForUserTest() {
        List<Addressbook> addressbookList = new ArrayList<>();

        Addressbook addressbook1 = new Addressbook();
        addressbook1.name="Book 1";
        addressbook1.id="id/book_1";
        addressbookList.add(addressbook1);

        Addressbook addressbook2 = new Addressbook();
        addressbook2.name="Book 2";
        addressbook2.id="id/book_2";
        addressbookList.add(addressbook2);
        return addressbookList;
    }

    @Override
    public URI autodiscoverUserDavURI(URI baseUri, String username, String password) {
        autodiscoverUserDavUriCalled=true;
        return super.autodiscoverUserDavURI(baseUri,username,password);
    }

    @Override
    public URI autodiscoverUserHomeURI(URI userUri, String username, String password) {
        autodiscoverUserHomeURICalled=true;
        return super.autodiscoverUserHomeURI(userUri, username, password);
    }
}
