package de.chdev.jasync.backend.carddav.prepare;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jackrabbit.webdav.client.methods.HttpMkcol;

import java.io.IOException;

public class AddressBookOperations {

    private String serverUrl = "http://carddav-server:5232";

    public AddressBookOperations(){
        if (System.getenv("backend.carddav.server")!=null){
            serverUrl = System.getenv("backend.carddav.server");
        }
    }

    public Integer deleteAddressbook(String username, String addressbookName) throws IOException {
        HttpDelete request = new HttpDelete(serverUrl + "/" + username + "/" + addressbookName);
        request.addHeader("Authorization", "Basic dGVzdDp0ZXN0");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        return response.getStatusLine().getStatusCode();
    }

    public Integer createAddressbook(String username, String addressbookName, String description) throws IOException {
        HttpMkcol request = new HttpMkcol(serverUrl + "/" + username + "/" + addressbookName);
        request.addHeader("Content-Type", "application/xml");
        request.addHeader("Authorization", "Basic dGVzdDp0ZXN0");
        request.setEntity(new StringEntity(getCreateAddressbookBodyContent(addressbookName, description)));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        return response.getStatusLine().getStatusCode();
    }

    private String getCreateAddressbookBodyContent(String name, String description) {
        String content = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                "<D:mkcol xmlns:D=\"DAV:\"\n" +
                "                 xmlns:C=\"urn:ietf:params:xml:ns:carddav\">\n" +
                "     <D:set>\n" +
                "       <D:prop>\n" +
                "         <D:resourcetype>\n" +
                "           <D:collection/>\n" +
                "           <C:addressbook/>\n" +
                "         </D:resourcetype>\n" +
                "         <D:displayname>%s</D:displayname>\n" +
                "         <C:addressbook-description xml:lang=\"en\">%s</C:addressbook-description>\n" +
                "       </D:prop>\n" +
                "     </D:set>\n" +
                "</D:mkcol>";
        return String.format(content, name, description);
    }
}
