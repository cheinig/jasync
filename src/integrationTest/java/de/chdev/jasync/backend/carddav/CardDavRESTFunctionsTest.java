package de.chdev.jasync.backend.carddav;

import de.chdev.jasync.backend.carddav.prepare.AddressBookOperations;
import de.chdev.jasync.backend.carddav.prepare.CarddavSzenarios;
import exceptions.AuthenticationFailedException;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.List;

public class CardDavRESTFunctionsTest {

    private String username = "test";
    private String wrongUsername = "wrong";
    private String password = "test";
    private String wrongPassword = "wrong";
    private String serverUrl = "";
    private String davUrl = "";
    private String userUrl = "/test/";
    private String homeUrl = "/test/";

    private CardDavRESTFunctions cardDavRESTFunctions;
    private CarddavSzenarios szenarios = new CarddavSzenarios(new AddressBookOperations());

    @Before
    public void setUp() throws Exception {
        cardDavRESTFunctions = new CardDavRESTFunctions();

        String serverName = "http://carddav-server:5232";
        if (System.getenv("backend.carddav.server")!=null){
            serverName = System.getenv("backend.carddav.server");
        }
        serverUrl = serverName + serverUrl;
        davUrl = serverName + davUrl;
        userUrl = serverName + userUrl;
        homeUrl = serverName + homeUrl;

        szenarios.createDefaultSzenario();
    }

    @Test
    public void autodiscoverUserURIFromDavURL() {
        URI userDavURI = cardDavRESTFunctions.autodiscoverUserDavURI(URI.create(davUrl), username, password);
        URI referenceURI = URI.create(userUrl);
        MatcherAssert.assertThat(userDavURI, Matchers.is(referenceURI));
    }

    @Test
    public void autodiscoverUserURIFromServerRoot() {
        URI davURI = cardDavRESTFunctions.autodiscoverUserDavURI(URI.create(serverUrl), username, password);
        URI referenceURI = URI.create(userUrl);
        MatcherAssert.assertThat(davURI, Matchers.is(referenceURI));
    }

    @Test
    public void autodiscoverAddressbookHomeURIFromUserUrl() {
        URI homeURI = cardDavRESTFunctions.autodiscoverUserHomeURI(URI.create(userUrl), username, password);
        URI referenceURI = URI.create(homeUrl);
        MatcherAssert.assertThat(homeURI, Matchers.is(referenceURI));
    }

    @Test
    public void addressbookListIsReturned() {
        List<CardDavRESTFunctions.Addressbook> addressbookList = cardDavRESTFunctions.getAddressbookList(URI.create(homeUrl), username, password);
        MatcherAssert.assertThat(addressbookList.size(), Matchers.is(2));
    }

    @Test(expected = AuthenticationFailedException.class)
    public void autodiscoverUserURIAccessDenied(){
        cardDavRESTFunctions.autodiscoverUserHomeURI(URI.create("http://httpstat.us/401"), wrongUsername, wrongPassword);
    }

    @Test(expected = AuthenticationFailedException.class)
    public void addressbookListAccessDenied(){
        cardDavRESTFunctions.getAddressbookList(URI.create("http://httpstat.us/401"), wrongUsername, wrongPassword);
    }

    @Test(expected = AuthenticationFailedException.class)
    public void autodiscoverDavURIAccessDenied(){
        cardDavRESTFunctions.autodiscoverUserDavURI(URI.create("http://httpstat.us/401"), wrongUsername, wrongPassword);
    }
}
