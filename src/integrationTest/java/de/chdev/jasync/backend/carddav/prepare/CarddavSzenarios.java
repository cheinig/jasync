package de.chdev.jasync.backend.carddav.prepare;

import java.io.IOException;

public class CarddavSzenarios {

    private AddressBookOperations operations;

    public CarddavSzenarios(AddressBookOperations operations) {
        this.operations = operations;
    }

    public void cleanup() throws IOException {
        operations.deleteAddressbook("test", "addressbook_1");
        operations.deleteAddressbook("test", "addressbook_2");
    }

    public void createDefaultSzenario() {
        try {
            cleanup();
            operations.createAddressbook("test", "addressbook_1", "The primary address book");
            operations.createAddressbook("test", "addressbook_2", "The secondary address book");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
