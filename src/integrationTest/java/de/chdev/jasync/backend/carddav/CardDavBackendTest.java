package de.chdev.jasync.backend.carddav;

import de.chdev.jasync.persistence.UserDevice;
import de.chdev.jasync.persistence.UserSettingRepositoryMock;
import de.chdev.jasync.xsd.response.folderhierarchy.FolderSync;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class CardDavBackendTest {

    private CardDavBackendImpl cardDavBackend = new CardDavBackendImpl();
    private CardDavRESTFunctionsMock restFunctionsMock = new CardDavRESTFunctionsMock();
    private UserSettingRepositoryMock userSettingRepositoryMock = new UserSettingRepositoryMock();

    private String serverUrl = "";
    private String davUrl = "";
    private String userUrl = "/test/";
    private String homeUrl = "/test/";
    private UserDevice userDeviceTest;
    private UserDevice userDeviceEmpty;

    @Before
    public void setup(){
        String serverName = "http://carddav-server:5232";
        if (System.getenv("backend.carddav.server")!=null){
            serverName = System.getenv("backend.carddav.server");
        }
        serverUrl = serverName + serverUrl;
        davUrl = serverName + davUrl;
        userUrl = serverName + userUrl;
        homeUrl = serverName + homeUrl;

        CardDavConfiguration.getInstance().setServer(davUrl);
        cardDavBackend.restFunctions = restFunctionsMock;
        cardDavBackend.userSettingRepository = userSettingRepositoryMock;
        restFunctionsMock.autodiscoverUserHomeURICalled=false;
        restFunctionsMock.autodiscoverUserDavUriCalled=false;
        userDeviceTest = new UserDevice("test", "test");
        userDeviceTest.setId(1l);
        userDeviceEmpty = new UserDevice("empty", "empty");
        userDeviceEmpty.setId(2l);
    }

    @Test
    public void changeWithTwoAddsIsReturnedForSyncKey0AndUserTest(){
        FolderSync.Changes folderChanges = cardDavBackend.getFolderChanges(0, userDeviceTest);
        assertThat(folderChanges.getAdd().size(), is(2));
    }

    @Test
    public void changeWithNoAddsIsReturnedForSyncKey0AndUserEmpty(){
        FolderSync.Changes folderChanges = cardDavBackend.getFolderChanges(0, userDeviceEmpty);
        assertThat(folderChanges.getAdd().size(), is(0));
    }

    @Test
    public void getAddressbookUriStartsAutodiscoveryOnFirstCall(){
        URI addressbookUri = cardDavBackend.getAddressbookUri(userDeviceTest);
        assertThat(addressbookUri, is(URI.create(homeUrl)));
        assertThat(((CardDavRESTFunctionsMock)cardDavBackend.restFunctions).autodiscoverUserHomeURICalled, is(true));
    }

    @Test
    public void getAddressbookUriNotStartsAutodiscoveryOnSecondCall(){
        URI addressbookUri = cardDavBackend.getAddressbookUri(userDeviceTest);
        assertThat(addressbookUri, is(URI.create(homeUrl)));
        assertThat(((CardDavRESTFunctionsMock)cardDavBackend.restFunctions).autodiscoverUserHomeURICalled, is(true));
        restFunctionsMock.autodiscoverUserHomeURICalled=false;
        restFunctionsMock.autodiscoverUserDavUriCalled=false;
        addressbookUri = cardDavBackend.getAddressbookUri(userDeviceTest);
        assertThat(addressbookUri, is(URI.create(homeUrl)));
        assertThat(((CardDavRESTFunctionsMock)cardDavBackend.restFunctions).autodiscoverUserHomeURICalled, is(false));
    }
}
