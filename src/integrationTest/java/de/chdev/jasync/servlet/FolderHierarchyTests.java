package de.chdev.jasync.servlet;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.backend.dummy.ContactDummyBackend;
import de.chdev.jasync.utils.HttpUtils;
import de.chdev.jasync.utils.IOUtils;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class FolderHierarchyTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void sendFolderSyncKey0() throws IOException {

        BackendRegistry.getInstance().addBackend("dummy", new ContactDummyBackend());

        byte[] bytesFromFile = IOUtils.getBytesFromFile("testdata/folderhierarchy/foldersyncRequestKey0.wbxml");
        byte[] resultBytesFromFile = IOUtils.getBytesFromFile("testdata/folderhierarchy/foldersyncResponseKey0OnlyContacts.wbxml");
        HttpHeaders headers = new HttpHeaders();
        headers.add("MS-ASProtocolVersion", "16.0");
        headers.add("Authorization", HttpUtils.getBasicAuth("empty", "empty"));
        HttpEntity httpEntity = new HttpEntity(bytesFromFile, headers);
        ResponseEntity<byte[]> responseEntity = this.restTemplate.exchange("http://localhost:" + port + "?Cmd=FolderSync", HttpMethod.POST, httpEntity, byte[].class);
        assertThat(responseEntity.getStatusCode().value(), is(HttpStatus.SC_OK));
        assertThat(responseEntity.hasBody(), is(true));
        assertThat(responseEntity.getBody(), is(resultBytesFromFile));
    }
}
