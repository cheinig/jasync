package de.chdev.jasync.servlet;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DispatcherServletTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void httpStatus405IsReturnedFromNotImplementedOperation() {
        assertThat(this.restTemplate.exchange("http://localhost:" + port + "/", HttpMethod.PUT, null, String.class).getStatusCode()).isEqualByComparingTo(HttpStatus.METHOD_NOT_ALLOWED);
        assertThat(this.restTemplate.exchange("http://localhost:" + port + "/", HttpMethod.DELETE, null, String.class).getStatusCode()).isEqualByComparingTo(HttpStatus.METHOD_NOT_ALLOWED);
        assertThat(this.restTemplate.exchange("http://localhost:" + port + "/", HttpMethod.GET, null, String.class).getStatusCode()).isEqualByComparingTo(HttpStatus.METHOD_NOT_ALLOWED);
        assertThat(this.restTemplate.exchange("http://localhost:" + port + "/", HttpMethod.TRACE, null, String.class).getStatusCode()).isEqualByComparingTo(HttpStatus.METHOD_NOT_ALLOWED);
    }

    @Test
    public void httpStatus401IsReturnedForUnauthorizedOptions() {
        assertThat(this.restTemplate.exchange("http://localhost:" + port + "/", HttpMethod.OPTIONS, null, String.class).getStatusCode()).isEqualByComparingTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void httpStatus401IsReturnedForUnauthorizedPost() {
        assertThat(this.restTemplate.exchange("http://localhost:" + port + "/", HttpMethod.POST, null, String.class).getStatusCode()).isEqualByComparingTo(HttpStatus.UNAUTHORIZED);
    }
}
