package de.chdev.jasync.backend.dummy;

import de.chdev.jasync.model.interfaces.IBackendHandler;
import de.chdev.jasync.model.interfaces.IFolderHierarchy;
import de.chdev.jasync.model.Constants;
import de.chdev.jasync.persistence.UserDevice;
import de.chdev.jasync.xsd.response.folderhierarchy.FolderSync;
import exceptions.AuthenticationFailedException;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ContactDummyBackend implements IBackendHandler, IFolderHierarchy {

    private List<Addressbook> addressbooks = new ArrayList<>();
    private String defaultAddressbookName = "Addr1";

    public ContactDummyBackend(){
        initDefaultAddressbooks();
    }

    @Override
    public FolderSync.Changes getFolderChanges(Integer syncKey, UserDevice userDevice) {
        FolderSync.Changes changes = new FolderSync.Changes();

        for (Addressbook addressbook : addressbooks){
            FolderSync.Changes.Add addAddress = new FolderSync.Changes.Add();
            addAddress.setDisplayName(addressbook.displayName);
            addAddress.setParentId("0");
            addAddress.setServerId(addressbook.serverId);
            addAddress.setType(BigInteger.valueOf(addressbook.displayName.equalsIgnoreCase(defaultAddressbookName)?Constants.FOLDERTYPE_DEFAULT_CONTACT:Constants.FOLDERTYPE_CUSTOM_CONTACT));
            changes.getAdd().add(addAddress);
        }

        return changes;
    }

    public void initDefaultAddressbooks(){
        addressbooks.clear();

        Addressbook addressbook = new Addressbook();
        addressbook.displayName="Addr1";
        addressbook.serverId="1";
        addressbooks.add(addressbook);

        addressbook = new Addressbook();
        addressbook.displayName="Addr2";
        addressbook.serverId="2";
        addressbooks.add(addressbook);
    }

    @Override
    public void validateCredentials(UserDevice userDevice) {

        if (userDevice==null || userDevice.getUsername() == null || userDevice.getPassword()== null || userDevice.getUsername().isEmpty() || userDevice.getUsername().equals("wrong") || userDevice.getPassword().isEmpty() || userDevice.getPassword().equals("wrong"))
            throw new AuthenticationFailedException();
    }

    class Addressbook {
        List<Contact> contactList = new ArrayList<>();

        String displayName;
        String serverId;
    }

    class Contact {

    }
}
