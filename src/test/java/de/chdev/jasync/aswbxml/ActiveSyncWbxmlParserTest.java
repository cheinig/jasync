package de.chdev.jasync.aswbxml;

import org.apache.commons.io.IOUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.xmlunit.matchers.CompareMatcher.isIdenticalTo;

public class ActiveSyncWbxmlParserTest {

    @Test
    public void checkCodePage0CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage0.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage0.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage1CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage1.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage1.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage2CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage2.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage2.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage4CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage4.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage4.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage5CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage5.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage5.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage6CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage6.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage6.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage7CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage7.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage7.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage8CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage8.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage8.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage9CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage9.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage9.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage10CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage10.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage10.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage11CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage11.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage11.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage12CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage12.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage12.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage13CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage13.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage13.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage14CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage14.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage14.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage15CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage15.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage15.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage16CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage16.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage16.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage17CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage17.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage17.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage18CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage18.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage18.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage19CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage19.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage19.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage20CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage20.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage20.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage21CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage21.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage21.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage22CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage22.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage22.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage23CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage23.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage23.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage24CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage24.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage24.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCodePage25CanConvertedFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/flatFullCodepage25.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/flatFullCodepage25.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    @Test
    public void checkCanConvertComplexFromWbxmlToXml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(getBytesFromFile("testdata/complexNamespaces.wbxml"));
        String xml = activeSyncWbxmlConverter.getXmlString();
        String resultXml = getXmlFromFile("testdata/complexNamespaces.xml");
        assertThat(xml, isIdenticalTo(resultXml));
    }

    protected byte[] getBytesFromFile(String filePath) throws IOException {
        InputStream wbxmlInputStream = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        return IOUtils.toByteArray(wbxmlInputStream);
    }

    protected String getXmlFromFile(String filePath) throws IOException {
        InputStream xmlInputStream = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        String xmlString = IOUtils.toString(xmlInputStream, "UTF-8");
        xmlString = xmlString.replaceAll("\n","");
        xmlString = xmlString.replaceAll("\t","");
        return  xmlString;
    }
}
