package de.chdev.jasync.aswbxml;

import org.apache.commons.io.IOUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class ActiveSyncWbxmlSerializerTest {

    @Test
    public void checkCodePage0CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage0.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage0.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage1CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage1.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage1.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage2CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage2.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage2.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage4CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage4.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage4.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage5CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage5.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage5.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage6CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage6.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage6.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage7CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage7.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage7.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage8CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage8.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage8.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage9CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage9.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage9.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage10CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage10.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage10.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage11CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage11.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage11.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage12CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage12.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage12.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage13CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage13.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage13.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage14CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage14.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage14.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage15CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage15.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage15.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage16CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage16.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage16.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage17CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage17.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage17.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage18CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage18.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage18.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage19CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage19.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage19.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage20CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage20.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage20.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage21CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage21.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage21.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage22CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage22.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage22.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage23CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage23.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage23.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage24CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage24.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage24.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCodePage25CanConvertedFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/flatFullCodepage25.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/flatFullCodepage25.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    @Test
    public void checkCanConvertedComplexFromXmlToWbxml() throws IOException {
        ActiveSyncWbxmlConverter activeSyncWbxmlConverter = getActiveSyncWbxmlConverter("testdata/complexNamespaces.xml");
        byte[] wbxml = activeSyncWbxmlConverter.getWbxml();
        byte[] resultWbxml = getBytesFromFile("testdata/complexNamespaces.wbxml");
        MatcherAssert.assertThat(wbxml, Matchers.equalTo(resultWbxml));
    }

    protected byte[] getBytesFromFile(String filePath) throws IOException {
        InputStream wbxmlInputStream = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        return IOUtils.toByteArray(wbxmlInputStream);
    }

    protected ActiveSyncWbxmlConverter getActiveSyncWbxmlConverter(String filePath) throws IOException {
        InputStream xmlInputStream = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        String xmlText = IOUtils.toString(xmlInputStream, "UTF-8");
        return new ActiveSyncWbxmlConverter(xmlText);
    }
}
