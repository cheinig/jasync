package de.chdev.jasync.workflow;

import de.chdev.jasync.ashttp.HttpParser;
import de.chdev.jasync.model.HttpTransfer;
import de.chdev.jasync.model.Request;

public class HttpParserSpy extends HttpParser {

    HttpTransfer transfer;

    @Override
    public Request getASRequestFromHttpDataAndValidate(HttpTransfer transfer) {
        this.transfer = transfer;
        return super.getASRequestFromHttpDataAndValidate(transfer);
    }
}
