package de.chdev.jasync.workflow;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.IOException;

public class ServletInputStreamMock extends ServletInputStream {

    private int counter = -1;

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void setReadListener(ReadListener listener) {

    }

    @Override
    public int read() throws IOException {
        if (counter < 10) {
            counter++;
            return counter;
        } else {
            return -1;
        }
    }
}
