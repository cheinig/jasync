package de.chdev.jasync.workflow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

import de.chdev.jasync.ascmd.CommandHandlerMock;
import de.chdev.jasync.ashttp.OptionsHandler;
import de.chdev.jasync.model.HttpTransfer;
import de.chdev.jasync.utils.HttpUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class IncomingRequestHandlerTest {

    private IncomingRequestHandler requestHandler;
    private HttpServletRequest mockHttpRequest;
    private Map<String, String> sampleHeaderMap;

    @Before
    public void setUp() {
        requestHandler = new IncomingRequestHandler();
        requestHandler.httpParser = new HttpParserSpy();
        requestHandler.optionsHandler = new OptionsHandler();
        requestHandler.commandHandler = new CommandHandlerMock();

        mockHttpRequest = Mockito.mock(HttpServletRequest.class);
        sampleHeaderMap = new HashMap<>();
        sampleHeaderMap.put("test1", "value1");
        sampleHeaderMap.put("test2", "value2");
        sampleHeaderMap.put("authorization", HttpUtils.getBasicAuth("test", "test"));
        when(mockHttpRequest.getMethod()).thenReturn("POST");
        when(mockHttpRequest.getHeaderNames()).thenReturn(new Vector(sampleHeaderMap.keySet()).elements());
        when(mockHttpRequest.getHeader("test1")).thenReturn("value1");
        when(mockHttpRequest.getHeader("test2")).thenReturn("value2");
        when(mockHttpRequest.getHeader("authorization")).thenReturn(HttpUtils.getBasicAuth("test", "test"));
    }

    @Test
    public void transferObjectIsCreatedAndDeliveredToHandler() throws IOException {

        when(mockHttpRequest.getMethod()).thenReturn("OPTIONS");

        requestHandler.handleIncomingRequest(mockHttpRequest, Mockito.mock(HttpServletResponse.class));

        assertThat(((HttpParserSpy)requestHandler.httpParser).transfer, notNullValue());
    }



    @Test
    public void transferObjectHasFilledOptionsMethod() throws IOException {

        when(mockHttpRequest.getMethod()).thenReturn("OPTIONS");

        requestHandler.handleIncomingRequest(mockHttpRequest, Mockito.mock(HttpServletResponse.class));

        HttpTransfer resultTransfer =  new HttpTransfer();
        resultTransfer.setMethod("OPTIONS");

        assertThat(((HttpParserSpy)requestHandler.httpParser).transfer.getMethod(),is(resultTransfer.getMethod()));
    }

    @Test
    public void transferObjectHasFilledPostMethod() throws IOException {

        when(mockHttpRequest.getMethod()).thenReturn("POST");

        requestHandler.handleIncomingRequest(mockHttpRequest, Mockito.mock(HttpServletResponse.class));

        HttpTransfer resultTransfer =  new HttpTransfer();
        resultTransfer.setMethod("POST");

        assertThat(((HttpParserSpy)requestHandler.httpParser).transfer.getMethod(),is(resultTransfer.getMethod()));
    }

    @Test
    public void transferObjectHasFilledQuery() throws IOException {

        when(mockHttpRequest.getQueryString()).thenReturn("cmd=TEST");
        when(mockHttpRequest.getMethod()).thenReturn("POST");

        requestHandler.handleIncomingRequest(mockHttpRequest, Mockito.mock(HttpServletResponse.class));

        HttpTransfer resultTransfer = new HttpTransfer();
        resultTransfer.setQuery("cmd=TEST");

        assertThat(((HttpParserSpy)requestHandler.httpParser).transfer.getQuery(),is(resultTransfer.getQuery()));
    }

    @Test
    public void transferObjectHasFilledBody() throws IOException {

        ServletInputStream inputStream = new ServletInputStreamMock();
        when(mockHttpRequest.getInputStream()).thenReturn(inputStream);
        when(mockHttpRequest.getMethod()).thenReturn("POST");

        requestHandler.handleIncomingRequest(mockHttpRequest, Mockito.mock(HttpServletResponse.class));

        HttpTransfer resultTransfer = new HttpTransfer();
        resultTransfer.setBody(new byte[]{0,1,2,3,4,5,6,7,8,9,10});

        assertThat(((HttpParserSpy)requestHandler.httpParser).transfer.getBody(),is(resultTransfer.getBody()));
    }

    @Test
    public void transferObjectHasFilledHeaders() throws IOException {
        requestHandler.handleIncomingRequest(mockHttpRequest, Mockito.mock(HttpServletResponse.class));
        assertThat(((HttpParserSpy)requestHandler.httpParser).transfer.getHeaders(),is(sampleHeaderMap));
    }

    @Test
    public void transferToHttpResponseFilledHeaders() {
        Map<String, String> sampleHeaderMap = new HashMap<>();
        sampleHeaderMap.put("test1", "value1");
        sampleHeaderMap.put("test2", "value2");

        HttpTransfer transfer = new HttpTransfer();
        transfer.setHeaders(sampleHeaderMap);

        HttpServletResponse httpResponse = new MockHttpServletResponse();
        httpResponse = requestHandler.mapResponseTransferToHttpResponse(transfer, httpResponse);

        assertThat(httpResponse.getHeader("test1"),is("value1"));
        assertThat(httpResponse.getHeader("test2"),is("value2"));
    }

    @Test
    public void transferToHttpResponseFilledBody() {

        HttpTransfer responseTransfer = new HttpTransfer();
        responseTransfer.setBody(new byte[]{0,1,2,3,4,5,6,7,8,9,10});

        MockHttpServletResponse httpResponse = new MockHttpServletResponse();
        requestHandler.mapResponseTransferToHttpResponse(responseTransfer, httpResponse);

        assertThat(httpResponse.getContentAsByteArray(),is(responseTransfer.getBody()));
    }
}
