package de.chdev.jasync.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserSettingRepositoryMock implements UserSettingRepository {

    List<UserSetting> userSettingList = new ArrayList<>();
    Long lastId = 0l;

    @Override
    public List<UserSetting> getUserSettingsByUserDeviceAndBackendIdAndParameterName(UserDevice userDevice, String backendId, String parameterName) {
        List<UserSetting> customUserSettingList = new ArrayList<>();
        for (UserSetting userSetting : userSettingList){
            if (userSetting.getUserDevice().getId().equals(userDevice.getId())
                    && userSetting.getBackendId().equals(backendId)
                    && userSetting.getParameterName().equals(parameterName)){
                customUserSettingList.add(userSetting);
            }
        }
        return customUserSettingList;
    }

    @Override
    public UserSetting save(UserSetting entity) {
        userSettingList.add(entity);
        entity.setId(lastId);
        lastId++;
        return entity;
    }

    @Override
    public Iterable<UserSetting> save(Iterable entities) {
        return null;
    }

    @Override
    public UserSetting findOne(Long aLong) {
        return null;
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<UserSetting> findAll() {
        return null;
    }

    @Override
    public Iterable<UserSetting> findAll(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Override
    public void delete(UserSetting entity) {

    }

    @Override
    public void delete(Iterable<? extends UserSetting> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
