package de.chdev.jasync.persistence;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserDeviceRepositoryTest {

    private static final Logger log = LoggerFactory.getLogger(UserDeviceRepositoryTest.class);

    @Autowired
    UserDeviceRepository userDeviceRepository;

    @Test
    public void userDeviceRepositoryGetSingleDeviceTest() {
        userDeviceRepository.deleteAll();
        userDeviceRepository.save(new UserDevice("Testuser 1","DevId1"));
        userDeviceRepository.save(new UserDevice("Testuser 2","DevId2"));
        userDeviceRepository.save(new UserDevice("Testuser 2","DevId3"));
        userDeviceRepository.save(new UserDevice("Testuser 1","DevId4"));

        UserDevice userDevice = userDeviceRepository.getFirstByUsernameAndDeviceId("Testuser 2", "DevId3");

        MatcherAssert.assertThat(userDevice.getUsername(), Matchers.is("Testuser 2"));
        MatcherAssert.assertThat(userDevice.getDeviceId(), Matchers.is("DevId3"));
    }

    @Test
    public void userDeviceRepositoryGetAllTest() {
        userDeviceRepository.deleteAll();
        userDeviceRepository.save(new UserDevice("Testuser 1","DevId1"));
        userDeviceRepository.save(new UserDevice("Testuser 2","DevId2"));
        userDeviceRepository.save(new UserDevice("Testuser 2","DevId3"));
        userDeviceRepository.save(new UserDevice("Testuser 1","DevId4"));

        int count=0;
        for (UserDevice userDevice : userDeviceRepository.findAll()) {
            count++;
        }

        MatcherAssert.assertThat(count, Matchers.is(4));
    }


}
