package de.chdev.jasync.persistence;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserSettingRepositoryTest {

    private static final Logger log = LoggerFactory.getLogger(UserSettingRepositoryTest.class);

    @Autowired
    UserSettingRepository userSettingRepository;

    @Autowired
    UserDeviceRepository userDeviceRepository;
    private UserDevice user1;
    private String backendId;
    private String parameterName1;
    private String parameterName2;

    @Before
    public void setup(){
        userDeviceRepository.deleteAll();

        user1 = new UserDevice("Username 1", "Device 1");
        userDeviceRepository.save(user1);
        backendId = "backend1";
        parameterName1 = "Parameter 1";
        parameterName2 = "Parameter 2";

        userSettingRepository.save(new UserSetting(parameterName1,"Value 1", null, user1));
        userSettingRepository.save(new UserSetting(parameterName2,"Value 2", null, user1));
        userSettingRepository.save(new UserSetting(parameterName1,"Value 3", backendId, user1));
        userSettingRepository.save(new UserSetting(parameterName2,"Value 4", backendId, user1));
    }

    @Test
    public void userDeviceRepositoryGetAllTest() {
        int count=0;
        for (UserSetting userSetting : userSettingRepository.findAll()) {
            count++;
        }

        MatcherAssert.assertThat(count, Matchers.is(4));
    }

    @Test
    public void userSettingByUserBackendParameterTest(){

        List<UserSetting> userSettings = userSettingRepository.getUserSettingsByUserDeviceAndBackendIdAndParameterName(user1, backendId, parameterName1);
        MatcherAssert.assertThat(userSettings.size(), Matchers.is(1));
    }

}
