package de.chdev.jasync.persistence;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CollectionRepositoryTest {

    private static final Logger log = LoggerFactory.getLogger(CollectionRepositoryTest.class);

    @Autowired
    CollectionRepository collectionRepository;

    @Autowired
    UserDeviceRepository userDeviceRepository;


    @Test
    public void userDeviceRepositoryGetAllTest() {
        userDeviceRepository.deleteAll();
        UserDevice user1 = new UserDevice("Username 1", "Device 1");
        userDeviceRepository.save(user1);

        Collection collection1 = new Collection("BackendId1","Collection1", "Col 1", "14", null, user1);
        collectionRepository.save(collection1);
        collectionRepository.save(new Collection("BackendId1","Collection2", "Col 2", "9", collection1, user1));
        collectionRepository.save(new Collection("BackendId2","Collection1", "Col a", "14", null, user1));
        collectionRepository.save(new Collection("BackendId2","Collection2", "Col b", "9", null, user1));

        int count=0;
        for (Collection collection : collectionRepository.findAll()) {
            count++;
        }

        MatcherAssert.assertThat(count, Matchers.is(4));
    }


}
