package de.chdev.jasync.persistence;

import java.util.ArrayList;
import java.util.List;

public class UserDeviceRepositoryMock implements UserDeviceRepository {

    public List<UserDevice> userDeviceList = new ArrayList<>();
    Long lastId = 0l;

    @Override
    public UserDevice getFirstByUsernameAndDeviceId(String username, String deviceId){
        List<UserDevice> customUserDeviceList = new ArrayList<>();
        for (UserDevice userDevice : userDeviceList){
            if (userDevice.getUsername().equals(username)
                    && userDevice.getDeviceId().equals(deviceId)){
                customUserDeviceList.add(userDevice);
            }
        }
        return customUserDeviceList.size()>0?customUserDeviceList.get(0):null;
    }

    @Override
    public UserDevice save(UserDevice entity) {
        userDeviceList.add(entity);
        entity.setId(lastId);
        lastId++;
        return entity;
    }

    @Override
    public Iterable<UserDevice> save(Iterable entities) {
        return null;
    }

    @Override
    public UserDevice findOne(Long aLong) {
        return null;
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<UserDevice> findAll() {
        return null;
    }

    @Override
    public Iterable<UserDevice> findAll(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Override
    public void delete(UserDevice entity) {

    }

    @Override
    public void delete(Iterable<? extends UserDevice> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
