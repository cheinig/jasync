package de.chdev.jasync.ascmd;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.backend.dummy.ContactDummyBackend;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Status;
import de.chdev.jasync.persistence.UserDevice;
import de.chdev.jasync.persistence.UserDeviceRepositoryMock;
import exceptions.AuthenticationFailedException;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isIdenticalTo;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

public class CommandHandlerTest {

    private Request request;
    private CommandHandler commandHandler;
    private UserDevice userDevice;

    @Before
    public void setUp() throws Exception {
        request = new Request();
        userDevice = new UserDevice("empty", "empty");
        userDevice.setPassword("empty");
        request.setUserDevice(userDevice);
        commandHandler = new CommandHandlerMock();
        BackendRegistry.getInstance().removeAllBackends();
        BackendRegistry.getInstance().addBackend("test", new ContactDummyBackend());
    }

    @Test
    public void validateCommandNotSupportedError(){
        Request validatedRequest = commandHandler.handleRequest(request);
        assertThat(validatedRequest.getRelatedResponse().getGlobalStatus().getIntValue(), Matchers.is(Status.ErrorCommandNotSupported));
    }

    @Test
    public void validateVersionNotSupportedError(){
        request.setCommand("FolderSync");
        request.setVersion("12.0");
        Request validatedRequest = commandHandler.handleRequest(request);
        assertThat(validatedRequest.getRelatedResponse().getGlobalStatus().getIntValue(), Matchers.is(Status.ErrorVersionNotSupported));
    }

    @Test
    public void checkExistingUserDeviceIsLoadedFromDatabase(){
        UserDevice persistedUserDevice = new UserDevice("empty", "empty");
        persistedUserDevice.setId(999l);
        ((UserDeviceRepositoryMock)commandHandler.userDeviceRepository).userDeviceList.add(persistedUserDevice);
        request.setCommand("FolderSync");
        request.setVersion("14.0");
        Request request = commandHandler.handleRequest(this.request);
        assertThat(request.getUserDevice().getId(), Matchers.is(persistedUserDevice.getId()));
    }

    @Test
    public void checkNewUserDeviceIsSavedToDatabase(){
        request.setCommand("FolderSync");
        request.setVersion("14.0");
        Request request = commandHandler.handleRequest(this.request);
        assertThat(request.getUserDevice().getId(), Matchers.is(0l));
    }

    @Test
    public void folderSyncXmlResponseBindingTest() throws IOException, JAXBException {
        String xmlText = getXmlFromFile("testdata/folderhierarchy/foldersyncResponseKey0.xml");
        request.setCommand("FolderSync");
        request.getRelatedResponse().setXmlData(xmlText);
        request.getRelatedResponse().setObjectData(commandHandler.unmarshalResponseXml(request));
        String resultXml = commandHandler.marshalResponseXml(request);

        assertThat(resultXml, isSimilarTo(xmlText).ignoreWhitespace());
    }

    @Test
    public void folderSyncXmlRequestBindingTest() throws IOException, JAXBException {
        String xmlText = getXmlFromFile("testdata/folderhierarchy/foldersyncRequestKey0.xml");
        request.setCommand("FolderSync");
        request.setXmlData(xmlText);
        request.setObjectData(commandHandler.unmarshalRequestXml(request));
        String resultXml = commandHandler.marshalRequestXml(request);

        assertThat(resultXml, isSimilarTo(xmlText).ignoreWhitespace());
    }

    @Test(expected = AuthenticationFailedException.class)
    public void authenticationValidationFailedOnMissingUsername() {
        request.getUserDevice().setUsername("");
        commandHandler.handleRequest(request);
    }

    @Test(expected = AuthenticationFailedException.class)
    public void authenticationValidationFailedOnWrongCredentials() {
        UserDevice userDevice = new UserDevice("test", "test");
        request.setUserDevice(userDevice);
        commandHandler.handleRequest(request);
    }

    protected String getXmlFromFile(String filePath) throws IOException {
        InputStream xmlInputStream = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        String xmlString = IOUtils.toString(xmlInputStream, "UTF-8");
        xmlString = xmlString.replaceAll("\n","");
        xmlString = xmlString.replaceAll("\t","");
        return  xmlString;
    }
}
