package de.chdev.jasync.ascmd;

import de.chdev.jasync.ascmd.folderhierarchy.FolderHierarchyHandler;
import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.backend.dummy.ContactDummyBackend;
import de.chdev.jasync.model.Constants;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.xsd.response.folderhierarchy.FolderSync;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Test Folder structure
 * Calendar
 *  Cal1
 *  Cal2
 * Contacts
 *  Addr1
 *  Addr2
 * Email
 *  Inbox
 *  Archive
 *  Drafts
 *  Sent
 *  Spam
 *  Trash
 *  Custom
 *      CustomSub1
 *      CustomSub2
 */
public class FolderHierarchyHandlerTest {

    private FolderHierarchyHandler folderHierarchyHandler;
    private Request request;

    @Before
    public void setup(){
        folderHierarchyHandler = new FolderHierarchyHandler();
        request = new Request();
        request.setCommand("FolderSync");
        BackendRegistry.getInstance().removeAllBackends();
    }

    @Test
    public void folderSyncInResponseAvailable(){
        folderHierarchyHandler.handleRequest(request);
        assertThat(request.getRelatedResponse().getObjectData(), Matchers.instanceOf(FolderSync.class));
    }

    @Test
    public void folderSyncHas2ItemsInResponseWithOnlyContactBackend(){
        BackendRegistry.getInstance().addBackend("dummy", new ContactDummyBackend());
        folderHierarchyHandler.handleRequest(request);
        FolderSync folderSyncResponse = (FolderSync) request.getRelatedResponse().getObjectData();
        assertThat(folderSyncResponse.getChanges(), notNullValue());
        assertThat(folderSyncResponse.getChanges().getCount(), is(2l));
        assertThat(folderSyncResponse.getChanges().getAdd().size(), is(2));
        assertThat(folderSyncResponse.getChanges().getAdd().get(0).getDisplayName(), is("Addr1"));
        assertThat(folderSyncResponse.getChanges().getAdd().get(0).getParentId(), is("0"));
        assertThat(folderSyncResponse.getChanges().getAdd().get(0).getServerId(), is("1"));
        assertThat(folderSyncResponse.getChanges().getAdd().get(0).getType(), is(BigInteger.valueOf(Constants.FOLDERTYPE_DEFAULT_CONTACT)));
        assertThat(folderSyncResponse.getChanges().getAdd().get(1).getDisplayName(), is("Addr2"));
        assertThat(folderSyncResponse.getChanges().getAdd().get(1).getParentId(), is("0"));
        assertThat(folderSyncResponse.getChanges().getAdd().get(1).getServerId(), is("2"));
        assertThat(folderSyncResponse.getChanges().getAdd().get(1).getType(), is(BigInteger.valueOf(Constants.FOLDERTYPE_CUSTOM_CONTACT)));
        assertThat(folderSyncResponse.getStatus().intValue(), is(1));
    }

}
