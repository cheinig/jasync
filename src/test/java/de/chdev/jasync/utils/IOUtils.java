package de.chdev.jasync.utils;

import java.io.IOException;
import java.io.InputStream;

public class IOUtils {

    public static String getXmlFromFile(String filePath) throws IOException {
        InputStream xmlInputStream = IOUtils.class.getClassLoader()
                .getResourceAsStream(filePath);
        String xmlString = org.apache.commons.io.IOUtils.toString(xmlInputStream, "UTF-8");
        xmlString = xmlString.replaceAll("\n","");
        xmlString = xmlString.replaceAll("\t","");
        return  xmlString;
    }

    public static byte[] getBytesFromFile(String filePath) throws IOException {
        InputStream wbxmlInputStream = IOUtils.class.getClassLoader()
                .getResourceAsStream(filePath);
        return org.apache.commons.io.IOUtils.toByteArray(wbxmlInputStream);
    }
}
