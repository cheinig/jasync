package de.chdev.jasync.utils;

import java.util.Base64;

public class HttpUtils {

    public static String getBasicAuth(String username, String password) {
        String authenticationString = username+":"+password;
        String encodedAuthenticationString = Base64.getEncoder().encodeToString(authenticationString.getBytes());
        return "BASIC "+encodedAuthenticationString;
    }
}
