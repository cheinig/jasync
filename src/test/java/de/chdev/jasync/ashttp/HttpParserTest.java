package de.chdev.jasync.ashttp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import de.chdev.jasync.model.HttpTransfer;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Response;
import de.chdev.jasync.utils.HttpUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;

import java.util.*;

public class HttpParserTest {

    HttpParser httpParser;
    private HttpTransfer transfer;
    private String username;
    private String password;

    @Before
    public void setUp(){
        username = "testuser";
        password = "testpw";
        String basicAuthenticationString = HttpUtils.getBasicAuth(username, password);
        transfer = new HttpTransfer();
        transfer.getHeaders().put("authorization", basicAuthenticationString);
        if (httpParser==null)
            httpParser = new HttpParser();
    }

    @Test
    public void requestWithOptionsMethod(){
        transfer.setMethod(HttpMethod.OPTIONS.name());
        Request request = httpParser.getASRequestFromHttpDataAndValidate(transfer);
        assertThat(request.getMethod(),equalTo(Request.Method.options));
    }

    @Test
    public void requestWithPostMethod(){
        transfer.setMethod(HttpMethod.POST.name());
        Request request = httpParser.getASRequestFromHttpDataAndValidate(transfer);
        assertThat(request.getMethod(),equalTo(Request.Method.request));
    }

    @Test
    public void queryParametersAreExtractedToRequest(){
        String username = "testuser";
        String command = "testcommand";
        String deviceId = "testdeviceid";
        String deviceType = "testdevicetype";
        String paramValue1 = "value1";
        String paramValue2 = "value2";

        transfer.setMethod(HttpMethod.POST.name());
        transfer.setQuery("Cmd="+command+"&User="+username+"&DeviceId="+deviceId+"&DeviceType="+deviceType+"&param1="+paramValue1+"&param2="+paramValue2);
        Request request = httpParser.getASRequestFromHttpDataAndValidate(transfer);
        assertThat(request.getUsername(), is(username));
        assertThat(request.getDeviceId(), is(deviceId));
        assertThat(request.getDeviceType(), is(deviceType));
        assertThat(request.getCommand(), is(command));
        assertThat(request.getUriParameters(), hasEntry("param1", paramValue1));
        assertThat(request.getUriParameters(), hasEntry("param2", paramValue2));
    }

    @Test
    public void requestWithUsernamePasswordIsExtractedForPost(){


        transfer.setMethod(HttpMethod.POST.name());

        Request request = httpParser.getASRequestFromHttpDataAndValidate(transfer);
        assertThat(request.getUserDevice().getUsername(),equalTo(username));
        assertThat(request.getUserDevice().getPassword(),equalTo(password));
    }



    @Test
    public void responseWithOptionsHasNoBody(){
        Response response = getSampleResponse();

        HttpTransfer transfer = httpParser.getASHttpDataFromResponse(response, new HttpTransfer());
        assertThat(transfer.getBody(),nullValue());
    }

    @Test
    public void responseWithOptionsHasCorrectHeaders(){
        Response response = getSampleResponse();

        HttpTransfer transfer = httpParser.getASHttpDataFromResponse(response, new HttpTransfer());
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("ms-asprotocolcommands", "Sync");
        resultMap.put("ms-asprotocolversions", "14.0");
        resultMap.put("ms-server-activesync", "14.xxx");
        assertThat(transfer.getHeaders(), is(resultMap));
    }

    private Response getSampleResponse() {
        Response response = new Request().getRelatedResponse();
        response.setServerVersion("14.xxx");
        List<String> versionList = new ArrayList<>();
        versionList.add("14.0");
        response.setSupportedVersions(versionList);
        List<String> commandList = new ArrayList<>();
        commandList.add("Sync");
        response.setSupportedCommands(commandList);
        return response;
    }
}
