package de.chdev.jasync.ashttp;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.backend.dummy.ContactDummyBackend;
import de.chdev.jasync.model.Constants;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Response;
import de.chdev.jasync.persistence.UserDevice;
import exceptions.AuthenticationFailedException;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OptionsHandlerTest {

    private Request request;
    private OptionsHandler optionsHandler;

    @Before
    public void setup() {
        request = new Request();
        request.setMethod(Request.Method.options);
        optionsHandler = new OptionsHandler();
        UserDevice userDevice = new UserDevice("test", "test");
        userDevice.setPassword("test");
        request.setUserDevice(userDevice);
        BackendRegistry.getInstance().removeAllBackends();
        BackendRegistry.getInstance().addBackend("test", new ContactDummyBackend());
    }

    @Test
    public void optionsResponseVersionCheck() {
        Response response = optionsHandler.getOptions(request);
        assertThat(response.getSupportedVersions()).contains("16.1");
    }

    @Test
    public void optionsResponseServerVersionCheck() {
        Response response = optionsHandler.getOptions(request);
        assertThat(response.getServerVersion()).isEqualTo(Constants.SERVER_VERSION);
    }

    @Test
    public void optionsResponseCommandsCheck() {
        Response response = optionsHandler.getOptions(request);
        assertThat(response.getSupportedCommands()).contains("Sync");
    }

    @Test(expected = AuthenticationFailedException.class)
    public void authenticationValidationFailedOnMissingUsername() {
        request.getUserDevice().setUsername("");
        optionsHandler.getOptions(request);
    }

    @Test(expected = AuthenticationFailedException.class)
    public void authenticationValidationFailedOnWrongCredentials() {
        UserDevice userDevice = new UserDevice("test", "test");
        request.setUserDevice(userDevice);
        optionsHandler.getOptions(request);
    }
}
