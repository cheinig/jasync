//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package com.microsoft.schemas.exchange.autodiscover.mobilesync.responseschema._2006;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.microsoft.schemas.exchange.autodiscover.mobilesync.responseschema._2006 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.microsoft.schemas.exchange.autodiscover.mobilesync.responseschema._2006
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link Response.Action }
     * 
     */
    public Response.Action createResponseAction() {
        return new Response.Action();
    }

    /**
     * Create an instance of {@link Response.Action.Settings }
     * 
     */
    public Response.Action.Settings createResponseActionSettings() {
        return new Response.Action.Settings();
    }

    /**
     * Create an instance of {@link Response.User }
     * 
     */
    public Response.User createResponseUser() {
        return new Response.User();
    }

    /**
     * Create an instance of {@link Response.Action.Error }
     * 
     */
    public Response.Action.Error createResponseActionError() {
        return new Response.Action.Error();
    }

    /**
     * Create an instance of {@link Response.Action.Settings.Server }
     * 
     */
    public Response.Action.Settings.Server createResponseActionSettingsServer() {
        return new Response.Action.Settings.Server();
    }

}
