//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.calendar;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.response.airsyncbase.Attachments;
import de.chdev.jasync.xsd.response.airsyncbase.Body;
import de.chdev.jasync.xsd.response.airsyncbase.Location;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="Exception" maxOccurs="1000">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Deleted" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *                   &lt;element name="ExceptionStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}Subject" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}StartTime" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}EndTime" minOccurs="0"/>
 *                   &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}Location" minOccurs="0"/>
 *                   &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
 *                   &lt;element ref="{AirSyncBase}Attachments" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}Categories" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}Sensitivity" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}BusyStatus" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}AllDayEvent" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}Reminder" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}DtStamp" minOccurs="0"/>
 *                   &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}MeetingStatus" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}Attendees" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}AppointmentReplyTime" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}ResponseType" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}OnlineMeetingConfLink" minOccurs="0"/>
 *                   &lt;element ref="{Calendar}OnlineMeetingExternalLink" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exception"
})
@XmlRootElement(name = "Exceptions")
public class Exceptions {

    @XmlElement(name = "Exception")
    protected List<Exceptions.Exception> exception;

    /**
     * Gets the value of the exception property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exception property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getException().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Exceptions.Exception }
     * 
     * 
     */
    public List<Exceptions.Exception> getException() {
        if (exception == null) {
            exception = new ArrayList<Exceptions.Exception>();
        }
        return this.exception;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Deleted" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
     *         &lt;element name="ExceptionStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element ref="{Calendar}Subject" minOccurs="0"/>
     *         &lt;element ref="{Calendar}StartTime" minOccurs="0"/>
     *         &lt;element ref="{Calendar}EndTime" minOccurs="0"/>
     *         &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
     *         &lt;element ref="{Calendar}Location" minOccurs="0"/>
     *         &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
     *         &lt;element ref="{AirSyncBase}Attachments" minOccurs="0"/>
     *         &lt;element ref="{Calendar}Categories" minOccurs="0"/>
     *         &lt;element ref="{Calendar}Sensitivity" minOccurs="0"/>
     *         &lt;element ref="{Calendar}BusyStatus" minOccurs="0"/>
     *         &lt;element ref="{Calendar}AllDayEvent" minOccurs="0"/>
     *         &lt;element ref="{Calendar}Reminder" minOccurs="0"/>
     *         &lt;element ref="{Calendar}DtStamp" minOccurs="0"/>
     *         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
     *         &lt;element ref="{Calendar}MeetingStatus" minOccurs="0"/>
     *         &lt;element ref="{Calendar}Attendees" minOccurs="0"/>
     *         &lt;element ref="{Calendar}AppointmentReplyTime" minOccurs="0"/>
     *         &lt;element ref="{Calendar}ResponseType" minOccurs="0"/>
     *         &lt;element ref="{Calendar}OnlineMeetingConfLink" minOccurs="0"/>
     *         &lt;element ref="{Calendar}OnlineMeetingExternalLink" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Exception {

        @XmlElementRefs({
            @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "InstanceId", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ExceptionStartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ResponseType", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OnlineMeetingExternalLink", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
            @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
            @XmlElementRef(name = "OnlineMeetingConfLink", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "AppointmentReplyTime", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Deleted", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Categories", namespace = "Calendar", type = Categories.class, required = false),
            @XmlElementRef(name = "Attachments", namespace = "AirSyncBase", type = Attachments.class, required = false),
            @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false)
        })
        protected List<Object> content;

        /**
         * Ruft das restliche Contentmodell ab. 
         * 
         * <p>
         * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
         * Der Feldname "Location" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
         * Zeile 199 von file:/Users/cheinig/Documents/Entwicklung/idea/Privat/jasync/src/main/resources/xsd/Calendar.xsd
         * Zeile 198 von file:/Users/cheinig/Documents/Entwicklung/idea/Privat/jasync/src/main/resources/xsd/Calendar.xsd
         * <p>
         * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung für eine
         * der beiden folgenden Deklarationen an, um deren Namen zu ändern: 
         * Gets the value of the content property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Long }{@code >}
         * {@link JAXBElement }{@code <}{@link Short }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link Body }
         * {@link JAXBElement }{@code <}{@link Short }{@code >}
         * {@link Location }
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Short }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Short }{@code >}
         * {@link Categories }
         * {@link Attachments }
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Long }{@code >}
         * {@link JAXBElement }{@code <}{@link Short }{@code >}
         * {@link Attendees }
         * 
         * 
         */
        public List<Object> getContent() {
            if (content == null) {
                content = new ArrayList<Object>();
            }
            return this.content;
        }

    }

}
