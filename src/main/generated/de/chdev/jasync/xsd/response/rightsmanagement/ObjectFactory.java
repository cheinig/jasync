//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.rightsmanagement;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.rightsmanagement package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RightsManagementSupport_QNAME = new QName("RightsManagement", "RightsManagementSupport");
    private final static QName _RemoveRightsManagementProtection_QNAME = new QName("RightsManagement", "RemoveRightsManagementProtection");
    private final static QName _TemplateID_QNAME = new QName("RightsManagement", "TemplateID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.rightsmanagement
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RightsManagementTemplates }
     * 
     */
    public RightsManagementTemplates createRightsManagementTemplates() {
        return new RightsManagementTemplates();
    }

    /**
     * Create an instance of {@link RightsManagementLicense }
     * 
     */
    public RightsManagementLicense createRightsManagementLicense() {
        return new RightsManagementLicense();
    }

    /**
     * Create an instance of {@link RightsManagementTemplates.RightsManagementTemplate }
     * 
     */
    public RightsManagementTemplates.RightsManagementTemplate createRightsManagementTemplatesRightsManagementTemplate() {
        return new RightsManagementTemplates.RightsManagementTemplate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "RightsManagement", name = "RightsManagementSupport")
    public JAXBElement<Boolean> createRightsManagementSupport(Boolean value) {
        return new JAXBElement<Boolean>(_RightsManagementSupport_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "RightsManagement", name = "RemoveRightsManagementProtection")
    public JAXBElement<String> createRemoveRightsManagementProtection(String value) {
        return new JAXBElement<String>(_RemoveRightsManagementProtection_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "RightsManagement", name = "TemplateID")
    public JAXBElement<String> createTemplateID(String value) {
        return new JAXBElement<String>(_TemplateID_QNAME, String.class, null, value);
    }

}
