//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.search;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.response.airsyncbase.BodyPartPreference;
import de.chdev.jasync.xsd.response.airsyncbase.BodyPreference;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{AirSync}MIMESupport" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}BodyPreference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}BodyPartPreference" minOccurs="0"/>
 *         &lt;element ref="{RightsManagement}RightsManagementSupport" minOccurs="0"/>
 *         &lt;element ref="{Search}Range"/>
 *         &lt;element name="UserName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Password">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeepTraversal" type="{AirSyncBase}EmptyTag"/>
 *         &lt;element name="RebuildResults" type="{AirSyncBase}EmptyTag"/>
 *         &lt;element name="Picture" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                   &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mimeSupportOrBodyPreferenceOrBodyPartPreference"
})
@XmlRootElement(name = "Options")
public class Options {

    @XmlElementRefs({
        @XmlElementRef(name = "BodyPartPreference", namespace = "AirSyncBase", type = BodyPartPreference.class, required = false),
        @XmlElementRef(name = "RightsManagementSupport", namespace = "RightsManagement", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UserName", namespace = "Search", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DeepTraversal", namespace = "Search", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Range", namespace = "Search", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MIMESupport", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RebuildResults", namespace = "Search", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyPreference", namespace = "AirSyncBase", type = BodyPreference.class, required = false),
        @XmlElementRef(name = "Password", namespace = "Search", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Picture", namespace = "Search", type = JAXBElement.class, required = false)
    })
    protected List<Object> mimeSupportOrBodyPreferenceOrBodyPartPreference;

    /**
     * Gets the value of the mimeSupportOrBodyPreferenceOrBodyPartPreference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mimeSupportOrBodyPreferenceOrBodyPartPreference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMIMESupportOrBodyPreferenceOrBodyPartPreference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BodyPartPreference }
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link BodyPreference }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Options.Picture }{@code >}
     * 
     * 
     */
    public List<Object> getMIMESupportOrBodyPreferenceOrBodyPartPreference() {
        if (mimeSupportOrBodyPreferenceOrBodyPartPreference == null) {
            mimeSupportOrBodyPreferenceOrBodyPartPreference = new ArrayList<Object>();
        }
        return this.mimeSupportOrBodyPreferenceOrBodyPartPreference;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *         &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Picture {

        @XmlElement(name = "MaxSize")
        @XmlSchemaType(name = "unsignedInt")
        protected Long maxSize;
        @XmlElement(name = "MaxPictures")
        @XmlSchemaType(name = "unsignedInt")
        protected Long maxPictures;

        /**
         * Ruft den Wert der maxSize-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getMaxSize() {
            return maxSize;
        }

        /**
         * Legt den Wert der maxSize-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setMaxSize(Long value) {
            this.maxSize = value;
        }

        /**
         * Ruft den Wert der maxPictures-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getMaxPictures() {
            return maxPictures;
        }

        /**
         * Legt den Wert der maxPictures-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setMaxPictures(Long value) {
            this.maxPictures = value;
        }

    }

}
