//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.airsync;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import de.chdev.jasync.xsd.response.airsyncbase.Body;
import de.chdev.jasync.xsd.response.airsyncbase.BodyPart;
import de.chdev.jasync.xsd.response.airsyncbase.Location;
import de.chdev.jasync.xsd.response.calendar.Attendees;
import de.chdev.jasync.xsd.response.calendar.Exceptions;
import de.chdev.jasync.xsd.response.contacts.Children;
import de.chdev.jasync.xsd.response.email.Flag;
import de.chdev.jasync.xsd.response.email.MeetingRequest;
import de.chdev.jasync.xsd.response.rightsmanagement.RightsManagementLicense;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{AirSync}Status" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element ref="{AirSync}Limit" minOccurs="0"/>
 *           &lt;element name="Collections" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Collection" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;choice maxOccurs="unbounded">
 *                                 &lt;element ref="{AirSync}SyncKey"/>
 *                                 &lt;element ref="{AirSync}CollectionId"/>
 *                                 &lt;element ref="{AirSync}Status"/>
 *                                 &lt;element name="Commands" minOccurs="0">
 *                                   &lt;complexType>
 *                                     &lt;complexContent>
 *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                         &lt;sequence>
 *                                           &lt;choice maxOccurs="unbounded">
 *                                             &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
 *                                               &lt;complexType>
 *                                                 &lt;complexContent>
 *                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                     &lt;sequence>
 *                                                       &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                                                       &lt;element ref="{AirSync}ServerId"/>
 *                                                     &lt;/sequence>
 *                                                   &lt;/restriction>
 *                                                 &lt;/complexContent>
 *                                               &lt;/complexType>
 *                                             &lt;/element>
 *                                             &lt;element name="SoftDelete" maxOccurs="unbounded" minOccurs="0">
 *                                               &lt;complexType>
 *                                                 &lt;complexContent>
 *                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                     &lt;sequence>
 *                                                       &lt;element ref="{AirSync}ServerId"/>
 *                                                     &lt;/sequence>
 *                                                   &lt;/restriction>
 *                                                 &lt;/complexContent>
 *                                               &lt;/complexType>
 *                                             &lt;/element>
 *                                             &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
 *                                               &lt;complexType>
 *                                                 &lt;complexContent>
 *                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                     &lt;sequence>
 *                                                       &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                                                       &lt;element ref="{AirSync}ServerId"/>
 *                                                       &lt;element name="ApplicationData">
 *                                                         &lt;complexType>
 *                                                           &lt;complexContent>
 *                                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                               &lt;choice maxOccurs="unbounded">
 *                                                                 &lt;element ref="{AirSyncBase}Body"/>
 *                                                                 &lt;element ref="{AirSyncBase}BodyPart"/>
 *                                                                 &lt;element ref="{AirSyncBase}NativeBodyType"/>
 *                                                                 &lt;element ref="{AirSyncBase}InstanceId"/>
 *                                                                 &lt;element ref="{AirSyncBase}Location"/>
 *                                                                 &lt;group ref="{Calendar}AllProps"/>
 *                                                                 &lt;group ref="{Contacts}AllProps"/>
 *                                                                 &lt;group ref="{Contacts2}AllProps"/>
 *                                                                 &lt;group ref="{DocumentLibrary}AllProps"/>
 *                                                                 &lt;group ref="{Email}AllProps"/>
 *                                                                 &lt;group ref="{Email2}AllProps"/>
 *                                                                 &lt;element ref="{Notes}Subject"/>
 *                                                                 &lt;element ref="{Notes}MessageClass"/>
 *                                                                 &lt;element ref="{Notes}LastModifiedDate"/>
 *                                                                 &lt;element ref="{Notes}Categories"/>
 *                                                                 &lt;group ref="{Tasks}AllProps"/>
 *                                                                 &lt;element ref="{RightsManagement}RightsManagementLicense"/>
 *                                                               &lt;/choice>
 *                                                             &lt;/restriction>
 *                                                           &lt;/complexContent>
 *                                                         &lt;/complexType>
 *                                                       &lt;/element>
 *                                                     &lt;/sequence>
 *                                                   &lt;/restriction>
 *                                                 &lt;/complexContent>
 *                                               &lt;/complexType>
 *                                             &lt;/element>
 *                                             &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
 *                                               &lt;complexType>
 *                                                 &lt;complexContent>
 *                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                     &lt;sequence>
 *                                                       &lt;element ref="{AirSync}ServerId"/>
 *                                                       &lt;element name="ApplicationData">
 *                                                         &lt;complexType>
 *                                                           &lt;complexContent>
 *                                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                               &lt;sequence>
 *                                                                 &lt;choice maxOccurs="unbounded">
 *                                                                   &lt;element ref="{AirSyncBase}Body"/>
 *                                                                   &lt;element ref="{AirSyncBase}BodyPart"/>
 *                                                                   &lt;element ref="{AirSyncBase}NativeBodyType"/>
 *                                                                   &lt;element ref="{AirSyncBase}InstanceId"/>
 *                                                                   &lt;element ref="{AirSyncBase}Location"/>
 *                                                                   &lt;group ref="{Calendar}AllProps"/>
 *                                                                   &lt;group ref="{Contacts}AllProps"/>
 *                                                                   &lt;group ref="{Contacts2}AllProps"/>
 *                                                                   &lt;group ref="{DocumentLibrary}AllProps"/>
 *                                                                   &lt;group ref="{Email}AllProps"/>
 *                                                                   &lt;group ref="{Email2}AllProps"/>
 *                                                                   &lt;element ref="{Notes}Subject"/>
 *                                                                   &lt;element ref="{Notes}MessageClass"/>
 *                                                                   &lt;element ref="{Notes}LastModifiedDate"/>
 *                                                                   &lt;element ref="{Notes}Categories"/>
 *                                                                   &lt;group ref="{Tasks}AllProps"/>
 *                                                                   &lt;element ref="{RightsManagement}RightsManagementLicense"/>
 *                                                                 &lt;/choice>
 *                                                               &lt;/sequence>
 *                                                             &lt;/restriction>
 *                                                           &lt;/complexContent>
 *                                                         &lt;/complexType>
 *                                                       &lt;/element>
 *                                                     &lt;/sequence>
 *                                                   &lt;/restriction>
 *                                                 &lt;/complexContent>
 *                                               &lt;/complexType>
 *                                             &lt;/element>
 *                                           &lt;/choice>
 *                                         &lt;/sequence>
 *                                       &lt;/restriction>
 *                                     &lt;/complexContent>
 *                                   &lt;/complexType>
 *                                 &lt;/element>
 *                                 &lt;element name="Responses" minOccurs="0">
 *                                   &lt;complexType>
 *                                     &lt;complexContent>
 *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                         &lt;sequence>
 *                                           &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                                                     &lt;element ref="{AirSync}ServerId"/>
 *                                                     &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
 *                                                     &lt;element ref="{AirSync}Status"/>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                           &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                                                     &lt;element ref="{AirSync}ClientId"/>
 *                                                     &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
 *                                                     &lt;element ref="{AirSync}Status"/>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                           &lt;element name="Delete" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element ref="{AirSync}ServerId"/>
 *                                                     &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
 *                                                     &lt;element ref="{AirSync}Status"/>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                           &lt;element name="Fetch" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
 *                                                     &lt;element ref="{AirSync}Status"/>
 *                                                     &lt;element name="ApplicationData">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;choice maxOccurs="unbounded">
 *                                                                 &lt;element ref="{AirSyncBase}Attachments"/>
 *                                                                 &lt;element ref="{AirSyncBase}Body"/>
 *                                                                 &lt;element ref="{AirSyncBase}NativeBodyType"/>
 *                                                                 &lt;element ref="{AirSyncBase}Location"/>
 *                                                                 &lt;group ref="{Calendar}AllProps"/>
 *                                                                 &lt;group ref="{Contacts}AllProps"/>
 *                                                                 &lt;group ref="{Contacts2}AllProps"/>
 *                                                                 &lt;group ref="{DocumentLibrary}AllProps"/>
 *                                                                 &lt;group ref="{Email}AllProps"/>
 *                                                                 &lt;group ref="{Email2}AllProps"/>
 *                                                                 &lt;group ref="{Tasks}AllProps"/>
 *                                                                 &lt;element ref="{RightsManagement}RightsManagementLicense"/>
 *                                                               &lt;/choice>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                         &lt;/sequence>
 *                                       &lt;/restriction>
 *                                     &lt;/complexContent>
 *                                   &lt;/complexType>
 *                                 &lt;/element>
 *                                 &lt;element ref="{AirSync}MoreAvailable" minOccurs="0"/>
 *                               &lt;/choice>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "limit",
    "collections"
})
@XmlRootElement(name = "Sync")
public class Sync {

    @XmlElement(name = "Status")
    protected BigInteger status;
    @XmlElement(name = "Limit")
    protected BigInteger limit;
    @XmlElement(name = "Collections")
    protected Sync.Collections collections;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der limit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimit() {
        return limit;
    }

    /**
     * Legt den Wert der limit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimit(BigInteger value) {
        this.limit = value;
    }

    /**
     * Ruft den Wert der collections-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Sync.Collections }
     *     
     */
    public Sync.Collections getCollections() {
        return collections;
    }

    /**
     * Legt den Wert der collections-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Sync.Collections }
     *     
     */
    public void setCollections(Sync.Collections value) {
        this.collections = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Collection" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;choice maxOccurs="unbounded">
     *                     &lt;element ref="{AirSync}SyncKey"/>
     *                     &lt;element ref="{AirSync}CollectionId"/>
     *                     &lt;element ref="{AirSync}Status"/>
     *                     &lt;element name="Commands" minOccurs="0">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;choice maxOccurs="unbounded">
     *                                 &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
     *                                   &lt;complexType>
     *                                     &lt;complexContent>
     *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                         &lt;sequence>
     *                                           &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                                           &lt;element ref="{AirSync}ServerId"/>
     *                                         &lt;/sequence>
     *                                       &lt;/restriction>
     *                                     &lt;/complexContent>
     *                                   &lt;/complexType>
     *                                 &lt;/element>
     *                                 &lt;element name="SoftDelete" maxOccurs="unbounded" minOccurs="0">
     *                                   &lt;complexType>
     *                                     &lt;complexContent>
     *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                         &lt;sequence>
     *                                           &lt;element ref="{AirSync}ServerId"/>
     *                                         &lt;/sequence>
     *                                       &lt;/restriction>
     *                                     &lt;/complexContent>
     *                                   &lt;/complexType>
     *                                 &lt;/element>
     *                                 &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
     *                                   &lt;complexType>
     *                                     &lt;complexContent>
     *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                         &lt;sequence>
     *                                           &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                                           &lt;element ref="{AirSync}ServerId"/>
     *                                           &lt;element name="ApplicationData">
     *                                             &lt;complexType>
     *                                               &lt;complexContent>
     *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                   &lt;choice maxOccurs="unbounded">
     *                                                     &lt;element ref="{AirSyncBase}Body"/>
     *                                                     &lt;element ref="{AirSyncBase}BodyPart"/>
     *                                                     &lt;element ref="{AirSyncBase}NativeBodyType"/>
     *                                                     &lt;element ref="{AirSyncBase}InstanceId"/>
     *                                                     &lt;element ref="{AirSyncBase}Location"/>
     *                                                     &lt;group ref="{Calendar}AllProps"/>
     *                                                     &lt;group ref="{Contacts}AllProps"/>
     *                                                     &lt;group ref="{Contacts2}AllProps"/>
     *                                                     &lt;group ref="{DocumentLibrary}AllProps"/>
     *                                                     &lt;group ref="{Email}AllProps"/>
     *                                                     &lt;group ref="{Email2}AllProps"/>
     *                                                     &lt;element ref="{Notes}Subject"/>
     *                                                     &lt;element ref="{Notes}MessageClass"/>
     *                                                     &lt;element ref="{Notes}LastModifiedDate"/>
     *                                                     &lt;element ref="{Notes}Categories"/>
     *                                                     &lt;group ref="{Tasks}AllProps"/>
     *                                                     &lt;element ref="{RightsManagement}RightsManagementLicense"/>
     *                                                   &lt;/choice>
     *                                                 &lt;/restriction>
     *                                               &lt;/complexContent>
     *                                             &lt;/complexType>
     *                                           &lt;/element>
     *                                         &lt;/sequence>
     *                                       &lt;/restriction>
     *                                     &lt;/complexContent>
     *                                   &lt;/complexType>
     *                                 &lt;/element>
     *                                 &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
     *                                   &lt;complexType>
     *                                     &lt;complexContent>
     *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                         &lt;sequence>
     *                                           &lt;element ref="{AirSync}ServerId"/>
     *                                           &lt;element name="ApplicationData">
     *                                             &lt;complexType>
     *                                               &lt;complexContent>
     *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                   &lt;sequence>
     *                                                     &lt;choice maxOccurs="unbounded">
     *                                                       &lt;element ref="{AirSyncBase}Body"/>
     *                                                       &lt;element ref="{AirSyncBase}BodyPart"/>
     *                                                       &lt;element ref="{AirSyncBase}NativeBodyType"/>
     *                                                       &lt;element ref="{AirSyncBase}InstanceId"/>
     *                                                       &lt;element ref="{AirSyncBase}Location"/>
     *                                                       &lt;group ref="{Calendar}AllProps"/>
     *                                                       &lt;group ref="{Contacts}AllProps"/>
     *                                                       &lt;group ref="{Contacts2}AllProps"/>
     *                                                       &lt;group ref="{DocumentLibrary}AllProps"/>
     *                                                       &lt;group ref="{Email}AllProps"/>
     *                                                       &lt;group ref="{Email2}AllProps"/>
     *                                                       &lt;element ref="{Notes}Subject"/>
     *                                                       &lt;element ref="{Notes}MessageClass"/>
     *                                                       &lt;element ref="{Notes}LastModifiedDate"/>
     *                                                       &lt;element ref="{Notes}Categories"/>
     *                                                       &lt;group ref="{Tasks}AllProps"/>
     *                                                       &lt;element ref="{RightsManagement}RightsManagementLicense"/>
     *                                                     &lt;/choice>
     *                                                   &lt;/sequence>
     *                                                 &lt;/restriction>
     *                                               &lt;/complexContent>
     *                                             &lt;/complexType>
     *                                           &lt;/element>
     *                                         &lt;/sequence>
     *                                       &lt;/restriction>
     *                                     &lt;/complexContent>
     *                                   &lt;/complexType>
     *                                 &lt;/element>
     *                               &lt;/choice>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                     &lt;element name="Responses" minOccurs="0">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                                         &lt;element ref="{AirSync}ServerId"/>
     *                                         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
     *                                         &lt;element ref="{AirSync}Status"/>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                                         &lt;element ref="{AirSync}ClientId"/>
     *                                         &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
     *                                         &lt;element ref="{AirSync}Status"/>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="Delete" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element ref="{AirSync}ServerId"/>
     *                                         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
     *                                         &lt;element ref="{AirSync}Status"/>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="Fetch" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
     *                                         &lt;element ref="{AirSync}Status"/>
     *                                         &lt;element name="ApplicationData">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;choice maxOccurs="unbounded">
     *                                                     &lt;element ref="{AirSyncBase}Attachments"/>
     *                                                     &lt;element ref="{AirSyncBase}Body"/>
     *                                                     &lt;element ref="{AirSyncBase}NativeBodyType"/>
     *                                                     &lt;element ref="{AirSyncBase}Location"/>
     *                                                     &lt;group ref="{Calendar}AllProps"/>
     *                                                     &lt;group ref="{Contacts}AllProps"/>
     *                                                     &lt;group ref="{Contacts2}AllProps"/>
     *                                                     &lt;group ref="{DocumentLibrary}AllProps"/>
     *                                                     &lt;group ref="{Email}AllProps"/>
     *                                                     &lt;group ref="{Email2}AllProps"/>
     *                                                     &lt;group ref="{Tasks}AllProps"/>
     *                                                     &lt;element ref="{RightsManagement}RightsManagementLicense"/>
     *                                                   &lt;/choice>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                     &lt;element ref="{AirSync}MoreAvailable" minOccurs="0"/>
     *                   &lt;/choice>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "collection"
    })
    public static class Collections {

        @XmlElement(name = "Collection")
        protected List<Sync.Collections.Collection> collection;

        /**
         * Gets the value of the collection property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the collection property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCollection().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Sync.Collections.Collection }
         * 
         * 
         */
        public List<Sync.Collections.Collection> getCollection() {
            if (collection == null) {
                collection = new ArrayList<Sync.Collections.Collection>();
            }
            return this.collection;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;choice maxOccurs="unbounded">
         *           &lt;element ref="{AirSync}SyncKey"/>
         *           &lt;element ref="{AirSync}CollectionId"/>
         *           &lt;element ref="{AirSync}Status"/>
         *           &lt;element name="Commands" minOccurs="0">
         *             &lt;complexType>
         *               &lt;complexContent>
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                   &lt;sequence>
         *                     &lt;choice maxOccurs="unbounded">
         *                       &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
         *                         &lt;complexType>
         *                           &lt;complexContent>
         *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                               &lt;sequence>
         *                                 &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *                                 &lt;element ref="{AirSync}ServerId"/>
         *                               &lt;/sequence>
         *                             &lt;/restriction>
         *                           &lt;/complexContent>
         *                         &lt;/complexType>
         *                       &lt;/element>
         *                       &lt;element name="SoftDelete" maxOccurs="unbounded" minOccurs="0">
         *                         &lt;complexType>
         *                           &lt;complexContent>
         *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                               &lt;sequence>
         *                                 &lt;element ref="{AirSync}ServerId"/>
         *                               &lt;/sequence>
         *                             &lt;/restriction>
         *                           &lt;/complexContent>
         *                         &lt;/complexType>
         *                       &lt;/element>
         *                       &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
         *                         &lt;complexType>
         *                           &lt;complexContent>
         *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                               &lt;sequence>
         *                                 &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *                                 &lt;element ref="{AirSync}ServerId"/>
         *                                 &lt;element name="ApplicationData">
         *                                   &lt;complexType>
         *                                     &lt;complexContent>
         *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                         &lt;choice maxOccurs="unbounded">
         *                                           &lt;element ref="{AirSyncBase}Body"/>
         *                                           &lt;element ref="{AirSyncBase}BodyPart"/>
         *                                           &lt;element ref="{AirSyncBase}NativeBodyType"/>
         *                                           &lt;element ref="{AirSyncBase}InstanceId"/>
         *                                           &lt;element ref="{AirSyncBase}Location"/>
         *                                           &lt;group ref="{Calendar}AllProps"/>
         *                                           &lt;group ref="{Contacts}AllProps"/>
         *                                           &lt;group ref="{Contacts2}AllProps"/>
         *                                           &lt;group ref="{DocumentLibrary}AllProps"/>
         *                                           &lt;group ref="{Email}AllProps"/>
         *                                           &lt;group ref="{Email2}AllProps"/>
         *                                           &lt;element ref="{Notes}Subject"/>
         *                                           &lt;element ref="{Notes}MessageClass"/>
         *                                           &lt;element ref="{Notes}LastModifiedDate"/>
         *                                           &lt;element ref="{Notes}Categories"/>
         *                                           &lt;group ref="{Tasks}AllProps"/>
         *                                           &lt;element ref="{RightsManagement}RightsManagementLicense"/>
         *                                         &lt;/choice>
         *                                       &lt;/restriction>
         *                                     &lt;/complexContent>
         *                                   &lt;/complexType>
         *                                 &lt;/element>
         *                               &lt;/sequence>
         *                             &lt;/restriction>
         *                           &lt;/complexContent>
         *                         &lt;/complexType>
         *                       &lt;/element>
         *                       &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
         *                         &lt;complexType>
         *                           &lt;complexContent>
         *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                               &lt;sequence>
         *                                 &lt;element ref="{AirSync}ServerId"/>
         *                                 &lt;element name="ApplicationData">
         *                                   &lt;complexType>
         *                                     &lt;complexContent>
         *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                         &lt;sequence>
         *                                           &lt;choice maxOccurs="unbounded">
         *                                             &lt;element ref="{AirSyncBase}Body"/>
         *                                             &lt;element ref="{AirSyncBase}BodyPart"/>
         *                                             &lt;element ref="{AirSyncBase}NativeBodyType"/>
         *                                             &lt;element ref="{AirSyncBase}InstanceId"/>
         *                                             &lt;element ref="{AirSyncBase}Location"/>
         *                                             &lt;group ref="{Calendar}AllProps"/>
         *                                             &lt;group ref="{Contacts}AllProps"/>
         *                                             &lt;group ref="{Contacts2}AllProps"/>
         *                                             &lt;group ref="{DocumentLibrary}AllProps"/>
         *                                             &lt;group ref="{Email}AllProps"/>
         *                                             &lt;group ref="{Email2}AllProps"/>
         *                                             &lt;element ref="{Notes}Subject"/>
         *                                             &lt;element ref="{Notes}MessageClass"/>
         *                                             &lt;element ref="{Notes}LastModifiedDate"/>
         *                                             &lt;element ref="{Notes}Categories"/>
         *                                             &lt;group ref="{Tasks}AllProps"/>
         *                                             &lt;element ref="{RightsManagement}RightsManagementLicense"/>
         *                                           &lt;/choice>
         *                                         &lt;/sequence>
         *                                       &lt;/restriction>
         *                                     &lt;/complexContent>
         *                                   &lt;/complexType>
         *                                 &lt;/element>
         *                               &lt;/sequence>
         *                             &lt;/restriction>
         *                           &lt;/complexContent>
         *                         &lt;/complexType>
         *                       &lt;/element>
         *                     &lt;/choice>
         *                   &lt;/sequence>
         *                 &lt;/restriction>
         *               &lt;/complexContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *           &lt;element name="Responses" minOccurs="0">
         *             &lt;complexType>
         *               &lt;complexContent>
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                   &lt;sequence>
         *                     &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *                               &lt;element ref="{AirSync}ServerId"/>
         *                               &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
         *                               &lt;element ref="{AirSync}Status"/>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *                               &lt;element ref="{AirSync}ClientId"/>
         *                               &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
         *                               &lt;element ref="{AirSync}Status"/>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="Delete" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element ref="{AirSync}ServerId"/>
         *                               &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
         *                               &lt;element ref="{AirSync}Status"/>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="Fetch" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
         *                               &lt;element ref="{AirSync}Status"/>
         *                               &lt;element name="ApplicationData">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;choice maxOccurs="unbounded">
         *                                           &lt;element ref="{AirSyncBase}Attachments"/>
         *                                           &lt;element ref="{AirSyncBase}Body"/>
         *                                           &lt;element ref="{AirSyncBase}NativeBodyType"/>
         *                                           &lt;element ref="{AirSyncBase}Location"/>
         *                                           &lt;group ref="{Calendar}AllProps"/>
         *                                           &lt;group ref="{Contacts}AllProps"/>
         *                                           &lt;group ref="{Contacts2}AllProps"/>
         *                                           &lt;group ref="{DocumentLibrary}AllProps"/>
         *                                           &lt;group ref="{Email}AllProps"/>
         *                                           &lt;group ref="{Email2}AllProps"/>
         *                                           &lt;group ref="{Tasks}AllProps"/>
         *                                           &lt;element ref="{RightsManagement}RightsManagementLicense"/>
         *                                         &lt;/choice>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/sequence>
         *                 &lt;/restriction>
         *               &lt;/complexContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *           &lt;element ref="{AirSync}MoreAvailable" minOccurs="0"/>
         *         &lt;/choice>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "syncKeyOrCollectionIdOrStatus"
        })
        public static class Collection {

            @XmlElementRefs({
                @XmlElementRef(name = "Commands", namespace = "AirSync", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "CollectionId", namespace = "AirSync", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "Responses", namespace = "AirSync", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "SyncKey", namespace = "AirSync", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "MoreAvailable", namespace = "AirSync", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "Status", namespace = "AirSync", type = JAXBElement.class, required = false)
            })
            protected List<JAXBElement<?>> syncKeyOrCollectionIdOrStatus;

            /**
             * Gets the value of the syncKeyOrCollectionIdOrStatus property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the syncKeyOrCollectionIdOrStatus property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSyncKeyOrCollectionIdOrStatus().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link Sync.Collections.Collection.Commands }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link Sync.Collections.Collection.Responses }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * 
             * 
             */
            public List<JAXBElement<?>> getSyncKeyOrCollectionIdOrStatus() {
                if (syncKeyOrCollectionIdOrStatus == null) {
                    syncKeyOrCollectionIdOrStatus = new ArrayList<JAXBElement<?>>();
                }
                return this.syncKeyOrCollectionIdOrStatus;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;choice maxOccurs="unbounded">
             *           &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element ref="{AirSync}Class" minOccurs="0"/>
             *                     &lt;element ref="{AirSync}ServerId"/>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *           &lt;element name="SoftDelete" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element ref="{AirSync}ServerId"/>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *           &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element ref="{AirSync}Class" minOccurs="0"/>
             *                     &lt;element ref="{AirSync}ServerId"/>
             *                     &lt;element name="ApplicationData">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;choice maxOccurs="unbounded">
             *                               &lt;element ref="{AirSyncBase}Body"/>
             *                               &lt;element ref="{AirSyncBase}BodyPart"/>
             *                               &lt;element ref="{AirSyncBase}NativeBodyType"/>
             *                               &lt;element ref="{AirSyncBase}InstanceId"/>
             *                               &lt;element ref="{AirSyncBase}Location"/>
             *                               &lt;group ref="{Calendar}AllProps"/>
             *                               &lt;group ref="{Contacts}AllProps"/>
             *                               &lt;group ref="{Contacts2}AllProps"/>
             *                               &lt;group ref="{DocumentLibrary}AllProps"/>
             *                               &lt;group ref="{Email}AllProps"/>
             *                               &lt;group ref="{Email2}AllProps"/>
             *                               &lt;element ref="{Notes}Subject"/>
             *                               &lt;element ref="{Notes}MessageClass"/>
             *                               &lt;element ref="{Notes}LastModifiedDate"/>
             *                               &lt;element ref="{Notes}Categories"/>
             *                               &lt;group ref="{Tasks}AllProps"/>
             *                               &lt;element ref="{RightsManagement}RightsManagementLicense"/>
             *                             &lt;/choice>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *           &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element ref="{AirSync}ServerId"/>
             *                     &lt;element name="ApplicationData">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;choice maxOccurs="unbounded">
             *                                 &lt;element ref="{AirSyncBase}Body"/>
             *                                 &lt;element ref="{AirSyncBase}BodyPart"/>
             *                                 &lt;element ref="{AirSyncBase}NativeBodyType"/>
             *                                 &lt;element ref="{AirSyncBase}InstanceId"/>
             *                                 &lt;element ref="{AirSyncBase}Location"/>
             *                                 &lt;group ref="{Calendar}AllProps"/>
             *                                 &lt;group ref="{Contacts}AllProps"/>
             *                                 &lt;group ref="{Contacts2}AllProps"/>
             *                                 &lt;group ref="{DocumentLibrary}AllProps"/>
             *                                 &lt;group ref="{Email}AllProps"/>
             *                                 &lt;group ref="{Email2}AllProps"/>
             *                                 &lt;element ref="{Notes}Subject"/>
             *                                 &lt;element ref="{Notes}MessageClass"/>
             *                                 &lt;element ref="{Notes}LastModifiedDate"/>
             *                                 &lt;element ref="{Notes}Categories"/>
             *                                 &lt;group ref="{Tasks}AllProps"/>
             *                                 &lt;element ref="{RightsManagement}RightsManagementLicense"/>
             *                               &lt;/choice>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *         &lt;/choice>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "deleteOrSoftDeleteOrChange"
            })
            public static class Commands {

                @XmlElements({
                    @XmlElement(name = "Delete", type = Sync.Collections.Collection.Commands.Delete.class),
                    @XmlElement(name = "SoftDelete", type = Sync.Collections.Collection.Commands.SoftDelete.class),
                    @XmlElement(name = "Change", type = Sync.Collections.Collection.Commands.Change.class),
                    @XmlElement(name = "Add", type = Sync.Collections.Collection.Commands.Add.class)
                })
                protected List<Object> deleteOrSoftDeleteOrChange;

                /**
                 * Gets the value of the deleteOrSoftDeleteOrChange property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the deleteOrSoftDeleteOrChange property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getDeleteOrSoftDeleteOrChange().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Sync.Collections.Collection.Commands.Delete }
                 * {@link Sync.Collections.Collection.Commands.SoftDelete }
                 * {@link Sync.Collections.Collection.Commands.Change }
                 * {@link Sync.Collections.Collection.Commands.Add }
                 * 
                 * 
                 */
                public List<Object> getDeleteOrSoftDeleteOrChange() {
                    if (deleteOrSoftDeleteOrChange == null) {
                        deleteOrSoftDeleteOrChange = new ArrayList<Object>();
                    }
                    return this.deleteOrSoftDeleteOrChange;
                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *         &lt;element name="ApplicationData">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;choice maxOccurs="unbounded">
                 *                     &lt;element ref="{AirSyncBase}Body"/>
                 *                     &lt;element ref="{AirSyncBase}BodyPart"/>
                 *                     &lt;element ref="{AirSyncBase}NativeBodyType"/>
                 *                     &lt;element ref="{AirSyncBase}InstanceId"/>
                 *                     &lt;element ref="{AirSyncBase}Location"/>
                 *                     &lt;group ref="{Calendar}AllProps"/>
                 *                     &lt;group ref="{Contacts}AllProps"/>
                 *                     &lt;group ref="{Contacts2}AllProps"/>
                 *                     &lt;group ref="{DocumentLibrary}AllProps"/>
                 *                     &lt;group ref="{Email}AllProps"/>
                 *                     &lt;group ref="{Email2}AllProps"/>
                 *                     &lt;element ref="{Notes}Subject"/>
                 *                     &lt;element ref="{Notes}MessageClass"/>
                 *                     &lt;element ref="{Notes}LastModifiedDate"/>
                 *                     &lt;element ref="{Notes}Categories"/>
                 *                     &lt;group ref="{Tasks}AllProps"/>
                 *                     &lt;element ref="{RightsManagement}RightsManagementLicense"/>
                 *                   &lt;/choice>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId",
                    "applicationData"
                })
                public static class Add {

                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;
                    @XmlElement(name = "ApplicationData", required = true)
                    protected Sync.Collections.Collection.Commands.Add.ApplicationData applicationData;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der applicationData-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Sync.Collections.Collection.Commands.Add.ApplicationData }
                     *     
                     */
                    public Sync.Collections.Collection.Commands.Add.ApplicationData getApplicationData() {
                        return applicationData;
                    }

                    /**
                     * Legt den Wert der applicationData-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Sync.Collections.Collection.Commands.Add.ApplicationData }
                     *     
                     */
                    public void setApplicationData(Sync.Collections.Collection.Commands.Add.ApplicationData value) {
                        this.applicationData = value;
                    }


                    /**
                     * <p>Java-Klasse für anonymous complex type.
                     * 
                     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;choice maxOccurs="unbounded">
                     *           &lt;element ref="{AirSyncBase}Body"/>
                     *           &lt;element ref="{AirSyncBase}BodyPart"/>
                     *           &lt;element ref="{AirSyncBase}NativeBodyType"/>
                     *           &lt;element ref="{AirSyncBase}InstanceId"/>
                     *           &lt;element ref="{AirSyncBase}Location"/>
                     *           &lt;group ref="{Calendar}AllProps"/>
                     *           &lt;group ref="{Contacts}AllProps"/>
                     *           &lt;group ref="{Contacts2}AllProps"/>
                     *           &lt;group ref="{DocumentLibrary}AllProps"/>
                     *           &lt;group ref="{Email}AllProps"/>
                     *           &lt;group ref="{Email2}AllProps"/>
                     *           &lt;element ref="{Notes}Subject"/>
                     *           &lt;element ref="{Notes}MessageClass"/>
                     *           &lt;element ref="{Notes}LastModifiedDate"/>
                     *           &lt;element ref="{Notes}Categories"/>
                     *           &lt;group ref="{Tasks}AllProps"/>
                     *           &lt;element ref="{RightsManagement}RightsManagementLicense"/>
                     *         &lt;/choice>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "bodyOrBodyPartOrNativeBodyType"
                    })
                    public static class ApplicationData {

                        @XmlElementRefs({
                            @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountId", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsHidden", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LinkId", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Send", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ResponseType", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
                            @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sender", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Alias", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "InstanceId", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
                            @XmlElementRef(name = "IsDraft", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingRequest", namespace = "Email", type = MeetingRequest.class, required = false),
                            @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attachments", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Attachments.class, required = false),
                            @XmlElementRef(name = "MIMEData", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MIMESize", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmAttDuration", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Notes", type = de.chdev.jasync.xsd.response.notes.Categories.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.response.contacts.Categories.class, required = false),
                            @XmlElementRef(name = "Bcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
                            @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
                            @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WeightedRank", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RightsManagementLicense", namespace = "RightsManagement", type = RightsManagementLicense.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentLength", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisplayName", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentClass", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ConversationIndex", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OnlineMeetingExternalLink", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastVerbExecutionTime", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OnlineMeetingConfLink", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReceivedAsBcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MIMETruncated", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastVerbExecuted", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstDayOfWeek", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyPart", namespace = "AirSyncBase", type = BodyPart.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "SubOrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ConversationId", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CalendarType", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsFolder", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Categories.class, required = false),
                            @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmCallerID", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Categories.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmUserNotes", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsLeapMonth", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ThreadTopic", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Recurrence.class, required = false),
                            @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Categories.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Recurrence.class, required = false),
                            @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentType", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CreationDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
                            @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AppointmentReplyTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmAttOrder", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingMessageType", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false)
                        })
                        protected List<Object> bodyOrBodyPartOrNativeBodyType;

                        /**
                         * Gets the value of the bodyOrBodyPartOrNativeBodyType property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the bodyOrBodyPartOrNativeBodyType property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getBodyOrBodyPartOrNativeBodyType().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Object }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Location }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Children }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link MeetingRequest }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.email.Attachments }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.notes.Categories }
                         * {@link de.chdev.jasync.xsd.response.contacts.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Attendees }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link Body }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Integer }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link RightsManagementLicense }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link BodyPart }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Flag }
                         * {@link de.chdev.jasync.xsd.response.calendar.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.tasks.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link de.chdev.jasync.xsd.response.calendar.Recurrence }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.email.Categories }
                         * {@link de.chdev.jasync.xsd.response.tasks.Recurrence }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link Exceptions }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * 
                         * 
                         */
                        public List<Object> getBodyOrBodyPartOrNativeBodyType() {
                            if (bodyOrBodyPartOrNativeBodyType == null) {
                                bodyOrBodyPartOrNativeBodyType = new ArrayList<Object>();
                            }
                            return this.bodyOrBodyPartOrNativeBodyType;
                        }

                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *         &lt;element name="ApplicationData">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;choice maxOccurs="unbounded">
                 *                   &lt;element ref="{AirSyncBase}Body"/>
                 *                   &lt;element ref="{AirSyncBase}BodyPart"/>
                 *                   &lt;element ref="{AirSyncBase}NativeBodyType"/>
                 *                   &lt;element ref="{AirSyncBase}InstanceId"/>
                 *                   &lt;element ref="{AirSyncBase}Location"/>
                 *                   &lt;group ref="{Calendar}AllProps"/>
                 *                   &lt;group ref="{Contacts}AllProps"/>
                 *                   &lt;group ref="{Contacts2}AllProps"/>
                 *                   &lt;group ref="{DocumentLibrary}AllProps"/>
                 *                   &lt;group ref="{Email}AllProps"/>
                 *                   &lt;group ref="{Email2}AllProps"/>
                 *                   &lt;element ref="{Notes}Subject"/>
                 *                   &lt;element ref="{Notes}MessageClass"/>
                 *                   &lt;element ref="{Notes}LastModifiedDate"/>
                 *                   &lt;element ref="{Notes}Categories"/>
                 *                   &lt;group ref="{Tasks}AllProps"/>
                 *                   &lt;element ref="{RightsManagement}RightsManagementLicense"/>
                 *                 &lt;/choice>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "clazz",
                    "serverId",
                    "applicationData"
                })
                public static class Change {

                    @XmlElement(name = "Class")
                    protected String clazz;
                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;
                    @XmlElement(name = "ApplicationData", required = true)
                    protected Sync.Collections.Collection.Commands.Change.ApplicationData applicationData;

                    /**
                     * Ruft den Wert der clazz-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClazz() {
                        return clazz;
                    }

                    /**
                     * Legt den Wert der clazz-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClazz(String value) {
                        this.clazz = value;
                    }

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der applicationData-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Sync.Collections.Collection.Commands.Change.ApplicationData }
                     *     
                     */
                    public Sync.Collections.Collection.Commands.Change.ApplicationData getApplicationData() {
                        return applicationData;
                    }

                    /**
                     * Legt den Wert der applicationData-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Sync.Collections.Collection.Commands.Change.ApplicationData }
                     *     
                     */
                    public void setApplicationData(Sync.Collections.Collection.Commands.Change.ApplicationData value) {
                        this.applicationData = value;
                    }


                    /**
                     * <p>Java-Klasse für anonymous complex type.
                     * 
                     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;choice maxOccurs="unbounded">
                     *         &lt;element ref="{AirSyncBase}Body"/>
                     *         &lt;element ref="{AirSyncBase}BodyPart"/>
                     *         &lt;element ref="{AirSyncBase}NativeBodyType"/>
                     *         &lt;element ref="{AirSyncBase}InstanceId"/>
                     *         &lt;element ref="{AirSyncBase}Location"/>
                     *         &lt;group ref="{Calendar}AllProps"/>
                     *         &lt;group ref="{Contacts}AllProps"/>
                     *         &lt;group ref="{Contacts2}AllProps"/>
                     *         &lt;group ref="{DocumentLibrary}AllProps"/>
                     *         &lt;group ref="{Email}AllProps"/>
                     *         &lt;group ref="{Email2}AllProps"/>
                     *         &lt;element ref="{Notes}Subject"/>
                     *         &lt;element ref="{Notes}MessageClass"/>
                     *         &lt;element ref="{Notes}LastModifiedDate"/>
                     *         &lt;element ref="{Notes}Categories"/>
                     *         &lt;group ref="{Tasks}AllProps"/>
                     *         &lt;element ref="{RightsManagement}RightsManagementLicense"/>
                     *       &lt;/choice>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "bodyOrBodyPartOrNativeBodyType"
                    })
                    public static class ApplicationData {

                        @XmlElementRefs({
                            @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountId", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsHidden", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LinkId", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Send", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ResponseType", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
                            @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sender", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Alias", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "InstanceId", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
                            @XmlElementRef(name = "IsDraft", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingRequest", namespace = "Email", type = MeetingRequest.class, required = false),
                            @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attachments", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Attachments.class, required = false),
                            @XmlElementRef(name = "MIMEData", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MIMESize", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmAttDuration", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Notes", type = de.chdev.jasync.xsd.response.notes.Categories.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.response.contacts.Categories.class, required = false),
                            @XmlElementRef(name = "Bcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
                            @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
                            @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WeightedRank", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RightsManagementLicense", namespace = "RightsManagement", type = RightsManagementLicense.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentLength", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisplayName", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentClass", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ConversationIndex", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OnlineMeetingExternalLink", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastVerbExecutionTime", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OnlineMeetingConfLink", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReceivedAsBcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MIMETruncated", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastVerbExecuted", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstDayOfWeek", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyPart", namespace = "AirSyncBase", type = BodyPart.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "SubOrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ConversationId", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CalendarType", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsFolder", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Categories.class, required = false),
                            @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmCallerID", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Categories.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmUserNotes", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsLeapMonth", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ThreadTopic", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Recurrence.class, required = false),
                            @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Categories.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Recurrence.class, required = false),
                            @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentType", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CreationDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
                            @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AppointmentReplyTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmAttOrder", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingMessageType", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false)
                        })
                        protected List<Object> bodyOrBodyPartOrNativeBodyType;

                        /**
                         * Gets the value of the bodyOrBodyPartOrNativeBodyType property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the bodyOrBodyPartOrNativeBodyType property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getBodyOrBodyPartOrNativeBodyType().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Object }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Location }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Children }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link MeetingRequest }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.email.Attachments }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.notes.Categories }
                         * {@link de.chdev.jasync.xsd.response.contacts.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Attendees }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link Body }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Integer }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link RightsManagementLicense }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link BodyPart }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Flag }
                         * {@link de.chdev.jasync.xsd.response.calendar.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.tasks.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link de.chdev.jasync.xsd.response.calendar.Recurrence }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.email.Categories }
                         * {@link de.chdev.jasync.xsd.response.tasks.Recurrence }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link Exceptions }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * 
                         * 
                         */
                        public List<Object> getBodyOrBodyPartOrNativeBodyType() {
                            if (bodyOrBodyPartOrNativeBodyType == null) {
                                bodyOrBodyPartOrNativeBodyType = new ArrayList<Object>();
                            }
                            return this.bodyOrBodyPartOrNativeBodyType;
                        }

                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "clazz",
                    "serverId"
                })
                public static class Delete {

                    @XmlElement(name = "Class")
                    protected String clazz;
                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;

                    /**
                     * Ruft den Wert der clazz-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClazz() {
                        return clazz;
                    }

                    /**
                     * Legt den Wert der clazz-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClazz(String value) {
                        this.clazz = value;
                    }

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId"
                })
                public static class SoftDelete {

                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                }

            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}Class" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}ServerId"/>
             *                   &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}Status"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}Class" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}ClientId"/>
             *                   &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}Status"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Delete" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}ServerId"/>
             *                   &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}Status"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Fetch" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}Status"/>
             *                   &lt;element name="ApplicationData">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;choice maxOccurs="unbounded">
             *                               &lt;element ref="{AirSyncBase}Attachments"/>
             *                               &lt;element ref="{AirSyncBase}Body"/>
             *                               &lt;element ref="{AirSyncBase}NativeBodyType"/>
             *                               &lt;element ref="{AirSyncBase}Location"/>
             *                               &lt;group ref="{Calendar}AllProps"/>
             *                               &lt;group ref="{Contacts}AllProps"/>
             *                               &lt;group ref="{Contacts2}AllProps"/>
             *                               &lt;group ref="{DocumentLibrary}AllProps"/>
             *                               &lt;group ref="{Email}AllProps"/>
             *                               &lt;group ref="{Email2}AllProps"/>
             *                               &lt;group ref="{Tasks}AllProps"/>
             *                               &lt;element ref="{RightsManagement}RightsManagementLicense"/>
             *                             &lt;/choice>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "change",
                "add",
                "delete",
                "fetch"
            })
            public static class Responses {

                @XmlElement(name = "Change")
                protected List<Sync.Collections.Collection.Responses.Change> change;
                @XmlElement(name = "Add")
                protected List<Sync.Collections.Collection.Responses.Add> add;
                @XmlElement(name = "Delete")
                protected Sync.Collections.Collection.Responses.Delete delete;
                @XmlElement(name = "Fetch")
                protected Sync.Collections.Collection.Responses.Fetch fetch;

                /**
                 * Gets the value of the change property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the change property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getChange().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Sync.Collections.Collection.Responses.Change }
                 * 
                 * 
                 */
                public List<Sync.Collections.Collection.Responses.Change> getChange() {
                    if (change == null) {
                        change = new ArrayList<Sync.Collections.Collection.Responses.Change>();
                    }
                    return this.change;
                }

                /**
                 * Gets the value of the add property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the add property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getAdd().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Sync.Collections.Collection.Responses.Add }
                 * 
                 * 
                 */
                public List<Sync.Collections.Collection.Responses.Add> getAdd() {
                    if (add == null) {
                        add = new ArrayList<Sync.Collections.Collection.Responses.Add>();
                    }
                    return this.add;
                }

                /**
                 * Ruft den Wert der delete-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Sync.Collections.Collection.Responses.Delete }
                 *     
                 */
                public Sync.Collections.Collection.Responses.Delete getDelete() {
                    return delete;
                }

                /**
                 * Legt den Wert der delete-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Sync.Collections.Collection.Responses.Delete }
                 *     
                 */
                public void setDelete(Sync.Collections.Collection.Responses.Delete value) {
                    this.delete = value;
                }

                /**
                 * Ruft den Wert der fetch-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Sync.Collections.Collection.Responses.Fetch }
                 *     
                 */
                public Sync.Collections.Collection.Responses.Fetch getFetch() {
                    return fetch;
                }

                /**
                 * Legt den Wert der fetch-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Sync.Collections.Collection.Responses.Fetch }
                 *     
                 */
                public void setFetch(Sync.Collections.Collection.Responses.Fetch value) {
                    this.fetch = value;
                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}ClientId"/>
                 *         &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}Status"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "clazz",
                    "clientId",
                    "serverId",
                    "status"
                })
                public static class Add {

                    @XmlElement(name = "Class")
                    protected String clazz;
                    @XmlElement(name = "ClientId", required = true)
                    protected String clientId;
                    @XmlElement(name = "ServerId")
                    protected String serverId;
                    @XmlElement(name = "Status", required = true)
                    protected BigInteger status;

                    /**
                     * Ruft den Wert der clazz-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClazz() {
                        return clazz;
                    }

                    /**
                     * Legt den Wert der clazz-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClazz(String value) {
                        this.clazz = value;
                    }

                    /**
                     * Ruft den Wert der clientId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClientId() {
                        return clientId;
                    }

                    /**
                     * Legt den Wert der clientId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClientId(String value) {
                        this.clientId = value;
                    }

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der status-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getStatus() {
                        return status;
                    }

                    /**
                     * Legt den Wert der status-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setStatus(BigInteger value) {
                        this.status = value;
                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}Status"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "clazz",
                    "serverId",
                    "instanceId",
                    "status"
                })
                public static class Change {

                    @XmlElement(name = "Class")
                    protected String clazz;
                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;
                    @XmlElement(name = "InstanceId", namespace = "AirSyncBase")
                    protected String instanceId;
                    @XmlElement(name = "Status", required = true)
                    protected BigInteger status;

                    /**
                     * Ruft den Wert der clazz-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClazz() {
                        return clazz;
                    }

                    /**
                     * Legt den Wert der clazz-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClazz(String value) {
                        this.clazz = value;
                    }

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der instanceId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInstanceId() {
                        return instanceId;
                    }

                    /**
                     * Legt den Wert der instanceId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInstanceId(String value) {
                        this.instanceId = value;
                    }

                    /**
                     * Ruft den Wert der status-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getStatus() {
                        return status;
                    }

                    /**
                     * Legt den Wert der status-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setStatus(BigInteger value) {
                        this.status = value;
                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}Status"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId",
                    "instanceId",
                    "status"
                })
                public static class Delete {

                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;
                    @XmlElement(name = "InstanceId", namespace = "AirSyncBase")
                    protected String instanceId;
                    @XmlElement(name = "Status", required = true)
                    protected BigInteger status;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der instanceId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInstanceId() {
                        return instanceId;
                    }

                    /**
                     * Legt den Wert der instanceId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInstanceId(String value) {
                        this.instanceId = value;
                    }

                    /**
                     * Ruft den Wert der status-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getStatus() {
                        return status;
                    }

                    /**
                     * Legt den Wert der status-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setStatus(BigInteger value) {
                        this.status = value;
                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}Status"/>
                 *         &lt;element name="ApplicationData">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;choice maxOccurs="unbounded">
                 *                     &lt;element ref="{AirSyncBase}Attachments"/>
                 *                     &lt;element ref="{AirSyncBase}Body"/>
                 *                     &lt;element ref="{AirSyncBase}NativeBodyType"/>
                 *                     &lt;element ref="{AirSyncBase}Location"/>
                 *                     &lt;group ref="{Calendar}AllProps"/>
                 *                     &lt;group ref="{Contacts}AllProps"/>
                 *                     &lt;group ref="{Contacts2}AllProps"/>
                 *                     &lt;group ref="{DocumentLibrary}AllProps"/>
                 *                     &lt;group ref="{Email}AllProps"/>
                 *                     &lt;group ref="{Email2}AllProps"/>
                 *                     &lt;group ref="{Tasks}AllProps"/>
                 *                     &lt;element ref="{RightsManagement}RightsManagementLicense"/>
                 *                   &lt;/choice>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId",
                    "status",
                    "applicationData"
                })
                public static class Fetch {

                    @XmlElement(name = "ServerId")
                    protected String serverId;
                    @XmlElement(name = "Status", required = true)
                    protected BigInteger status;
                    @XmlElement(name = "ApplicationData", required = true)
                    protected Sync.Collections.Collection.Responses.Fetch.ApplicationData applicationData;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der status-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getStatus() {
                        return status;
                    }

                    /**
                     * Legt den Wert der status-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setStatus(BigInteger value) {
                        this.status = value;
                    }

                    /**
                     * Ruft den Wert der applicationData-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Sync.Collections.Collection.Responses.Fetch.ApplicationData }
                     *     
                     */
                    public Sync.Collections.Collection.Responses.Fetch.ApplicationData getApplicationData() {
                        return applicationData;
                    }

                    /**
                     * Legt den Wert der applicationData-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Sync.Collections.Collection.Responses.Fetch.ApplicationData }
                     *     
                     */
                    public void setApplicationData(Sync.Collections.Collection.Responses.Fetch.ApplicationData value) {
                        this.applicationData = value;
                    }


                    /**
                     * <p>Java-Klasse für anonymous complex type.
                     * 
                     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;choice maxOccurs="unbounded">
                     *           &lt;element ref="{AirSyncBase}Attachments"/>
                     *           &lt;element ref="{AirSyncBase}Body"/>
                     *           &lt;element ref="{AirSyncBase}NativeBodyType"/>
                     *           &lt;element ref="{AirSyncBase}Location"/>
                     *           &lt;group ref="{Calendar}AllProps"/>
                     *           &lt;group ref="{Contacts}AllProps"/>
                     *           &lt;group ref="{Contacts2}AllProps"/>
                     *           &lt;group ref="{DocumentLibrary}AllProps"/>
                     *           &lt;group ref="{Email}AllProps"/>
                     *           &lt;group ref="{Email2}AllProps"/>
                     *           &lt;group ref="{Tasks}AllProps"/>
                     *           &lt;element ref="{RightsManagement}RightsManagementLicense"/>
                     *         &lt;/choice>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "attachmentsOrBodyOrNativeBodyType"
                    })
                    public static class ApplicationData {

                        @XmlElementRefs({
                            @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountId", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attachments", namespace = "AirSyncBase", type = de.chdev.jasync.xsd.response.airsyncbase.Attachments.class, required = false),
                            @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsHidden", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LinkId", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Send", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ResponseType", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
                            @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sender", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Alias", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
                            @XmlElementRef(name = "IsDraft", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingRequest", namespace = "Email", type = MeetingRequest.class, required = false),
                            @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attachments", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Attachments.class, required = false),
                            @XmlElementRef(name = "MIMEData", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MIMESize", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmAttDuration", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.response.contacts.Categories.class, required = false),
                            @XmlElementRef(name = "Bcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
                            @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
                            @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WeightedRank", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RightsManagementLicense", namespace = "RightsManagement", type = RightsManagementLicense.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentLength", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisplayName", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentClass", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ConversationIndex", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OnlineMeetingExternalLink", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastVerbExecutionTime", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OnlineMeetingConfLink", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReceivedAsBcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MIMETruncated", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastVerbExecuted", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstDayOfWeek", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "SubOrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ConversationId", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CalendarType", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsFolder", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Categories.class, required = false),
                            @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmCallerID", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Categories.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmUserNotes", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IsLeapMonth", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ThreadTopic", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodySize", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Recurrence.class, required = false),
                            @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Categories.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Recurrence.class, required = false),
                            @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ContentType", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CreationDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
                            @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AppointmentReplyTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BodyTruncated", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UmAttOrder", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingMessageType", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false)
                        })
                        protected List<Object> attachmentsOrBodyOrNativeBodyType;

                        /**
                         * Gets the value of the attachmentsOrBodyOrNativeBodyType property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the attachmentsOrBodyOrNativeBodyType property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getAttachmentsOrBodyOrNativeBodyType().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link de.chdev.jasync.xsd.response.airsyncbase.Attachments }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Object }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Location }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Children }
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link MeetingRequest }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.email.Attachments }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.contacts.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Attendees }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link Body }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Integer }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link RightsManagementLicense }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Flag }
                         * {@link de.chdev.jasync.xsd.response.calendar.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.tasks.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link de.chdev.jasync.xsd.response.calendar.Recurrence }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.response.email.Categories }
                         * {@link de.chdev.jasync.xsd.response.tasks.Recurrence }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link Exceptions }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * 
                         * 
                         */
                        public List<Object> getAttachmentsOrBodyOrNativeBodyType() {
                            if (attachmentsOrBodyOrNativeBodyType == null) {
                                attachmentsOrBodyOrNativeBodyType = new ArrayList<Object>();
                            }
                            return this.attachmentsOrBodyOrNativeBodyType;
                        }

                    }

                }

            }

        }

    }

}
