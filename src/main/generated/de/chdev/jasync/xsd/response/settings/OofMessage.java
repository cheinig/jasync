//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.settings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AppliesToInternal" type="{AirSyncBase}EmptyTag" minOccurs="0"/>
 *         &lt;element name="AppliesToExternalKnown" type="{AirSyncBase}EmptyTag" minOccurs="0"/>
 *         &lt;element name="AppliesToExternalUnknown" type="{AirSyncBase}EmptyTag" minOccurs="0"/>
 *         &lt;element name="Enabled" type="{Settings}NonEmptyStringType" minOccurs="0"/>
 *         &lt;element name="ReplyMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{Settings}BodyType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "OofMessage")
public class OofMessage {

    @XmlElement(name = "AppliesToInternal")
    protected String appliesToInternal;
    @XmlElement(name = "AppliesToExternalKnown")
    protected String appliesToExternalKnown;
    @XmlElement(name = "AppliesToExternalUnknown")
    protected String appliesToExternalUnknown;
    @XmlElement(name = "Enabled")
    protected String enabled;
    @XmlElement(name = "ReplyMessage")
    protected String replyMessage;
    @XmlElement(name = "BodyType")
    protected String bodyType;

    /**
     * Ruft den Wert der appliesToInternal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliesToInternal() {
        return appliesToInternal;
    }

    /**
     * Legt den Wert der appliesToInternal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliesToInternal(String value) {
        this.appliesToInternal = value;
    }

    /**
     * Ruft den Wert der appliesToExternalKnown-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliesToExternalKnown() {
        return appliesToExternalKnown;
    }

    /**
     * Legt den Wert der appliesToExternalKnown-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliesToExternalKnown(String value) {
        this.appliesToExternalKnown = value;
    }

    /**
     * Ruft den Wert der appliesToExternalUnknown-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliesToExternalUnknown() {
        return appliesToExternalUnknown;
    }

    /**
     * Legt den Wert der appliesToExternalUnknown-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliesToExternalUnknown(String value) {
        this.appliesToExternalUnknown = value;
    }

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnabled(String value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der replyMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplyMessage() {
        return replyMessage;
    }

    /**
     * Legt den Wert der replyMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplyMessage(String value) {
        this.replyMessage = value;
    }

    /**
     * Ruft den Wert der bodyType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyType() {
        return bodyType;
    }

    /**
     * Legt den Wert der bodyType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyType(String value) {
        this.bodyType = value;
    }

}
