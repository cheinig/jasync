//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.airsync;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.airsync package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SyncKey_QNAME = new QName("AirSync", "SyncKey");
    private final static QName _Class_QNAME = new QName("AirSync", "Class");
    private final static QName _FilterType_QNAME = new QName("AirSync", "FilterType");
    private final static QName _Conflict_QNAME = new QName("AirSync", "Conflict");
    private final static QName _Partial_QNAME = new QName("AirSync", "Partial");
    private final static QName _HeartbeatInterval_QNAME = new QName("AirSync", "HeartbeatInterval");
    private final static QName _MIMESupport_QNAME = new QName("AirSync", "MIMESupport");
    private final static QName _MaxItems_QNAME = new QName("AirSync", "MaxItems");
    private final static QName _Status_QNAME = new QName("AirSync", "Status");
    private final static QName _ConversationMode_QNAME = new QName("AirSync", "ConversationMode");
    private final static QName _GetChanges_QNAME = new QName("AirSync", "GetChanges");
    private final static QName _WindowSize_QNAME = new QName("AirSync", "WindowSize");
    private final static QName _Limit_QNAME = new QName("AirSync", "Limit");
    private final static QName _ServerId_QNAME = new QName("AirSync", "ServerId");
    private final static QName _DeletesAsMoves_QNAME = new QName("AirSync", "DeletesAsMoves");
    private final static QName _Wait_QNAME = new QName("AirSync", "Wait");
    private final static QName _ClientId_QNAME = new QName("AirSync", "ClientId");
    private final static QName _MoreAvailable_QNAME = new QName("AirSync", "MoreAvailable");
    private final static QName _CollectionId_QNAME = new QName("AirSync", "CollectionId");
    private final static QName _Truncation_QNAME = new QName("AirSync", "Truncation");
    private final static QName _MIMETruncation_QNAME = new QName("AirSync", "MIMETruncation");
    private final static QName _SyncCollectionsCollectionCommands_QNAME = new QName("AirSync", "Commands");
    private final static QName _SyncCollectionsCollectionResponses_QNAME = new QName("AirSync", "Responses");
    private final static QName _SupportedReminder_QNAME = new QName("Calendar", "Reminder");
    private final static QName _SupportedAssistnamePhoneNumber_QNAME = new QName("Contacts", "AssistnamePhoneNumber");
    private final static QName _SupportedEmail1Address_QNAME = new QName("Contacts", "Email1Address");
    private final static QName _SupportedSubject_QNAME = new QName("Calendar", "Subject");
    private final static QName _SupportedOfficeLocation_QNAME = new QName("Contacts", "OfficeLocation");
    private final static QName _SupportedEmail3Address_QNAME = new QName("Contacts", "Email3Address");
    private final static QName _SupportedOrganizerEmail_QNAME = new QName("Calendar", "OrganizerEmail");
    private final static QName _SupportedFirstName_QNAME = new QName("Contacts", "FirstName");
    private final static QName _SupportedSensitivity_QNAME = new QName("Calendar", "Sensitivity");
    private final static QName _SupportedHomeAddressPostalCode_QNAME = new QName("Contacts", "HomeAddressPostalCode");
    private final static QName _SupportedLocation_QNAME = new QName("Calendar", "Location");
    private final static QName _SupportedMiddleName_QNAME = new QName("Contacts", "MiddleName");
    private final static QName _SupportedRadioPhoneNumber_QNAME = new QName("Contacts", "RadioPhoneNumber");
    private final static QName _SupportedOtherAddressState_QNAME = new QName("Contacts", "OtherAddressState");
    private final static QName _SupportedTitle_QNAME = new QName("Contacts", "Title");
    private final static QName _SupportedHomeFaxNumber_QNAME = new QName("Contacts", "HomeFaxNumber");
    private final static QName _SupportedAssistantPhoneNumber_QNAME = new QName("Contacts", "AssistantPhoneNumber");
    private final static QName _SupportedMeetingStatus_QNAME = new QName("Calendar", "MeetingStatus");
    private final static QName _SupportedHomePhoneNumber_QNAME = new QName("Contacts", "HomePhoneNumber");
    private final static QName _SupportedEndTime_QNAME = new QName("Calendar", "EndTime");
    private final static QName _SupportedOtherAddressPostalCode_QNAME = new QName("Contacts", "OtherAddressPostalCode");
    private final static QName _SupportedYomiFirstName_QNAME = new QName("Contacts", "YomiFirstName");
    private final static QName _SupportedChildren_QNAME = new QName("Contacts", "Children");
    private final static QName _SupportedYomiCompanyName_QNAME = new QName("Contacts", "YomiCompanyName");
    private final static QName _SupportedAssistantName_QNAME = new QName("Contacts", "AssistantName");
    private final static QName _SupportedWebPage_QNAME = new QName("Contacts", "WebPage");
    private final static QName _SupportedMobilePhoneNumber_QNAME = new QName("Contacts", "MobilePhoneNumber");
    private final static QName _SupportedJobTitle_QNAME = new QName("Contacts", "JobTitle");
    private final static QName _SupportedOtherAddressCountry_QNAME = new QName("Contacts", "OtherAddressCountry");
    private final static QName _SupportedOtherAddressCity_QNAME = new QName("Contacts", "OtherAddressCity");
    private final static QName _SupportedExceptions_QNAME = new QName("Calendar", "Exceptions");
    private final static QName _SupportedSpouse_QNAME = new QName("Contacts", "Spouse");
    private final static QName _SupportedNickName_QNAME = new QName("Contacts2", "NickName");
    private final static QName _SupportedHomeAddressState_QNAME = new QName("Contacts", "HomeAddressState");
    private final static QName _SupportedCustomerId_QNAME = new QName("Contacts2", "CustomerId");
    private final static QName _SupportedSuffix_QNAME = new QName("Contacts", "Suffix");
    private final static QName _SupportedCompanyMainPhone_QNAME = new QName("Contacts2", "CompanyMainPhone");
    private final static QName _SupportedStartTime_QNAME = new QName("Calendar", "StartTime");
    private final static QName _SupportedEmail2Address_QNAME = new QName("Contacts", "Email2Address");
    private final static QName _SupportedLastName_QNAME = new QName("Contacts", "LastName");
    private final static QName _SupportedIMAddress3_QNAME = new QName("Contacts2", "IMAddress3");
    private final static QName _SupportedAccountName_QNAME = new QName("Contacts2", "AccountName");
    private final static QName _SupportedHomeAddressCountry_QNAME = new QName("Contacts", "HomeAddressCountry");
    private final static QName _SupportedCategories_QNAME = new QName("Contacts", "Categories");
    private final static QName _SupportedIMAddress_QNAME = new QName("Contacts2", "IMAddress");
    private final static QName _SupportedIMAddress2_QNAME = new QName("Contacts2", "IMAddress2");
    private final static QName _SupportedResponseRequested_QNAME = new QName("Calendar", "ResponseRequested");
    private final static QName _SupportedUID_QNAME = new QName("Calendar", "UID");
    private final static QName _SupportedPagerNumber_QNAME = new QName("Contacts", "PagerNumber");
    private final static QName _SupportedBusiness2PhoneNumber_QNAME = new QName("Contacts", "Business2PhoneNumber");
    private final static QName _SupportedBusinessAddressCountry_QNAME = new QName("Contacts", "BusinessAddressCountry");
    private final static QName _SupportedYomiLastName_QNAME = new QName("Contacts", "YomiLastName");
    private final static QName _SupportedAnniversary_QNAME = new QName("Contacts", "Anniversary");
    private final static QName _SupportedGovernmentId_QNAME = new QName("Contacts2", "GovernmentId");
    private final static QName _SupportedMMS_QNAME = new QName("Contacts2", "MMS");
    private final static QName _SupportedDisallowNewTimeProposal_QNAME = new QName("Calendar", "DisallowNewTimeProposal");
    private final static QName _SupportedHomeAddressStreet_QNAME = new QName("Contacts", "HomeAddressStreet");
    private final static QName _SupportedBusinessPhoneNumber_QNAME = new QName("Contacts", "BusinessPhoneNumber");
    private final static QName _SupportedBusinessAddressStreet_QNAME = new QName("Contacts", "BusinessAddressStreet");
    private final static QName _SupportedBusinessAddressState_QNAME = new QName("Contacts", "BusinessAddressState");
    private final static QName _SupportedPicture_QNAME = new QName("Contacts", "Picture");
    private final static QName _SupportedCompanyName_QNAME = new QName("Contacts", "CompanyName");
    private final static QName _SupportedRecurrence_QNAME = new QName("Calendar", "Recurrence");
    private final static QName _SupportedDepartment_QNAME = new QName("Contacts", "Department");
    private final static QName _SupportedOtherAddressStreet_QNAME = new QName("Contacts", "OtherAddressStreet");
    private final static QName _SupportedTimezone_QNAME = new QName("Calendar", "Timezone");
    private final static QName _SupportedManagerName_QNAME = new QName("Contacts2", "ManagerName");
    private final static QName _SupportedBusinessAddressPostalCode_QNAME = new QName("Contacts", "BusinessAddressPostalCode");
    private final static QName _SupportedCarPhoneNumber_QNAME = new QName("Contacts", "CarPhoneNumber");
    private final static QName _SupportedBusyStatus_QNAME = new QName("Calendar", "BusyStatus");
    private final static QName _SupportedAllDayEvent_QNAME = new QName("Calendar", "AllDayEvent");
    private final static QName _SupportedAttendees_QNAME = new QName("Calendar", "Attendees");
    private final static QName _SupportedDtStamp_QNAME = new QName("Calendar", "DtStamp");
    private final static QName _SupportedFileAs_QNAME = new QName("Contacts", "FileAs");
    private final static QName _SupportedHome2PhoneNumber_QNAME = new QName("Contacts", "Home2PhoneNumber");
    private final static QName _SupportedBusinessAddressCity_QNAME = new QName("Contacts", "BusinessAddressCity");
    private final static QName _SupportedBirthday_QNAME = new QName("Contacts", "Birthday");
    private final static QName _SupportedHomeAddressCity_QNAME = new QName("Contacts", "HomeAddressCity");
    private final static QName _SupportedOrganizerName_QNAME = new QName("Calendar", "OrganizerName");
    private final static QName _SupportedBusinessFaxNumber_QNAME = new QName("Contacts", "BusinessFaxNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.airsync
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Sync }
     * 
     */
    public Sync createSync() {
        return new Sync();
    }

    /**
     * Create an instance of {@link Sync.Collections }
     * 
     */
    public Sync.Collections createSyncCollections() {
        return new Sync.Collections();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection }
     * 
     */
    public Sync.Collections.Collection createSyncCollectionsCollection() {
        return new Sync.Collections.Collection();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Responses }
     * 
     */
    public Sync.Collections.Collection.Responses createSyncCollectionsCollectionResponses() {
        return new Sync.Collections.Collection.Responses();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Responses.Fetch }
     * 
     */
    public Sync.Collections.Collection.Responses.Fetch createSyncCollectionsCollectionResponsesFetch() {
        return new Sync.Collections.Collection.Responses.Fetch();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands }
     * 
     */
    public Sync.Collections.Collection.Commands createSyncCollectionsCollectionCommands() {
        return new Sync.Collections.Collection.Commands();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands.Add }
     * 
     */
    public Sync.Collections.Collection.Commands.Add createSyncCollectionsCollectionCommandsAdd() {
        return new Sync.Collections.Collection.Commands.Add();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands.Change }
     * 
     */
    public Sync.Collections.Collection.Commands.Change createSyncCollectionsCollectionCommandsChange() {
        return new Sync.Collections.Collection.Commands.Change();
    }

    /**
     * Create an instance of {@link Options }
     * 
     */
    public Options createOptions() {
        return new Options();
    }

    /**
     * Create an instance of {@link de.chdev.jasync.xsd.response.airsync.SoftDelete }
     * 
     */
    public de.chdev.jasync.xsd.response.airsync.SoftDelete createSoftDelete() {
        return new de.chdev.jasync.xsd.response.airsync.SoftDelete();
    }

    /**
     * Create an instance of {@link Supported }
     * 
     */
    public Supported createSupported() {
        return new Supported();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Responses.Change }
     * 
     */
    public Sync.Collections.Collection.Responses.Change createSyncCollectionsCollectionResponsesChange() {
        return new Sync.Collections.Collection.Responses.Change();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Responses.Add }
     * 
     */
    public Sync.Collections.Collection.Responses.Add createSyncCollectionsCollectionResponsesAdd() {
        return new Sync.Collections.Collection.Responses.Add();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Responses.Delete }
     * 
     */
    public Sync.Collections.Collection.Responses.Delete createSyncCollectionsCollectionResponsesDelete() {
        return new Sync.Collections.Collection.Responses.Delete();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Responses.Fetch.ApplicationData }
     * 
     */
    public Sync.Collections.Collection.Responses.Fetch.ApplicationData createSyncCollectionsCollectionResponsesFetchApplicationData() {
        return new Sync.Collections.Collection.Responses.Fetch.ApplicationData();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands.Delete }
     * 
     */
    public Sync.Collections.Collection.Commands.Delete createSyncCollectionsCollectionCommandsDelete() {
        return new Sync.Collections.Collection.Commands.Delete();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands.SoftDelete }
     * 
     */
    public Sync.Collections.Collection.Commands.SoftDelete createSyncCollectionsCollectionCommandsSoftDelete() {
        return new Sync.Collections.Collection.Commands.SoftDelete();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands.Add.ApplicationData }
     * 
     */
    public Sync.Collections.Collection.Commands.Add.ApplicationData createSyncCollectionsCollectionCommandsAddApplicationData() {
        return new Sync.Collections.Collection.Commands.Add.ApplicationData();
    }

    /**
     * Create an instance of {@link Sync.Collections.Collection.Commands.Change.ApplicationData }
     * 
     */
    public Sync.Collections.Collection.Commands.Change.ApplicationData createSyncCollectionsCollectionCommandsChangeApplicationData() {
        return new Sync.Collections.Collection.Commands.Change.ApplicationData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "SyncKey")
    public JAXBElement<String> createSyncKey(String value) {
        return new JAXBElement<String>(_SyncKey_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Class")
    public JAXBElement<String> createClass(String value) {
        return new JAXBElement<String>(_Class_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "FilterType")
    public JAXBElement<Short> createFilterType(Short value) {
        return new JAXBElement<Short>(_FilterType_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Conflict")
    public JAXBElement<Short> createConflict(Short value) {
        return new JAXBElement<Short>(_Conflict_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Partial")
    public JAXBElement<String> createPartial(String value) {
        return new JAXBElement<String>(_Partial_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "HeartbeatInterval")
    public JAXBElement<Integer> createHeartbeatInterval(Integer value) {
        return new JAXBElement<Integer>(_HeartbeatInterval_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "MIMESupport")
    public JAXBElement<Short> createMIMESupport(Short value) {
        return new JAXBElement<Short>(_MIMESupport_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "MaxItems")
    public JAXBElement<BigInteger> createMaxItems(BigInteger value) {
        return new JAXBElement<BigInteger>(_MaxItems_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "ConversationMode")
    public JAXBElement<Boolean> createConversationMode(Boolean value) {
        return new JAXBElement<Boolean>(_ConversationMode_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "GetChanges")
    public JAXBElement<Boolean> createGetChanges(Boolean value) {
        return new JAXBElement<Boolean>(_GetChanges_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "WindowSize")
    public JAXBElement<Integer> createWindowSize(Integer value) {
        return new JAXBElement<Integer>(_WindowSize_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Limit")
    public JAXBElement<BigInteger> createLimit(BigInteger value) {
        return new JAXBElement<BigInteger>(_Limit_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "ServerId")
    public JAXBElement<String> createServerId(String value) {
        return new JAXBElement<String>(_ServerId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "DeletesAsMoves")
    public JAXBElement<Boolean> createDeletesAsMoves(Boolean value) {
        return new JAXBElement<Boolean>(_DeletesAsMoves_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Wait")
    public JAXBElement<Integer> createWait(Integer value) {
        return new JAXBElement<Integer>(_Wait_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "ClientId")
    public JAXBElement<String> createClientId(String value) {
        return new JAXBElement<String>(_ClientId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "MoreAvailable")
    public JAXBElement<String> createMoreAvailable(String value) {
        return new JAXBElement<String>(_MoreAvailable_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "CollectionId")
    public JAXBElement<String> createCollectionId(String value) {
        return new JAXBElement<String>(_CollectionId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Truncation")
    public JAXBElement<Short> createTruncation(Short value) {
        return new JAXBElement<Short>(_Truncation_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "MIMETruncation")
    public JAXBElement<Short> createMIMETruncation(Short value) {
        return new JAXBElement<Short>(_MIMETruncation_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sync.Collections.Collection.Commands }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Commands", scope = Sync.Collections.Collection.class)
    public JAXBElement<Sync.Collections.Collection.Commands> createSyncCollectionsCollectionCommands(Sync.Collections.Collection.Commands value) {
        return new JAXBElement<Sync.Collections.Collection.Commands>(_SyncCollectionsCollectionCommands_QNAME, Sync.Collections.Collection.Commands.class, Sync.Collections.Collection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sync.Collections.Collection.Responses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSync", name = "Responses", scope = Sync.Collections.Collection.class)
    public JAXBElement<Sync.Collections.Collection.Responses> createSyncCollectionsCollectionResponses(Sync.Collections.Collection.Responses value) {
        return new JAXBElement<Sync.Collections.Collection.Responses>(_SyncCollectionsCollectionResponses_QNAME, Sync.Collections.Collection.Responses.class, Sync.Collections.Collection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Reminder", scope = Supported.class)
    public JAXBElement<String> createSupportedReminder(String value) {
        return new JAXBElement<String>(_SupportedReminder_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistnamePhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedAssistnamePhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedAssistnamePhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email1Address", scope = Supported.class)
    public JAXBElement<String> createSupportedEmail1Address(String value) {
        return new JAXBElement<String>(_SupportedEmail1Address_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Subject", scope = Supported.class)
    public JAXBElement<String> createSupportedSubject(String value) {
        return new JAXBElement<String>(_SupportedSubject_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OfficeLocation", scope = Supported.class)
    public JAXBElement<String> createSupportedOfficeLocation(String value) {
        return new JAXBElement<String>(_SupportedOfficeLocation_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email3Address", scope = Supported.class)
    public JAXBElement<String> createSupportedEmail3Address(String value) {
        return new JAXBElement<String>(_SupportedEmail3Address_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OrganizerEmail", scope = Supported.class)
    public JAXBElement<String> createSupportedOrganizerEmail(String value) {
        return new JAXBElement<String>(_SupportedOrganizerEmail_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "FirstName", scope = Supported.class)
    public JAXBElement<String> createSupportedFirstName(String value) {
        return new JAXBElement<String>(_SupportedFirstName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Sensitivity", scope = Supported.class)
    public JAXBElement<String> createSupportedSensitivity(String value) {
        return new JAXBElement<String>(_SupportedSensitivity_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressPostalCode", scope = Supported.class)
    public JAXBElement<String> createSupportedHomeAddressPostalCode(String value) {
        return new JAXBElement<String>(_SupportedHomeAddressPostalCode_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Location", scope = Supported.class)
    public JAXBElement<String> createSupportedLocation(String value) {
        return new JAXBElement<String>(_SupportedLocation_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "MiddleName", scope = Supported.class)
    public JAXBElement<String> createSupportedMiddleName(String value) {
        return new JAXBElement<String>(_SupportedMiddleName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "RadioPhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedRadioPhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedRadioPhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressState", scope = Supported.class)
    public JAXBElement<String> createSupportedOtherAddressState(String value) {
        return new JAXBElement<String>(_SupportedOtherAddressState_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Title", scope = Supported.class)
    public JAXBElement<String> createSupportedTitle(String value) {
        return new JAXBElement<String>(_SupportedTitle_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeFaxNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedHomeFaxNumber(String value) {
        return new JAXBElement<String>(_SupportedHomeFaxNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistantPhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedAssistantPhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedAssistantPhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "MeetingStatus", scope = Supported.class)
    public JAXBElement<String> createSupportedMeetingStatus(String value) {
        return new JAXBElement<String>(_SupportedMeetingStatus_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomePhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedHomePhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedHomePhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "EndTime", scope = Supported.class)
    public JAXBElement<String> createSupportedEndTime(String value) {
        return new JAXBElement<String>(_SupportedEndTime_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressPostalCode", scope = Supported.class)
    public JAXBElement<String> createSupportedOtherAddressPostalCode(String value) {
        return new JAXBElement<String>(_SupportedOtherAddressPostalCode_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiFirstName", scope = Supported.class)
    public JAXBElement<String> createSupportedYomiFirstName(String value) {
        return new JAXBElement<String>(_SupportedYomiFirstName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Children", scope = Supported.class)
    public JAXBElement<String> createSupportedChildren(String value) {
        return new JAXBElement<String>(_SupportedChildren_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiCompanyName", scope = Supported.class)
    public JAXBElement<String> createSupportedYomiCompanyName(String value) {
        return new JAXBElement<String>(_SupportedYomiCompanyName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistantName", scope = Supported.class)
    public JAXBElement<String> createSupportedAssistantName(String value) {
        return new JAXBElement<String>(_SupportedAssistantName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "WebPage", scope = Supported.class)
    public JAXBElement<String> createSupportedWebPage(String value) {
        return new JAXBElement<String>(_SupportedWebPage_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "MobilePhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedMobilePhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedMobilePhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "JobTitle", scope = Supported.class)
    public JAXBElement<String> createSupportedJobTitle(String value) {
        return new JAXBElement<String>(_SupportedJobTitle_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressCountry", scope = Supported.class)
    public JAXBElement<String> createSupportedOtherAddressCountry(String value) {
        return new JAXBElement<String>(_SupportedOtherAddressCountry_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressCity", scope = Supported.class)
    public JAXBElement<String> createSupportedOtherAddressCity(String value) {
        return new JAXBElement<String>(_SupportedOtherAddressCity_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Exceptions", scope = Supported.class)
    public JAXBElement<String> createSupportedExceptions(String value) {
        return new JAXBElement<String>(_SupportedExceptions_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Spouse", scope = Supported.class)
    public JAXBElement<String> createSupportedSpouse(String value) {
        return new JAXBElement<String>(_SupportedSpouse_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link Supported.CategoriesCalendar }}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Categories", scope = Supported.class)
    public Supported.CategoriesCalendar createSupportedCategoriesCalendar(String value) {
        return new Supported.CategoriesCalendar(value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "NickName", scope = Supported.class)
    public JAXBElement<String> createSupportedNickName(String value) {
        return new JAXBElement<String>(_SupportedNickName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressState", scope = Supported.class)
    public JAXBElement<String> createSupportedHomeAddressState(String value) {
        return new JAXBElement<String>(_SupportedHomeAddressState_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "CustomerId", scope = Supported.class)
    public JAXBElement<String> createSupportedCustomerId(String value) {
        return new JAXBElement<String>(_SupportedCustomerId_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Suffix", scope = Supported.class)
    public JAXBElement<String> createSupportedSuffix(String value) {
        return new JAXBElement<String>(_SupportedSuffix_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "CompanyMainPhone", scope = Supported.class)
    public JAXBElement<String> createSupportedCompanyMainPhone(String value) {
        return new JAXBElement<String>(_SupportedCompanyMainPhone_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "StartTime", scope = Supported.class)
    public JAXBElement<String> createSupportedStartTime(String value) {
        return new JAXBElement<String>(_SupportedStartTime_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email2Address", scope = Supported.class)
    public JAXBElement<String> createSupportedEmail2Address(String value) {
        return new JAXBElement<String>(_SupportedEmail2Address_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "LastName", scope = Supported.class)
    public JAXBElement<String> createSupportedLastName(String value) {
        return new JAXBElement<String>(_SupportedLastName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress3", scope = Supported.class)
    public JAXBElement<String> createSupportedIMAddress3(String value) {
        return new JAXBElement<String>(_SupportedIMAddress3_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "AccountName", scope = Supported.class)
    public JAXBElement<String> createSupportedAccountName(String value) {
        return new JAXBElement<String>(_SupportedAccountName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressCountry", scope = Supported.class)
    public JAXBElement<String> createSupportedHomeAddressCountry(String value) {
        return new JAXBElement<String>(_SupportedHomeAddressCountry_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Categories", scope = Supported.class)
    public JAXBElement<String> createSupportedCategories(String value) {
        return new JAXBElement<String>(_SupportedCategories_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress", scope = Supported.class)
    public JAXBElement<String> createSupportedIMAddress(String value) {
        return new JAXBElement<String>(_SupportedIMAddress_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress2", scope = Supported.class)
    public JAXBElement<String> createSupportedIMAddress2(String value) {
        return new JAXBElement<String>(_SupportedIMAddress2_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "ResponseRequested", scope = Supported.class)
    public JAXBElement<String> createSupportedResponseRequested(String value) {
        return new JAXBElement<String>(_SupportedResponseRequested_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "UID", scope = Supported.class)
    public JAXBElement<String> createSupportedUID(String value) {
        return new JAXBElement<String>(_SupportedUID_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "PagerNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedPagerNumber(String value) {
        return new JAXBElement<String>(_SupportedPagerNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Business2PhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedBusiness2PhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedBusiness2PhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressCountry", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessAddressCountry(String value) {
        return new JAXBElement<String>(_SupportedBusinessAddressCountry_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiLastName", scope = Supported.class)
    public JAXBElement<String> createSupportedYomiLastName(String value) {
        return new JAXBElement<String>(_SupportedYomiLastName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Anniversary", scope = Supported.class)
    public JAXBElement<String> createSupportedAnniversary(String value) {
        return new JAXBElement<String>(_SupportedAnniversary_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "GovernmentId", scope = Supported.class)
    public JAXBElement<String> createSupportedGovernmentId(String value) {
        return new JAXBElement<String>(_SupportedGovernmentId_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "MMS", scope = Supported.class)
    public JAXBElement<String> createSupportedMMS(String value) {
        return new JAXBElement<String>(_SupportedMMS_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "DisallowNewTimeProposal", scope = Supported.class)
    public JAXBElement<String> createSupportedDisallowNewTimeProposal(String value) {
        return new JAXBElement<String>(_SupportedDisallowNewTimeProposal_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressStreet", scope = Supported.class)
    public JAXBElement<String> createSupportedHomeAddressStreet(String value) {
        return new JAXBElement<String>(_SupportedHomeAddressStreet_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessPhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessPhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedBusinessPhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressStreet", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessAddressStreet(String value) {
        return new JAXBElement<String>(_SupportedBusinessAddressStreet_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressState", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessAddressState(String value) {
        return new JAXBElement<String>(_SupportedBusinessAddressState_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Picture", scope = Supported.class)
    public JAXBElement<String> createSupportedPicture(String value) {
        return new JAXBElement<String>(_SupportedPicture_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "CompanyName", scope = Supported.class)
    public JAXBElement<String> createSupportedCompanyName(String value) {
        return new JAXBElement<String>(_SupportedCompanyName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Recurrence", scope = Supported.class)
    public JAXBElement<String> createSupportedRecurrence(String value) {
        return new JAXBElement<String>(_SupportedRecurrence_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Department", scope = Supported.class)
    public JAXBElement<String> createSupportedDepartment(String value) {
        return new JAXBElement<String>(_SupportedDepartment_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressStreet", scope = Supported.class)
    public JAXBElement<String> createSupportedOtherAddressStreet(String value) {
        return new JAXBElement<String>(_SupportedOtherAddressStreet_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Timezone", scope = Supported.class)
    public JAXBElement<String> createSupportedTimezone(String value) {
        return new JAXBElement<String>(_SupportedTimezone_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "ManagerName", scope = Supported.class)
    public JAXBElement<String> createSupportedManagerName(String value) {
        return new JAXBElement<String>(_SupportedManagerName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressPostalCode", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessAddressPostalCode(String value) {
        return new JAXBElement<String>(_SupportedBusinessAddressPostalCode_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "CarPhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedCarPhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedCarPhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "BusyStatus", scope = Supported.class)
    public JAXBElement<String> createSupportedBusyStatus(String value) {
        return new JAXBElement<String>(_SupportedBusyStatus_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "AllDayEvent", scope = Supported.class)
    public JAXBElement<String> createSupportedAllDayEvent(String value) {
        return new JAXBElement<String>(_SupportedAllDayEvent_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Attendees", scope = Supported.class)
    public JAXBElement<String> createSupportedAttendees(String value) {
        return new JAXBElement<String>(_SupportedAttendees_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "DtStamp", scope = Supported.class)
    public JAXBElement<String> createSupportedDtStamp(String value) {
        return new JAXBElement<String>(_SupportedDtStamp_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "FileAs", scope = Supported.class)
    public JAXBElement<String> createSupportedFileAs(String value) {
        return new JAXBElement<String>(_SupportedFileAs_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Home2PhoneNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedHome2PhoneNumber(String value) {
        return new JAXBElement<String>(_SupportedHome2PhoneNumber_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressCity", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessAddressCity(String value) {
        return new JAXBElement<String>(_SupportedBusinessAddressCity_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Birthday", scope = Supported.class)
    public JAXBElement<String> createSupportedBirthday(String value) {
        return new JAXBElement<String>(_SupportedBirthday_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressCity", scope = Supported.class)
    public JAXBElement<String> createSupportedHomeAddressCity(String value) {
        return new JAXBElement<String>(_SupportedHomeAddressCity_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OrganizerName", scope = Supported.class)
    public JAXBElement<String> createSupportedOrganizerName(String value) {
        return new JAXBElement<String>(_SupportedOrganizerName_QNAME, String.class, Supported.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessFaxNumber", scope = Supported.class)
    public JAXBElement<String> createSupportedBusinessFaxNumber(String value) {
        return new JAXBElement<String>(_SupportedBusinessFaxNumber_QNAME, String.class, Supported.class, value);
    }

}
