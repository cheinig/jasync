//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.find;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.find package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Range_QNAME = new QName("Find", "Range");
    private final static QName _Status_QNAME = new QName("Find", "Status");
    private final static QName _FindResponseResultPropertiesPhone_QNAME = new QName("GAL", "Phone");
    private final static QName _FindResponseResultPropertiesAlias_QNAME = new QName("GAL", "Alias");
    private final static QName _FindResponseResultPropertiesPicture_QNAME = new QName("GAL", "Picture");
    private final static QName _FindResponseResultPropertiesHomePhone_QNAME = new QName("GAL", "HomePhone");
    private final static QName _FindResponseResultPropertiesLastName_QNAME = new QName("GAL", "LastName");
    private final static QName _FindResponseResultPropertiesDisplayName_QNAME = new QName("GAL", "DisplayName");
    private final static QName _FindResponseResultPropertiesDisplayCc_QNAME = new QName("Find", "DisplayCc");
    private final static QName _FindResponseResultPropertiesPreview_QNAME = new QName("Find", "Preview");
    private final static QName _FindResponseResultPropertiesCompany_QNAME = new QName("GAL", "Company");
    private final static QName _FindResponseResultPropertiesOffice_QNAME = new QName("GAL", "Office");
    private final static QName _FindResponseResultPropertiesFirstName_QNAME = new QName("GAL", "FirstName");
    private final static QName _FindResponseResultPropertiesDisplayBcc_QNAME = new QName("Find", "DisplayBcc");
    private final static QName _FindResponseResultPropertiesTitle_QNAME = new QName("GAL", "Title");
    private final static QName _FindResponseResultPropertiesMobilePhone_QNAME = new QName("GAL", "MobilePhone");
    private final static QName _FindResponseResultPropertiesEmailAddress_QNAME = new QName("GAL", "EmailAddress");
    private final static QName _FindResponseResultPropertiesHasAttachments_QNAME = new QName("Find", "HasAttachments");
    private final static QName _QueryTypeFreeText_QNAME = new QName("Find", "FreeText");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.find
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Find }
     * 
     */
    public Find createFind() {
        return new Find();
    }

    /**
     * Create an instance of {@link Find.Response }
     * 
     */
    public Find.Response createFindResponse() {
        return new Find.Response();
    }

    /**
     * Create an instance of {@link Find.Response.Result }
     * 
     */
    public Find.Response.Result createFindResponseResult() {
        return new Find.Response.Result();
    }

    /**
     * Create an instance of {@link EmptyTag }
     * 
     */
    public EmptyTag createEmptyTag() {
        return new EmptyTag();
    }

    /**
     * Create an instance of {@link QueryType }
     * 
     */
    public QueryType createQueryType() {
        return new QueryType();
    }

    /**
     * Create an instance of {@link Find.Response.Result.Properties }
     * 
     */
    public Find.Response.Result.Properties createFindResponseResultProperties() {
        return new Find.Response.Result.Properties();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "Range")
    public JAXBElement<String> createRange(String value) {
        return new JAXBElement<String>(_Range_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Phone", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesPhone(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesPhone_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Alias", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesAlias(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesAlias_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link de.chdev.jasync.xsd.response.search.Search.Response.Store.Result.Properties.Picture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Picture", scope = Find.Response.Result.Properties.class)
    public JAXBElement<de.chdev.jasync.xsd.response.search.Search.Response.Store.Result.Properties.Picture> createFindResponseResultPropertiesPicture(de.chdev.jasync.xsd.response.search.Search.Response.Store.Result.Properties.Picture value) {
        return new JAXBElement<de.chdev.jasync.xsd.response.search.Search.Response.Store.Result.Properties.Picture>(_FindResponseResultPropertiesPicture_QNAME, de.chdev.jasync.xsd.response.search.Search.Response.Store.Result.Properties.Picture.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "HomePhone", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesHomePhone(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesHomePhone_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "LastName", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesLastName(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesLastName_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "DisplayName", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesDisplayName(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesDisplayName_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "DisplayCc", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesDisplayCc(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesDisplayCc_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "Preview", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesPreview(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesPreview_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Company", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesCompany(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesCompany_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Office", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesOffice(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesOffice_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "FirstName", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesFirstName(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesFirstName_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "DisplayBcc", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesDisplayBcc(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesDisplayBcc_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Title", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesTitle(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesTitle_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "MobilePhone", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesMobilePhone(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesMobilePhone_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "EmailAddress", scope = Find.Response.Result.Properties.class)
    public JAXBElement<String> createFindResponseResultPropertiesEmailAddress(String value) {
        return new JAXBElement<String>(_FindResponseResultPropertiesEmailAddress_QNAME, String.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "HasAttachments", scope = Find.Response.Result.Properties.class)
    public JAXBElement<Boolean> createFindResponseResultPropertiesHasAttachments(Boolean value) {
        return new JAXBElement<Boolean>(_FindResponseResultPropertiesHasAttachments_QNAME, Boolean.class, Find.Response.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "FreeText", scope = QueryType.class)
    public JAXBElement<String> createQueryTypeFreeText(String value) {
        return new JAXBElement<String>(_QueryTypeFreeText_QNAME, String.class, QueryType.class, value);
    }

}
