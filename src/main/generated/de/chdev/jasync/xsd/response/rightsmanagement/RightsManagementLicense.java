//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.rightsmanagement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Owner" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ContentOwner" type="{RightsManagement}NonEmptyStringType"/>
 *         &lt;element name="ReplyAllAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EditAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ReplyAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ForwardAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ExportAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ModifyRecipientsAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element ref="{RightsManagement}TemplateID"/>
 *         &lt;element name="ExtractAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TemplateDescription" type="{RightsManagement}NonEmptyStringType"/>
 *         &lt;element name="ContentExpiryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TemplateName" type="{RightsManagement}NonEmptyStringType"/>
 *         &lt;element name="PrintAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ProgrammaticAccessAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "RightsManagementLicense")
public class RightsManagementLicense {

    @XmlElement(name = "Owner")
    protected boolean owner;
    @XmlElement(name = "ContentOwner", required = true)
    protected String contentOwner;
    @XmlElement(name = "ReplyAllAllowed")
    protected boolean replyAllAllowed;
    @XmlElement(name = "EditAllowed")
    protected boolean editAllowed;
    @XmlElement(name = "ReplyAllowed")
    protected boolean replyAllowed;
    @XmlElement(name = "ForwardAllowed")
    protected boolean forwardAllowed;
    @XmlElement(name = "ExportAllowed")
    protected boolean exportAllowed;
    @XmlElement(name = "ModifyRecipientsAllowed")
    protected boolean modifyRecipientsAllowed;
    @XmlElement(name = "TemplateID", required = true)
    protected String templateID;
    @XmlElement(name = "ExtractAllowed")
    protected boolean extractAllowed;
    @XmlElement(name = "TemplateDescription", required = true)
    protected String templateDescription;
    @XmlElement(name = "ContentExpiryDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar contentExpiryDate;
    @XmlElement(name = "TemplateName", required = true)
    protected String templateName;
    @XmlElement(name = "PrintAllowed")
    protected boolean printAllowed;
    @XmlElement(name = "ProgrammaticAccessAllowed")
    protected boolean programmaticAccessAllowed;

    /**
     * Ruft den Wert der owner-Eigenschaft ab.
     * 
     */
    public boolean isOwner() {
        return owner;
    }

    /**
     * Legt den Wert der owner-Eigenschaft fest.
     * 
     */
    public void setOwner(boolean value) {
        this.owner = value;
    }

    /**
     * Ruft den Wert der contentOwner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentOwner() {
        return contentOwner;
    }

    /**
     * Legt den Wert der contentOwner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentOwner(String value) {
        this.contentOwner = value;
    }

    /**
     * Ruft den Wert der replyAllAllowed-Eigenschaft ab.
     * 
     */
    public boolean isReplyAllAllowed() {
        return replyAllAllowed;
    }

    /**
     * Legt den Wert der replyAllAllowed-Eigenschaft fest.
     * 
     */
    public void setReplyAllAllowed(boolean value) {
        this.replyAllAllowed = value;
    }

    /**
     * Ruft den Wert der editAllowed-Eigenschaft ab.
     * 
     */
    public boolean isEditAllowed() {
        return editAllowed;
    }

    /**
     * Legt den Wert der editAllowed-Eigenschaft fest.
     * 
     */
    public void setEditAllowed(boolean value) {
        this.editAllowed = value;
    }

    /**
     * Ruft den Wert der replyAllowed-Eigenschaft ab.
     * 
     */
    public boolean isReplyAllowed() {
        return replyAllowed;
    }

    /**
     * Legt den Wert der replyAllowed-Eigenschaft fest.
     * 
     */
    public void setReplyAllowed(boolean value) {
        this.replyAllowed = value;
    }

    /**
     * Ruft den Wert der forwardAllowed-Eigenschaft ab.
     * 
     */
    public boolean isForwardAllowed() {
        return forwardAllowed;
    }

    /**
     * Legt den Wert der forwardAllowed-Eigenschaft fest.
     * 
     */
    public void setForwardAllowed(boolean value) {
        this.forwardAllowed = value;
    }

    /**
     * Ruft den Wert der exportAllowed-Eigenschaft ab.
     * 
     */
    public boolean isExportAllowed() {
        return exportAllowed;
    }

    /**
     * Legt den Wert der exportAllowed-Eigenschaft fest.
     * 
     */
    public void setExportAllowed(boolean value) {
        this.exportAllowed = value;
    }

    /**
     * Ruft den Wert der modifyRecipientsAllowed-Eigenschaft ab.
     * 
     */
    public boolean isModifyRecipientsAllowed() {
        return modifyRecipientsAllowed;
    }

    /**
     * Legt den Wert der modifyRecipientsAllowed-Eigenschaft fest.
     * 
     */
    public void setModifyRecipientsAllowed(boolean value) {
        this.modifyRecipientsAllowed = value;
    }

    /**
     * Ruft den Wert der templateID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateID() {
        return templateID;
    }

    /**
     * Legt den Wert der templateID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateID(String value) {
        this.templateID = value;
    }

    /**
     * Ruft den Wert der extractAllowed-Eigenschaft ab.
     * 
     */
    public boolean isExtractAllowed() {
        return extractAllowed;
    }

    /**
     * Legt den Wert der extractAllowed-Eigenschaft fest.
     * 
     */
    public void setExtractAllowed(boolean value) {
        this.extractAllowed = value;
    }

    /**
     * Ruft den Wert der templateDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateDescription() {
        return templateDescription;
    }

    /**
     * Legt den Wert der templateDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateDescription(String value) {
        this.templateDescription = value;
    }

    /**
     * Ruft den Wert der contentExpiryDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getContentExpiryDate() {
        return contentExpiryDate;
    }

    /**
     * Legt den Wert der contentExpiryDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setContentExpiryDate(XMLGregorianCalendar value) {
        this.contentExpiryDate = value;
    }

    /**
     * Ruft den Wert der templateName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Legt den Wert der templateName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateName(String value) {
        this.templateName = value;
    }

    /**
     * Ruft den Wert der printAllowed-Eigenschaft ab.
     * 
     */
    public boolean isPrintAllowed() {
        return printAllowed;
    }

    /**
     * Legt den Wert der printAllowed-Eigenschaft fest.
     * 
     */
    public void setPrintAllowed(boolean value) {
        this.printAllowed = value;
    }

    /**
     * Ruft den Wert der programmaticAccessAllowed-Eigenschaft ab.
     * 
     */
    public boolean isProgrammaticAccessAllowed() {
        return programmaticAccessAllowed;
    }

    /**
     * Legt den Wert der programmaticAccessAllowed-Eigenschaft fest.
     * 
     */
    public void setProgrammaticAccessAllowed(boolean value) {
        this.programmaticAccessAllowed = value;
    }

}
