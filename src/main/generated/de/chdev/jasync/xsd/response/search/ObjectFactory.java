//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.search;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.search package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Name_QNAME = new QName("Search", "Name");
    private final static QName _LongId_QNAME = new QName("Search", "LongId");
    private final static QName _Query_QNAME = new QName("Search", "Query");
    private final static QName _Status_QNAME = new QName("Search", "Status");
    private final static QName _Value_QNAME = new QName("Search", "Value");
    private final static QName _Range_QNAME = new QName("Search", "Range");
    private final static QName _Total_QNAME = new QName("Search", "Total");
    private final static QName _SearchResponseStoreResultPropertiesCompany_QNAME = new QName("GAL", "Company");
    private final static QName _SearchResponseStoreResultPropertiesOffice_QNAME = new QName("GAL", "Office");
    private final static QName _SearchResponseStoreResultPropertiesPhone_QNAME = new QName("GAL", "Phone");
    private final static QName _SearchResponseStoreResultPropertiesAlias_QNAME = new QName("GAL", "Alias");
    private final static QName _SearchResponseStoreResultPropertiesFirstName_QNAME = new QName("GAL", "FirstName");
    private final static QName _SearchResponseStoreResultPropertiesPicture_QNAME = new QName("GAL", "Picture");
    private final static QName _SearchResponseStoreResultPropertiesHomePhone_QNAME = new QName("GAL", "HomePhone");
    private final static QName _SearchResponseStoreResultPropertiesLastName_QNAME = new QName("GAL", "LastName");
    private final static QName _SearchResponseStoreResultPropertiesTitle_QNAME = new QName("GAL", "Title");
    private final static QName _SearchResponseStoreResultPropertiesDisplayName_QNAME = new QName("GAL", "DisplayName");
    private final static QName _SearchResponseStoreResultPropertiesMobilePhone_QNAME = new QName("GAL", "MobilePhone");
    private final static QName _SearchResponseStoreResultPropertiesEmailAddress_QNAME = new QName("GAL", "EmailAddress");
    private final static QName _QueryTypeAnd_QNAME = new QName("Search", "And");
    private final static QName _QueryTypeGreaterThan_QNAME = new QName("Search", "GreaterThan");
    private final static QName _QueryTypeOr_QNAME = new QName("Search", "Or");
    private final static QName _QueryTypeLessThan_QNAME = new QName("Search", "LessThan");
    private final static QName _QueryTypeConversationId_QNAME = new QName("Search", "ConversationId");
    private final static QName _QueryTypeFreeText_QNAME = new QName("Search", "FreeText");
    private final static QName _QueryTypeEqualTo_QNAME = new QName("Search", "EqualTo");
    private final static QName _OptionsPicture_QNAME = new QName("Search", "Picture");
    private final static QName _OptionsDeepTraversal_QNAME = new QName("Search", "DeepTraversal");
    private final static QName _OptionsUserName_QNAME = new QName("Search", "UserName");
    private final static QName _OptionsRebuildResults_QNAME = new QName("Search", "RebuildResults");
    private final static QName _OptionsPassword_QNAME = new QName("Search", "Password");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.search
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Options }
     * 
     */
    public Options createOptions() {
        return new Options();
    }

    /**
     * Create an instance of {@link Search }
     * 
     */
    public Search createSearch() {
        return new Search();
    }

    /**
     * Create an instance of {@link Search.Response }
     * 
     */
    public Search.Response createSearchResponse() {
        return new Search.Response();
    }

    /**
     * Create an instance of {@link Search.Response.Store }
     * 
     */
    public Search.Response.Store createSearchResponseStore() {
        return new Search.Response.Store();
    }

    /**
     * Create an instance of {@link Search.Response.Store.Result }
     * 
     */
    public Search.Response.Store.Result createSearchResponseStoreResult() {
        return new Search.Response.Store.Result();
    }

    /**
     * Create an instance of {@link Search.Response.Store.Result.Properties }
     * 
     */
    public Search.Response.Store.Result.Properties createSearchResponseStoreResultProperties() {
        return new Search.Response.Store.Result.Properties();
    }

    /**
     * Create an instance of {@link QueryType }
     * 
     */
    public QueryType createQueryType() {
        return new QueryType();
    }

    /**
     * Create an instance of {@link Options.Picture }
     * 
     */
    public Options.Picture createOptionsPicture() {
        return new Options.Picture();
    }

    /**
     * Create an instance of {@link Search.Response.Store.Result.Properties.Picture }
     * 
     */
    public Search.Response.Store.Result.Properties.Picture createSearchResponseStoreResultPropertiesPicture() {
        return new Search.Response.Store.Result.Properties.Picture();
    }

    /**
     * Create an instance of {@link QueryType.EqualTo }
     * 
     */
    public QueryType.EqualTo createQueryTypeEqualTo() {
        return new QueryType.EqualTo();
    }

    /**
     * Create an instance of {@link QueryType.GreaterThan }
     * 
     */
    public QueryType.GreaterThan createQueryTypeGreaterThan() {
        return new QueryType.GreaterThan();
    }

    /**
     * Create an instance of {@link QueryType.LessThan }
     * 
     */
    public QueryType.LessThan createQueryTypeLessThan() {
        return new QueryType.LessThan();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Name")
    public JAXBElement<String> createName(String value) {
        return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "LongId")
    public JAXBElement<String> createLongId(String value) {
        return new JAXBElement<String>(_LongId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Query")
    public JAXBElement<QueryType> createQuery(QueryType value) {
        return new JAXBElement<QueryType>(_Query_QNAME, QueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Value")
    public JAXBElement<String> createValue(String value) {
        return new JAXBElement<String>(_Value_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Range")
    public JAXBElement<String> createRange(String value) {
        return new JAXBElement<String>(_Range_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Total")
    public JAXBElement<BigInteger> createTotal(BigInteger value) {
        return new JAXBElement<BigInteger>(_Total_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Company", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesCompany(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesCompany_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Office", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesOffice(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesOffice_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Phone", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesPhone(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesPhone_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Alias", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesAlias(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesAlias_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "FirstName", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesFirstName(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesFirstName_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Search.Response.Store.Result.Properties.Picture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Picture", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<Search.Response.Store.Result.Properties.Picture> createSearchResponseStoreResultPropertiesPicture(Search.Response.Store.Result.Properties.Picture value) {
        return new JAXBElement<Search.Response.Store.Result.Properties.Picture>(_SearchResponseStoreResultPropertiesPicture_QNAME, Search.Response.Store.Result.Properties.Picture.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "HomePhone", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesHomePhone(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesHomePhone_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "LastName", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesLastName(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesLastName_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "Title", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesTitle(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesTitle_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "DisplayName", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesDisplayName(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesDisplayName_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "MobilePhone", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesMobilePhone(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesMobilePhone_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "GAL", name = "EmailAddress", scope = Search.Response.Store.Result.Properties.class)
    public JAXBElement<String> createSearchResponseStoreResultPropertiesEmailAddress(String value) {
        return new JAXBElement<String>(_SearchResponseStoreResultPropertiesEmailAddress_QNAME, String.class, Search.Response.Store.Result.Properties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "And", scope = QueryType.class)
    public JAXBElement<QueryType> createQueryTypeAnd(QueryType value) {
        return new JAXBElement<QueryType>(_QueryTypeAnd_QNAME, QueryType.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType.GreaterThan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "GreaterThan", scope = QueryType.class)
    public JAXBElement<QueryType.GreaterThan> createQueryTypeGreaterThan(QueryType.GreaterThan value) {
        return new JAXBElement<QueryType.GreaterThan>(_QueryTypeGreaterThan_QNAME, QueryType.GreaterThan.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Or", scope = QueryType.class)
    public JAXBElement<QueryType> createQueryTypeOr(QueryType value) {
        return new JAXBElement<QueryType>(_QueryTypeOr_QNAME, QueryType.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType.LessThan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "LessThan", scope = QueryType.class)
    public JAXBElement<QueryType.LessThan> createQueryTypeLessThan(QueryType.LessThan value) {
        return new JAXBElement<QueryType.LessThan>(_QueryTypeLessThan_QNAME, QueryType.LessThan.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "ConversationId", scope = QueryType.class)
    public JAXBElement<String> createQueryTypeConversationId(String value) {
        return new JAXBElement<String>(_QueryTypeConversationId_QNAME, String.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "FreeText", scope = QueryType.class)
    public JAXBElement<String> createQueryTypeFreeText(String value) {
        return new JAXBElement<String>(_QueryTypeFreeText_QNAME, String.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType.EqualTo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "EqualTo", scope = QueryType.class)
    public JAXBElement<QueryType.EqualTo> createQueryTypeEqualTo(QueryType.EqualTo value) {
        return new JAXBElement<QueryType.EqualTo>(_QueryTypeEqualTo_QNAME, QueryType.EqualTo.class, QueryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Options.Picture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Picture", scope = Options.class)
    public JAXBElement<Options.Picture> createOptionsPicture(Options.Picture value) {
        return new JAXBElement<Options.Picture>(_OptionsPicture_QNAME, Options.Picture.class, Options.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "DeepTraversal", scope = Options.class)
    public JAXBElement<String> createOptionsDeepTraversal(String value) {
        return new JAXBElement<String>(_OptionsDeepTraversal_QNAME, String.class, Options.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "UserName", scope = Options.class)
    public JAXBElement<String> createOptionsUserName(String value) {
        return new JAXBElement<String>(_OptionsUserName_QNAME, String.class, Options.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "RebuildResults", scope = Options.class)
    public JAXBElement<String> createOptionsRebuildResults(String value) {
        return new JAXBElement<String>(_OptionsRebuildResults_QNAME, String.class, Options.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Search", name = "Password", scope = Options.class)
    public JAXBElement<String> createOptionsPassword(String value) {
        return new JAXBElement<String>(_OptionsPassword_QNAME, String.class, Options.class, value);
    }

}
