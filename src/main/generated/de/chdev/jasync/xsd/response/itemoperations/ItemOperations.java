//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.itemoperations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{ItemOperations}Status"/>
 *         &lt;element name="Response" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Move" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element ref="{ItemOperations}Status"/>
 *                             &lt;element ref="{ItemOperations}ConversationId"/>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="EmptyFolderContents" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{ItemOperations}Status"/>
 *                             &lt;element ref="{AirSync}CollectionId"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{ItemOperations}Status"/>
 *                             &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
 *                             &lt;element ref="{Search}LongId" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                             &lt;element ref="{DocumentLibrary}LinkId" minOccurs="0"/>
 *                             &lt;element ref="{AirSyncBase}FileReference" minOccurs="0"/>
 *                             &lt;element ref="{ItemOperations}Properties" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "response"
})
@XmlRootElement(name = "ItemOperations")
public class ItemOperations {

    @XmlElement(name = "Status", required = true)
    protected BigInteger status;
    @XmlElement(name = "Response")
    protected ItemOperations.Response response;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der response-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ItemOperations.Response }
     *     
     */
    public ItemOperations.Response getResponse() {
        return response;
    }

    /**
     * Legt den Wert der response-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemOperations.Response }
     *     
     */
    public void setResponse(ItemOperations.Response value) {
        this.response = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Move" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element ref="{ItemOperations}Status"/>
     *                   &lt;element ref="{ItemOperations}ConversationId"/>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="EmptyFolderContents" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{ItemOperations}Status"/>
     *                   &lt;element ref="{AirSync}CollectionId"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{ItemOperations}Status"/>
     *                   &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
     *                   &lt;element ref="{Search}LongId" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                   &lt;element ref="{DocumentLibrary}LinkId" minOccurs="0"/>
     *                   &lt;element ref="{AirSyncBase}FileReference" minOccurs="0"/>
     *                   &lt;element ref="{ItemOperations}Properties" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "move",
        "emptyFolderContents",
        "fetch"
    })
    public static class Response {

        @XmlElement(name = "Move")
        protected List<ItemOperations.Response.Move> move;
        @XmlElement(name = "EmptyFolderContents")
        protected List<ItemOperations.Response.EmptyFolderContents> emptyFolderContents;
        @XmlElement(name = "Fetch")
        protected List<ItemOperations.Response.Fetch> fetch;

        /**
         * Gets the value of the move property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the move property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMove().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItemOperations.Response.Move }
         * 
         * 
         */
        public List<ItemOperations.Response.Move> getMove() {
            if (move == null) {
                move = new ArrayList<ItemOperations.Response.Move>();
            }
            return this.move;
        }

        /**
         * Gets the value of the emptyFolderContents property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the emptyFolderContents property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEmptyFolderContents().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItemOperations.Response.EmptyFolderContents }
         * 
         * 
         */
        public List<ItemOperations.Response.EmptyFolderContents> getEmptyFolderContents() {
            if (emptyFolderContents == null) {
                emptyFolderContents = new ArrayList<ItemOperations.Response.EmptyFolderContents>();
            }
            return this.emptyFolderContents;
        }

        /**
         * Gets the value of the fetch property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fetch property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFetch().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItemOperations.Response.Fetch }
         * 
         * 
         */
        public List<ItemOperations.Response.Fetch> getFetch() {
            if (fetch == null) {
                fetch = new ArrayList<ItemOperations.Response.Fetch>();
            }
            return this.fetch;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{ItemOperations}Status"/>
         *         &lt;element ref="{AirSync}CollectionId"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "status",
            "collectionId"
        })
        public static class EmptyFolderContents {

            @XmlElement(name = "Status", required = true)
            protected BigInteger status;
            @XmlElement(name = "CollectionId", namespace = "AirSync", required = true)
            protected String collectionId;

            /**
             * Ruft den Wert der status-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getStatus() {
                return status;
            }

            /**
             * Legt den Wert der status-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setStatus(BigInteger value) {
                this.status = value;
            }

            /**
             * Ruft den Wert der collectionId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCollectionId() {
                return collectionId;
            }

            /**
             * Legt den Wert der collectionId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCollectionId(String value) {
                this.collectionId = value;
            }

        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{ItemOperations}Status"/>
         *         &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
         *         &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
         *         &lt;element ref="{Search}LongId" minOccurs="0"/>
         *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *         &lt;element ref="{DocumentLibrary}LinkId" minOccurs="0"/>
         *         &lt;element ref="{AirSyncBase}FileReference" minOccurs="0"/>
         *         &lt;element ref="{ItemOperations}Properties" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "status",
            "collectionId",
            "serverId",
            "longId",
            "clazz",
            "linkId",
            "fileReference",
            "properties"
        })
        public static class Fetch {

            @XmlElement(name = "Status", required = true)
            protected BigInteger status;
            @XmlElement(name = "CollectionId", namespace = "AirSync")
            protected String collectionId;
            @XmlElement(name = "ServerId", namespace = "AirSync")
            protected String serverId;
            @XmlElement(name = "LongId", namespace = "Search")
            protected String longId;
            @XmlElement(name = "Class", namespace = "AirSync")
            protected String clazz;
            @XmlElement(name = "LinkId", namespace = "DocumentLibrary")
            protected String linkId;
            @XmlElement(name = "FileReference", namespace = "AirSyncBase")
            protected String fileReference;
            @XmlElement(name = "Properties")
            protected Properties properties;

            /**
             * Ruft den Wert der status-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getStatus() {
                return status;
            }

            /**
             * Legt den Wert der status-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setStatus(BigInteger value) {
                this.status = value;
            }

            /**
             * Ruft den Wert der collectionId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCollectionId() {
                return collectionId;
            }

            /**
             * Legt den Wert der collectionId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCollectionId(String value) {
                this.collectionId = value;
            }

            /**
             * Ruft den Wert der serverId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getServerId() {
                return serverId;
            }

            /**
             * Legt den Wert der serverId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setServerId(String value) {
                this.serverId = value;
            }

            /**
             * Ruft den Wert der longId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLongId() {
                return longId;
            }

            /**
             * Legt den Wert der longId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLongId(String value) {
                this.longId = value;
            }

            /**
             * Ruft den Wert der clazz-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClazz() {
                return clazz;
            }

            /**
             * Legt den Wert der clazz-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClazz(String value) {
                this.clazz = value;
            }

            /**
             * Ruft den Wert der linkId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLinkId() {
                return linkId;
            }

            /**
             * Legt den Wert der linkId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLinkId(String value) {
                this.linkId = value;
            }

            /**
             * Ruft den Wert der fileReference-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFileReference() {
                return fileReference;
            }

            /**
             * Legt den Wert der fileReference-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFileReference(String value) {
                this.fileReference = value;
            }

            /**
             * Ruft den Wert der properties-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Properties }
             *     
             */
            public Properties getProperties() {
                return properties;
            }

            /**
             * Legt den Wert der properties-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Properties }
             *     
             */
            public void setProperties(Properties value) {
                this.properties = value;
            }

        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element ref="{ItemOperations}Status"/>
         *         &lt;element ref="{ItemOperations}ConversationId"/>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class Move {

            @XmlElement(name = "Status", required = true)
            protected BigInteger status;
            @XmlElement(name = "ConversationId", required = true)
            protected String conversationId;

            /**
             * Ruft den Wert der status-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getStatus() {
                return status;
            }

            /**
             * Legt den Wert der status-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setStatus(BigInteger value) {
                this.status = value;
            }

            /**
             * Ruft den Wert der conversationId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getConversationId() {
                return conversationId;
            }

            /**
             * Legt den Wert der conversationId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setConversationId(String value) {
                this.conversationId = value;
            }

        }

    }

}
