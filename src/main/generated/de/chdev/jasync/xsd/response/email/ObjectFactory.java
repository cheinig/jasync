//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.email;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.email package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cc_QNAME = new QName("Email", "Cc");
    private final static QName _DisplayTo_QNAME = new QName("Email", "DisplayTo");
    private final static QName _ReplyTo_QNAME = new QName("Email", "ReplyTo");
    private final static QName _To_QNAME = new QName("Email", "To");
    private final static QName _ThreadTopic_QNAME = new QName("Email", "ThreadTopic");
    private final static QName _Subject_QNAME = new QName("Email", "Subject");
    private final static QName _DateReceived_QNAME = new QName("Email", "DateReceived");
    private final static QName _MessageClass_QNAME = new QName("Email", "MessageClass");
    private final static QName _MIMEData_QNAME = new QName("Email", "MIMEData");
    private final static QName _Read_QNAME = new QName("Email", "Read");
    private final static QName _BodySize_QNAME = new QName("Email", "BodySize");
    private final static QName _Body_QNAME = new QName("Email", "Body");
    private final static QName _From_QNAME = new QName("Email", "From");
    private final static QName _MIMESize_QNAME = new QName("Email", "MIMESize");
    private final static QName _MIMETruncated_QNAME = new QName("Email", "MIMETruncated");
    private final static QName _Importance_QNAME = new QName("Email", "Importance");
    private final static QName _BodyTruncated_QNAME = new QName("Email", "BodyTruncated");
    private final static QName _ContentClass_QNAME = new QName("Email", "ContentClass");
    private final static QName _InternetCPID_QNAME = new QName("Email", "InternetCPID");
    private final static QName _MeetingRequestReminder_QNAME = new QName("Email", "Reminder");
    private final static QName _MeetingRequestEndTime_QNAME = new QName("Email", "EndTime");
    private final static QName _MeetingRequestOrganizer_QNAME = new QName("Email", "Organizer");
    private final static QName _MeetingRequestBusyStatus_QNAME = new QName("Email", "BusyStatus");
    private final static QName _MeetingRequestAllDayEvent_QNAME = new QName("Email", "AllDayEvent");
    private final static QName _MeetingRequestStartTime_QNAME = new QName("Email", "StartTime");
    private final static QName _MeetingRequestRecurrences_QNAME = new QName("Email", "Recurrences");
    private final static QName _MeetingRequestDtStamp_QNAME = new QName("Email", "DtStamp");
    private final static QName _MeetingRequestSensitivity_QNAME = new QName("Email", "Sensitivity");
    private final static QName _MeetingRequestTimeZone_QNAME = new QName("Email", "TimeZone");
    private final static QName _MeetingRequestResponseRequested_QNAME = new QName("Email", "ResponseRequested");
    private final static QName _MeetingRequestRecurrenceId_QNAME = new QName("Email", "RecurrenceId");
    private final static QName _MeetingRequestDisallowNewTimeProposal_QNAME = new QName("Email", "DisallowNewTimeProposal");
    private final static QName _MeetingRequestGlobalObjId_QNAME = new QName("Email", "GlobalObjId");
    private final static QName _MeetingRequestLocation_QNAME = new QName("Email", "Location");
    private final static QName _MeetingRequestInstanceType_QNAME = new QName("Email", "InstanceType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.email
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MeetingRequest }
     * 
     */
    public MeetingRequest createMeetingRequest() {
        return new MeetingRequest();
    }

    /**
     * Create an instance of {@link Attachments }
     * 
     */
    public Attachments createAttachments() {
        return new Attachments();
    }

    /**
     * Create an instance of {@link MeetingRequest.Recurrences }
     * 
     */
    public MeetingRequest.Recurrences createMeetingRequestRecurrences() {
        return new MeetingRequest.Recurrences();
    }

    /**
     * Create an instance of {@link Flag }
     * 
     */
    public Flag createFlag() {
        return new Flag();
    }

    /**
     * Create an instance of {@link Categories }
     * 
     */
    public Categories createCategories() {
        return new Categories();
    }

    /**
     * Create an instance of {@link Attachments.Attachment }
     * 
     */
    public Attachments.Attachment createAttachmentsAttachment() {
        return new Attachments.Attachment();
    }

    /**
     * Create an instance of {@link MeetingRequest.Recurrences.Recurrence }
     * 
     */
    public MeetingRequest.Recurrences.Recurrence createMeetingRequestRecurrencesRecurrence() {
        return new MeetingRequest.Recurrences.Recurrence();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Cc")
    public JAXBElement<String> createCc(String value) {
        return new JAXBElement<String>(_Cc_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "DisplayTo")
    public JAXBElement<String> createDisplayTo(String value) {
        return new JAXBElement<String>(_DisplayTo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "ReplyTo")
    public JAXBElement<String> createReplyTo(String value) {
        return new JAXBElement<String>(_ReplyTo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "To")
    public JAXBElement<String> createTo(String value) {
        return new JAXBElement<String>(_To_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "ThreadTopic")
    public JAXBElement<String> createThreadTopic(String value) {
        return new JAXBElement<String>(_ThreadTopic_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Subject")
    public JAXBElement<String> createSubject(String value) {
        return new JAXBElement<String>(_Subject_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "DateReceived")
    public JAXBElement<XMLGregorianCalendar> createDateReceived(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateReceived_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "MessageClass")
    public JAXBElement<String> createMessageClass(String value) {
        return new JAXBElement<String>(_MessageClass_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "MIMEData")
    public JAXBElement<String> createMIMEData(String value) {
        return new JAXBElement<String>(_MIMEData_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Read")
    public JAXBElement<Boolean> createRead(Boolean value) {
        return new JAXBElement<Boolean>(_Read_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "BodySize")
    public JAXBElement<BigInteger> createBodySize(BigInteger value) {
        return new JAXBElement<BigInteger>(_BodySize_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Body")
    public JAXBElement<String> createBody(String value) {
        return new JAXBElement<String>(_Body_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "From")
    public JAXBElement<String> createFrom(String value) {
        return new JAXBElement<String>(_From_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "MIMESize")
    public JAXBElement<BigInteger> createMIMESize(BigInteger value) {
        return new JAXBElement<BigInteger>(_MIMESize_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "MIMETruncated")
    public JAXBElement<Boolean> createMIMETruncated(Boolean value) {
        return new JAXBElement<Boolean>(_MIMETruncated_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Importance")
    public JAXBElement<Short> createImportance(Short value) {
        return new JAXBElement<Short>(_Importance_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "BodyTruncated")
    public JAXBElement<Boolean> createBodyTruncated(Boolean value) {
        return new JAXBElement<Boolean>(_BodyTruncated_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "ContentClass")
    public JAXBElement<String> createContentClass(String value) {
        return new JAXBElement<String>(_ContentClass_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "InternetCPID")
    public JAXBElement<String> createInternetCPID(String value) {
        return new JAXBElement<String>(_InternetCPID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Reminder", scope = MeetingRequest.class)
    public JAXBElement<Integer> createMeetingRequestReminder(Integer value) {
        return new JAXBElement<Integer>(_MeetingRequestReminder_QNAME, Integer.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "EndTime", scope = MeetingRequest.class)
    public JAXBElement<XMLGregorianCalendar> createMeetingRequestEndTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MeetingRequestEndTime_QNAME, XMLGregorianCalendar.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Organizer", scope = MeetingRequest.class)
    public JAXBElement<String> createMeetingRequestOrganizer(String value) {
        return new JAXBElement<String>(_MeetingRequestOrganizer_QNAME, String.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "BusyStatus", scope = MeetingRequest.class)
    public JAXBElement<BigInteger> createMeetingRequestBusyStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_MeetingRequestBusyStatus_QNAME, BigInteger.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "AllDayEvent", scope = MeetingRequest.class)
    public JAXBElement<Short> createMeetingRequestAllDayEvent(Short value) {
        return new JAXBElement<Short>(_MeetingRequestAllDayEvent_QNAME, Short.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "StartTime", scope = MeetingRequest.class)
    public JAXBElement<XMLGregorianCalendar> createMeetingRequestStartTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MeetingRequestStartTime_QNAME, XMLGregorianCalendar.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeetingRequest.Recurrences }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Recurrences", scope = MeetingRequest.class)
    public JAXBElement<MeetingRequest.Recurrences> createMeetingRequestRecurrences(MeetingRequest.Recurrences value) {
        return new JAXBElement<MeetingRequest.Recurrences>(_MeetingRequestRecurrences_QNAME, MeetingRequest.Recurrences.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "DtStamp", scope = MeetingRequest.class)
    public JAXBElement<XMLGregorianCalendar> createMeetingRequestDtStamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MeetingRequestDtStamp_QNAME, XMLGregorianCalendar.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Sensitivity", scope = MeetingRequest.class)
    public JAXBElement<BigInteger> createMeetingRequestSensitivity(BigInteger value) {
        return new JAXBElement<BigInteger>(_MeetingRequestSensitivity_QNAME, BigInteger.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "TimeZone", scope = MeetingRequest.class)
    public JAXBElement<String> createMeetingRequestTimeZone(String value) {
        return new JAXBElement<String>(_MeetingRequestTimeZone_QNAME, String.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "ResponseRequested", scope = MeetingRequest.class)
    public JAXBElement<Short> createMeetingRequestResponseRequested(Short value) {
        return new JAXBElement<Short>(_MeetingRequestResponseRequested_QNAME, Short.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "RecurrenceId", scope = MeetingRequest.class)
    public JAXBElement<XMLGregorianCalendar> createMeetingRequestRecurrenceId(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MeetingRequestRecurrenceId_QNAME, XMLGregorianCalendar.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "DisallowNewTimeProposal", scope = MeetingRequest.class)
    public JAXBElement<Short> createMeetingRequestDisallowNewTimeProposal(Short value) {
        return new JAXBElement<Short>(_MeetingRequestDisallowNewTimeProposal_QNAME, Short.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "GlobalObjId", scope = MeetingRequest.class)
    public JAXBElement<String> createMeetingRequestGlobalObjId(String value) {
        return new JAXBElement<String>(_MeetingRequestGlobalObjId_QNAME, String.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Location", scope = MeetingRequest.class)
    public JAXBElement<String> createMeetingRequestLocation(String value) {
        return new JAXBElement<String>(_MeetingRequestLocation_QNAME, String.class, MeetingRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "InstanceType", scope = MeetingRequest.class)
    public JAXBElement<Short> createMeetingRequestInstanceType(Short value) {
        return new JAXBElement<Short>(_MeetingRequestInstanceType_QNAME, Short.class, MeetingRequest.class, value);
    }

}
