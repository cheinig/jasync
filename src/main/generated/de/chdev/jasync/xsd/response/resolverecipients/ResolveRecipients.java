//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.resolverecipients;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="Response" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="To">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="256"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="RecipientCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="Recipient" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                             &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Availability" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="MergedFreeBusy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Certificates" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="CertificateCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                       &lt;element name="RecipientCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                       &lt;element name="Certificate" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                                       &lt;element name="MiniCertificate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Picture" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;all>
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/all>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "response"
})
@XmlRootElement(name = "ResolveRecipients")
public class ResolveRecipients {

    @XmlElement(name = "Status", required = true)
    protected BigInteger status;
    @XmlElement(name = "Response")
    protected List<ResolveRecipients.Response> response;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the response property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResolveRecipients.Response }
     * 
     * 
     */
    public List<ResolveRecipients.Response> getResponse() {
        if (response == null) {
            response = new ArrayList<ResolveRecipients.Response>();
        }
        return this.response;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="To">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="256"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="RecipientCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="Recipient" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                   &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Availability" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="MergedFreeBusy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Certificates" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="CertificateCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                             &lt;element name="RecipientCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                             &lt;element name="Certificate" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *                             &lt;element name="MiniCertificate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Picture" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;all>
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/all>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "to",
        "status",
        "recipientCount",
        "recipient"
    })
    public static class Response {

        @XmlElement(name = "To", required = true)
        protected String to;
        @XmlElement(name = "Status", required = true)
        protected BigInteger status;
        @XmlElement(name = "RecipientCount")
        protected BigInteger recipientCount;
        @XmlElement(name = "Recipient")
        protected List<ResolveRecipients.Response.Recipient> recipient;

        /**
         * Ruft den Wert der to-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTo() {
            return to;
        }

        /**
         * Legt den Wert der to-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTo(String value) {
            this.to = value;
        }

        /**
         * Ruft den Wert der status-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getStatus() {
            return status;
        }

        /**
         * Legt den Wert der status-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setStatus(BigInteger value) {
            this.status = value;
        }

        /**
         * Ruft den Wert der recipientCount-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRecipientCount() {
            return recipientCount;
        }

        /**
         * Legt den Wert der recipientCount-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRecipientCount(BigInteger value) {
            this.recipientCount = value;
        }

        /**
         * Gets the value of the recipient property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the recipient property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRecipient().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResolveRecipients.Response.Recipient }
         * 
         * 
         */
        public List<ResolveRecipients.Response.Recipient> getRecipient() {
            if (recipient == null) {
                recipient = new ArrayList<ResolveRecipients.Response.Recipient>();
            }
            return this.recipient;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *         &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Availability" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="MergedFreeBusy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Certificates" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="CertificateCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                   &lt;element name="RecipientCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                   &lt;element name="Certificate" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
         *                   &lt;element name="MiniCertificate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Picture" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;all>
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/all>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "displayName",
            "emailAddress",
            "availability",
            "certificates",
            "picture"
        })
        public static class Recipient {

            @XmlElement(name = "Type")
            @XmlSchemaType(name = "unsignedByte")
            protected short type;
            @XmlElement(name = "DisplayName", required = true)
            protected String displayName;
            @XmlElement(name = "EmailAddress", required = true)
            protected String emailAddress;
            @XmlElement(name = "Availability")
            protected ResolveRecipients.Response.Recipient.Availability availability;
            @XmlElement(name = "Certificates")
            protected ResolveRecipients.Response.Recipient.Certificates certificates;
            @XmlElement(name = "Picture")
            protected List<ResolveRecipients.Response.Recipient.Picture> picture;

            /**
             * Ruft den Wert der type-Eigenschaft ab.
             * 
             */
            public short getType() {
                return type;
            }

            /**
             * Legt den Wert der type-Eigenschaft fest.
             * 
             */
            public void setType(short value) {
                this.type = value;
            }

            /**
             * Ruft den Wert der displayName-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDisplayName() {
                return displayName;
            }

            /**
             * Legt den Wert der displayName-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDisplayName(String value) {
                this.displayName = value;
            }

            /**
             * Ruft den Wert der emailAddress-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmailAddress() {
                return emailAddress;
            }

            /**
             * Legt den Wert der emailAddress-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmailAddress(String value) {
                this.emailAddress = value;
            }

            /**
             * Ruft den Wert der availability-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link ResolveRecipients.Response.Recipient.Availability }
             *     
             */
            public ResolveRecipients.Response.Recipient.Availability getAvailability() {
                return availability;
            }

            /**
             * Legt den Wert der availability-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link ResolveRecipients.Response.Recipient.Availability }
             *     
             */
            public void setAvailability(ResolveRecipients.Response.Recipient.Availability value) {
                this.availability = value;
            }

            /**
             * Ruft den Wert der certificates-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link ResolveRecipients.Response.Recipient.Certificates }
             *     
             */
            public ResolveRecipients.Response.Recipient.Certificates getCertificates() {
                return certificates;
            }

            /**
             * Legt den Wert der certificates-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link ResolveRecipients.Response.Recipient.Certificates }
             *     
             */
            public void setCertificates(ResolveRecipients.Response.Recipient.Certificates value) {
                this.certificates = value;
            }

            /**
             * Gets the value of the picture property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the picture property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPicture().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ResolveRecipients.Response.Recipient.Picture }
             * 
             * 
             */
            public List<ResolveRecipients.Response.Recipient.Picture> getPicture() {
                if (picture == null) {
                    picture = new ArrayList<ResolveRecipients.Response.Recipient.Picture>();
                }
                return this.picture;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="MergedFreeBusy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "status",
                "mergedFreeBusy"
            })
            public static class Availability {

                @XmlElement(name = "Status", required = true)
                protected BigInteger status;
                @XmlElement(name = "MergedFreeBusy")
                protected String mergedFreeBusy;

                /**
                 * Ruft den Wert der status-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getStatus() {
                    return status;
                }

                /**
                 * Legt den Wert der status-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setStatus(BigInteger value) {
                    this.status = value;
                }

                /**
                 * Ruft den Wert der mergedFreeBusy-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMergedFreeBusy() {
                    return mergedFreeBusy;
                }

                /**
                 * Legt den Wert der mergedFreeBusy-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMergedFreeBusy(String value) {
                    this.mergedFreeBusy = value;
                }

            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="CertificateCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *         &lt;element name="RecipientCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *         &lt;element name="Certificate" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
             *         &lt;element name="MiniCertificate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "status",
                "certificateCount",
                "recipientCount",
                "certificate",
                "miniCertificate"
            })
            public static class Certificates {

                @XmlElement(name = "Status", required = true)
                protected BigInteger status;
                @XmlElement(name = "CertificateCount")
                protected BigInteger certificateCount;
                @XmlElement(name = "RecipientCount")
                protected BigInteger recipientCount;
                @XmlElement(name = "Certificate")
                protected List<String> certificate;
                @XmlElement(name = "MiniCertificate")
                protected String miniCertificate;

                /**
                 * Ruft den Wert der status-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getStatus() {
                    return status;
                }

                /**
                 * Legt den Wert der status-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setStatus(BigInteger value) {
                    this.status = value;
                }

                /**
                 * Ruft den Wert der certificateCount-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCertificateCount() {
                    return certificateCount;
                }

                /**
                 * Legt den Wert der certificateCount-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCertificateCount(BigInteger value) {
                    this.certificateCount = value;
                }

                /**
                 * Ruft den Wert der recipientCount-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getRecipientCount() {
                    return recipientCount;
                }

                /**
                 * Legt den Wert der recipientCount-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setRecipientCount(BigInteger value) {
                    this.recipientCount = value;
                }

                /**
                 * Gets the value of the certificate property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the certificate property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getCertificate().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getCertificate() {
                    if (certificate == null) {
                        certificate = new ArrayList<String>();
                    }
                    return this.certificate;
                }

                /**
                 * Ruft den Wert der miniCertificate-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMiniCertificate() {
                    return miniCertificate;
                }

                /**
                 * Legt den Wert der miniCertificate-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMiniCertificate(String value) {
                    this.miniCertificate = value;
                }

            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;all>
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/all>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {

            })
            public static class Picture {

                @XmlElement(name = "Status", required = true)
                protected BigInteger status;
                @XmlElement(name = "Data")
                protected String data;

                /**
                 * Ruft den Wert der status-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getStatus() {
                    return status;
                }

                /**
                 * Legt den Wert der status-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setStatus(BigInteger value) {
                    this.status = value;
                }

                /**
                 * Ruft den Wert der data-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getData() {
                    return data;
                }

                /**
                 * Legt den Wert der data-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setData(String value) {
                    this.data = value;
                }

            }

        }

    }

}
