//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.composemail;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.composemail package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SaveInSentItems_QNAME = new QName("ComposeMail", "SaveInSentItems");
    private final static QName _ReplaceMime_QNAME = new QName("ComposeMail", "ReplaceMime");
    private final static QName _ClientId_QNAME = new QName("ComposeMail", "ClientId");
    private final static QName _Mime_QNAME = new QName("ComposeMail", "Mime");
    private final static QName _Status_QNAME = new QName("ComposeMail", "Status");
    private final static QName _AccountId_QNAME = new QName("ComposeMail", "AccountId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.composemail
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Forwardees }
     * 
     */
    public Forwardees createForwardees() {
        return new Forwardees();
    }

    /**
     * Create an instance of {@link SmartReply }
     * 
     */
    public SmartReply createSmartReply() {
        return new SmartReply();
    }

    /**
     * Create an instance of {@link Forwardees.Forwardee }
     * 
     */
    public Forwardees.Forwardee createForwardeesForwardee() {
        return new Forwardees.Forwardee();
    }

    /**
     * Create an instance of {@link SmartForward }
     * 
     */
    public SmartForward createSmartForward() {
        return new SmartForward();
    }

    /**
     * Create an instance of {@link Source }
     * 
     */
    public Source createSource() {
        return new Source();
    }

    /**
     * Create an instance of {@link SendMail }
     * 
     */
    public SendMail createSendMail() {
        return new SendMail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ComposeMail", name = "SaveInSentItems")
    public JAXBElement<String> createSaveInSentItems(String value) {
        return new JAXBElement<String>(_SaveInSentItems_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ComposeMail", name = "ReplaceMime")
    public JAXBElement<String> createReplaceMime(String value) {
        return new JAXBElement<String>(_ReplaceMime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ComposeMail", name = "ClientId")
    public JAXBElement<String> createClientId(String value) {
        return new JAXBElement<String>(_ClientId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ComposeMail", name = "Mime")
    public JAXBElement<String> createMime(String value) {
        return new JAXBElement<String>(_Mime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ComposeMail", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ComposeMail", name = "AccountId")
    public JAXBElement<String> createAccountId(String value) {
        return new JAXBElement<String>(_AccountId_QNAME, String.class, null, value);
    }

}
