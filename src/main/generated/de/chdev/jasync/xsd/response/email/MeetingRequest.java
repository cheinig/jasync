//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.email;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import de.chdev.jasync.xsd.response.airsyncbase.Location;
import de.chdev.jasync.xsd.response.composemail.Forwardees;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllDayEvent" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DtStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InstanceType" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="Location" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32768"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
 *         &lt;element name="Organizer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecurrenceId" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Reminder" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="ResponseRequested" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="Recurrences" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Recurrence">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                             &lt;element name="Interval" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="Until" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                             &lt;element name="Occurrences" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="WeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="DayOfMonth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="DayOfWeek" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="MonthOfYear" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element ref="{Email2}CalendarType" minOccurs="0"/>
 *                             &lt;element ref="{Email2}IsLeapMonth" minOccurs="0"/>
 *                             &lt;element ref="{Email2}FirstDayOfWeek" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Sensitivity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="BusyStatus" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="TimeZone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GlobalObjId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisallowNewTimeProposal" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element ref="{Email2}MeetingMessageType"/>
 *         &lt;element ref="{Calendar}UID" minOccurs="0"/>
 *         &lt;element ref="{MeetingResponse}ProposedStartTime" minOccurs="0"/>
 *         &lt;element ref="{MeetingResponse}ProposedEndTime" minOccurs="0"/>
 *         &lt;element ref="{ComposeMail}Forwardees" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "MeetingRequest")
public class MeetingRequest {

    @XmlElementRefs({
        @XmlElementRef(name = "DtStamp", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "GlobalObjId", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AllDayEvent", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusyStatus", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "TimeZone", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "EndTime", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Location", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ProposedStartTime", namespace = "MeetingResponse", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
        @XmlElementRef(name = "Organizer", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Sensitivity", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Forwardees", namespace = "ComposeMail", type = Forwardees.class, required = false),
        @XmlElementRef(name = "InstanceType", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ProposedEndTime", namespace = "MeetingResponse", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RecurrenceId", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ResponseRequested", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Reminder", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "StartTime", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MeetingMessageType", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Recurrences", namespace = "Email", type = JAXBElement.class, required = false)
    })
    protected List<Object> content;

    /**
     * Ruft das restliche Contentmodell ab. 
     * 
     * <p>
     * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
     * Der Feldname "Location" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
     * Zeile 47 von file:/Users/cheinig/Documents/Entwicklung/idea/Privat/jasync/src/main/resources/xsd/Email.xsd
     * Zeile 40 von file:/Users/cheinig/Documents/Entwicklung/idea/Privat/jasync/src/main/resources/xsd/Email.xsd
     * <p>
     * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung für eine
     * der beiden folgenden Deklarationen an, um deren Namen zu ändern: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Location }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link Forwardees }
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link MeetingRequest.Recurrences }{@code >}
     * 
     * 
     */
    public List<Object> getContent() {
        if (content == null) {
            content = new ArrayList<Object>();
        }
        return this.content;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Recurrence">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                   &lt;element name="Interval" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="Until" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *                   &lt;element name="Occurrences" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="WeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="DayOfMonth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="DayOfWeek" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="MonthOfYear" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element ref="{Email2}CalendarType" minOccurs="0"/>
     *                   &lt;element ref="{Email2}IsLeapMonth" minOccurs="0"/>
     *                   &lt;element ref="{Email2}FirstDayOfWeek" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recurrence"
    })
    public static class Recurrences {

        @XmlElement(name = "Recurrence", required = true)
        protected MeetingRequest.Recurrences.Recurrence recurrence;

        /**
         * Ruft den Wert der recurrence-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link MeetingRequest.Recurrences.Recurrence }
         *     
         */
        public MeetingRequest.Recurrences.Recurrence getRecurrence() {
            return recurrence;
        }

        /**
         * Legt den Wert der recurrence-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link MeetingRequest.Recurrences.Recurrence }
         *     
         */
        public void setRecurrence(MeetingRequest.Recurrences.Recurrence value) {
            this.recurrence = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *         &lt;element name="Interval" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="Until" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
         *         &lt;element name="Occurrences" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="WeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="DayOfMonth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="DayOfWeek" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="MonthOfYear" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element ref="{Email2}CalendarType" minOccurs="0"/>
         *         &lt;element ref="{Email2}IsLeapMonth" minOccurs="0"/>
         *         &lt;element ref="{Email2}FirstDayOfWeek" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "interval",
            "until",
            "occurrences",
            "weekOfMonth",
            "dayOfMonth",
            "dayOfWeek",
            "monthOfYear",
            "calendarType",
            "isLeapMonth",
            "firstDayOfWeek"
        })
        public static class Recurrence {

            @XmlElement(name = "Type")
            @XmlSchemaType(name = "unsignedByte")
            protected short type;
            @XmlElement(name = "Interval", required = true)
            protected BigInteger interval;
            @XmlElement(name = "Until")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar until;
            @XmlElement(name = "Occurrences")
            protected BigInteger occurrences;
            @XmlElement(name = "WeekOfMonth")
            protected BigInteger weekOfMonth;
            @XmlElement(name = "DayOfMonth")
            protected BigInteger dayOfMonth;
            @XmlElement(name = "DayOfWeek")
            protected BigInteger dayOfWeek;
            @XmlElement(name = "MonthOfYear")
            protected BigInteger monthOfYear;
            @XmlElement(name = "CalendarType", namespace = "Email2")
            protected BigInteger calendarType;
            @XmlElement(name = "IsLeapMonth", namespace = "Email2")
            @XmlSchemaType(name = "unsignedByte")
            protected Short isLeapMonth;
            @XmlElement(name = "FirstDayOfWeek", namespace = "Email2")
            @XmlSchemaType(name = "unsignedByte")
            protected Short firstDayOfWeek;

            /**
             * Ruft den Wert der type-Eigenschaft ab.
             * 
             */
            public short getType() {
                return type;
            }

            /**
             * Legt den Wert der type-Eigenschaft fest.
             * 
             */
            public void setType(short value) {
                this.type = value;
            }

            /**
             * Ruft den Wert der interval-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getInterval() {
                return interval;
            }

            /**
             * Legt den Wert der interval-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setInterval(BigInteger value) {
                this.interval = value;
            }

            /**
             * Ruft den Wert der until-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getUntil() {
                return until;
            }

            /**
             * Legt den Wert der until-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setUntil(XMLGregorianCalendar value) {
                this.until = value;
            }

            /**
             * Ruft den Wert der occurrences-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getOccurrences() {
                return occurrences;
            }

            /**
             * Legt den Wert der occurrences-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setOccurrences(BigInteger value) {
                this.occurrences = value;
            }

            /**
             * Ruft den Wert der weekOfMonth-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getWeekOfMonth() {
                return weekOfMonth;
            }

            /**
             * Legt den Wert der weekOfMonth-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setWeekOfMonth(BigInteger value) {
                this.weekOfMonth = value;
            }

            /**
             * Ruft den Wert der dayOfMonth-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDayOfMonth() {
                return dayOfMonth;
            }

            /**
             * Legt den Wert der dayOfMonth-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDayOfMonth(BigInteger value) {
                this.dayOfMonth = value;
            }

            /**
             * Ruft den Wert der dayOfWeek-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDayOfWeek() {
                return dayOfWeek;
            }

            /**
             * Legt den Wert der dayOfWeek-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDayOfWeek(BigInteger value) {
                this.dayOfWeek = value;
            }

            /**
             * Ruft den Wert der monthOfYear-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMonthOfYear() {
                return monthOfYear;
            }

            /**
             * Legt den Wert der monthOfYear-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMonthOfYear(BigInteger value) {
                this.monthOfYear = value;
            }

            /**
             * Ruft den Wert der calendarType-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCalendarType() {
                return calendarType;
            }

            /**
             * Legt den Wert der calendarType-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCalendarType(BigInteger value) {
                this.calendarType = value;
            }

            /**
             * Ruft den Wert der isLeapMonth-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Short }
             *     
             */
            public Short getIsLeapMonth() {
                return isLeapMonth;
            }

            /**
             * Legt den Wert der isLeapMonth-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Short }
             *     
             */
            public void setIsLeapMonth(Short value) {
                this.isLeapMonth = value;
            }

            /**
             * Ruft den Wert der firstDayOfWeek-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Short }
             *     
             */
            public Short getFirstDayOfWeek() {
                return firstDayOfWeek;
            }

            /**
             * Legt den Wert der firstDayOfWeek-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Short }
             *     
             */
            public void setFirstDayOfWeek(Short value) {
                this.firstDayOfWeek = value;
            }

        }

    }

}
