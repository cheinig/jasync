//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.itemoperations;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import de.chdev.jasync.xsd.response.airsyncbase.Location;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
 *         &lt;group ref="{Email}TopLevelSchemaProps"/>
 *         &lt;group ref="{AirSyncBase}TopLevelSchemaProps"/>
 *         &lt;group ref="{Calendar}TopLevelSchemaProps"/>
 *         &lt;group ref="{Contacts}TopLevelSchemaProps"/>
 *         &lt;group ref="{Contacts2}TopLevelSchemaProps"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "locationOrToOrCc"
})
@XmlRootElement(name = "Schema")
public class Schema {

    @XmlElementRefs({
        @XmlElementRef(name = "BusinessTelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MessageClass", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Webpage", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MeetingRequest", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
        @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyPart", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categories", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeTelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MobileTelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CarTelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AssistantTelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Attendees", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ThreadTopic", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Attachments", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categories", namespace = "Calendar", type = Schema.CategoriesCalendar.class, required = false),
        @XmlElementRef(name = "Business2TelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Home2TelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Subject", namespace = "Calendar", type = Schema.SubjectCalendar.class, required = false),
        @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RadioTelephoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Children", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false)
    })
    protected List<Object> locationOrToOrCc;

    /**
     * Gets the value of the locationOrToOrCc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationOrToOrCc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationOrToOrCc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Location }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Schema.CategoriesCalendar }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Schema.SubjectCalendar }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<Object> getLocationOrToOrCc() {
        if (locationOrToOrCc == null) {
            locationOrToOrCc = new ArrayList<Object>();
        }
        return this.locationOrToOrCc;
    }

    public static class CategoriesCalendar
        extends JAXBElement<String>
    {

        protected final static QName NAME = new QName("Calendar", "Categories");

        public CategoriesCalendar(String value) {
            super(NAME, ((Class) String.class), Schema.class, value);
        }

        public CategoriesCalendar() {
            super(NAME, ((Class) String.class), Schema.class, null);
        }

    }

    public static class SubjectCalendar
        extends JAXBElement<String>
    {

        protected final static QName NAME = new QName("Calendar", "Subject");

        public SubjectCalendar(String value) {
            super(NAME, ((Class) String.class), Schema.class, value);
        }

        public SubjectCalendar() {
            super(NAME, ((Class) String.class), Schema.class, null);
        }

    }

}
