//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.documentlibrary;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.documentlibrary package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IsFolder_QNAME = new QName("DocumentLibrary", "IsFolder");
    private final static QName _LastModifiedDate_QNAME = new QName("DocumentLibrary", "LastModifiedDate");
    private final static QName _IsHidden_QNAME = new QName("DocumentLibrary", "IsHidden");
    private final static QName _CreationDate_QNAME = new QName("DocumentLibrary", "CreationDate");
    private final static QName _ContentType_QNAME = new QName("DocumentLibrary", "ContentType");
    private final static QName _LinkId_QNAME = new QName("DocumentLibrary", "LinkId");
    private final static QName _ContentLength_QNAME = new QName("DocumentLibrary", "ContentLength");
    private final static QName _DisplayName_QNAME = new QName("DocumentLibrary", "DisplayName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.documentlibrary
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "IsFolder")
    public JAXBElement<Short> createIsFolder(Short value) {
        return new JAXBElement<Short>(_IsFolder_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "LastModifiedDate")
    public JAXBElement<XMLGregorianCalendar> createLastModifiedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_LastModifiedDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "IsHidden")
    public JAXBElement<Short> createIsHidden(Short value) {
        return new JAXBElement<Short>(_IsHidden_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "CreationDate")
    public JAXBElement<XMLGregorianCalendar> createCreationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CreationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "ContentType")
    public JAXBElement<String> createContentType(String value) {
        return new JAXBElement<String>(_ContentType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "LinkId")
    public JAXBElement<String> createLinkId(String value) {
        return new JAXBElement<String>(_LinkId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "ContentLength")
    public JAXBElement<BigInteger> createContentLength(BigInteger value) {
        return new JAXBElement<BigInteger>(_ContentLength_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "DocumentLibrary", name = "DisplayName")
    public JAXBElement<String> createDisplayName(String value) {
        return new JAXBElement<String>(_DisplayName_QNAME, String.class, null, value);
    }

}
