//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.airsyncbase;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Attachment">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element ref="{AirSyncBase}FileReference"/>
 *                   &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Method" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *                   &lt;element name="EstimatedDataSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                   &lt;element name="ContentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ContentLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IsInline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element ref="{Email2}UmAttDuration" minOccurs="0"/>
 *                   &lt;element ref="{Email2}UmAttOrder" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Add" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Method" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element ref="{AirSyncBase}ContentType"/>
 *                   &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ContentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ContentLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IsInline" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Delete" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{AirSyncBase}FileReference"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attachmentOrAddOrDelete"
})
@XmlRootElement(name = "Attachments")
public class Attachments {

    @XmlElements({
        @XmlElement(name = "Attachment", type = Attachments.Attachment.class),
        @XmlElement(name = "Add", type = Attachments.Add.class),
        @XmlElement(name = "Delete", type = Attachments.Delete.class)
    })
    protected List<Object> attachmentOrAddOrDelete;

    /**
     * Gets the value of the attachmentOrAddOrDelete property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachmentOrAddOrDelete property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachmentOrAddOrDelete().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attachments.Attachment }
     * {@link Attachments.Add }
     * {@link Attachments.Delete }
     * 
     * 
     */
    public List<Object> getAttachmentOrAddOrDelete() {
        if (attachmentOrAddOrDelete == null) {
            attachmentOrAddOrDelete = new ArrayList<Object>();
        }
        return this.attachmentOrAddOrDelete;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Method" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element ref="{AirSyncBase}ContentType"/>
     *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ContentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ContentLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IsInline" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Add {

        @XmlElement(name = "ClientId", required = true)
        protected String clientId;
        @XmlElement(name = "Method")
        @XmlSchemaType(name = "unsignedByte")
        protected short method;
        @XmlElement(name = "ContentType", required = true, nillable = true)
        protected String contentType;
        @XmlElement(name = "Content", required = true)
        protected String content;
        @XmlElement(name = "DisplayName", required = true)
        protected String displayName;
        @XmlElement(name = "ContentId")
        protected String contentId;
        @XmlElement(name = "ContentLocation")
        protected String contentLocation;
        @XmlElement(name = "IsInline")
        protected Object isInline;

        /**
         * Ruft den Wert der clientId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientId() {
            return clientId;
        }

        /**
         * Legt den Wert der clientId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientId(String value) {
            this.clientId = value;
        }

        /**
         * Ruft den Wert der method-Eigenschaft ab.
         * 
         */
        public short getMethod() {
            return method;
        }

        /**
         * Legt den Wert der method-Eigenschaft fest.
         * 
         */
        public void setMethod(short value) {
            this.method = value;
        }

        /**
         * Ruft den Wert der contentType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContentType() {
            return contentType;
        }

        /**
         * Legt den Wert der contentType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContentType(String value) {
            this.contentType = value;
        }

        /**
         * Ruft den Wert der content-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContent() {
            return content;
        }

        /**
         * Legt den Wert der content-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContent(String value) {
            this.content = value;
        }

        /**
         * Ruft den Wert der displayName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * Legt den Wert der displayName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayName(String value) {
            this.displayName = value;
        }

        /**
         * Ruft den Wert der contentId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContentId() {
            return contentId;
        }

        /**
         * Legt den Wert der contentId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContentId(String value) {
            this.contentId = value;
        }

        /**
         * Ruft den Wert der contentLocation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContentLocation() {
            return contentLocation;
        }

        /**
         * Legt den Wert der contentLocation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContentLocation(String value) {
            this.contentLocation = value;
        }

        /**
         * Ruft den Wert der isInline-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getIsInline() {
            return isInline;
        }

        /**
         * Legt den Wert der isInline-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setIsInline(Object value) {
            this.isInline = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element ref="{AirSyncBase}FileReference"/>
     *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Method" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
     *         &lt;element name="EstimatedDataSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *         &lt;element name="ContentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ContentLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IsInline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element ref="{Email2}UmAttDuration" minOccurs="0"/>
     *         &lt;element ref="{Email2}UmAttOrder" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Attachment {

        @XmlElement(name = "DisplayName")
        protected String displayName;
        @XmlElement(name = "FileReference", required = true)
        protected String fileReference;
        @XmlElement(name = "ClientId")
        protected String clientId;
        @XmlElement(name = "Method")
        @XmlSchemaType(name = "unsignedByte")
        protected Short method;
        @XmlElement(name = "EstimatedDataSize")
        @XmlSchemaType(name = "unsignedInt")
        protected Long estimatedDataSize;
        @XmlElement(name = "ContentId")
        protected String contentId;
        @XmlElement(name = "ContentLocation")
        protected String contentLocation;
        @XmlElement(name = "IsInline")
        protected Boolean isInline;
        @XmlElement(name = "UmAttDuration", namespace = "Email2")
        protected BigInteger umAttDuration;
        @XmlElement(name = "UmAttOrder", namespace = "Email2")
        protected BigInteger umAttOrder;

        /**
         * Ruft den Wert der displayName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * Legt den Wert der displayName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayName(String value) {
            this.displayName = value;
        }

        /**
         * Ruft den Wert der fileReference-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileReference() {
            return fileReference;
        }

        /**
         * Legt den Wert der fileReference-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileReference(String value) {
            this.fileReference = value;
        }

        /**
         * Ruft den Wert der clientId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientId() {
            return clientId;
        }

        /**
         * Legt den Wert der clientId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientId(String value) {
            this.clientId = value;
        }

        /**
         * Ruft den Wert der method-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Short }
         *     
         */
        public Short getMethod() {
            return method;
        }

        /**
         * Legt den Wert der method-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Short }
         *     
         */
        public void setMethod(Short value) {
            this.method = value;
        }

        /**
         * Ruft den Wert der estimatedDataSize-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getEstimatedDataSize() {
            return estimatedDataSize;
        }

        /**
         * Legt den Wert der estimatedDataSize-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setEstimatedDataSize(Long value) {
            this.estimatedDataSize = value;
        }

        /**
         * Ruft den Wert der contentId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContentId() {
            return contentId;
        }

        /**
         * Legt den Wert der contentId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContentId(String value) {
            this.contentId = value;
        }

        /**
         * Ruft den Wert der contentLocation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContentLocation() {
            return contentLocation;
        }

        /**
         * Legt den Wert der contentLocation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContentLocation(String value) {
            this.contentLocation = value;
        }

        /**
         * Ruft den Wert der isInline-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsInline() {
            return isInline;
        }

        /**
         * Legt den Wert der isInline-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsInline(Boolean value) {
            this.isInline = value;
        }

        /**
         * Ruft den Wert der umAttDuration-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getUmAttDuration() {
            return umAttDuration;
        }

        /**
         * Legt den Wert der umAttDuration-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setUmAttDuration(BigInteger value) {
            this.umAttDuration = value;
        }

        /**
         * Ruft den Wert der umAttOrder-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getUmAttOrder() {
            return umAttOrder;
        }

        /**
         * Legt den Wert der umAttOrder-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setUmAttOrder(BigInteger value) {
            this.umAttOrder = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{AirSyncBase}FileReference"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Delete {

        @XmlElement(name = "FileReference", required = true)
        protected String fileReference;

        /**
         * Ruft den Wert der fileReference-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileReference() {
            return fileReference;
        }

        /**
         * Legt den Wert der fileReference-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileReference(String value) {
            this.fileReference = value;
        }

    }

}
