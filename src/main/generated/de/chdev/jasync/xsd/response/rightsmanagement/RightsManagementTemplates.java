//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.rightsmanagement;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RightsManagementTemplate" maxOccurs="20" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{RightsManagement}TemplateID"/>
 *                   &lt;element name="TemplateName" type="{RightsManagement}NonEmptyStringType"/>
 *                   &lt;element name="TemplateDescription" type="{RightsManagement}NonEmptyStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rightsManagementTemplate"
})
@XmlRootElement(name = "RightsManagementTemplates")
public class RightsManagementTemplates {

    @XmlElement(name = "RightsManagementTemplate")
    protected List<RightsManagementTemplates.RightsManagementTemplate> rightsManagementTemplate;

    /**
     * Gets the value of the rightsManagementTemplate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rightsManagementTemplate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRightsManagementTemplate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RightsManagementTemplates.RightsManagementTemplate }
     * 
     * 
     */
    public List<RightsManagementTemplates.RightsManagementTemplate> getRightsManagementTemplate() {
        if (rightsManagementTemplate == null) {
            rightsManagementTemplate = new ArrayList<RightsManagementTemplates.RightsManagementTemplate>();
        }
        return this.rightsManagementTemplate;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{RightsManagement}TemplateID"/>
     *         &lt;element name="TemplateName" type="{RightsManagement}NonEmptyStringType"/>
     *         &lt;element name="TemplateDescription" type="{RightsManagement}NonEmptyStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "templateID",
        "templateName",
        "templateDescription"
    })
    public static class RightsManagementTemplate {

        @XmlElement(name = "TemplateID", required = true)
        protected String templateID;
        @XmlElement(name = "TemplateName", required = true)
        protected String templateName;
        @XmlElement(name = "TemplateDescription", required = true)
        protected String templateDescription;

        /**
         * Ruft den Wert der templateID-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTemplateID() {
            return templateID;
        }

        /**
         * Legt den Wert der templateID-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTemplateID(String value) {
            this.templateID = value;
        }

        /**
         * Ruft den Wert der templateName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTemplateName() {
            return templateName;
        }

        /**
         * Legt den Wert der templateName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTemplateName(String value) {
            this.templateName = value;
        }

        /**
         * Ruft den Wert der templateDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTemplateDescription() {
            return templateDescription;
        }

        /**
         * Legt den Wert der templateDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTemplateDescription(String value) {
            this.templateDescription = value;
        }

    }

}
