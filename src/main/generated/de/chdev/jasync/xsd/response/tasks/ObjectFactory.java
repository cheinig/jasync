//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.tasks;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.response.tasks package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BodySize_QNAME = new QName("Tasks", "BodySize");
    private final static QName _ReminderTime_QNAME = new QName("Tasks", "ReminderTime");
    private final static QName _StartDate_QNAME = new QName("Tasks", "StartDate");
    private final static QName _DateCompleted_QNAME = new QName("Tasks", "DateCompleted");
    private final static QName _SubOrdinalDate_QNAME = new QName("Tasks", "SubOrdinalDate");
    private final static QName _Body_QNAME = new QName("Tasks", "Body");
    private final static QName _DueDate_QNAME = new QName("Tasks", "DueDate");
    private final static QName _BodyTruncated_QNAME = new QName("Tasks", "BodyTruncated");
    private final static QName _Importance_QNAME = new QName("Tasks", "Importance");
    private final static QName _Sensitivity_QNAME = new QName("Tasks", "Sensitivity");
    private final static QName _Complete_QNAME = new QName("Tasks", "Complete");
    private final static QName _UtcStartDate_QNAME = new QName("Tasks", "UtcStartDate");
    private final static QName _Subject_QNAME = new QName("Tasks", "Subject");
    private final static QName _UtcDueDate_QNAME = new QName("Tasks", "UtcDueDate");
    private final static QName _OrdinalDate_QNAME = new QName("Tasks", "OrdinalDate");
    private final static QName _ReminderSet_QNAME = new QName("Tasks", "ReminderSet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.response.tasks
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Categories }
     * 
     */
    public Categories createCategories() {
        return new Categories();
    }

    /**
     * Create an instance of {@link Recurrence }
     * 
     */
    public Recurrence createRecurrence() {
        return new Recurrence();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "BodySize")
    public JAXBElement<BigInteger> createBodySize(BigInteger value) {
        return new JAXBElement<BigInteger>(_BodySize_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "ReminderTime")
    public JAXBElement<XMLGregorianCalendar> createReminderTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ReminderTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "StartDate")
    public JAXBElement<XMLGregorianCalendar> createStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_StartDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "DateCompleted")
    public JAXBElement<XMLGregorianCalendar> createDateCompleted(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateCompleted_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "SubOrdinalDate")
    public JAXBElement<String> createSubOrdinalDate(String value) {
        return new JAXBElement<String>(_SubOrdinalDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "Body")
    public JAXBElement<String> createBody(String value) {
        return new JAXBElement<String>(_Body_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "DueDate")
    public JAXBElement<XMLGregorianCalendar> createDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DueDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "BodyTruncated")
    public JAXBElement<Boolean> createBodyTruncated(Boolean value) {
        return new JAXBElement<Boolean>(_BodyTruncated_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "Importance")
    public JAXBElement<Short> createImportance(Short value) {
        return new JAXBElement<Short>(_Importance_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "Sensitivity")
    public JAXBElement<Short> createSensitivity(Short value) {
        return new JAXBElement<Short>(_Sensitivity_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "Complete")
    public JAXBElement<Short> createComplete(Short value) {
        return new JAXBElement<Short>(_Complete_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "UtcStartDate")
    public JAXBElement<XMLGregorianCalendar> createUtcStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_UtcStartDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "Subject")
    public JAXBElement<String> createSubject(String value) {
        return new JAXBElement<String>(_Subject_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "UtcDueDate")
    public JAXBElement<XMLGregorianCalendar> createUtcDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_UtcDueDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "OrdinalDate")
    public JAXBElement<XMLGregorianCalendar> createOrdinalDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrdinalDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Tasks", name = "ReminderSet")
    public JAXBElement<Short> createReminderSet(Short value) {
        return new JAXBElement<Short>(_ReminderSet_QNAME, Short.class, null, value);
    }

}
