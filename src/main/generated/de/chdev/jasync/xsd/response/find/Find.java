//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.find;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{Find}Status"/>
 *         &lt;element name="Response" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{ItemOperations}Store"/>
 *                   &lt;element ref="{Find}Status"/>
 *                   &lt;element name="Result" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{AirSync}Class"/>
 *                             &lt;element ref="{AirSync}ServerId"/>
 *                             &lt;element ref="{AirSync}CollectionId"/>
 *                             &lt;element name="Properties">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;group ref="{Find}FindProperties"/>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element ref="{Find}Range" minOccurs="0"/>
 *                   &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "response"
})
@XmlRootElement(name = "Find")
public class Find {

    @XmlElement(name = "Status", required = true)
    protected BigInteger status;
    @XmlElement(name = "Response")
    protected Find.Response response;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der response-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Find.Response }
     *     
     */
    public Find.Response getResponse() {
        return response;
    }

    /**
     * Legt den Wert der response-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Find.Response }
     *     
     */
    public void setResponse(Find.Response value) {
        this.response = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{ItemOperations}Store"/>
     *         &lt;element ref="{Find}Status"/>
     *         &lt;element name="Result" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{AirSync}Class"/>
     *                   &lt;element ref="{AirSync}ServerId"/>
     *                   &lt;element ref="{AirSync}CollectionId"/>
     *                   &lt;element name="Properties">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;group ref="{Find}FindProperties"/>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element ref="{Find}Range" minOccurs="0"/>
     *         &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Response {

        @XmlElement(name = "Store", namespace = "ItemOperations", required = true)
        protected String store;
        @XmlElement(name = "Status", required = true)
        protected BigInteger status;
        @XmlElement(name = "Result")
        protected Find.Response.Result result;
        @XmlElement(name = "Range", nillable = true)
        protected String range;
        @XmlElement(name = "Total", required = true)
        protected BigInteger total;

        /**
         * Ruft den Wert der store-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStore() {
            return store;
        }

        /**
         * Legt den Wert der store-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStore(String value) {
            this.store = value;
        }

        /**
         * Ruft den Wert der status-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getStatus() {
            return status;
        }

        /**
         * Legt den Wert der status-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setStatus(BigInteger value) {
            this.status = value;
        }

        /**
         * Ruft den Wert der result-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Find.Response.Result }
         *     
         */
        public Find.Response.Result getResult() {
            return result;
        }

        /**
         * Legt den Wert der result-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Find.Response.Result }
         *     
         */
        public void setResult(Find.Response.Result value) {
            this.result = value;
        }

        /**
         * Ruft den Wert der range-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRange() {
            return range;
        }

        /**
         * Legt den Wert der range-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRange(String value) {
            this.range = value;
        }

        /**
         * Ruft den Wert der total-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotal() {
            return total;
        }

        /**
         * Legt den Wert der total-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotal(BigInteger value) {
            this.total = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{AirSync}Class"/>
         *         &lt;element ref="{AirSync}ServerId"/>
         *         &lt;element ref="{AirSync}CollectionId"/>
         *         &lt;element name="Properties">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;group ref="{Find}FindProperties"/>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "clazz",
            "serverId",
            "collectionId",
            "properties"
        })
        public static class Result {

            @XmlElement(name = "Class", namespace = "AirSync", required = true)
            protected String clazz;
            @XmlElement(name = "ServerId", namespace = "AirSync", required = true)
            protected String serverId;
            @XmlElement(name = "CollectionId", namespace = "AirSync", required = true)
            protected String collectionId;
            @XmlElement(name = "Properties", required = true)
            protected Find.Response.Result.Properties properties;

            /**
             * Ruft den Wert der clazz-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClazz() {
                return clazz;
            }

            /**
             * Legt den Wert der clazz-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClazz(String value) {
                this.clazz = value;
            }

            /**
             * Ruft den Wert der serverId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getServerId() {
                return serverId;
            }

            /**
             * Legt den Wert der serverId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setServerId(String value) {
                this.serverId = value;
            }

            /**
             * Ruft den Wert der collectionId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCollectionId() {
                return collectionId;
            }

            /**
             * Legt den Wert der collectionId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCollectionId(String value) {
                this.collectionId = value;
            }

            /**
             * Ruft den Wert der properties-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Find.Response.Result.Properties }
             *     
             */
            public Find.Response.Result.Properties getProperties() {
                return properties;
            }

            /**
             * Legt den Wert der properties-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Find.Response.Result.Properties }
             *     
             */
            public void setProperties(Find.Response.Result.Properties value) {
                this.properties = value;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;group ref="{Find}FindProperties"/>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "subjectOrDateReceivedOrDisplayTo"
            })
            public static class Properties {

                @XmlElementRefs({
                    @XmlElementRef(name = "IsDraft", namespace = "Email2", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Preview", namespace = "Find", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Phone", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "LastName", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Title", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "DisplayBcc", namespace = "Find", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Picture", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "HasAttachments", namespace = "Find", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Company", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "DisplayName", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "HomePhone", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "MobilePhone", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "DisplayCc", namespace = "Find", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Office", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "EmailAddress", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "FirstName", namespace = "GAL", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Alias", namespace = "GAL", type = JAXBElement.class, required = false)
                })
                protected List<JAXBElement<?>> subjectOrDateReceivedOrDisplayTo;

                /**
                 * Gets the value of the subjectOrDateReceivedOrDisplayTo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the subjectOrDateReceivedOrDisplayTo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getSubjectOrDateReceivedOrDisplayTo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                 * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link de.chdev.jasync.xsd.response.search.Search.Response.Store.Result.Properties.Picture }{@code >}
                 * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link Short }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * 
                 * 
                 */
                public List<JAXBElement<?>> getSubjectOrDateReceivedOrDisplayTo() {
                    if (subjectOrDateReceivedOrDisplayTo == null) {
                        subjectOrDateReceivedOrDisplayTo = new ArrayList<JAXBElement<?>>();
                    }
                    return this.subjectOrDateReceivedOrDisplayTo;
                }

            }

        }

    }

}
