//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:34 AM CEST 
//


package de.chdev.jasync.xsd.response.search;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import de.chdev.jasync.xsd.response.airsyncbase.Attachments;
import de.chdev.jasync.xsd.response.airsyncbase.Body;
import de.chdev.jasync.xsd.response.airsyncbase.BodyPart;
import de.chdev.jasync.xsd.response.airsyncbase.Location;
import de.chdev.jasync.xsd.response.calendar.Attendees;
import de.chdev.jasync.xsd.response.calendar.Exceptions;
import de.chdev.jasync.xsd.response.contacts.Children;
import de.chdev.jasync.xsd.response.email.Flag;
import de.chdev.jasync.xsd.response.email.MeetingRequest;
import de.chdev.jasync.xsd.response.rightsmanagement.RightsManagementLicense;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{Search}Status" minOccurs="0"/>
 *         &lt;element name="Response" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Store">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{Search}Status"/>
 *                             &lt;element name="Result" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                                       &lt;element ref="{Search}LongId" minOccurs="0"/>
 *                                       &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
 *                                       &lt;element name="Properties" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;group ref="{Search}SearchProperties"/>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element ref="{Search}Range" minOccurs="0"/>
 *                             &lt;element ref="{Search}Total" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "response"
})
@XmlRootElement(name = "Search")
public class Search {

    @XmlElement(name = "Status")
    protected BigInteger status;
    @XmlElement(name = "Response")
    protected Search.Response response;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der response-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Search.Response }
     *     
     */
    public Search.Response getResponse() {
        return response;
    }

    /**
     * Legt den Wert der response-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Search.Response }
     *     
     */
    public void setResponse(Search.Response value) {
        this.response = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Store">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{Search}Status"/>
     *                   &lt;element name="Result" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                             &lt;element ref="{Search}LongId" minOccurs="0"/>
     *                             &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
     *                             &lt;element name="Properties" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;group ref="{Search}SearchProperties"/>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element ref="{Search}Range" minOccurs="0"/>
     *                   &lt;element ref="{Search}Total" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Response {

        @XmlElement(name = "Store", required = true)
        protected Search.Response.Store store;

        /**
         * Ruft den Wert der store-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Search.Response.Store }
         *     
         */
        public Search.Response.Store getStore() {
            return store;
        }

        /**
         * Legt den Wert der store-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Search.Response.Store }
         *     
         */
        public void setStore(Search.Response.Store value) {
            this.store = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{Search}Status"/>
         *         &lt;element name="Result" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *                   &lt;element ref="{Search}LongId" minOccurs="0"/>
         *                   &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
         *                   &lt;element name="Properties" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;group ref="{Search}SearchProperties"/>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element ref="{Search}Range" minOccurs="0"/>
         *         &lt;element ref="{Search}Total" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "status",
            "result",
            "range",
            "total"
        })
        public static class Store {

            @XmlElement(name = "Status", required = true)
            protected BigInteger status;
            @XmlElement(name = "Result", nillable = true)
            protected List<Search.Response.Store.Result> result;
            @XmlElement(name = "Range")
            protected String range;
            @XmlElement(name = "Total")
            protected BigInteger total;

            /**
             * Ruft den Wert der status-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getStatus() {
                return status;
            }

            /**
             * Legt den Wert der status-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setStatus(BigInteger value) {
                this.status = value;
            }

            /**
             * Gets the value of the result property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the result property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getResult().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Search.Response.Store.Result }
             * 
             * 
             */
            public List<Search.Response.Store.Result> getResult() {
                if (result == null) {
                    result = new ArrayList<Search.Response.Store.Result>();
                }
                return this.result;
            }

            /**
             * Ruft den Wert der range-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRange() {
                return range;
            }

            /**
             * Legt den Wert der range-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRange(String value) {
                this.range = value;
            }

            /**
             * Ruft den Wert der total-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTotal() {
                return total;
            }

            /**
             * Legt den Wert der total-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTotal(BigInteger value) {
                this.total = value;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
             *         &lt;element ref="{Search}LongId" minOccurs="0"/>
             *         &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
             *         &lt;element name="Properties" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;group ref="{Search}SearchProperties"/>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "clazz",
                "longId",
                "collectionId",
                "properties"
            })
            public static class Result {

                @XmlElement(name = "Class", namespace = "AirSync")
                protected String clazz;
                @XmlElement(name = "LongId")
                protected String longId;
                @XmlElement(name = "CollectionId", namespace = "AirSync")
                protected String collectionId;
                @XmlElement(name = "Properties")
                protected Search.Response.Store.Result.Properties properties;

                /**
                 * Ruft den Wert der clazz-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getClazz() {
                    return clazz;
                }

                /**
                 * Legt den Wert der clazz-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setClazz(String value) {
                    this.clazz = value;
                }

                /**
                 * Ruft den Wert der longId-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLongId() {
                    return longId;
                }

                /**
                 * Legt den Wert der longId-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLongId(String value) {
                    this.longId = value;
                }

                /**
                 * Ruft den Wert der collectionId-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCollectionId() {
                    return collectionId;
                }

                /**
                 * Legt den Wert der collectionId-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCollectionId(String value) {
                    this.collectionId = value;
                }

                /**
                 * Ruft den Wert der properties-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Search.Response.Store.Result.Properties }
                 *     
                 */
                public Search.Response.Store.Result.Properties getProperties() {
                    return properties;
                }

                /**
                 * Legt den Wert der properties-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Search.Response.Store.Result.Properties }
                 *     
                 */
                public void setProperties(Search.Response.Store.Result.Properties value) {
                    this.properties = value;
                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;group ref="{Search}SearchProperties"/>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "attachmentsOrBodyOrBodyPart"
                })
                public static class Properties {

                    @XmlElementRefs({
                        @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Attachments", namespace = "AirSyncBase", type = Attachments.class, required = false),
                        @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "LastName", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "IsHidden", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "LinkId", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ResponseType", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Company", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
                        @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Sender", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Alias", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
                        @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MeetingRequest", namespace = "Email", type = MeetingRequest.class, required = false),
                        @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Categories", namespace = "Notes", type = de.chdev.jasync.xsd.response.notes.Categories.class, required = false),
                        @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.response.contacts.Categories.class, required = false),
                        @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
                        @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
                        @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "WeightedRank", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "RightsManagementLicense", namespace = "RightsManagement", type = RightsManagementLicense.class, required = false),
                        @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "LastModifiedDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Title", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ContentLength", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "EmailAddress", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DisplayName", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ContentClass", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ConversationIndex", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MobilePhone", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "LastVerbExecutionTime", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ReceivedAsBcc", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "LastVerbExecuted", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Alias", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BodyPart", namespace = "AirSyncBase", type = BodyPart.class, required = false),
                        @XmlElementRef(name = "Phone", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "LastModifiedDate", namespace = "Notes", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ConversationId", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DisplayName", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "IsFolder", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
                        @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "UmCallerID", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Categories.class, required = false),
                        @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "UmUserNotes", namespace = "Email2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MessageClass", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ThreadTopic", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Picture", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "FirstName", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.response.calendar.Recurrence.class, required = false),
                        @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Subject", namespace = "Notes", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Categories", namespace = "Email", type = de.chdev.jasync.xsd.response.email.Categories.class, required = false),
                        @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.response.tasks.Recurrence.class, required = false),
                        @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "ContentType", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Office", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "CreationDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
                        @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomePhone", namespace = "GAL", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "AppointmentReplyTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "MessageClass", namespace = "Notes", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false)
                    })
                    protected List<Object> attachmentsOrBodyOrBodyPart;

                    /**
                     * Gets the value of the attachmentsOrBodyOrBodyPart property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the attachmentsOrBodyOrBodyPart property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAttachmentsOrBodyOrBodyPart().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link Attachments }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Long }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link Location }
                     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link Children }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link MeetingRequest }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link de.chdev.jasync.xsd.response.notes.Categories }
                     * {@link de.chdev.jasync.xsd.response.contacts.Categories }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link Attendees }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link Body }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link RightsManagementLicense }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Long }{@code >}
                     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link BodyPart }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link Flag }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link de.chdev.jasync.xsd.response.tasks.Categories }
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Search.Response.Store.Result.Properties.Picture }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link de.chdev.jasync.xsd.response.calendar.Recurrence }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link de.chdev.jasync.xsd.response.email.Categories }
                     * {@link de.chdev.jasync.xsd.response.tasks.Recurrence }
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link Exceptions }
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link Short }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                     * 
                     * 
                     */
                    public List<Object> getAttachmentsOrBodyOrBodyPart() {
                        if (attachmentsOrBodyOrBodyPart == null) {
                            attachmentsOrBodyOrBodyPart = new ArrayList<Object>();
                        }
                        return this.attachmentsOrBodyOrBodyPart;
                    }


                    /**
                     * <p>Java-Klasse für anonymous complex type.
                     * 
                     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
                     *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "status",
                        "data"
                    })
                    public static class Picture {

                        @XmlElement(name = "Status", namespace = "GAL", required = true)
                        protected BigInteger status;
                        @XmlElement(name = "Data", namespace = "GAL")
                        protected String data;

                        /**
                         * Ruft den Wert der status-Eigenschaft ab.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getStatus() {
                            return status;
                        }

                        /**
                         * Legt den Wert der status-Eigenschaft fest.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setStatus(BigInteger value) {
                            this.status = value;
                        }

                        /**
                         * Ruft den Wert der data-Eigenschaft ab.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getData() {
                            return data;
                        }

                        /**
                         * Legt den Wert der data-Eigenschaft fest.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setData(String value) {
                            this.data = value;
                        }

                    }

                }

            }

        }

    }

}
