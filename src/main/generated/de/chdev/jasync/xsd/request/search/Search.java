//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.search;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Store">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{Search}Name"/>
 *                   &lt;element name="Query" type="{Search}queryType"/>
 *                   &lt;element ref="{Search}Options" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "store"
})
@XmlRootElement(name = "Search")
public class Search {

    @XmlElement(name = "Store")
    protected List<Search.Store> store;

    /**
     * Gets the value of the store property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the store property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Search.Store }
     * 
     * 
     */
    public List<Search.Store> getStore() {
        if (store == null) {
            store = new ArrayList<Search.Store>();
        }
        return this.store;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{Search}Name"/>
     *         &lt;element name="Query" type="{Search}queryType"/>
     *         &lt;element ref="{Search}Options" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Store {

        @XmlElement(name = "Name", required = true)
        protected String name;
        @XmlElement(name = "Query", required = true)
        protected QueryType query;
        @XmlElement(name = "Options")
        protected Options options;

        /**
         * Ruft den Wert der name-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Legt den Wert der name-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Ruft den Wert der query-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link QueryType }
         *     
         */
        public QueryType getQuery() {
            return query;
        }

        /**
         * Legt den Wert der query-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryType }
         *     
         */
        public void setQuery(QueryType value) {
            this.query = value;
        }

        /**
         * Ruft den Wert der options-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Options }
         *     
         */
        public Options getOptions() {
            return options;
        }

        /**
         * Legt den Wert der options-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Options }
         *     
         */
        public void setOptions(Options value) {
            this.options = value;
        }

    }

}
