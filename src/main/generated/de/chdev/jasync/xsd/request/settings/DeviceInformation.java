//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.settings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Set">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{Settings}Model" minOccurs="0"/>
 *                   &lt;element ref="{Settings}IMEI" minOccurs="0"/>
 *                   &lt;element ref="{Settings}FriendlyName" minOccurs="0"/>
 *                   &lt;element ref="{Settings}OS" minOccurs="0"/>
 *                   &lt;element ref="{Settings}OSLanguage" minOccurs="0"/>
 *                   &lt;element ref="{Settings}PhoneNumber" minOccurs="0"/>
 *                   &lt;element ref="{Settings}UserAgent" minOccurs="0"/>
 *                   &lt;element ref="{Settings}EnableOutboundSMS" minOccurs="0"/>
 *                   &lt;element ref="{Settings}MobileOperator" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "DeviceInformation")
public class DeviceInformation {

    @XmlElement(name = "Set", required = true)
    protected DeviceInformation.Set set;

    /**
     * Ruft den Wert der set-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceInformation.Set }
     *     
     */
    public DeviceInformation.Set getSet() {
        return set;
    }

    /**
     * Legt den Wert der set-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceInformation.Set }
     *     
     */
    public void setSet(DeviceInformation.Set value) {
        this.set = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{Settings}Model" minOccurs="0"/>
     *         &lt;element ref="{Settings}IMEI" minOccurs="0"/>
     *         &lt;element ref="{Settings}FriendlyName" minOccurs="0"/>
     *         &lt;element ref="{Settings}OS" minOccurs="0"/>
     *         &lt;element ref="{Settings}OSLanguage" minOccurs="0"/>
     *         &lt;element ref="{Settings}PhoneNumber" minOccurs="0"/>
     *         &lt;element ref="{Settings}UserAgent" minOccurs="0"/>
     *         &lt;element ref="{Settings}EnableOutboundSMS" minOccurs="0"/>
     *         &lt;element ref="{Settings}MobileOperator" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Set {

        @XmlElement(name = "Model")
        protected String model;
        @XmlElement(name = "IMEI")
        protected String imei;
        @XmlElement(name = "FriendlyName")
        protected String friendlyName;
        @XmlElement(name = "OS")
        protected String os;
        @XmlElement(name = "OSLanguage")
        protected String osLanguage;
        @XmlElement(name = "PhoneNumber")
        protected String phoneNumber;
        @XmlElement(name = "UserAgent")
        protected String userAgent;
        @XmlElement(name = "EnableOutboundSMS")
        protected Integer enableOutboundSMS;
        @XmlElement(name = "MobileOperator")
        protected String mobileOperator;

        /**
         * Ruft den Wert der model-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModel() {
            return model;
        }

        /**
         * Legt den Wert der model-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModel(String value) {
            this.model = value;
        }

        /**
         * Ruft den Wert der imei-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIMEI() {
            return imei;
        }

        /**
         * Legt den Wert der imei-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIMEI(String value) {
            this.imei = value;
        }

        /**
         * Ruft den Wert der friendlyName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFriendlyName() {
            return friendlyName;
        }

        /**
         * Legt den Wert der friendlyName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFriendlyName(String value) {
            this.friendlyName = value;
        }

        /**
         * Ruft den Wert der os-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOS() {
            return os;
        }

        /**
         * Legt den Wert der os-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOS(String value) {
            this.os = value;
        }

        /**
         * Ruft den Wert der osLanguage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOSLanguage() {
            return osLanguage;
        }

        /**
         * Legt den Wert der osLanguage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOSLanguage(String value) {
            this.osLanguage = value;
        }

        /**
         * Ruft den Wert der phoneNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPhoneNumber() {
            return phoneNumber;
        }

        /**
         * Legt den Wert der phoneNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPhoneNumber(String value) {
            this.phoneNumber = value;
        }

        /**
         * Ruft den Wert der userAgent-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserAgent() {
            return userAgent;
        }

        /**
         * Legt den Wert der userAgent-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserAgent(String value) {
            this.userAgent = value;
        }

        /**
         * Ruft den Wert der enableOutboundSMS-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getEnableOutboundSMS() {
            return enableOutboundSMS;
        }

        /**
         * Legt den Wert der enableOutboundSMS-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setEnableOutboundSMS(Integer value) {
            this.enableOutboundSMS = value;
        }

        /**
         * Ruft den Wert der mobileOperator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMobileOperator() {
            return mobileOperator;
        }

        /**
         * Legt den Wert der mobileOperator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMobileOperator(String value) {
            this.mobileOperator = value;
        }

    }

}
