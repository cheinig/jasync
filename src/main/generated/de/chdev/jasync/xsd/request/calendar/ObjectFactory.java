//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.calendar;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.calendar package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Reminder_QNAME = new QName("Calendar", "Reminder");
    private final static QName _OnlineMeetingConfLink_QNAME = new QName("Calendar", "OnlineMeetingConfLink");
    private final static QName _MeetingStatus_QNAME = new QName("Calendar", "MeetingStatus");
    private final static QName _UID_QNAME = new QName("Calendar", "UID");
    private final static QName _Timezone_QNAME = new QName("Calendar", "Timezone");
    private final static QName _BusyStatus_QNAME = new QName("Calendar", "BusyStatus");
    private final static QName _Subject_QNAME = new QName("Calendar", "Subject");
    private final static QName _OrganizerEmail_QNAME = new QName("Calendar", "OrganizerEmail");
    private final static QName _Location_QNAME = new QName("Calendar", "Location");
    private final static QName _ResponseType_QNAME = new QName("Calendar", "ResponseType");
    private final static QName _StartTime_QNAME = new QName("Calendar", "StartTime");
    private final static QName _OrganizerName_QNAME = new QName("Calendar", "OrganizerName");
    private final static QName _Sensitivity_QNAME = new QName("Calendar", "Sensitivity");
    private final static QName _EndTime_QNAME = new QName("Calendar", "EndTime");
    private final static QName _ResponseRequested_QNAME = new QName("Calendar", "ResponseRequested");
    private final static QName _BodyTruncated_QNAME = new QName("Calendar", "BodyTruncated");
    private final static QName _DtStamp_QNAME = new QName("Calendar", "DtStamp");
    private final static QName _OnlineMeetingExternalLink_QNAME = new QName("Calendar", "OnlineMeetingExternalLink");
    private final static QName _Body_QNAME = new QName("Calendar", "Body");
    private final static QName _AppointmentReplyTime_QNAME = new QName("Calendar", "AppointmentReplyTime");
    private final static QName _DisallowNewTimeProposal_QNAME = new QName("Calendar", "DisallowNewTimeProposal");
    private final static QName _AllDayEvent_QNAME = new QName("Calendar", "AllDayEvent");
    private final static QName _ClientUid_QNAME = new QName("Calendar", "ClientUid");
    private final static QName _ExceptionsExceptionExceptionStartTime_QNAME = new QName("Calendar", "ExceptionStartTime");
    private final static QName _ExceptionsExceptionDeleted_QNAME = new QName("Calendar", "Deleted");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.calendar
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Attendees }
     * 
     */
    public Attendees createAttendees() {
        return new Attendees();
    }

    /**
     * Create an instance of {@link Exceptions }
     * 
     */
    public Exceptions createExceptions() {
        return new Exceptions();
    }

    /**
     * Create an instance of {@link Attendees.Attendee }
     * 
     */
    public Attendees.Attendee createAttendeesAttendee() {
        return new Attendees.Attendee();
    }

    /**
     * Create an instance of {@link Categories }
     * 
     */
    public Categories createCategories() {
        return new Categories();
    }

    /**
     * Create an instance of {@link Recurrence }
     * 
     */
    public Recurrence createRecurrence() {
        return new Recurrence();
    }

    /**
     * Create an instance of {@link Exceptions.Exception }
     * 
     */
    public Exceptions.Exception createExceptionsException() {
        return new Exceptions.Exception();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Reminder")
    public JAXBElement<Long> createReminder(Long value) {
        return new JAXBElement<Long>(_Reminder_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OnlineMeetingConfLink")
    public JAXBElement<String> createOnlineMeetingConfLink(String value) {
        return new JAXBElement<String>(_OnlineMeetingConfLink_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "MeetingStatus")
    public JAXBElement<Short> createMeetingStatus(Short value) {
        return new JAXBElement<Short>(_MeetingStatus_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "UID")
    public JAXBElement<String> createUID(String value) {
        return new JAXBElement<String>(_UID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Timezone")
    public JAXBElement<String> createTimezone(String value) {
        return new JAXBElement<String>(_Timezone_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "BusyStatus")
    public JAXBElement<Short> createBusyStatus(Short value) {
        return new JAXBElement<Short>(_BusyStatus_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Subject")
    public JAXBElement<String> createSubject(String value) {
        return new JAXBElement<String>(_Subject_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OrganizerEmail")
    public JAXBElement<String> createOrganizerEmail(String value) {
        return new JAXBElement<String>(_OrganizerEmail_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Location")
    public JAXBElement<String> createLocation(String value) {
        return new JAXBElement<String>(_Location_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "ResponseType")
    public JAXBElement<Long> createResponseType(Long value) {
        return new JAXBElement<Long>(_ResponseType_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "StartTime")
    public JAXBElement<String> createStartTime(String value) {
        return new JAXBElement<String>(_StartTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OrganizerName")
    public JAXBElement<String> createOrganizerName(String value) {
        return new JAXBElement<String>(_OrganizerName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Sensitivity")
    public JAXBElement<Short> createSensitivity(Short value) {
        return new JAXBElement<Short>(_Sensitivity_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "EndTime")
    public JAXBElement<String> createEndTime(String value) {
        return new JAXBElement<String>(_EndTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "ResponseRequested")
    public JAXBElement<Boolean> createResponseRequested(Boolean value) {
        return new JAXBElement<Boolean>(_ResponseRequested_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "BodyTruncated")
    public JAXBElement<Boolean> createBodyTruncated(Boolean value) {
        return new JAXBElement<Boolean>(_BodyTruncated_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "DtStamp")
    public JAXBElement<String> createDtStamp(String value) {
        return new JAXBElement<String>(_DtStamp_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OnlineMeetingExternalLink")
    public JAXBElement<String> createOnlineMeetingExternalLink(String value) {
        return new JAXBElement<String>(_OnlineMeetingExternalLink_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Body")
    public JAXBElement<String> createBody(String value) {
        return new JAXBElement<String>(_Body_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "AppointmentReplyTime")
    public JAXBElement<String> createAppointmentReplyTime(String value) {
        return new JAXBElement<String>(_AppointmentReplyTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "DisallowNewTimeProposal")
    public JAXBElement<Boolean> createDisallowNewTimeProposal(Boolean value) {
        return new JAXBElement<Boolean>(_DisallowNewTimeProposal_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "AllDayEvent")
    public JAXBElement<Short> createAllDayEvent(Short value) {
        return new JAXBElement<Short>(_AllDayEvent_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "ClientUid")
    public JAXBElement<String> createClientUid(String value) {
        return new JAXBElement<String>(_ClientUid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "ExceptionStartTime", scope = Exceptions.Exception.class)
    public JAXBElement<String> createExceptionsExceptionExceptionStartTime(String value) {
        return new JAXBElement<String>(_ExceptionsExceptionExceptionStartTime_QNAME, String.class, Exceptions.Exception.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Deleted", scope = Exceptions.Exception.class)
    public JAXBElement<Short> createExceptionsExceptionDeleted(Short value) {
        return new JAXBElement<Short>(_ExceptionsExceptionDeleted_QNAME, Short.class, Exceptions.Exception.class, value);
    }

}
