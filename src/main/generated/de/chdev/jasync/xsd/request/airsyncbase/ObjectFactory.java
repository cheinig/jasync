//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.airsyncbase;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.airsyncbase package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ContentType_QNAME = new QName("AirSyncBase", "ContentType");
    private final static QName _NativeBodyType_QNAME = new QName("AirSyncBase", "NativeBodyType");
    private final static QName _InstanceId_QNAME = new QName("AirSyncBase", "InstanceId");
    private final static QName _FileReference_QNAME = new QName("AirSyncBase", "FileReference");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.airsyncbase
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Attachments }
     * 
     */
    public Attachments createAttachments() {
        return new Attachments();
    }

    /**
     * Create an instance of {@link BodyPreference }
     * 
     */
    public BodyPreference createBodyPreference() {
        return new BodyPreference();
    }

    /**
     * Create an instance of {@link BodyPartPreference }
     * 
     */
    public BodyPartPreference createBodyPartPreference() {
        return new BodyPartPreference();
    }

    /**
     * Create an instance of {@link BodyPart }
     * 
     */
    public BodyPart createBodyPart() {
        return new BodyPart();
    }

    /**
     * Create an instance of {@link Body }
     * 
     */
    public Body createBody() {
        return new Body();
    }

    /**
     * Create an instance of {@link Attachments.Attachment }
     * 
     */
    public Attachments.Attachment createAttachmentsAttachment() {
        return new Attachments.Attachment();
    }

    /**
     * Create an instance of {@link Attachments.Add }
     * 
     */
    public Attachments.Add createAttachmentsAdd() {
        return new Attachments.Add();
    }

    /**
     * Create an instance of {@link Attachments.Delete }
     * 
     */
    public Attachments.Delete createAttachmentsDelete() {
        return new Attachments.Delete();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "ContentType")
    public JAXBElement<String> createContentType(String value) {
        return new JAXBElement<String>(_ContentType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "NativeBodyType")
    public JAXBElement<Short> createNativeBodyType(Short value) {
        return new JAXBElement<Short>(_NativeBodyType_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "InstanceId")
    public JAXBElement<String> createInstanceId(String value) {
        return new JAXBElement<String>(_InstanceId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "FileReference")
    public JAXBElement<String> createFileReference(String value) {
        return new JAXBElement<String>(_FileReference_QNAME, String.class, null, value);
    }

}
