//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.email2;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.email2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Send_QNAME = new QName("Email2", "Send");
    private final static QName _UmCallerID_QNAME = new QName("Email2", "UmCallerID");
    private final static QName _IsDraft_QNAME = new QName("Email2", "IsDraft");
    private final static QName _ConversationIndex_QNAME = new QName("Email2", "ConversationIndex");
    private final static QName _ConversationId_QNAME = new QName("Email2", "ConversationId");
    private final static QName _CalendarType_QNAME = new QName("Email2", "CalendarType");
    private final static QName _FirstDayOfWeek_QNAME = new QName("Email2", "FirstDayOfWeek");
    private final static QName _LastVerbExecutionTime_QNAME = new QName("Email2", "LastVerbExecutionTime");
    private final static QName _Sender_QNAME = new QName("Email2", "Sender");
    private final static QName _UmAttDuration_QNAME = new QName("Email2", "UmAttDuration");
    private final static QName _IsLeapMonth_QNAME = new QName("Email2", "IsLeapMonth");
    private final static QName _AccountId_QNAME = new QName("Email2", "AccountId");
    private final static QName _UmAttOrder_QNAME = new QName("Email2", "UmAttOrder");
    private final static QName _ReceivedAsBcc_QNAME = new QName("Email2", "ReceivedAsBcc");
    private final static QName _LastVerbExecuted_QNAME = new QName("Email2", "LastVerbExecuted");
    private final static QName _MeetingMessageType_QNAME = new QName("Email2", "MeetingMessageType");
    private final static QName _UmUserNotes_QNAME = new QName("Email2", "UmUserNotes");
    private final static QName _Bcc_QNAME = new QName("Email2", "Bcc");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.email2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "Send")
    public JAXBElement<Object> createSend(Object value) {
        return new JAXBElement<Object>(_Send_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "UmCallerID")
    public JAXBElement<String> createUmCallerID(String value) {
        return new JAXBElement<String>(_UmCallerID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "IsDraft")
    public JAXBElement<Boolean> createIsDraft(Boolean value) {
        return new JAXBElement<Boolean>(_IsDraft_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "ConversationIndex")
    public JAXBElement<String> createConversationIndex(String value) {
        return new JAXBElement<String>(_ConversationIndex_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "ConversationId")
    public JAXBElement<String> createConversationId(String value) {
        return new JAXBElement<String>(_ConversationId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "CalendarType")
    public JAXBElement<BigInteger> createCalendarType(BigInteger value) {
        return new JAXBElement<BigInteger>(_CalendarType_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "FirstDayOfWeek")
    public JAXBElement<Short> createFirstDayOfWeek(Short value) {
        return new JAXBElement<Short>(_FirstDayOfWeek_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "LastVerbExecutionTime")
    public JAXBElement<XMLGregorianCalendar> createLastVerbExecutionTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_LastVerbExecutionTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "Sender")
    public JAXBElement<String> createSender(String value) {
        return new JAXBElement<String>(_Sender_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "UmAttDuration")
    public JAXBElement<BigInteger> createUmAttDuration(BigInteger value) {
        return new JAXBElement<BigInteger>(_UmAttDuration_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "IsLeapMonth")
    public JAXBElement<Short> createIsLeapMonth(Short value) {
        return new JAXBElement<Short>(_IsLeapMonth_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "AccountId")
    public JAXBElement<String> createAccountId(String value) {
        return new JAXBElement<String>(_AccountId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "UmAttOrder")
    public JAXBElement<BigInteger> createUmAttOrder(BigInteger value) {
        return new JAXBElement<BigInteger>(_UmAttOrder_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "ReceivedAsBcc")
    public JAXBElement<Boolean> createReceivedAsBcc(Boolean value) {
        return new JAXBElement<Boolean>(_ReceivedAsBcc_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "LastVerbExecuted")
    public JAXBElement<BigInteger> createLastVerbExecuted(BigInteger value) {
        return new JAXBElement<BigInteger>(_LastVerbExecuted_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "MeetingMessageType")
    public JAXBElement<Short> createMeetingMessageType(Short value) {
        return new JAXBElement<Short>(_MeetingMessageType_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "UmUserNotes")
    public JAXBElement<String> createUmUserNotes(String value) {
        return new JAXBElement<String>(_UmUserNotes_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email2", name = "Bcc")
    public JAXBElement<String> createBcc(String value) {
        return new JAXBElement<String>(_Bcc_QNAME, String.class, null, value);
    }

}
