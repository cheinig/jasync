//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.email;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{Tasks}Subject" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="FlagType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{Tasks}DateCompleted" minOccurs="0"/>
 *         &lt;element name="CompleteTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element ref="{Tasks}StartDate" minOccurs="0"/>
 *         &lt;element ref="{Tasks}DueDate" minOccurs="0"/>
 *         &lt;element ref="{Tasks}UtcStartDate" minOccurs="0"/>
 *         &lt;element ref="{Tasks}UtcDueDate" minOccurs="0"/>
 *         &lt;element ref="{Tasks}ReminderSet" minOccurs="0"/>
 *         &lt;element ref="{Tasks}ReminderTime" minOccurs="0"/>
 *         &lt;element ref="{Tasks}OrdinalDate" minOccurs="0"/>
 *         &lt;element ref="{Tasks}SubOrdinalDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subject",
    "status",
    "flagType",
    "dateCompleted",
    "completeTime",
    "startDate",
    "dueDate",
    "utcStartDate",
    "utcDueDate",
    "reminderSet",
    "reminderTime",
    "ordinalDate",
    "subOrdinalDate"
})
@XmlRootElement(name = "Flag")
public class Flag {

    @XmlElement(name = "Subject", namespace = "Tasks")
    protected String subject;
    @XmlElement(name = "Status")
    protected BigInteger status;
    @XmlElement(name = "FlagType")
    protected String flagType;
    @XmlElement(name = "DateCompleted", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateCompleted;
    @XmlElement(name = "CompleteTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar completeTime;
    @XmlElement(name = "StartDate", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement(name = "DueDate", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueDate;
    @XmlElement(name = "UtcStartDate", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar utcStartDate;
    @XmlElement(name = "UtcDueDate", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar utcDueDate;
    @XmlElement(name = "ReminderSet", namespace = "Tasks")
    @XmlSchemaType(name = "unsignedByte")
    protected Short reminderSet;
    @XmlElement(name = "ReminderTime", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reminderTime;
    @XmlElement(name = "OrdinalDate", namespace = "Tasks")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ordinalDate;
    @XmlElement(name = "SubOrdinalDate", namespace = "Tasks")
    protected String subOrdinalDate;

    /**
     * Ruft den Wert der subject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Legt den Wert der subject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der flagType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagType() {
        return flagType;
    }

    /**
     * Legt den Wert der flagType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagType(String value) {
        this.flagType = value;
    }

    /**
     * Ruft den Wert der dateCompleted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateCompleted() {
        return dateCompleted;
    }

    /**
     * Legt den Wert der dateCompleted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateCompleted(XMLGregorianCalendar value) {
        this.dateCompleted = value;
    }

    /**
     * Ruft den Wert der completeTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCompleteTime() {
        return completeTime;
    }

    /**
     * Legt den Wert der completeTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCompleteTime(XMLGregorianCalendar value) {
        this.completeTime = value;
    }

    /**
     * Ruft den Wert der startDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Legt den Wert der startDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Ruft den Wert der dueDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Legt den Wert der dueDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Ruft den Wert der utcStartDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUtcStartDate() {
        return utcStartDate;
    }

    /**
     * Legt den Wert der utcStartDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUtcStartDate(XMLGregorianCalendar value) {
        this.utcStartDate = value;
    }

    /**
     * Ruft den Wert der utcDueDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUtcDueDate() {
        return utcDueDate;
    }

    /**
     * Legt den Wert der utcDueDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUtcDueDate(XMLGregorianCalendar value) {
        this.utcDueDate = value;
    }

    /**
     * Ruft den Wert der reminderSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getReminderSet() {
        return reminderSet;
    }

    /**
     * Legt den Wert der reminderSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setReminderSet(Short value) {
        this.reminderSet = value;
    }

    /**
     * Ruft den Wert der reminderTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReminderTime() {
        return reminderTime;
    }

    /**
     * Legt den Wert der reminderTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReminderTime(XMLGregorianCalendar value) {
        this.reminderTime = value;
    }

    /**
     * Ruft den Wert der ordinalDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOrdinalDate() {
        return ordinalDate;
    }

    /**
     * Legt den Wert der ordinalDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOrdinalDate(XMLGregorianCalendar value) {
        this.ordinalDate = value;
    }

    /**
     * Ruft den Wert der subOrdinalDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubOrdinalDate() {
        return subOrdinalDate;
    }

    /**
     * Legt den Wert der subOrdinalDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubOrdinalDate(String value) {
        this.subOrdinalDate = value;
    }

}
