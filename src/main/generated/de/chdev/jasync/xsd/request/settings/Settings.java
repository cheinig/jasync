//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.settings;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="RightsManagementInformation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Get" type="{AirSyncBase}EmptyTag"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Oof" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Get" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{Settings}BodyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Set">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{Settings}OofState" minOccurs="0"/>
 *                             &lt;element ref="{Settings}StartTime" minOccurs="0"/>
 *                             &lt;element ref="{Settings}EndTime" minOccurs="0"/>
 *                             &lt;element ref="{Settings}OofMessage" maxOccurs="3" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DevicePassword" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Set">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{Settings}Password"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{Settings}DeviceInformation" minOccurs="0"/>
 *         &lt;element name="UserInformation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Get" type="{AirSyncBase}EmptyTag"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "Settings")
public class Settings {

    @XmlElement(name = "RightsManagementInformation")
    protected Settings.RightsManagementInformation rightsManagementInformation;
    @XmlElement(name = "Oof")
    protected Settings.Oof oof;
    @XmlElement(name = "DevicePassword")
    protected Settings.DevicePassword devicePassword;
    @XmlElement(name = "DeviceInformation")
    protected DeviceInformation deviceInformation;
    @XmlElement(name = "UserInformation")
    protected Settings.UserInformation userInformation;

    /**
     * Ruft den Wert der rightsManagementInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Settings.RightsManagementInformation }
     *     
     */
    public Settings.RightsManagementInformation getRightsManagementInformation() {
        return rightsManagementInformation;
    }

    /**
     * Legt den Wert der rightsManagementInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Settings.RightsManagementInformation }
     *     
     */
    public void setRightsManagementInformation(Settings.RightsManagementInformation value) {
        this.rightsManagementInformation = value;
    }

    /**
     * Ruft den Wert der oof-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Settings.Oof }
     *     
     */
    public Settings.Oof getOof() {
        return oof;
    }

    /**
     * Legt den Wert der oof-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Settings.Oof }
     *     
     */
    public void setOof(Settings.Oof value) {
        this.oof = value;
    }

    /**
     * Ruft den Wert der devicePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Settings.DevicePassword }
     *     
     */
    public Settings.DevicePassword getDevicePassword() {
        return devicePassword;
    }

    /**
     * Legt den Wert der devicePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Settings.DevicePassword }
     *     
     */
    public void setDevicePassword(Settings.DevicePassword value) {
        this.devicePassword = value;
    }

    /**
     * Ruft den Wert der deviceInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceInformation }
     *     
     */
    public DeviceInformation getDeviceInformation() {
        return deviceInformation;
    }

    /**
     * Legt den Wert der deviceInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceInformation }
     *     
     */
    public void setDeviceInformation(DeviceInformation value) {
        this.deviceInformation = value;
    }

    /**
     * Ruft den Wert der userInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Settings.UserInformation }
     *     
     */
    public Settings.UserInformation getUserInformation() {
        return userInformation;
    }

    /**
     * Legt den Wert der userInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Settings.UserInformation }
     *     
     */
    public void setUserInformation(Settings.UserInformation value) {
        this.userInformation = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Set">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{Settings}Password"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "set"
    })
    public static class DevicePassword {

        @XmlElement(name = "Set")
        protected Settings.DevicePassword.Set set;

        /**
         * Ruft den Wert der set-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Settings.DevicePassword.Set }
         *     
         */
        public Settings.DevicePassword.Set getSet() {
            return set;
        }

        /**
         * Legt den Wert der set-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Settings.DevicePassword.Set }
         *     
         */
        public void setSet(Settings.DevicePassword.Set value) {
            this.set = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{Settings}Password"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "password"
        })
        public static class Set {

            @XmlElement(name = "Password", required = true)
            protected String password;

            /**
             * Ruft den Wert der password-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassword() {
                return password;
            }

            /**
             * Legt den Wert der password-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassword(String value) {
                this.password = value;
            }

        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Get" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{Settings}BodyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Set">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{Settings}OofState" minOccurs="0"/>
     *                   &lt;element ref="{Settings}StartTime" minOccurs="0"/>
     *                   &lt;element ref="{Settings}EndTime" minOccurs="0"/>
     *                   &lt;element ref="{Settings}OofMessage" maxOccurs="3" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "get",
        "set"
    })
    public static class Oof {

        @XmlElement(name = "Get")
        protected Settings.Oof.Get get;
        @XmlElement(name = "Set")
        protected Settings.Oof.Set set;

        /**
         * Ruft den Wert der get-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Settings.Oof.Get }
         *     
         */
        public Settings.Oof.Get getGet() {
            return get;
        }

        /**
         * Legt den Wert der get-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Settings.Oof.Get }
         *     
         */
        public void setGet(Settings.Oof.Get value) {
            this.get = value;
        }

        /**
         * Ruft den Wert der set-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Settings.Oof.Set }
         *     
         */
        public Settings.Oof.Set getSet() {
            return set;
        }

        /**
         * Legt den Wert der set-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Settings.Oof.Set }
         *     
         */
        public void setSet(Settings.Oof.Set value) {
            this.set = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{Settings}BodyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "bodyType"
        })
        public static class Get {

            @XmlElement(name = "BodyType", required = true)
            protected String bodyType;

            /**
             * Ruft den Wert der bodyType-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBodyType() {
                return bodyType;
            }

            /**
             * Legt den Wert der bodyType-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBodyType(String value) {
                this.bodyType = value;
            }

        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{Settings}OofState" minOccurs="0"/>
         *         &lt;element ref="{Settings}StartTime" minOccurs="0"/>
         *         &lt;element ref="{Settings}EndTime" minOccurs="0"/>
         *         &lt;element ref="{Settings}OofMessage" maxOccurs="3" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "oofState",
            "startTime",
            "endTime",
            "oofMessage"
        })
        public static class Set {

            @XmlElement(name = "OofState")
            protected BigInteger oofState;
            @XmlElement(name = "StartTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar startTime;
            @XmlElement(name = "EndTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar endTime;
            @XmlElement(name = "OofMessage")
            protected List<OofMessage> oofMessage;

            /**
             * Ruft den Wert der oofState-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getOofState() {
                return oofState;
            }

            /**
             * Legt den Wert der oofState-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setOofState(BigInteger value) {
                this.oofState = value;
            }

            /**
             * Ruft den Wert der startTime-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getStartTime() {
                return startTime;
            }

            /**
             * Legt den Wert der startTime-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setStartTime(XMLGregorianCalendar value) {
                this.startTime = value;
            }

            /**
             * Ruft den Wert der endTime-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEndTime() {
                return endTime;
            }

            /**
             * Legt den Wert der endTime-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEndTime(XMLGregorianCalendar value) {
                this.endTime = value;
            }

            /**
             * Gets the value of the oofMessage property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the oofMessage property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOofMessage().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link OofMessage }
             * 
             * 
             */
            public List<OofMessage> getOofMessage() {
                if (oofMessage == null) {
                    oofMessage = new ArrayList<OofMessage>();
                }
                return this.oofMessage;
            }

        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Get" type="{AirSyncBase}EmptyTag"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "get"
    })
    public static class RightsManagementInformation {

        @XmlElement(name = "Get", required = true)
        protected String get;

        /**
         * Ruft den Wert der get-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGet() {
            return get;
        }

        /**
         * Legt den Wert der get-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGet(String value) {
            this.get = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Get" type="{AirSyncBase}EmptyTag"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "get"
    })
    public static class UserInformation {

        @XmlElement(name = "Get")
        protected String get;

        /**
         * Ruft den Wert der get-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGet() {
            return get;
        }

        /**
         * Legt den Wert der get-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGet(String value) {
            this.get = value;
        }

    }

}
