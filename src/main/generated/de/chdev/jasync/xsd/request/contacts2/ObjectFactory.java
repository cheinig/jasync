//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.contacts2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.contacts2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NickName_QNAME = new QName("Contacts2", "NickName");
    private final static QName _IMAddress3_QNAME = new QName("Contacts2", "IMAddress3");
    private final static QName _IMAddress2_QNAME = new QName("Contacts2", "IMAddress2");
    private final static QName _MMS_QNAME = new QName("Contacts2", "MMS");
    private final static QName _GovernmentId_QNAME = new QName("Contacts2", "GovernmentId");
    private final static QName _ManagerName_QNAME = new QName("Contacts2", "ManagerName");
    private final static QName _CompanyMainPhone_QNAME = new QName("Contacts2", "CompanyMainPhone");
    private final static QName _CustomerId_QNAME = new QName("Contacts2", "CustomerId");
    private final static QName _AccountName_QNAME = new QName("Contacts2", "AccountName");
    private final static QName _IMAddress_QNAME = new QName("Contacts2", "IMAddress");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.contacts2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "NickName")
    public JAXBElement<String> createNickName(String value) {
        return new JAXBElement<String>(_NickName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress3")
    public JAXBElement<String> createIMAddress3(String value) {
        return new JAXBElement<String>(_IMAddress3_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress2")
    public JAXBElement<String> createIMAddress2(String value) {
        return new JAXBElement<String>(_IMAddress2_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "MMS")
    public JAXBElement<String> createMMS(String value) {
        return new JAXBElement<String>(_MMS_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "GovernmentId")
    public JAXBElement<String> createGovernmentId(String value) {
        return new JAXBElement<String>(_GovernmentId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "ManagerName")
    public JAXBElement<String> createManagerName(String value) {
        return new JAXBElement<String>(_ManagerName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "CompanyMainPhone")
    public JAXBElement<String> createCompanyMainPhone(String value) {
        return new JAXBElement<String>(_CompanyMainPhone_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "CustomerId")
    public JAXBElement<String> createCustomerId(String value) {
        return new JAXBElement<String>(_CustomerId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "AccountName")
    public JAXBElement<String> createAccountName(String value) {
        return new JAXBElement<String>(_AccountName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress")
    public JAXBElement<String> createIMAddress(String value) {
        return new JAXBElement<String>(_IMAddress_QNAME, String.class, null, value);
    }

}
