//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.airsync;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.request.airsyncbase.BodyPartPreference;
import de.chdev.jasync.xsd.request.airsyncbase.BodyPreference;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{AirSync}FilterType" minOccurs="0"/>
 *         &lt;element ref="{AirSync}Truncation" minOccurs="0"/>
 *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}BodyPreference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}BodyPartPreference" minOccurs="0"/>
 *         &lt;element ref="{AirSync}Conflict" minOccurs="0"/>
 *         &lt;element ref="{AirSync}MIMESupport" minOccurs="0"/>
 *         &lt;element ref="{AirSync}MIMETruncation" minOccurs="0"/>
 *         &lt;element ref="{AirSync}MaxItems" minOccurs="0"/>
 *         &lt;element ref="{RightsManagement}RightsManagementSupport" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "filterTypeOrTruncationOrClazz"
})
@XmlRootElement(name = "Options")
public class Options {

    @XmlElementRefs({
        @XmlElementRef(name = "FilterType", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Class", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyPartPreference", namespace = "AirSyncBase", type = BodyPartPreference.class, required = false),
        @XmlElementRef(name = "MIMETruncation", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MIMESupport", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MaxItems", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RightsManagementSupport", namespace = "RightsManagement", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyPreference", namespace = "AirSyncBase", type = BodyPreference.class, required = false),
        @XmlElementRef(name = "Conflict", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Truncation", namespace = "AirSync", type = JAXBElement.class, required = false)
    })
    protected List<Object> filterTypeOrTruncationOrClazz;

    /**
     * Gets the value of the filterTypeOrTruncationOrClazz property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filterTypeOrTruncationOrClazz property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterTypeOrTruncationOrClazz().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link BodyPartPreference }
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link BodyPreference }
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     * 
     */
    public List<Object> getFilterTypeOrTruncationOrClazz() {
        if (filterTypeOrTruncationOrClazz == null) {
            filterTypeOrTruncationOrClazz = new ArrayList<Object>();
        }
        return this.filterTypeOrTruncationOrClazz;
    }

}
