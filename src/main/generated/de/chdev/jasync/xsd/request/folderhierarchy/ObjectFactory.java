//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.folderhierarchy;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.folderhierarchy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServerId_QNAME = new QName("FolderHierarchy", "ServerId");
    private final static QName _Status_QNAME = new QName("FolderHierarchy", "Status");
    private final static QName _ParentId_QNAME = new QName("FolderHierarchy", "ParentId");
    private final static QName _SyncKey_QNAME = new QName("FolderHierarchy", "SyncKey");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.folderhierarchy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FolderUpdate }
     * 
     */
    public FolderUpdate createFolderUpdate() {
        return new FolderUpdate();
    }

    /**
     * Create an instance of {@link FolderCreate }
     * 
     */
    public FolderCreate createFolderCreate() {
        return new FolderCreate();
    }

    /**
     * Create an instance of {@link FolderDelete }
     * 
     */
    public FolderDelete createFolderDelete() {
        return new FolderDelete();
    }

    /**
     * Create an instance of {@link FolderSync }
     * 
     */
    public FolderSync createFolderSync() {
        return new FolderSync();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "FolderHierarchy", name = "ServerId")
    public JAXBElement<String> createServerId(String value) {
        return new JAXBElement<String>(_ServerId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "FolderHierarchy", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "FolderHierarchy", name = "ParentId")
    public JAXBElement<String> createParentId(String value) {
        return new JAXBElement<String>(_ParentId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "FolderHierarchy", name = "SyncKey")
    public JAXBElement<String> createSyncKey(String value) {
        return new JAXBElement<String>(_SyncKey_QNAME, String.class, null, value);
    }

}
