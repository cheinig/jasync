//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.getitemestimate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.request.airsync.Options;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Collections">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Collection" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{AirSync}SyncKey"/>
 *                             &lt;element name="CollectionId">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="64"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element ref="{AirSync}FilterType" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}ConversationMode" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}Options" maxOccurs="2" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "collections"
})
@XmlRootElement(name = "GetItemEstimate")
public class GetItemEstimate {

    @XmlElement(name = "Collections", required = true)
    protected GetItemEstimate.Collections collections;

    /**
     * Ruft den Wert der collections-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GetItemEstimate.Collections }
     *     
     */
    public GetItemEstimate.Collections getCollections() {
        return collections;
    }

    /**
     * Legt den Wert der collections-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GetItemEstimate.Collections }
     *     
     */
    public void setCollections(GetItemEstimate.Collections value) {
        this.collections = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Collection" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{AirSync}SyncKey"/>
     *                   &lt;element name="CollectionId">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="64"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element ref="{AirSync}FilterType" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}ConversationMode" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}Options" maxOccurs="2" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "collection"
    })
    public static class Collections {

        @XmlElement(name = "Collection", required = true)
        protected List<GetItemEstimate.Collections.Collection> collection;

        /**
         * Gets the value of the collection property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the collection property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCollection().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetItemEstimate.Collections.Collection }
         * 
         * 
         */
        public List<GetItemEstimate.Collections.Collection> getCollection() {
            if (collection == null) {
                collection = new ArrayList<GetItemEstimate.Collections.Collection>();
            }
            return this.collection;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{AirSync}SyncKey"/>
         *         &lt;element name="CollectionId">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="64"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element ref="{AirSync}FilterType" minOccurs="0"/>
         *         &lt;element ref="{AirSync}ConversationMode" minOccurs="0"/>
         *         &lt;element ref="{AirSync}Options" maxOccurs="2" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "syncKey",
            "collectionId",
            "filterType",
            "conversationMode",
            "options"
        })
        public static class Collection {

            @XmlElement(name = "SyncKey", namespace = "AirSync", required = true)
            protected String syncKey;
            @XmlElement(name = "CollectionId", required = true)
            protected String collectionId;
            @XmlElement(name = "FilterType", namespace = "AirSync")
            protected Short filterType;
            @XmlElement(name = "ConversationMode", namespace = "AirSync")
            protected Boolean conversationMode;
            @XmlElement(name = "Options", namespace = "AirSync")
            protected List<Options> options;

            /**
             * Ruft den Wert der syncKey-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSyncKey() {
                return syncKey;
            }

            /**
             * Legt den Wert der syncKey-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSyncKey(String value) {
                this.syncKey = value;
            }

            /**
             * Ruft den Wert der collectionId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCollectionId() {
                return collectionId;
            }

            /**
             * Legt den Wert der collectionId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCollectionId(String value) {
                this.collectionId = value;
            }

            /**
             * Ruft den Wert der filterType-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Short }
             *     
             */
            public Short getFilterType() {
                return filterType;
            }

            /**
             * Legt den Wert der filterType-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Short }
             *     
             */
            public void setFilterType(Short value) {
                this.filterType = value;
            }

            /**
             * Ruft den Wert der conversationMode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isConversationMode() {
                return conversationMode;
            }

            /**
             * Legt den Wert der conversationMode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setConversationMode(Boolean value) {
                this.conversationMode = value;
            }

            /**
             * Gets the value of the options property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the options property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOptions().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Options }
             * 
             * 
             */
            public List<Options> getOptions() {
                if (options == null) {
                    options = new ArrayList<Options>();
                }
                return this.options;
            }

        }

    }

}
