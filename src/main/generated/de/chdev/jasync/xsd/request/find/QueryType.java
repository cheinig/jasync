//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.find;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für queryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="queryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element name="FreeText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element ref="{AirSync}Class"/>
 *           &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryType", propOrder = {
    "freeTextOrClazzOrCollectionId"
})
public class QueryType {

    @XmlElementRefs({
        @XmlElementRef(name = "Class", namespace = "AirSync", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "FreeText", namespace = "Find", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CollectionId", namespace = "AirSync", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<String>> freeTextOrClazzOrCollectionId;

    /**
     * Gets the value of the freeTextOrClazzOrCollectionId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeTextOrClazzOrCollectionId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeTextOrClazzOrCollectionId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<String>> getFreeTextOrClazzOrCollectionId() {
        if (freeTextOrClazzOrCollectionId == null) {
            freeTextOrClazzOrCollectionId = new ArrayList<JAXBElement<String>>();
        }
        return this.freeTextOrClazzOrCollectionId;
    }

}
