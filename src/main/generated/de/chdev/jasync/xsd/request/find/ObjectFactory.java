//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.find;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.find package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Range_QNAME = new QName("Find", "Range");
    private final static QName _Status_QNAME = new QName("Find", "Status");
    private final static QName _QueryTypeFreeText_QNAME = new QName("Find", "FreeText");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.find
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Find }
     * 
     */
    public Find createFind() {
        return new Find();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch }
     * 
     */
    public Find.ExecuteSearch createFindExecuteSearch() {
        return new Find.ExecuteSearch();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch.GALSearchCriterion }
     * 
     */
    public Find.ExecuteSearch.GALSearchCriterion createFindExecuteSearchGALSearchCriterion() {
        return new Find.ExecuteSearch.GALSearchCriterion();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch.GALSearchCriterion.Options }
     * 
     */
    public Find.ExecuteSearch.GALSearchCriterion.Options createFindExecuteSearchGALSearchCriterionOptions() {
        return new Find.ExecuteSearch.GALSearchCriterion.Options();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch.MailBoxSearchCriterion }
     * 
     */
    public Find.ExecuteSearch.MailBoxSearchCriterion createFindExecuteSearchMailBoxSearchCriterion() {
        return new Find.ExecuteSearch.MailBoxSearchCriterion();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch.MailBoxSearchCriterion.Options }
     * 
     */
    public Find.ExecuteSearch.MailBoxSearchCriterion.Options createFindExecuteSearchMailBoxSearchCriterionOptions() {
        return new Find.ExecuteSearch.MailBoxSearchCriterion.Options();
    }

    /**
     * Create an instance of {@link EmptyTag }
     * 
     */
    public EmptyTag createEmptyTag() {
        return new EmptyTag();
    }

    /**
     * Create an instance of {@link QueryType }
     * 
     */
    public QueryType createQueryType() {
        return new QueryType();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch.GALSearchCriterion.Options.Picture }
     * 
     */
    public Find.ExecuteSearch.GALSearchCriterion.Options.Picture createFindExecuteSearchGALSearchCriterionOptionsPicture() {
        return new Find.ExecuteSearch.GALSearchCriterion.Options.Picture();
    }

    /**
     * Create an instance of {@link Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture }
     * 
     */
    public Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture createFindExecuteSearchMailBoxSearchCriterionOptionsPicture() {
        return new Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "Range")
    public JAXBElement<String> createRange(String value) {
        return new JAXBElement<String>(_Range_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Find", name = "FreeText", scope = QueryType.class)
    public JAXBElement<String> createQueryTypeFreeText(String value) {
        return new JAXBElement<String>(_QueryTypeFreeText_QNAME, String.class, QueryType.class, value);
    }

}
