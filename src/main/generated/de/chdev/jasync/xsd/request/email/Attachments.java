//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.email;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Attachment" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AttName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AttSize" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="AttMethod" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attachment"
})
@XmlRootElement(name = "Attachments")
public class Attachments {

    @XmlElement(name = "Attachment", required = true)
    protected List<Attachments.Attachment> attachment;

    /**
     * Gets the value of the attachment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attachments.Attachment }
     * 
     * 
     */
    public List<Attachments.Attachment> getAttachment() {
        if (attachment == null) {
            attachment = new ArrayList<Attachments.Attachment>();
        }
        return this.attachment;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AttName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AttSize" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="AttMethod" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "attName",
        "attSize",
        "attMethod",
        "displayName"
    })
    public static class Attachment {

        @XmlElement(name = "AttName", required = true)
        protected String attName;
        @XmlElement(name = "AttSize", required = true)
        protected BigInteger attSize;
        @XmlElement(name = "AttMethod")
        @XmlSchemaType(name = "unsignedByte")
        protected short attMethod;
        @XmlElement(name = "DisplayName")
        protected String displayName;

        /**
         * Ruft den Wert der attName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAttName() {
            return attName;
        }

        /**
         * Legt den Wert der attName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAttName(String value) {
            this.attName = value;
        }

        /**
         * Ruft den Wert der attSize-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getAttSize() {
            return attSize;
        }

        /**
         * Legt den Wert der attSize-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setAttSize(BigInteger value) {
            this.attSize = value;
        }

        /**
         * Ruft den Wert der attMethod-Eigenschaft ab.
         * 
         */
        public short getAttMethod() {
            return attMethod;
        }

        /**
         * Legt den Wert der attMethod-Eigenschaft fest.
         * 
         */
        public void setAttMethod(short value) {
            this.attMethod = value;
        }

        /**
         * Ruft den Wert der displayName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * Legt den Wert der displayName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayName(String value) {
            this.displayName = value;
        }

    }

}
