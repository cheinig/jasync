//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.tasks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="Start" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Until" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Occurrences" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="Interval" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="DayOfWeek" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="DayOfMonth" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="WeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="MonthOfYear" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="Regenerate" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="DeadOccur" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="CalendarType" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="IsLeapMonth" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="FirstDayOfWeek" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "Recurrence")
public class Recurrence {

    @XmlElement(name = "Type")
    @XmlSchemaType(name = "unsignedByte")
    protected short type;
    @XmlElement(name = "Start", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar start;
    @XmlElement(name = "Until")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar until;
    @XmlElement(name = "Occurrences")
    @XmlSchemaType(name = "unsignedByte")
    protected Short occurrences;
    @XmlElement(name = "Interval")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer interval;
    @XmlElement(name = "DayOfWeek")
    @XmlSchemaType(name = "unsignedByte")
    protected Short dayOfWeek;
    @XmlElement(name = "DayOfMonth")
    @XmlSchemaType(name = "unsignedByte")
    protected Short dayOfMonth;
    @XmlElement(name = "WeekOfMonth")
    @XmlSchemaType(name = "unsignedByte")
    protected Short weekOfMonth;
    @XmlElement(name = "MonthOfYear")
    @XmlSchemaType(name = "unsignedByte")
    protected Short monthOfYear;
    @XmlElement(name = "Regenerate")
    @XmlSchemaType(name = "unsignedByte")
    protected Short regenerate;
    @XmlElement(name = "DeadOccur")
    @XmlSchemaType(name = "unsignedByte")
    protected Short deadOccur;
    @XmlElement(name = "CalendarType")
    @XmlSchemaType(name = "unsignedByte")
    protected Short calendarType;
    @XmlElement(name = "IsLeapMonth")
    @XmlSchemaType(name = "unsignedByte")
    protected Short isLeapMonth;
    @XmlElement(name = "FirstDayOfWeek")
    protected Short firstDayOfWeek;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     */
    public short getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     */
    public void setType(short value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der start-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStart() {
        return start;
    }

    /**
     * Legt den Wert der start-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStart(XMLGregorianCalendar value) {
        this.start = value;
    }

    /**
     * Ruft den Wert der until-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUntil() {
        return until;
    }

    /**
     * Legt den Wert der until-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUntil(XMLGregorianCalendar value) {
        this.until = value;
    }

    /**
     * Ruft den Wert der occurrences-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getOccurrences() {
        return occurrences;
    }

    /**
     * Legt den Wert der occurrences-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setOccurrences(Short value) {
        this.occurrences = value;
    }

    /**
     * Ruft den Wert der interval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInterval() {
        return interval;
    }

    /**
     * Legt den Wert der interval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInterval(Integer value) {
        this.interval = value;
    }

    /**
     * Ruft den Wert der dayOfWeek-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * Legt den Wert der dayOfWeek-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDayOfWeek(Short value) {
        this.dayOfWeek = value;
    }

    /**
     * Ruft den Wert der dayOfMonth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDayOfMonth() {
        return dayOfMonth;
    }

    /**
     * Legt den Wert der dayOfMonth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDayOfMonth(Short value) {
        this.dayOfMonth = value;
    }

    /**
     * Ruft den Wert der weekOfMonth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getWeekOfMonth() {
        return weekOfMonth;
    }

    /**
     * Legt den Wert der weekOfMonth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setWeekOfMonth(Short value) {
        this.weekOfMonth = value;
    }

    /**
     * Ruft den Wert der monthOfYear-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getMonthOfYear() {
        return monthOfYear;
    }

    /**
     * Legt den Wert der monthOfYear-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setMonthOfYear(Short value) {
        this.monthOfYear = value;
    }

    /**
     * Ruft den Wert der regenerate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getRegenerate() {
        return regenerate;
    }

    /**
     * Legt den Wert der regenerate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setRegenerate(Short value) {
        this.regenerate = value;
    }

    /**
     * Ruft den Wert der deadOccur-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDeadOccur() {
        return deadOccur;
    }

    /**
     * Legt den Wert der deadOccur-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDeadOccur(Short value) {
        this.deadOccur = value;
    }

    /**
     * Ruft den Wert der calendarType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCalendarType() {
        return calendarType;
    }

    /**
     * Legt den Wert der calendarType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCalendarType(Short value) {
        this.calendarType = value;
    }

    /**
     * Ruft den Wert der isLeapMonth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsLeapMonth() {
        return isLeapMonth;
    }

    /**
     * Legt den Wert der isLeapMonth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsLeapMonth(Short value) {
        this.isLeapMonth = value;
    }

    /**
     * Ruft den Wert der firstDayOfWeek-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    /**
     * Legt den Wert der firstDayOfWeek-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setFirstDayOfWeek(Short value) {
        this.firstDayOfWeek = value;
    }

}
