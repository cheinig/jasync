//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.validatecert;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.validatecert package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.validatecert
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidateCert }
     * 
     */
    public ValidateCert createValidateCert() {
        return new ValidateCert();
    }

    /**
     * Create an instance of {@link ValidateCert.CertificateChain }
     * 
     */
    public ValidateCert.CertificateChain createValidateCertCertificateChain() {
        return new ValidateCert.CertificateChain();
    }

    /**
     * Create an instance of {@link ValidateCert.Certificates }
     * 
     */
    public ValidateCert.Certificates createValidateCertCertificates() {
        return new ValidateCert.Certificates();
    }

}
