//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.itemoperations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import de.chdev.jasync.xsd.request.airsyncbase.Body;
import de.chdev.jasync.xsd.request.airsyncbase.BodyPart;
import de.chdev.jasync.xsd.request.airsyncbase.Location;
import de.chdev.jasync.xsd.request.calendar.Attendees;
import de.chdev.jasync.xsd.request.calendar.Exceptions;
import de.chdev.jasync.xsd.request.contacts.Children;
import de.chdev.jasync.xsd.request.email.Flag;
import de.chdev.jasync.xsd.request.email.MeetingRequest;
import de.chdev.jasync.xsd.request.rightsmanagement.RightsManagementLicense;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{ItemOperations}Range" minOccurs="0"/>
 *         &lt;element ref="{ItemOperations}Total" minOccurs="0"/>
 *         &lt;element ref="{ItemOperations}Data" minOccurs="0"/>
 *         &lt;element ref="{ItemOperations}Part" minOccurs="0"/>
 *         &lt;element ref="{ItemOperations}Version" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}BodyPart" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}Attachments" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}NativeBodyType" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}ContentType" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
 *         &lt;group ref="{Calendar}AllProps"/>
 *         &lt;group ref="{Contacts}AllProps"/>
 *         &lt;group ref="{Contacts2}AllProps"/>
 *         &lt;group ref="{DocumentLibrary}AllProps"/>
 *         &lt;group ref="{Email}AllProps"/>
 *         &lt;group ref="{Email2}AllProps"/>
 *         &lt;group ref="{Tasks}AllProps"/>
 *         &lt;element ref="{RightsManagement}RightsManagementLicense" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rangeOrTotalOrData"
})
@XmlRootElement(name = "Properties")
public class Properties {

    @XmlElementRefs({
        @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Send", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
        @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "LastVerbExecuted", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Body", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "WeightedRank", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DisplayTo", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IsHidden", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IsFolder", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "LastVerbExecutionTime", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Attachments", namespace = "Email", type = de.chdev.jasync.xsd.request.email.Attachments.class, required = false),
        @XmlElementRef(name = "BodyTruncated", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Attachments", namespace = "AirSyncBase", type = de.chdev.jasync.xsd.request.airsyncbase.Attachments.class, required = false),
        @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.request.calendar.Recurrence.class, required = false),
        @XmlElementRef(name = "ResponseType", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.request.contacts.Categories.class, required = false),
        @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Body", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MIMEData", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodySize", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Sender", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
        @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyTruncated", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "LinkId", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CalendarType", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CreationDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IsLeapMonth", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MeetingRequest", namespace = "Email", type = MeetingRequest.class, required = false),
        @XmlElementRef(name = "BodySize", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DisplayName", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AppointmentReplyTime", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OnlineMeetingConfLink", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
        @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ContentLength", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyPart", namespace = "AirSyncBase", type = BodyPart.class, required = false),
        @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MIMESize", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "LastModifiedDate", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Data", namespace = "ItemOperations", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ContentType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Alias", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UmAttOrder", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Version", namespace = "ItemOperations", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyTruncated", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UmAttDuration", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.request.tasks.Recurrence.class, required = false),
        @XmlElementRef(name = "Body", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ContentClass", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RightsManagementLicense", namespace = "RightsManagement", type = RightsManagementLicense.class, required = false),
        @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Body", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ContentType", namespace = "DocumentLibrary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AccountId", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Bcc", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IsDraft", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categories", namespace = "Email", type = de.chdev.jasync.xsd.request.email.Categories.class, required = false),
        @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodyTruncated", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Total", namespace = "ItemOperations", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ReceivedAsBcc", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Part", namespace = "ItemOperations", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "FirstDayOfWeek", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MessageClass", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ThreadTopic", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categories", namespace = "Calendar", type = de.chdev.jasync.xsd.request.calendar.Categories.class, required = false),
        @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UmUserNotes", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OnlineMeetingExternalLink", namespace = "Calendar", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
        @XmlElementRef(name = "SubOrdinalDate", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
        @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "UmCallerID", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MeetingMessageType", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
        @XmlElementRef(name = "MIMETruncated", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Range", namespace = "ItemOperations", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "BodySize", namespace = "Tasks", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ConversationId", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ConversationIndex", namespace = "Email2", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.request.tasks.Categories.class, required = false),
        @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false)
    })
    protected List<Object> rangeOrTotalOrData;

    /**
     * Gets the value of the rangeOrTotalOrData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeOrTotalOrData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeOrTotalOrData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Location }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link de.chdev.jasync.xsd.request.email.Attachments }
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link de.chdev.jasync.xsd.request.airsyncbase.Attachments }
     * {@link de.chdev.jasync.xsd.request.calendar.Recurrence }
     * {@link JAXBElement }{@code <}{@link Long }{@code >}
     * {@link de.chdev.jasync.xsd.request.contacts.Categories }
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Flag }
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link MeetingRequest }
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Attendees }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link BodyPart }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link Long }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link de.chdev.jasync.xsd.request.tasks.Recurrence }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link RightsManagementLicense }
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link de.chdev.jasync.xsd.request.email.Categories }
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link de.chdev.jasync.xsd.request.calendar.Categories }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Body }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Exceptions }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link Children }
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * {@link de.chdev.jasync.xsd.request.tasks.Categories }
     * {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     * 
     */
    public List<Object> getRangeOrTotalOrData() {
        if (rangeOrTotalOrData == null) {
            rangeOrTotalOrData = new ArrayList<Object>();
        }
        return this.rangeOrTotalOrData;
    }

}
