//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.itemoperations;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.request.airsyncbase.BodyPartPreference;
import de.chdev.jasync.xsd.request.airsyncbase.BodyPreference;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="EmptyFolderContents">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{AirSync}CollectionId"/>
 *                   &lt;element name="Options" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element ref="{ItemOperations}DeleteSubFolders"/>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{ItemOperations}Store"/>
 *                   &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
 *                   &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
 *                   &lt;element ref="{DocumentLibrary}LinkId" minOccurs="0"/>
 *                   &lt;element ref="{Search}LongId" minOccurs="0"/>
 *                   &lt;element ref="{AirSyncBase}FileReference" minOccurs="0"/>
 *                   &lt;element name="Options" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice maxOccurs="unbounded">
 *                             &lt;element ref="{ItemOperations}Schema" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element ref="{ItemOperations}Range" minOccurs="0"/>
 *                             &lt;element ref="{ItemOperations}UserName" minOccurs="0"/>
 *                             &lt;element ref="{ItemOperations}Password" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}MIMESupport" minOccurs="0"/>
 *                             &lt;element ref="{AirSyncBase}BodyPreference" maxOccurs="256" minOccurs="0"/>
 *                             &lt;element ref="{AirSyncBase}BodyPartPreference" minOccurs="0"/>
 *                             &lt;element ref="{RightsManagement}RightsManagementSupport" minOccurs="0"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element ref="{RightsManagement}RemoveRightsManagementProtection" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Move">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element ref="{ItemOperations}ConversationId"/>
 *                   &lt;element ref="{ItemOperations}DstFldId"/>
 *                   &lt;element name="Options" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element ref="{ItemOperations}MoveAlways" minOccurs="0"/>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "emptyFolderContentsOrFetchOrMove"
})
@XmlRootElement(name = "ItemOperations")
public class ItemOperations {

    @XmlElements({
        @XmlElement(name = "EmptyFolderContents", type = ItemOperations.EmptyFolderContents.class),
        @XmlElement(name = "Fetch", type = ItemOperations.Fetch.class),
        @XmlElement(name = "Move", type = ItemOperations.Move.class)
    })
    protected List<Object> emptyFolderContentsOrFetchOrMove;

    /**
     * Gets the value of the emptyFolderContentsOrFetchOrMove property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emptyFolderContentsOrFetchOrMove property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmptyFolderContentsOrFetchOrMove().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemOperations.EmptyFolderContents }
     * {@link ItemOperations.Fetch }
     * {@link ItemOperations.Move }
     * 
     * 
     */
    public List<Object> getEmptyFolderContentsOrFetchOrMove() {
        if (emptyFolderContentsOrFetchOrMove == null) {
            emptyFolderContentsOrFetchOrMove = new ArrayList<Object>();
        }
        return this.emptyFolderContentsOrFetchOrMove;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{AirSync}CollectionId"/>
     *         &lt;element name="Options" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element ref="{ItemOperations}DeleteSubFolders"/>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EmptyFolderContents {

        @XmlElement(name = "CollectionId", namespace = "AirSync", required = true)
        protected String collectionId;
        @XmlElement(name = "Options")
        protected ItemOperations.EmptyFolderContents.Options options;

        /**
         * Ruft den Wert der collectionId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCollectionId() {
            return collectionId;
        }

        /**
         * Legt den Wert der collectionId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCollectionId(String value) {
            this.collectionId = value;
        }

        /**
         * Ruft den Wert der options-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ItemOperations.EmptyFolderContents.Options }
         *     
         */
        public ItemOperations.EmptyFolderContents.Options getOptions() {
            return options;
        }

        /**
         * Legt den Wert der options-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemOperations.EmptyFolderContents.Options }
         *     
         */
        public void setOptions(ItemOperations.EmptyFolderContents.Options value) {
            this.options = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element ref="{ItemOperations}DeleteSubFolders"/>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class Options {

            @XmlElement(name = "DeleteSubFolders", required = true)
            protected String deleteSubFolders;

            /**
             * Ruft den Wert der deleteSubFolders-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDeleteSubFolders() {
                return deleteSubFolders;
            }

            /**
             * Legt den Wert der deleteSubFolders-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDeleteSubFolders(String value) {
                this.deleteSubFolders = value;
            }

        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{ItemOperations}Store"/>
     *         &lt;element ref="{AirSync}ServerId" minOccurs="0"/>
     *         &lt;element ref="{AirSync}CollectionId" minOccurs="0"/>
     *         &lt;element ref="{DocumentLibrary}LinkId" minOccurs="0"/>
     *         &lt;element ref="{Search}LongId" minOccurs="0"/>
     *         &lt;element ref="{AirSyncBase}FileReference" minOccurs="0"/>
     *         &lt;element name="Options" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice maxOccurs="unbounded">
     *                   &lt;element ref="{ItemOperations}Schema" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element ref="{ItemOperations}Range" minOccurs="0"/>
     *                   &lt;element ref="{ItemOperations}UserName" minOccurs="0"/>
     *                   &lt;element ref="{ItemOperations}Password" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}MIMESupport" minOccurs="0"/>
     *                   &lt;element ref="{AirSyncBase}BodyPreference" maxOccurs="256" minOccurs="0"/>
     *                   &lt;element ref="{AirSyncBase}BodyPartPreference" minOccurs="0"/>
     *                   &lt;element ref="{RightsManagement}RightsManagementSupport" minOccurs="0"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element ref="{RightsManagement}RemoveRightsManagementProtection" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Fetch {

        @XmlElement(name = "Store", required = true)
        protected String store;
        @XmlElement(name = "ServerId", namespace = "AirSync")
        protected String serverId;
        @XmlElement(name = "CollectionId", namespace = "AirSync")
        protected String collectionId;
        @XmlElement(name = "LinkId", namespace = "DocumentLibrary")
        protected String linkId;
        @XmlElement(name = "LongId", namespace = "Search")
        protected String longId;
        @XmlElement(name = "FileReference", namespace = "AirSyncBase")
        protected String fileReference;
        @XmlElement(name = "Options")
        protected ItemOperations.Fetch.Options options;
        @XmlElement(name = "RemoveRightsManagementProtection", namespace = "RightsManagement")
        protected String removeRightsManagementProtection;

        /**
         * Ruft den Wert der store-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStore() {
            return store;
        }

        /**
         * Legt den Wert der store-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStore(String value) {
            this.store = value;
        }

        /**
         * Ruft den Wert der serverId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServerId() {
            return serverId;
        }

        /**
         * Legt den Wert der serverId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServerId(String value) {
            this.serverId = value;
        }

        /**
         * Ruft den Wert der collectionId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCollectionId() {
            return collectionId;
        }

        /**
         * Legt den Wert der collectionId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCollectionId(String value) {
            this.collectionId = value;
        }

        /**
         * Ruft den Wert der linkId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLinkId() {
            return linkId;
        }

        /**
         * Legt den Wert der linkId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLinkId(String value) {
            this.linkId = value;
        }

        /**
         * Ruft den Wert der longId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLongId() {
            return longId;
        }

        /**
         * Legt den Wert der longId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLongId(String value) {
            this.longId = value;
        }

        /**
         * Ruft den Wert der fileReference-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileReference() {
            return fileReference;
        }

        /**
         * Legt den Wert der fileReference-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileReference(String value) {
            this.fileReference = value;
        }

        /**
         * Ruft den Wert der options-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ItemOperations.Fetch.Options }
         *     
         */
        public ItemOperations.Fetch.Options getOptions() {
            return options;
        }

        /**
         * Legt den Wert der options-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemOperations.Fetch.Options }
         *     
         */
        public void setOptions(ItemOperations.Fetch.Options value) {
            this.options = value;
        }

        /**
         * Ruft den Wert der removeRightsManagementProtection-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemoveRightsManagementProtection() {
            return removeRightsManagementProtection;
        }

        /**
         * Legt den Wert der removeRightsManagementProtection-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemoveRightsManagementProtection(String value) {
            this.removeRightsManagementProtection = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice maxOccurs="unbounded">
         *         &lt;element ref="{ItemOperations}Schema" maxOccurs="unbounded" minOccurs="0"/>
         *         &lt;element ref="{ItemOperations}Range" minOccurs="0"/>
         *         &lt;element ref="{ItemOperations}UserName" minOccurs="0"/>
         *         &lt;element ref="{ItemOperations}Password" minOccurs="0"/>
         *         &lt;element ref="{AirSync}MIMESupport" minOccurs="0"/>
         *         &lt;element ref="{AirSyncBase}BodyPreference" maxOccurs="256" minOccurs="0"/>
         *         &lt;element ref="{AirSyncBase}BodyPartPreference" minOccurs="0"/>
         *         &lt;element ref="{RightsManagement}RightsManagementSupport" minOccurs="0"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "schemaOrRangeOrUserName"
        })
        public static class Options {

            @XmlElementRefs({
                @XmlElementRef(name = "BodyPartPreference", namespace = "AirSyncBase", type = BodyPartPreference.class, required = false),
                @XmlElementRef(name = "Range", namespace = "ItemOperations", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "MIMESupport", namespace = "AirSync", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "Password", namespace = "ItemOperations", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "RightsManagementSupport", namespace = "RightsManagement", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "BodyPreference", namespace = "AirSyncBase", type = BodyPreference.class, required = false),
                @XmlElementRef(name = "UserName", namespace = "ItemOperations", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "Schema", namespace = "ItemOperations", type = Schema.class, required = false)
            })
            protected List<Object> schemaOrRangeOrUserName;

            /**
             * Gets the value of the schemaOrRangeOrUserName property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the schemaOrRangeOrUserName property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSchemaOrRangeOrUserName().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link BodyPartPreference }
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link Short }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
             * {@link BodyPreference }
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link Schema }
             * 
             * 
             */
            public List<Object> getSchemaOrRangeOrUserName() {
                if (schemaOrRangeOrUserName == null) {
                    schemaOrRangeOrUserName = new ArrayList<Object>();
                }
                return this.schemaOrRangeOrUserName;
            }

        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element ref="{ItemOperations}ConversationId"/>
     *         &lt;element ref="{ItemOperations}DstFldId"/>
     *         &lt;element name="Options" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element ref="{ItemOperations}MoveAlways" minOccurs="0"/>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Move {

        @XmlElement(name = "ConversationId", required = true)
        protected String conversationId;
        @XmlElement(name = "DstFldId", required = true)
        protected String dstFldId;
        @XmlElement(name = "Options")
        protected ItemOperations.Move.Options options;

        /**
         * Ruft den Wert der conversationId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConversationId() {
            return conversationId;
        }

        /**
         * Legt den Wert der conversationId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConversationId(String value) {
            this.conversationId = value;
        }

        /**
         * Ruft den Wert der dstFldId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDstFldId() {
            return dstFldId;
        }

        /**
         * Legt den Wert der dstFldId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDstFldId(String value) {
            this.dstFldId = value;
        }

        /**
         * Ruft den Wert der options-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ItemOperations.Move.Options }
         *     
         */
        public ItemOperations.Move.Options getOptions() {
            return options;
        }

        /**
         * Legt den Wert der options-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemOperations.Move.Options }
         *     
         */
        public void setOptions(ItemOperations.Move.Options value) {
            this.options = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element ref="{ItemOperations}MoveAlways" minOccurs="0"/>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class Options {

            @XmlElement(name = "MoveAlways")
            protected String moveAlways;

            /**
             * Ruft den Wert der moveAlways-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoveAlways() {
                return moveAlways;
            }

            /**
             * Legt den Wert der moveAlways-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoveAlways(String value) {
                this.moveAlways = value;
            }

        }

    }

}
