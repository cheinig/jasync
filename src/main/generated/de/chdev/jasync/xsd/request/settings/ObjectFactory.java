//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.settings;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.settings package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BodyType_QNAME = new QName("Settings", "BodyType");
    private final static QName _EndTime_QNAME = new QName("Settings", "EndTime");
    private final static QName _Model_QNAME = new QName("Settings", "Model");
    private final static QName _OSLanguage_QNAME = new QName("Settings", "OSLanguage");
    private final static QName _UserAgent_QNAME = new QName("Settings", "UserAgent");
    private final static QName _MobileOperator_QNAME = new QName("Settings", "MobileOperator");
    private final static QName _OS_QNAME = new QName("Settings", "OS");
    private final static QName _Status_QNAME = new QName("Settings", "Status");
    private final static QName _EnableOutboundSMS_QNAME = new QName("Settings", "EnableOutboundSMS");
    private final static QName _PhoneNumber_QNAME = new QName("Settings", "PhoneNumber");
    private final static QName _StartTime_QNAME = new QName("Settings", "StartTime");
    private final static QName _FriendlyName_QNAME = new QName("Settings", "FriendlyName");
    private final static QName _Password_QNAME = new QName("Settings", "Password");
    private final static QName _OofState_QNAME = new QName("Settings", "OofState");
    private final static QName _IMEI_QNAME = new QName("Settings", "IMEI");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.settings
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Accounts }
     * 
     */
    public Accounts createAccounts() {
        return new Accounts();
    }

    /**
     * Create an instance of {@link DeviceInformation }
     * 
     */
    public DeviceInformation createDeviceInformation() {
        return new DeviceInformation();
    }

    /**
     * Create an instance of {@link Settings }
     * 
     */
    public Settings createSettings() {
        return new Settings();
    }

    /**
     * Create an instance of {@link Settings.DevicePassword }
     * 
     */
    public Settings.DevicePassword createSettingsDevicePassword() {
        return new Settings.DevicePassword();
    }

    /**
     * Create an instance of {@link Settings.Oof }
     * 
     */
    public Settings.Oof createSettingsOof() {
        return new Settings.Oof();
    }

    /**
     * Create an instance of {@link OofMessage }
     * 
     */
    public OofMessage createOofMessage() {
        return new OofMessage();
    }

    /**
     * Create an instance of {@link EmailAddresses }
     * 
     */
    public EmailAddresses createEmailAddresses() {
        return new EmailAddresses();
    }

    /**
     * Create an instance of {@link Accounts.Account }
     * 
     */
    public Accounts.Account createAccountsAccount() {
        return new Accounts.Account();
    }

    /**
     * Create an instance of {@link DeviceInformation.Set }
     * 
     */
    public DeviceInformation.Set createDeviceInformationSet() {
        return new DeviceInformation.Set();
    }

    /**
     * Create an instance of {@link Settings.RightsManagementInformation }
     * 
     */
    public Settings.RightsManagementInformation createSettingsRightsManagementInformation() {
        return new Settings.RightsManagementInformation();
    }

    /**
     * Create an instance of {@link Settings.UserInformation }
     * 
     */
    public Settings.UserInformation createSettingsUserInformation() {
        return new Settings.UserInformation();
    }

    /**
     * Create an instance of {@link Settings.DevicePassword.Set }
     * 
     */
    public Settings.DevicePassword.Set createSettingsDevicePasswordSet() {
        return new Settings.DevicePassword.Set();
    }

    /**
     * Create an instance of {@link Settings.Oof.Get }
     * 
     */
    public Settings.Oof.Get createSettingsOofGet() {
        return new Settings.Oof.Get();
    }

    /**
     * Create an instance of {@link Settings.Oof.Set }
     * 
     */
    public Settings.Oof.Set createSettingsOofSet() {
        return new Settings.Oof.Set();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "BodyType")
    public JAXBElement<String> createBodyType(String value) {
        return new JAXBElement<String>(_BodyType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "EndTime")
    public JAXBElement<XMLGregorianCalendar> createEndTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_EndTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "Model")
    public JAXBElement<String> createModel(String value) {
        return new JAXBElement<String>(_Model_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "OSLanguage")
    public JAXBElement<String> createOSLanguage(String value) {
        return new JAXBElement<String>(_OSLanguage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "UserAgent")
    public JAXBElement<String> createUserAgent(String value) {
        return new JAXBElement<String>(_UserAgent_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "MobileOperator")
    public JAXBElement<String> createMobileOperator(String value) {
        return new JAXBElement<String>(_MobileOperator_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "OS")
    public JAXBElement<String> createOS(String value) {
        return new JAXBElement<String>(_OS_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "EnableOutboundSMS")
    public JAXBElement<Integer> createEnableOutboundSMS(Integer value) {
        return new JAXBElement<Integer>(_EnableOutboundSMS_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "PhoneNumber")
    public JAXBElement<String> createPhoneNumber(String value) {
        return new JAXBElement<String>(_PhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "StartTime")
    public JAXBElement<XMLGregorianCalendar> createStartTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_StartTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "FriendlyName")
    public JAXBElement<String> createFriendlyName(String value) {
        return new JAXBElement<String>(_FriendlyName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "Password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_Password_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "OofState")
    public JAXBElement<BigInteger> createOofState(BigInteger value) {
        return new JAXBElement<BigInteger>(_OofState_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Settings", name = "IMEI")
    public JAXBElement<String> createIMEI(String value) {
        return new JAXBElement<String>(_IMEI_QNAME, String.class, null, value);
    }

}
