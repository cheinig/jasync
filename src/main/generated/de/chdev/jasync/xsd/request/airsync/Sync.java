//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.airsync;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import de.chdev.jasync.xsd.request.airsyncbase.Body;
import de.chdev.jasync.xsd.request.airsyncbase.Location;
import de.chdev.jasync.xsd.request.calendar.Attendees;
import de.chdev.jasync.xsd.request.calendar.Exceptions;
import de.chdev.jasync.xsd.request.contacts.Children;
import de.chdev.jasync.xsd.request.email.Flag;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Collections" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Collection" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{AirSync}SyncKey"/>
 *                             &lt;element ref="{AirSync}CollectionId"/>
 *                             &lt;element ref="{AirSync}Supported" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}DeletesAsMoves" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}GetChanges" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}WindowSize" minOccurs="0"/>
 *                             &lt;element name="ConversationMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                             &lt;element ref="{AirSync}Options" maxOccurs="2" minOccurs="0"/>
 *                             &lt;element name="Commands" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;choice maxOccurs="unbounded">
 *                                       &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element ref="{AirSync}ServerId"/>
 *                                                 &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
 *                                                 &lt;element name="ApplicationData">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;choice maxOccurs="unbounded">
 *                                                             &lt;group ref="{AirSync}ItemProperties"/>
 *                                                             &lt;element ref="{Email}To"/>
 *                                                             &lt;element ref="{Email}Cc"/>
 *                                                             &lt;element ref="{Email2}Bcc"/>
 *                                                             &lt;element ref="{Email}Subject"/>
 *                                                             &lt;element ref="{Email}Importance"/>
 *                                                             &lt;element ref="{Email}ReplyTo"/>
 *                                                             &lt;element ref="{Email}Categories"/>
 *                                                             &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
 *                                                           &lt;/choice>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element ref="{Email2}Send" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element ref="{AirSync}ServerId"/>
 *                                                 &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element ref="{AirSync}Class" minOccurs="0"/>
 *                                                 &lt;element ref="{AirSync}ClientId"/>
 *                                                 &lt;element name="ApplicationData">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;choice maxOccurs="unbounded">
 *                                                             &lt;group ref="{AirSync}ItemProperties"/>
 *                                                             &lt;element ref="{Email}To"/>
 *                                                             &lt;element ref="{Email}From"/>
 *                                                             &lt;element ref="{Email}DateReceived"/>
 *                                                             &lt;element ref="{Email}InternetCPID"/>
 *                                                             &lt;element ref="{Email}Importance"/>
 *                                                           &lt;/choice>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element ref="{Email2}Send" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element ref="{AirSync}ServerId"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/choice>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{AirSync}Wait" minOccurs="0"/>
 *         &lt;element ref="{AirSync}HeartbeatInterval" minOccurs="0"/>
 *         &lt;element ref="{AirSync}WindowSize" minOccurs="0"/>
 *         &lt;element ref="{AirSync}Partial" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "collections",
    "wait",
    "heartbeatInterval",
    "windowSize",
    "partial"
})
@XmlRootElement(name = "Sync")
public class Sync {

    @XmlElement(name = "Collections")
    protected Sync.Collections collections;
    @XmlElement(name = "Wait")
    protected Integer wait;
    @XmlElement(name = "HeartbeatInterval")
    protected Integer heartbeatInterval;
    @XmlElement(name = "WindowSize")
    protected Integer windowSize;
    @XmlElement(name = "Partial")
    protected String partial;

    /**
     * Ruft den Wert der collections-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Sync.Collections }
     *     
     */
    public Sync.Collections getCollections() {
        return collections;
    }

    /**
     * Legt den Wert der collections-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Sync.Collections }
     *     
     */
    public void setCollections(Sync.Collections value) {
        this.collections = value;
    }

    /**
     * Ruft den Wert der wait-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWait() {
        return wait;
    }

    /**
     * Legt den Wert der wait-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWait(Integer value) {
        this.wait = value;
    }

    /**
     * Ruft den Wert der heartbeatInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHeartbeatInterval() {
        return heartbeatInterval;
    }

    /**
     * Legt den Wert der heartbeatInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHeartbeatInterval(Integer value) {
        this.heartbeatInterval = value;
    }

    /**
     * Ruft den Wert der windowSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWindowSize() {
        return windowSize;
    }

    /**
     * Legt den Wert der windowSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWindowSize(Integer value) {
        this.windowSize = value;
    }

    /**
     * Ruft den Wert der partial-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartial() {
        return partial;
    }

    /**
     * Legt den Wert der partial-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartial(String value) {
        this.partial = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Collection" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{AirSync}SyncKey"/>
     *                   &lt;element ref="{AirSync}CollectionId"/>
     *                   &lt;element ref="{AirSync}Supported" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}DeletesAsMoves" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}GetChanges" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}WindowSize" minOccurs="0"/>
     *                   &lt;element name="ConversationMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                   &lt;element ref="{AirSync}Options" maxOccurs="2" minOccurs="0"/>
     *                   &lt;element name="Commands" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;choice maxOccurs="unbounded">
     *                             &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element ref="{AirSync}ServerId"/>
     *                                       &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
     *                                       &lt;element name="ApplicationData">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;choice maxOccurs="unbounded">
     *                                                   &lt;group ref="{AirSync}ItemProperties"/>
     *                                                   &lt;element ref="{Email}To"/>
     *                                                   &lt;element ref="{Email}Cc"/>
     *                                                   &lt;element ref="{Email2}Bcc"/>
     *                                                   &lt;element ref="{Email}Subject"/>
     *                                                   &lt;element ref="{Email}Importance"/>
     *                                                   &lt;element ref="{Email}ReplyTo"/>
     *                                                   &lt;element ref="{Email}Categories"/>
     *                                                   &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
     *                                                 &lt;/choice>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element ref="{Email2}Send" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element ref="{AirSync}ServerId"/>
     *                                       &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element ref="{AirSync}Class" minOccurs="0"/>
     *                                       &lt;element ref="{AirSync}ClientId"/>
     *                                       &lt;element name="ApplicationData">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;choice maxOccurs="unbounded">
     *                                                   &lt;group ref="{AirSync}ItemProperties"/>
     *                                                   &lt;element ref="{Email}To"/>
     *                                                   &lt;element ref="{Email}From"/>
     *                                                   &lt;element ref="{Email}DateReceived"/>
     *                                                   &lt;element ref="{Email}InternetCPID"/>
     *                                                   &lt;element ref="{Email}Importance"/>
     *                                                 &lt;/choice>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element ref="{Email2}Send" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element ref="{AirSync}ServerId"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/choice>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "collection"
    })
    public static class Collections {

        @XmlElement(name = "Collection", required = true)
        protected List<Sync.Collections.Collection> collection;

        /**
         * Gets the value of the collection property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the collection property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCollection().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Sync.Collections.Collection }
         * 
         * 
         */
        public List<Sync.Collections.Collection> getCollection() {
            if (collection == null) {
                collection = new ArrayList<Sync.Collections.Collection>();
            }
            return this.collection;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{AirSync}SyncKey"/>
         *         &lt;element ref="{AirSync}CollectionId"/>
         *         &lt;element ref="{AirSync}Supported" minOccurs="0"/>
         *         &lt;element ref="{AirSync}DeletesAsMoves" minOccurs="0"/>
         *         &lt;element ref="{AirSync}GetChanges" minOccurs="0"/>
         *         &lt;element ref="{AirSync}WindowSize" minOccurs="0"/>
         *         &lt;element name="ConversationMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *         &lt;element ref="{AirSync}Options" maxOccurs="2" minOccurs="0"/>
         *         &lt;element name="Commands" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;choice maxOccurs="unbounded">
         *                   &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element ref="{AirSync}ServerId"/>
         *                             &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
         *                             &lt;element name="ApplicationData">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;choice maxOccurs="unbounded">
         *                                         &lt;group ref="{AirSync}ItemProperties"/>
         *                                         &lt;element ref="{Email}To"/>
         *                                         &lt;element ref="{Email}Cc"/>
         *                                         &lt;element ref="{Email2}Bcc"/>
         *                                         &lt;element ref="{Email}Subject"/>
         *                                         &lt;element ref="{Email}Importance"/>
         *                                         &lt;element ref="{Email}ReplyTo"/>
         *                                         &lt;element ref="{Email}Categories"/>
         *                                         &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
         *                                       &lt;/choice>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element ref="{Email2}Send" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element ref="{AirSync}ServerId"/>
         *                             &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element ref="{AirSync}Class" minOccurs="0"/>
         *                             &lt;element ref="{AirSync}ClientId"/>
         *                             &lt;element name="ApplicationData">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;choice maxOccurs="unbounded">
         *                                         &lt;group ref="{AirSync}ItemProperties"/>
         *                                         &lt;element ref="{Email}To"/>
         *                                         &lt;element ref="{Email}From"/>
         *                                         &lt;element ref="{Email}DateReceived"/>
         *                                         &lt;element ref="{Email}InternetCPID"/>
         *                                         &lt;element ref="{Email}Importance"/>
         *                                       &lt;/choice>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element ref="{Email2}Send" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element ref="{AirSync}ServerId"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/choice>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "syncKey",
            "collectionId",
            "supported",
            "deletesAsMoves",
            "getChanges",
            "windowSize",
            "conversationMode",
            "options",
            "commands"
        })
        public static class Collection {

            @XmlElement(name = "SyncKey", required = true)
            protected String syncKey;
            @XmlElement(name = "CollectionId", required = true)
            protected String collectionId;
            @XmlElement(name = "Supported")
            protected Supported supported;
            @XmlElement(name = "DeletesAsMoves")
            protected Boolean deletesAsMoves;
            @XmlElement(name = "GetChanges")
            protected Boolean getChanges;
            @XmlElement(name = "WindowSize")
            protected Integer windowSize;
            @XmlElement(name = "ConversationMode")
            protected Boolean conversationMode;
            @XmlElement(name = "Options")
            protected List<Options> options;
            @XmlElement(name = "Commands")
            protected Sync.Collections.Collection.Commands commands;

            /**
             * Ruft den Wert der syncKey-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSyncKey() {
                return syncKey;
            }

            /**
             * Legt den Wert der syncKey-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSyncKey(String value) {
                this.syncKey = value;
            }

            /**
             * Ruft den Wert der collectionId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCollectionId() {
                return collectionId;
            }

            /**
             * Legt den Wert der collectionId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCollectionId(String value) {
                this.collectionId = value;
            }

            /**
             * Ruft den Wert der supported-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Supported }
             *     
             */
            public Supported getSupported() {
                return supported;
            }

            /**
             * Legt den Wert der supported-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Supported }
             *     
             */
            public void setSupported(Supported value) {
                this.supported = value;
            }

            /**
             * Ruft den Wert der deletesAsMoves-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDeletesAsMoves() {
                return deletesAsMoves;
            }

            /**
             * Legt den Wert der deletesAsMoves-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDeletesAsMoves(Boolean value) {
                this.deletesAsMoves = value;
            }

            /**
             * Ruft den Wert der getChanges-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isGetChanges() {
                return getChanges;
            }

            /**
             * Legt den Wert der getChanges-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setGetChanges(Boolean value) {
                this.getChanges = value;
            }

            /**
             * Ruft den Wert der windowSize-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getWindowSize() {
                return windowSize;
            }

            /**
             * Legt den Wert der windowSize-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setWindowSize(Integer value) {
                this.windowSize = value;
            }

            /**
             * Ruft den Wert der conversationMode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isConversationMode() {
                return conversationMode;
            }

            /**
             * Legt den Wert der conversationMode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setConversationMode(Boolean value) {
                this.conversationMode = value;
            }

            /**
             * Gets the value of the options property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the options property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOptions().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Options }
             * 
             * 
             */
            public List<Options> getOptions() {
                if (options == null) {
                    options = new ArrayList<Options>();
                }
                return this.options;
            }

            /**
             * Ruft den Wert der commands-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Sync.Collections.Collection.Commands }
             *     
             */
            public Sync.Collections.Collection.Commands getCommands() {
                return commands;
            }

            /**
             * Legt den Wert der commands-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Sync.Collections.Collection.Commands }
             *     
             */
            public void setCommands(Sync.Collections.Collection.Commands value) {
                this.commands = value;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;choice maxOccurs="unbounded">
             *         &lt;element name="Change" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}ServerId"/>
             *                   &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
             *                   &lt;element name="ApplicationData">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;choice maxOccurs="unbounded">
             *                               &lt;group ref="{AirSync}ItemProperties"/>
             *                               &lt;element ref="{Email}To"/>
             *                               &lt;element ref="{Email}Cc"/>
             *                               &lt;element ref="{Email2}Bcc"/>
             *                               &lt;element ref="{Email}Subject"/>
             *                               &lt;element ref="{Email}Importance"/>
             *                               &lt;element ref="{Email}ReplyTo"/>
             *                               &lt;element ref="{Email}Categories"/>
             *                               &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
             *                             &lt;/choice>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element ref="{Email2}Send" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Delete" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}ServerId"/>
             *                   &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Add" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}Class" minOccurs="0"/>
             *                   &lt;element ref="{AirSync}ClientId"/>
             *                   &lt;element name="ApplicationData">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;choice maxOccurs="unbounded">
             *                               &lt;group ref="{AirSync}ItemProperties"/>
             *                               &lt;element ref="{Email}To"/>
             *                               &lt;element ref="{Email}From"/>
             *                               &lt;element ref="{Email}DateReceived"/>
             *                               &lt;element ref="{Email}InternetCPID"/>
             *                               &lt;element ref="{Email}Importance"/>
             *                             &lt;/choice>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element ref="{Email2}Send" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Fetch" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element ref="{AirSync}ServerId"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/choice>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "changeOrDeleteOrAdd"
            })
            public static class Commands {

                @XmlElements({
                    @XmlElement(name = "Change", type = Sync.Collections.Collection.Commands.Change.class),
                    @XmlElement(name = "Delete", type = Sync.Collections.Collection.Commands.Delete.class),
                    @XmlElement(name = "Add", type = Sync.Collections.Collection.Commands.Add.class),
                    @XmlElement(name = "Fetch", type = Sync.Collections.Collection.Commands.Fetch.class)
                })
                protected List<Object> changeOrDeleteOrAdd;

                /**
                 * Gets the value of the changeOrDeleteOrAdd property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the changeOrDeleteOrAdd property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getChangeOrDeleteOrAdd().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Sync.Collections.Collection.Commands.Change }
                 * {@link Sync.Collections.Collection.Commands.Delete }
                 * {@link Sync.Collections.Collection.Commands.Add }
                 * {@link Sync.Collections.Collection.Commands.Fetch }
                 * 
                 * 
                 */
                public List<Object> getChangeOrDeleteOrAdd() {
                    if (changeOrDeleteOrAdd == null) {
                        changeOrDeleteOrAdd = new ArrayList<Object>();
                    }
                    return this.changeOrDeleteOrAdd;
                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}Class" minOccurs="0"/>
                 *         &lt;element ref="{AirSync}ClientId"/>
                 *         &lt;element name="ApplicationData">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;choice maxOccurs="unbounded">
                 *                     &lt;group ref="{AirSync}ItemProperties"/>
                 *                     &lt;element ref="{Email}To"/>
                 *                     &lt;element ref="{Email}From"/>
                 *                     &lt;element ref="{Email}DateReceived"/>
                 *                     &lt;element ref="{Email}InternetCPID"/>
                 *                     &lt;element ref="{Email}Importance"/>
                 *                   &lt;/choice>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element ref="{Email2}Send" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "clazz",
                    "clientId",
                    "applicationData",
                    "send"
                })
                public static class Add {

                    @XmlElement(name = "Class")
                    protected String clazz;
                    @XmlElement(name = "ClientId", required = true)
                    protected String clientId;
                    @XmlElement(name = "ApplicationData", required = true)
                    protected Sync.Collections.Collection.Commands.Add.ApplicationData applicationData;
                    @XmlElement(name = "Send", namespace = "Email2")
                    protected Object send;

                    /**
                     * Ruft den Wert der clazz-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClazz() {
                        return clazz;
                    }

                    /**
                     * Legt den Wert der clazz-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClazz(String value) {
                        this.clazz = value;
                    }

                    /**
                     * Ruft den Wert der clientId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClientId() {
                        return clientId;
                    }

                    /**
                     * Legt den Wert der clientId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClientId(String value) {
                        this.clientId = value;
                    }

                    /**
                     * Ruft den Wert der applicationData-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Sync.Collections.Collection.Commands.Add.ApplicationData }
                     *     
                     */
                    public Sync.Collections.Collection.Commands.Add.ApplicationData getApplicationData() {
                        return applicationData;
                    }

                    /**
                     * Legt den Wert der applicationData-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Sync.Collections.Collection.Commands.Add.ApplicationData }
                     *     
                     */
                    public void setApplicationData(Sync.Collections.Collection.Commands.Add.ApplicationData value) {
                        this.applicationData = value;
                    }

                    /**
                     * Ruft den Wert der send-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getSend() {
                        return send;
                    }

                    /**
                     * Legt den Wert der send-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setSend(Object value) {
                        this.send = value;
                    }


                    /**
                     * <p>Java-Klasse für anonymous complex type.
                     * 
                     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;choice maxOccurs="unbounded">
                     *           &lt;group ref="{AirSync}ItemProperties"/>
                     *           &lt;element ref="{Email}To"/>
                     *           &lt;element ref="{Email}From"/>
                     *           &lt;element ref="{Email}DateReceived"/>
                     *           &lt;element ref="{Email}InternetCPID"/>
                     *           &lt;element ref="{Email}Importance"/>
                     *         &lt;/choice>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "nativeBodyTypeOrBodyOrOrganizerName"
                    })
                    public static class ApplicationData {

                        @XmlElementRefs({
                            @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Notes", type = de.chdev.jasync.xsd.request.notes.Categories.class, required = false),
                            @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.request.calendar.Recurrence.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.request.contacts.Categories.class, required = false),
                            @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
                            @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
                            @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "InternetCPID", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.request.tasks.Recurrence.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateReceived", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "From", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Calendar", type = de.chdev.jasync.xsd.request.calendar.Categories.class, required = false),
                            @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
                            @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
                            @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
                            @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.request.tasks.Categories.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false)
                        })
                        protected List<Object> nativeBodyTypeOrBodyOrOrganizerName;

                        /**
                         * Gets the value of the nativeBodyTypeOrBodyOrOrganizerName property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the nativeBodyTypeOrBodyOrOrganizerName property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getNativeBodyTypeOrBodyOrOrganizerName().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.notes.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.calendar.Recurrence }
                         * {@link de.chdev.jasync.xsd.request.contacts.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Flag }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Attendees }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.tasks.Recurrence }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link de.chdev.jasync.xsd.request.calendar.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Body }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Exceptions }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Children }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link de.chdev.jasync.xsd.request.tasks.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * 
                         * 
                         */
                        public List<Object> getNativeBodyTypeOrBodyOrOrganizerName() {
                            if (nativeBodyTypeOrBodyOrOrganizerName == null) {
                                nativeBodyTypeOrBodyOrOrganizerName = new ArrayList<Object>();
                            }
                            return this.nativeBodyTypeOrBodyOrOrganizerName;
                        }

                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
                 *         &lt;element name="ApplicationData">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;choice maxOccurs="unbounded">
                 *                     &lt;group ref="{AirSync}ItemProperties"/>
                 *                     &lt;element ref="{Email}To"/>
                 *                     &lt;element ref="{Email}Cc"/>
                 *                     &lt;element ref="{Email2}Bcc"/>
                 *                     &lt;element ref="{Email}Subject"/>
                 *                     &lt;element ref="{Email}Importance"/>
                 *                     &lt;element ref="{Email}ReplyTo"/>
                 *                     &lt;element ref="{Email}Categories"/>
                 *                     &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
                 *                   &lt;/choice>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element ref="{Email2}Send" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId",
                    "instanceId",
                    "applicationData",
                    "send"
                })
                public static class Change {

                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;
                    @XmlElement(name = "InstanceId", namespace = "AirSyncBase")
                    protected String instanceId;
                    @XmlElement(name = "ApplicationData", required = true)
                    protected Sync.Collections.Collection.Commands.Change.ApplicationData applicationData;
                    @XmlElement(name = "Send", namespace = "Email2")
                    protected Object send;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der instanceId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInstanceId() {
                        return instanceId;
                    }

                    /**
                     * Legt den Wert der instanceId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInstanceId(String value) {
                        this.instanceId = value;
                    }

                    /**
                     * Ruft den Wert der applicationData-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Sync.Collections.Collection.Commands.Change.ApplicationData }
                     *     
                     */
                    public Sync.Collections.Collection.Commands.Change.ApplicationData getApplicationData() {
                        return applicationData;
                    }

                    /**
                     * Legt den Wert der applicationData-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Sync.Collections.Collection.Commands.Change.ApplicationData }
                     *     
                     */
                    public void setApplicationData(Sync.Collections.Collection.Commands.Change.ApplicationData value) {
                        this.applicationData = value;
                    }

                    /**
                     * Ruft den Wert der send-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getSend() {
                        return send;
                    }

                    /**
                     * Legt den Wert der send-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setSend(Object value) {
                        this.send = value;
                    }


                    /**
                     * <p>Java-Klasse für anonymous complex type.
                     * 
                     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;choice maxOccurs="unbounded">
                     *           &lt;group ref="{AirSync}ItemProperties"/>
                     *           &lt;element ref="{Email}To"/>
                     *           &lt;element ref="{Email}Cc"/>
                     *           &lt;element ref="{Email2}Bcc"/>
                     *           &lt;element ref="{Email}Subject"/>
                     *           &lt;element ref="{Email}Importance"/>
                     *           &lt;element ref="{Email}ReplyTo"/>
                     *           &lt;element ref="{Email}Categories"/>
                     *           &lt;element ref="{AirSyncBase}Location" minOccurs="0"/>
                     *         &lt;/choice>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "nativeBodyTypeOrBodyOrOrganizerName"
                    })
                    public static class ApplicationData {

                        @XmlElementRefs({
                            @XmlElementRef(name = "Location", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NickName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Suffix", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FileAs", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Spouse", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DisallowNewTimeProposal", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "EndTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress3", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Complete", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Location", namespace = "AirSyncBase", type = Location.class, required = false),
                            @XmlElementRef(name = "ReminderSet", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Department", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DtStamp", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Notes", type = de.chdev.jasync.xsd.request.notes.Categories.class, required = false),
                            @XmlElementRef(name = "MMS", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Calendar", type = de.chdev.jasync.xsd.request.calendar.Recurrence.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Contacts", type = de.chdev.jasync.xsd.request.contacts.Categories.class, required = false),
                            @XmlElementRef(name = "BusyStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiFirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ReplyTo", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiCompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartTime", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress2", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Flag", namespace = "Email", type = Flag.class, required = false),
                            @XmlElementRef(name = "AllDayEvent", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Home2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MobilePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerEmail", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email1Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcDueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OrganizerName", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastModifiedDate", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "IMAddress", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MessageClass", namespace = "Notes", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Attendees", namespace = "Calendar", type = Attendees.class, required = false),
                            @XmlElementRef(name = "Business2PhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Timezone", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "WebPage", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessFaxNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "StartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Anniversary", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "GovernmentId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UtcStartDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Picture", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "To", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MiddleName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OfficeLocation", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "MeetingStatus", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Reminder", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Birthday", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CarPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Recurrence", namespace = "Tasks", type = de.chdev.jasync.xsd.request.tasks.Recurrence.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "UID", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email2Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "FirstName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "JobTitle", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AccountName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomeAddressState", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressPostalCode", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Email3Address", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "AssistantPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Bcc", namespace = "Email2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Email", type = de.chdev.jasync.xsd.request.email.Categories.class, required = false),
                            @XmlElementRef(name = "ReminderTime", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "NativeBodyType", namespace = "AirSyncBase", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Read", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CustomerId", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DueDate", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Subject", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "BusinessAddressCity", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "ResponseRequested", namespace = "Calendar", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "CompanyMainPhone", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "DateCompleted", namespace = "Tasks", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Calendar", type = de.chdev.jasync.xsd.request.calendar.Categories.class, required = false),
                            @XmlElementRef(name = "Cc", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "LastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "YomiLastName", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "RadioPhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Body", namespace = "AirSyncBase", type = Body.class, required = false),
                            @XmlElementRef(name = "Title", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Exceptions", namespace = "Calendar", type = Exceptions.class, required = false),
                            @XmlElementRef(name = "PagerNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "HomePhoneNumber", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Children", namespace = "Contacts", type = Children.class, required = false),
                            @XmlElementRef(name = "ManagerName", namespace = "Contacts2", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressCountry", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "OtherAddressStreet", namespace = "Contacts", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Importance", namespace = "Email", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "Categories", namespace = "Tasks", type = de.chdev.jasync.xsd.request.tasks.Categories.class, required = false),
                            @XmlElementRef(name = "Sensitivity", namespace = "Calendar", type = JAXBElement.class, required = false)
                        })
                        protected List<Object> nativeBodyTypeOrBodyOrOrganizerName;

                        /**
                         * Gets the value of the nativeBodyTypeOrBodyOrOrganizerName property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the nativeBodyTypeOrBodyOrOrganizerName property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getNativeBodyTypeOrBodyOrOrganizerName().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link Location }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.notes.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.calendar.Recurrence }
                         * {@link de.chdev.jasync.xsd.request.contacts.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Flag }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Attendees }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Long }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.tasks.Recurrence }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link de.chdev.jasync.xsd.request.email.Categories }
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
                         * {@link de.chdev.jasync.xsd.request.calendar.Categories }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Body }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Exceptions }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link Children }
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link String }{@code >}
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * {@link de.chdev.jasync.xsd.request.tasks.Categories }
                         * {@link JAXBElement }{@code <}{@link Short }{@code >}
                         * 
                         * 
                         */
                        public List<Object> getNativeBodyTypeOrBodyOrOrganizerName() {
                            if (nativeBodyTypeOrBodyOrOrganizerName == null) {
                                nativeBodyTypeOrBodyOrOrganizerName = new ArrayList<Object>();
                            }
                            return this.nativeBodyTypeOrBodyOrOrganizerName;
                        }

                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *         &lt;element ref="{AirSyncBase}InstanceId" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId",
                    "instanceId"
                })
                public static class Delete {

                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;
                    @XmlElement(name = "InstanceId", namespace = "AirSyncBase")
                    protected String instanceId;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                    /**
                     * Ruft den Wert der instanceId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInstanceId() {
                        return instanceId;
                    }

                    /**
                     * Legt den Wert der instanceId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInstanceId(String value) {
                        this.instanceId = value;
                    }

                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element ref="{AirSync}ServerId"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serverId"
                })
                public static class Fetch {

                    @XmlElement(name = "ServerId", required = true)
                    protected String serverId;

                    /**
                     * Ruft den Wert der serverId-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServerId() {
                        return serverId;
                    }

                    /**
                     * Legt den Wert der serverId-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServerId(String value) {
                        this.serverId = value;
                    }

                }

            }

        }

    }

}
