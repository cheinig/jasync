//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.meetingresponse;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.meetingresponse package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProposedStartTime_QNAME = new QName("MeetingResponse", "ProposedStartTime");
    private final static QName _ProposedEndTime_QNAME = new QName("MeetingResponse", "ProposedEndTime");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.meetingresponse
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MeetingResponse }
     * 
     */
    public MeetingResponse createMeetingResponse() {
        return new MeetingResponse();
    }

    /**
     * Create an instance of {@link MeetingResponse.Request }
     * 
     */
    public MeetingResponse.Request createMeetingResponseRequest() {
        return new MeetingResponse.Request();
    }

    /**
     * Create an instance of {@link MeetingResponse.Request.SendResponse }
     * 
     */
    public MeetingResponse.Request.SendResponse createMeetingResponseRequestSendResponse() {
        return new MeetingResponse.Request.SendResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "MeetingResponse", name = "ProposedStartTime")
    public JAXBElement<String> createProposedStartTime(String value) {
        return new JAXBElement<String>(_ProposedStartTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "MeetingResponse", name = "ProposedEndTime")
    public JAXBElement<String> createProposedEndTime(String value) {
        return new JAXBElement<String>(_ProposedEndTime_QNAME, String.class, null, value);
    }

}
