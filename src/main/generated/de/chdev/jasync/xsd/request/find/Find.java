//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.find;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}- [a-fA-F0-9]{12}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ExecuteSearch">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="MailBoxSearchCriterion" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element name="Query" type="{Find}queryType"/>
 *                             &lt;element name="Options" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element ref="{Find}Range"/>
 *                                       &lt;element name="DeepTraversal" type="{Find}EmptyTag" minOccurs="0"/>
 *                                       &lt;element name="Picture" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;all>
 *                                                 &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                                                 &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                                               &lt;/all>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="GALSearchCriterion" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element name="Query">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="4"/>
 *                                   &lt;maxLength value="256"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="Options" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element ref="{Find}Range"/>
 *                                       &lt;element name="Picture" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;all>
 *                                                 &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                                                 &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *                                               &lt;/all>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchId",
    "executeSearch"
})
@XmlRootElement(name = "Find")
public class Find {

    @XmlElement(name = "SearchId", required = true)
    protected String searchId;
    @XmlElement(name = "ExecuteSearch", required = true)
    protected Find.ExecuteSearch executeSearch;

    /**
     * Ruft den Wert der searchId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchId() {
        return searchId;
    }

    /**
     * Legt den Wert der searchId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchId(String value) {
        this.searchId = value;
    }

    /**
     * Ruft den Wert der executeSearch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Find.ExecuteSearch }
     *     
     */
    public Find.ExecuteSearch getExecuteSearch() {
        return executeSearch;
    }

    /**
     * Legt den Wert der executeSearch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Find.ExecuteSearch }
     *     
     */
    public void setExecuteSearch(Find.ExecuteSearch value) {
        this.executeSearch = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="MailBoxSearchCriterion" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element name="Query" type="{Find}queryType"/>
     *                   &lt;element name="Options" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element ref="{Find}Range"/>
     *                             &lt;element name="DeepTraversal" type="{Find}EmptyTag" minOccurs="0"/>
     *                             &lt;element name="Picture" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;all>
     *                                       &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *                                       &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *                                     &lt;/all>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="GALSearchCriterion" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element name="Query">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="4"/>
     *                         &lt;maxLength value="256"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="Options" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element ref="{Find}Range"/>
     *                             &lt;element name="Picture" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;all>
     *                                       &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *                                       &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
     *                                     &lt;/all>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mailBoxSearchCriterion",
        "galSearchCriterion"
    })
    public static class ExecuteSearch {

        @XmlElement(name = "MailBoxSearchCriterion")
        protected Find.ExecuteSearch.MailBoxSearchCriterion mailBoxSearchCriterion;
        @XmlElement(name = "GALSearchCriterion")
        protected Find.ExecuteSearch.GALSearchCriterion galSearchCriterion;

        /**
         * Ruft den Wert der mailBoxSearchCriterion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Find.ExecuteSearch.MailBoxSearchCriterion }
         *     
         */
        public Find.ExecuteSearch.MailBoxSearchCriterion getMailBoxSearchCriterion() {
            return mailBoxSearchCriterion;
        }

        /**
         * Legt den Wert der mailBoxSearchCriterion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Find.ExecuteSearch.MailBoxSearchCriterion }
         *     
         */
        public void setMailBoxSearchCriterion(Find.ExecuteSearch.MailBoxSearchCriterion value) {
            this.mailBoxSearchCriterion = value;
        }

        /**
         * Ruft den Wert der galSearchCriterion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Find.ExecuteSearch.GALSearchCriterion }
         *     
         */
        public Find.ExecuteSearch.GALSearchCriterion getGALSearchCriterion() {
            return galSearchCriterion;
        }

        /**
         * Legt den Wert der galSearchCriterion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Find.ExecuteSearch.GALSearchCriterion }
         *     
         */
        public void setGALSearchCriterion(Find.ExecuteSearch.GALSearchCriterion value) {
            this.galSearchCriterion = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element name="Query">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="4"/>
         *               &lt;maxLength value="256"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="Options" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element ref="{Find}Range"/>
         *                   &lt;element name="Picture" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;all>
         *                             &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
         *                             &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
         *                           &lt;/all>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class GALSearchCriterion {

            @XmlElement(name = "Query", required = true)
            protected String query;
            @XmlElement(name = "Options")
            protected Find.ExecuteSearch.GALSearchCriterion.Options options;

            /**
             * Ruft den Wert der query-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuery() {
                return query;
            }

            /**
             * Legt den Wert der query-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuery(String value) {
                this.query = value;
            }

            /**
             * Ruft den Wert der options-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Find.ExecuteSearch.GALSearchCriterion.Options }
             *     
             */
            public Find.ExecuteSearch.GALSearchCriterion.Options getOptions() {
                return options;
            }

            /**
             * Legt den Wert der options-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Find.ExecuteSearch.GALSearchCriterion.Options }
             *     
             */
            public void setOptions(Find.ExecuteSearch.GALSearchCriterion.Options value) {
                this.options = value;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element ref="{Find}Range"/>
             *         &lt;element name="Picture" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;all>
             *                   &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
             *                   &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
             *                 &lt;/all>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "range",
                "picture"
            })
            public static class Options {

                @XmlElement(name = "Range", required = true, nillable = true)
                protected String range;
                @XmlElement(name = "Picture")
                protected Find.ExecuteSearch.GALSearchCriterion.Options.Picture picture;

                /**
                 * Ruft den Wert der range-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRange() {
                    return range;
                }

                /**
                 * Legt den Wert der range-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRange(String value) {
                    this.range = value;
                }

                /**
                 * Ruft den Wert der picture-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Find.ExecuteSearch.GALSearchCriterion.Options.Picture }
                 *     
                 */
                public Find.ExecuteSearch.GALSearchCriterion.Options.Picture getPicture() {
                    return picture;
                }

                /**
                 * Legt den Wert der picture-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Find.ExecuteSearch.GALSearchCriterion.Options.Picture }
                 *     
                 */
                public void setPicture(Find.ExecuteSearch.GALSearchCriterion.Options.Picture value) {
                    this.picture = value;
                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;all>
                 *         &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
                 *         &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
                 *       &lt;/all>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {

                })
                public static class Picture {

                    @XmlElement(name = "MaxSize")
                    @XmlSchemaType(name = "unsignedInt")
                    protected Long maxSize;
                    @XmlElement(name = "MaxPictures")
                    @XmlSchemaType(name = "unsignedInt")
                    protected Long maxPictures;

                    /**
                     * Ruft den Wert der maxSize-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    public Long getMaxSize() {
                        return maxSize;
                    }

                    /**
                     * Legt den Wert der maxSize-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setMaxSize(Long value) {
                        this.maxSize = value;
                    }

                    /**
                     * Ruft den Wert der maxPictures-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    public Long getMaxPictures() {
                        return maxPictures;
                    }

                    /**
                     * Legt den Wert der maxPictures-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setMaxPictures(Long value) {
                        this.maxPictures = value;
                    }

                }

            }

        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element name="Query" type="{Find}queryType"/>
         *         &lt;element name="Options" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element ref="{Find}Range"/>
         *                   &lt;element name="DeepTraversal" type="{Find}EmptyTag" minOccurs="0"/>
         *                   &lt;element name="Picture" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;all>
         *                             &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
         *                             &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
         *                           &lt;/all>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class MailBoxSearchCriterion {

            @XmlElement(name = "Query", required = true)
            protected QueryType query;
            @XmlElement(name = "Options")
            protected Find.ExecuteSearch.MailBoxSearchCriterion.Options options;

            /**
             * Ruft den Wert der query-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link QueryType }
             *     
             */
            public QueryType getQuery() {
                return query;
            }

            /**
             * Legt den Wert der query-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link QueryType }
             *     
             */
            public void setQuery(QueryType value) {
                this.query = value;
            }

            /**
             * Ruft den Wert der options-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Find.ExecuteSearch.MailBoxSearchCriterion.Options }
             *     
             */
            public Find.ExecuteSearch.MailBoxSearchCriterion.Options getOptions() {
                return options;
            }

            /**
             * Legt den Wert der options-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Find.ExecuteSearch.MailBoxSearchCriterion.Options }
             *     
             */
            public void setOptions(Find.ExecuteSearch.MailBoxSearchCriterion.Options value) {
                this.options = value;
            }


            /**
             * <p>Java-Klasse für anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element ref="{Find}Range"/>
             *         &lt;element name="DeepTraversal" type="{Find}EmptyTag" minOccurs="0"/>
             *         &lt;element name="Picture" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;all>
             *                   &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
             *                   &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
             *                 &lt;/all>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "range",
                "deepTraversal",
                "picture"
            })
            public static class Options {

                @XmlElement(name = "Range", required = true, nillable = true)
                protected String range;
                @XmlElement(name = "DeepTraversal")
                protected EmptyTag deepTraversal;
                @XmlElement(name = "Picture")
                protected Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture picture;

                /**
                 * Ruft den Wert der range-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRange() {
                    return range;
                }

                /**
                 * Legt den Wert der range-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRange(String value) {
                    this.range = value;
                }

                /**
                 * Ruft den Wert der deepTraversal-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link EmptyTag }
                 *     
                 */
                public EmptyTag getDeepTraversal() {
                    return deepTraversal;
                }

                /**
                 * Legt den Wert der deepTraversal-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EmptyTag }
                 *     
                 */
                public void setDeepTraversal(EmptyTag value) {
                    this.deepTraversal = value;
                }

                /**
                 * Ruft den Wert der picture-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture }
                 *     
                 */
                public Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture getPicture() {
                    return picture;
                }

                /**
                 * Legt den Wert der picture-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture }
                 *     
                 */
                public void setPicture(Find.ExecuteSearch.MailBoxSearchCriterion.Options.Picture value) {
                    this.picture = value;
                }


                /**
                 * <p>Java-Klasse für anonymous complex type.
                 * 
                 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;all>
                 *         &lt;element name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
                 *         &lt;element name="MaxPictures" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
                 *       &lt;/all>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {

                })
                public static class Picture {

                    @XmlElement(name = "MaxSize")
                    @XmlSchemaType(name = "unsignedInt")
                    protected Long maxSize;
                    @XmlElement(name = "MaxPictures")
                    @XmlSchemaType(name = "unsignedInt")
                    protected Long maxPictures;

                    /**
                     * Ruft den Wert der maxSize-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    public Long getMaxSize() {
                        return maxSize;
                    }

                    /**
                     * Legt den Wert der maxSize-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setMaxSize(Long value) {
                        this.maxSize = value;
                    }

                    /**
                     * Ruft den Wert der maxPictures-Eigenschaft ab.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    public Long getMaxPictures() {
                        return maxPictures;
                    }

                    /**
                     * Legt den Wert der maxPictures-Eigenschaft fest.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setMaxPictures(Long value) {
                        this.maxPictures = value;
                    }

                }

            }

        }

    }

}
