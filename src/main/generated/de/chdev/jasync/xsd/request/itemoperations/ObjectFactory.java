//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.itemoperations;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.itemoperations package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DstFldId_QNAME = new QName("ItemOperations", "DstFldId");
    private final static QName _MoveAlways_QNAME = new QName("ItemOperations", "MoveAlways");
    private final static QName _Store_QNAME = new QName("ItemOperations", "Store");
    private final static QName _Part_QNAME = new QName("ItemOperations", "Part");
    private final static QName _Status_QNAME = new QName("ItemOperations", "Status");
    private final static QName _Password_QNAME = new QName("ItemOperations", "Password");
    private final static QName _UserName_QNAME = new QName("ItemOperations", "UserName");
    private final static QName _Version_QNAME = new QName("ItemOperations", "Version");
    private final static QName _ConversationId_QNAME = new QName("ItemOperations", "ConversationId");
    private final static QName _Total_QNAME = new QName("ItemOperations", "Total");
    private final static QName _Data_QNAME = new QName("ItemOperations", "Data");
    private final static QName _DeleteSubFolders_QNAME = new QName("ItemOperations", "DeleteSubFolders");
    private final static QName _Range_QNAME = new QName("ItemOperations", "Range");
    private final static QName _SchemaCc_QNAME = new QName("Email", "Cc");
    private final static QName _SchemaReminder_QNAME = new QName("Calendar", "Reminder");
    private final static QName _SchemaWebpage_QNAME = new QName("Contacts", "Webpage");
    private final static QName _SchemaEmail1Address_QNAME = new QName("Contacts", "Email1Address");
    private final static QName _SchemaDateReceived_QNAME = new QName("Email", "DateReceived");
    private final static QName _SchemaDisplayTo_QNAME = new QName("Email", "DisplayTo");
    private final static QName _SchemaMobileTelephoneNumber_QNAME = new QName("Contacts", "MobileTelephoneNumber");
    private final static QName _SchemaOfficeLocation_QNAME = new QName("Contacts", "OfficeLocation");
    private final static QName _SchemaEmail3Address_QNAME = new QName("Contacts", "Email3Address");
    private final static QName _SchemaBodyPart_QNAME = new QName("AirSyncBase", "BodyPart");
    private final static QName _SchemaOrganizerEmail_QNAME = new QName("Calendar", "OrganizerEmail");
    private final static QName _SchemaFirstName_QNAME = new QName("Contacts", "FirstName");
    private final static QName _SchemaReplyTo_QNAME = new QName("Email", "ReplyTo");
    private final static QName _SchemaImportance_QNAME = new QName("Email", "Importance");
    private final static QName _SchemaRead_QNAME = new QName("Email", "Read");
    private final static QName _SchemaSensitivity_QNAME = new QName("Calendar", "Sensitivity");
    private final static QName _SchemaHomeAddressPostalCode_QNAME = new QName("Contacts", "HomeAddressPostalCode");
    private final static QName _SchemaTo_QNAME = new QName("Email", "To");
    private final static QName _SchemaLocation_QNAME = new QName("Calendar", "Location");
    private final static QName _SchemaMiddleName_QNAME = new QName("Contacts", "MiddleName");
    private final static QName _SchemaOtherAddressState_QNAME = new QName("Contacts", "OtherAddressState");
    private final static QName _SchemaTitle_QNAME = new QName("Contacts", "Title");
    private final static QName _SchemaHomeFaxNumber_QNAME = new QName("Contacts", "HomeFaxNumber");
    private final static QName _SchemaMeetingStatus_QNAME = new QName("Calendar", "MeetingStatus");
    private final static QName _SchemaEndTime_QNAME = new QName("Calendar", "EndTime");
    private final static QName _SchemaCarTelephoneNumber_QNAME = new QName("Contacts", "CarTelephoneNumber");
    private final static QName _SchemaOtherAddressPostalCode_QNAME = new QName("Contacts", "OtherAddressPostalCode");
    private final static QName _SchemaYomiFirstName_QNAME = new QName("Contacts", "YomiFirstName");
    private final static QName _SchemaChildren_QNAME = new QName("Contacts", "Children");
    private final static QName _SchemaYomiCompanyName_QNAME = new QName("Contacts", "YomiCompanyName");
    private final static QName _SchemaMeetingRequest_QNAME = new QName("Email", "MeetingRequest");
    private final static QName _SchemaAssistantName_QNAME = new QName("Contacts", "AssistantName");
    private final static QName _SchemaJobTitle_QNAME = new QName("Contacts", "JobTitle");
    private final static QName _SchemaOtherAddressCountry_QNAME = new QName("Contacts", "OtherAddressCountry");
    private final static QName _SchemaAssistantTelephoneNumber_QNAME = new QName("Contacts", "AssistantTelephoneNumber");
    private final static QName _SchemaOtherAddressCity_QNAME = new QName("Contacts", "OtherAddressCity");
    private final static QName _SchemaExceptions_QNAME = new QName("Calendar", "Exceptions");
    private final static QName _SchemaBusinessTelephoneNumber_QNAME = new QName("Contacts", "BusinessTelephoneNumber");
    private final static QName _SchemaSpouse_QNAME = new QName("Contacts", "Spouse");
    private final static QName _SchemaThreadTopic_QNAME = new QName("Email", "ThreadTopic");
    private final static QName _SchemaNickName_QNAME = new QName("Contacts2", "NickName");
    private final static QName _SchemaHomeAddressState_QNAME = new QName("Contacts", "HomeAddressState");
    private final static QName _SchemaCustomerId_QNAME = new QName("Contacts2", "CustomerId");
    private final static QName _SchemaMessageClass_QNAME = new QName("Email", "MessageClass");
    private final static QName _SchemaSuffix_QNAME = new QName("Contacts", "Suffix");
    private final static QName _SchemaCompanyMainPhone_QNAME = new QName("Contacts2", "CompanyMainPhone");
    private final static QName _SchemaBody_QNAME = new QName("AirSyncBase", "Body");
    private final static QName _SchemaStartTime_QNAME = new QName("Calendar", "StartTime");
    private final static QName _SchemaEmail2Address_QNAME = new QName("Contacts", "Email2Address");
    private final static QName _SchemaLastName_QNAME = new QName("Contacts", "LastName");
    private final static QName _SchemaIMAddress3_QNAME = new QName("Contacts2", "IMAddress3");
    private final static QName _SchemaAccountName_QNAME = new QName("Contacts2", "AccountName");
    private final static QName _SchemaHomeAddressCountry_QNAME = new QName("Contacts", "HomeAddressCountry");
    private final static QName _SchemaCategories_QNAME = new QName("Contacts", "Categories");
    private final static QName _SchemaIMAddress_QNAME = new QName("Contacts2", "IMAddress");
    private final static QName _SchemaIMAddress2_QNAME = new QName("Contacts2", "IMAddress2");
    private final static QName _SchemaResponseRequested_QNAME = new QName("Calendar", "ResponseRequested");
    private final static QName _SchemaUID_QNAME = new QName("Calendar", "UID");
    private final static QName _SchemaHomeTelephoneNumber_QNAME = new QName("Contacts", "HomeTelephoneNumber");
    private final static QName _SchemaPagerNumber_QNAME = new QName("Contacts", "PagerNumber");
    private final static QName _SchemaBusinessAddressCountry_QNAME = new QName("Contacts", "BusinessAddressCountry");
    private final static QName _SchemaYomiLastName_QNAME = new QName("Contacts", "YomiLastName");
    private final static QName _SchemaAnniversary_QNAME = new QName("Contacts", "Anniversary");
    private final static QName _SchemaGovernmentId_QNAME = new QName("Contacts2", "GovernmentId");
    private final static QName _SchemaMMS_QNAME = new QName("Contacts2", "MMS");
    private final static QName _SchemaAttachments_QNAME = new QName("AirSyncBase", "Attachments");
    private final static QName _SchemaDisallowNewTimeProposal_QNAME = new QName("Calendar", "DisallowNewTimeProposal");
    private final static QName _SchemaHomeAddressStreet_QNAME = new QName("Contacts", "HomeAddressStreet");
    private final static QName _SchemaBusinessAddressStreet_QNAME = new QName("Contacts", "BusinessAddressStreet");
    private final static QName _SchemaBusinessAddressState_QNAME = new QName("Contacts", "BusinessAddressState");
    private final static QName _SchemaPicture_QNAME = new QName("Contacts", "Picture");
    private final static QName _SchemaCompanyName_QNAME = new QName("Contacts", "CompanyName");
    private final static QName _SchemaRecurrence_QNAME = new QName("Calendar", "Recurrence");
    private final static QName _SchemaDepartment_QNAME = new QName("Contacts", "Department");
    private final static QName _SchemaOtherAddressStreet_QNAME = new QName("Contacts", "OtherAddressStreet");
    private final static QName _SchemaTimezone_QNAME = new QName("Calendar", "Timezone");
    private final static QName _SchemaFrom_QNAME = new QName("Email", "From");
    private final static QName _SchemaRadioTelephoneNumber_QNAME = new QName("Contacts", "RadioTelephoneNumber");
    private final static QName _SchemaInternetCPID_QNAME = new QName("Email", "InternetCPID");
    private final static QName _SchemaManagerName_QNAME = new QName("Contacts2", "ManagerName");
    private final static QName _SchemaHome2TelephoneNumber_QNAME = new QName("Contacts", "Home2TelephoneNumber");
    private final static QName _SchemaBusinessAddressPostalCode_QNAME = new QName("Contacts", "BusinessAddressPostalCode");
    private final static QName _SchemaSubject_QNAME = new QName("Email", "Subject");
    private final static QName _SchemaBusyStatus_QNAME = new QName("Calendar", "BusyStatus");
    private final static QName _SchemaAllDayEvent_QNAME = new QName("Calendar", "AllDayEvent");
    private final static QName _SchemaAttendees_QNAME = new QName("Calendar", "Attendees");
    private final static QName _SchemaDtStamp_QNAME = new QName("Calendar", "DtStamp");
    private final static QName _SchemaBusiness2TelephoneNumber_QNAME = new QName("Contacts", "Business2TelephoneNumber");
    private final static QName _SchemaFileAs_QNAME = new QName("Contacts", "FileAs");
    private final static QName _SchemaBusinessAddressCity_QNAME = new QName("Contacts", "BusinessAddressCity");
    private final static QName _SchemaBirthday_QNAME = new QName("Contacts", "Birthday");
    private final static QName _SchemaHomeAddressCity_QNAME = new QName("Contacts", "HomeAddressCity");
    private final static QName _SchemaOrganizerName_QNAME = new QName("Calendar", "OrganizerName");
    private final static QName _SchemaBusinessFaxNumber_QNAME = new QName("Contacts", "BusinessFaxNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.itemoperations
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ItemOperations }
     * 
     */
    public ItemOperations createItemOperations() {
        return new ItemOperations();
    }

    /**
     * Create an instance of {@link ItemOperations.Move }
     * 
     */
    public ItemOperations.Move createItemOperationsMove() {
        return new ItemOperations.Move();
    }

    /**
     * Create an instance of {@link ItemOperations.Fetch }
     * 
     */
    public ItemOperations.Fetch createItemOperationsFetch() {
        return new ItemOperations.Fetch();
    }

    /**
     * Create an instance of {@link ItemOperations.EmptyFolderContents }
     * 
     */
    public ItemOperations.EmptyFolderContents createItemOperationsEmptyFolderContents() {
        return new ItemOperations.EmptyFolderContents();
    }

    /**
     * Create an instance of {@link Schema }
     * 
     */
    public Schema createSchema() {
        return new Schema();
    }

    /**
     * Create an instance of {@link Properties }
     * 
     */
    public Properties createProperties() {
        return new Properties();
    }

    /**
     * Create an instance of {@link ItemOperations.Move.Options }
     * 
     */
    public ItemOperations.Move.Options createItemOperationsMoveOptions() {
        return new ItemOperations.Move.Options();
    }

    /**
     * Create an instance of {@link ItemOperations.Fetch.Options }
     * 
     */
    public ItemOperations.Fetch.Options createItemOperationsFetchOptions() {
        return new ItemOperations.Fetch.Options();
    }

    /**
     * Create an instance of {@link ItemOperations.EmptyFolderContents.Options }
     * 
     */
    public ItemOperations.EmptyFolderContents.Options createItemOperationsEmptyFolderContentsOptions() {
        return new ItemOperations.EmptyFolderContents.Options();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "DstFldId")
    public JAXBElement<String> createDstFldId(String value) {
        return new JAXBElement<String>(_DstFldId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "MoveAlways")
    public JAXBElement<String> createMoveAlways(String value) {
        return new JAXBElement<String>(_MoveAlways_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Store")
    public JAXBElement<String> createStore(String value) {
        return new JAXBElement<String>(_Store_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Part")
    public JAXBElement<BigInteger> createPart(BigInteger value) {
        return new JAXBElement<BigInteger>(_Part_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Status")
    public JAXBElement<BigInteger> createStatus(BigInteger value) {
        return new JAXBElement<BigInteger>(_Status_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_Password_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "UserName")
    public JAXBElement<String> createUserName(String value) {
        return new JAXBElement<String>(_UserName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Version")
    public JAXBElement<XMLGregorianCalendar> createVersion(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_Version_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "ConversationId")
    public JAXBElement<String> createConversationId(String value) {
        return new JAXBElement<String>(_ConversationId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Total")
    public JAXBElement<BigInteger> createTotal(BigInteger value) {
        return new JAXBElement<BigInteger>(_Total_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Data")
    public JAXBElement<String> createData(String value) {
        return new JAXBElement<String>(_Data_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "DeleteSubFolders")
    public JAXBElement<String> createDeleteSubFolders(String value) {
        return new JAXBElement<String>(_DeleteSubFolders_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ItemOperations", name = "Range")
    public JAXBElement<String> createRange(String value) {
        return new JAXBElement<String>(_Range_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Cc", scope = Schema.class)
    public JAXBElement<String> createSchemaCc(String value) {
        return new JAXBElement<String>(_SchemaCc_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Reminder", scope = Schema.class)
    public JAXBElement<String> createSchemaReminder(String value) {
        return new JAXBElement<String>(_SchemaReminder_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Webpage", scope = Schema.class)
    public JAXBElement<String> createSchemaWebpage(String value) {
        return new JAXBElement<String>(_SchemaWebpage_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email1Address", scope = Schema.class)
    public JAXBElement<String> createSchemaEmail1Address(String value) {
        return new JAXBElement<String>(_SchemaEmail1Address_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link Schema.SubjectCalendar }}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Subject", scope = Schema.class)
    public Schema.SubjectCalendar createSchemaSubjectCalendar(String value) {
        return new Schema.SubjectCalendar(value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "DateReceived", scope = Schema.class)
    public JAXBElement<String> createSchemaDateReceived(String value) {
        return new JAXBElement<String>(_SchemaDateReceived_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "DisplayTo", scope = Schema.class)
    public JAXBElement<String> createSchemaDisplayTo(String value) {
        return new JAXBElement<String>(_SchemaDisplayTo_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "MobileTelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaMobileTelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaMobileTelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OfficeLocation", scope = Schema.class)
    public JAXBElement<String> createSchemaOfficeLocation(String value) {
        return new JAXBElement<String>(_SchemaOfficeLocation_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email3Address", scope = Schema.class)
    public JAXBElement<String> createSchemaEmail3Address(String value) {
        return new JAXBElement<String>(_SchemaEmail3Address_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "BodyPart", scope = Schema.class)
    public JAXBElement<String> createSchemaBodyPart(String value) {
        return new JAXBElement<String>(_SchemaBodyPart_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OrganizerEmail", scope = Schema.class)
    public JAXBElement<String> createSchemaOrganizerEmail(String value) {
        return new JAXBElement<String>(_SchemaOrganizerEmail_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "FirstName", scope = Schema.class)
    public JAXBElement<String> createSchemaFirstName(String value) {
        return new JAXBElement<String>(_SchemaFirstName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "ReplyTo", scope = Schema.class)
    public JAXBElement<String> createSchemaReplyTo(String value) {
        return new JAXBElement<String>(_SchemaReplyTo_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Importance", scope = Schema.class)
    public JAXBElement<String> createSchemaImportance(String value) {
        return new JAXBElement<String>(_SchemaImportance_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Read", scope = Schema.class)
    public JAXBElement<String> createSchemaRead(String value) {
        return new JAXBElement<String>(_SchemaRead_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Sensitivity", scope = Schema.class)
    public JAXBElement<String> createSchemaSensitivity(String value) {
        return new JAXBElement<String>(_SchemaSensitivity_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressPostalCode", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeAddressPostalCode(String value) {
        return new JAXBElement<String>(_SchemaHomeAddressPostalCode_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "To", scope = Schema.class)
    public JAXBElement<String> createSchemaTo(String value) {
        return new JAXBElement<String>(_SchemaTo_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Location", scope = Schema.class)
    public JAXBElement<String> createSchemaLocation(String value) {
        return new JAXBElement<String>(_SchemaLocation_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "MiddleName", scope = Schema.class)
    public JAXBElement<String> createSchemaMiddleName(String value) {
        return new JAXBElement<String>(_SchemaMiddleName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressState", scope = Schema.class)
    public JAXBElement<String> createSchemaOtherAddressState(String value) {
        return new JAXBElement<String>(_SchemaOtherAddressState_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Title", scope = Schema.class)
    public JAXBElement<String> createSchemaTitle(String value) {
        return new JAXBElement<String>(_SchemaTitle_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeFaxNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeFaxNumber(String value) {
        return new JAXBElement<String>(_SchemaHomeFaxNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "MeetingStatus", scope = Schema.class)
    public JAXBElement<String> createSchemaMeetingStatus(String value) {
        return new JAXBElement<String>(_SchemaMeetingStatus_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "EndTime", scope = Schema.class)
    public JAXBElement<String> createSchemaEndTime(String value) {
        return new JAXBElement<String>(_SchemaEndTime_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "CarTelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaCarTelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaCarTelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressPostalCode", scope = Schema.class)
    public JAXBElement<String> createSchemaOtherAddressPostalCode(String value) {
        return new JAXBElement<String>(_SchemaOtherAddressPostalCode_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiFirstName", scope = Schema.class)
    public JAXBElement<String> createSchemaYomiFirstName(String value) {
        return new JAXBElement<String>(_SchemaYomiFirstName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Children", scope = Schema.class)
    public JAXBElement<String> createSchemaChildren(String value) {
        return new JAXBElement<String>(_SchemaChildren_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiCompanyName", scope = Schema.class)
    public JAXBElement<String> createSchemaYomiCompanyName(String value) {
        return new JAXBElement<String>(_SchemaYomiCompanyName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "MeetingRequest", scope = Schema.class)
    public JAXBElement<String> createSchemaMeetingRequest(String value) {
        return new JAXBElement<String>(_SchemaMeetingRequest_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistantName", scope = Schema.class)
    public JAXBElement<String> createSchemaAssistantName(String value) {
        return new JAXBElement<String>(_SchemaAssistantName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "JobTitle", scope = Schema.class)
    public JAXBElement<String> createSchemaJobTitle(String value) {
        return new JAXBElement<String>(_SchemaJobTitle_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressCountry", scope = Schema.class)
    public JAXBElement<String> createSchemaOtherAddressCountry(String value) {
        return new JAXBElement<String>(_SchemaOtherAddressCountry_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistantTelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaAssistantTelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaAssistantTelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressCity", scope = Schema.class)
    public JAXBElement<String> createSchemaOtherAddressCity(String value) {
        return new JAXBElement<String>(_SchemaOtherAddressCity_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Exceptions", scope = Schema.class)
    public JAXBElement<String> createSchemaExceptions(String value) {
        return new JAXBElement<String>(_SchemaExceptions_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessTelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessTelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaBusinessTelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Spouse", scope = Schema.class)
    public JAXBElement<String> createSchemaSpouse(String value) {
        return new JAXBElement<String>(_SchemaSpouse_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link Schema.CategoriesCalendar }}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Categories", scope = Schema.class)
    public Schema.CategoriesCalendar createSchemaCategoriesCalendar(String value) {
        return new Schema.CategoriesCalendar(value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "ThreadTopic", scope = Schema.class)
    public JAXBElement<String> createSchemaThreadTopic(String value) {
        return new JAXBElement<String>(_SchemaThreadTopic_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "NickName", scope = Schema.class)
    public JAXBElement<String> createSchemaNickName(String value) {
        return new JAXBElement<String>(_SchemaNickName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressState", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeAddressState(String value) {
        return new JAXBElement<String>(_SchemaHomeAddressState_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "CustomerId", scope = Schema.class)
    public JAXBElement<String> createSchemaCustomerId(String value) {
        return new JAXBElement<String>(_SchemaCustomerId_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "MessageClass", scope = Schema.class)
    public JAXBElement<String> createSchemaMessageClass(String value) {
        return new JAXBElement<String>(_SchemaMessageClass_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Suffix", scope = Schema.class)
    public JAXBElement<String> createSchemaSuffix(String value) {
        return new JAXBElement<String>(_SchemaSuffix_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "CompanyMainPhone", scope = Schema.class)
    public JAXBElement<String> createSchemaCompanyMainPhone(String value) {
        return new JAXBElement<String>(_SchemaCompanyMainPhone_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "Body", scope = Schema.class)
    public JAXBElement<String> createSchemaBody(String value) {
        return new JAXBElement<String>(_SchemaBody_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "StartTime", scope = Schema.class)
    public JAXBElement<String> createSchemaStartTime(String value) {
        return new JAXBElement<String>(_SchemaStartTime_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email2Address", scope = Schema.class)
    public JAXBElement<String> createSchemaEmail2Address(String value) {
        return new JAXBElement<String>(_SchemaEmail2Address_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "LastName", scope = Schema.class)
    public JAXBElement<String> createSchemaLastName(String value) {
        return new JAXBElement<String>(_SchemaLastName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress3", scope = Schema.class)
    public JAXBElement<String> createSchemaIMAddress3(String value) {
        return new JAXBElement<String>(_SchemaIMAddress3_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "AccountName", scope = Schema.class)
    public JAXBElement<String> createSchemaAccountName(String value) {
        return new JAXBElement<String>(_SchemaAccountName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressCountry", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeAddressCountry(String value) {
        return new JAXBElement<String>(_SchemaHomeAddressCountry_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Categories", scope = Schema.class)
    public JAXBElement<String> createSchemaCategories(String value) {
        return new JAXBElement<String>(_SchemaCategories_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress", scope = Schema.class)
    public JAXBElement<String> createSchemaIMAddress(String value) {
        return new JAXBElement<String>(_SchemaIMAddress_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "IMAddress2", scope = Schema.class)
    public JAXBElement<String> createSchemaIMAddress2(String value) {
        return new JAXBElement<String>(_SchemaIMAddress2_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "ResponseRequested", scope = Schema.class)
    public JAXBElement<String> createSchemaResponseRequested(String value) {
        return new JAXBElement<String>(_SchemaResponseRequested_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "UID", scope = Schema.class)
    public JAXBElement<String> createSchemaUID(String value) {
        return new JAXBElement<String>(_SchemaUID_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeTelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeTelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaHomeTelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "PagerNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaPagerNumber(String value) {
        return new JAXBElement<String>(_SchemaPagerNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressCountry", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessAddressCountry(String value) {
        return new JAXBElement<String>(_SchemaBusinessAddressCountry_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiLastName", scope = Schema.class)
    public JAXBElement<String> createSchemaYomiLastName(String value) {
        return new JAXBElement<String>(_SchemaYomiLastName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Anniversary", scope = Schema.class)
    public JAXBElement<String> createSchemaAnniversary(String value) {
        return new JAXBElement<String>(_SchemaAnniversary_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "GovernmentId", scope = Schema.class)
    public JAXBElement<String> createSchemaGovernmentId(String value) {
        return new JAXBElement<String>(_SchemaGovernmentId_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "MMS", scope = Schema.class)
    public JAXBElement<String> createSchemaMMS(String value) {
        return new JAXBElement<String>(_SchemaMMS_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "AirSyncBase", name = "Attachments", scope = Schema.class)
    public JAXBElement<String> createSchemaAttachments(String value) {
        return new JAXBElement<String>(_SchemaAttachments_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "DisallowNewTimeProposal", scope = Schema.class)
    public JAXBElement<String> createSchemaDisallowNewTimeProposal(String value) {
        return new JAXBElement<String>(_SchemaDisallowNewTimeProposal_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressStreet", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeAddressStreet(String value) {
        return new JAXBElement<String>(_SchemaHomeAddressStreet_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressStreet", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessAddressStreet(String value) {
        return new JAXBElement<String>(_SchemaBusinessAddressStreet_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressState", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessAddressState(String value) {
        return new JAXBElement<String>(_SchemaBusinessAddressState_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Picture", scope = Schema.class)
    public JAXBElement<String> createSchemaPicture(String value) {
        return new JAXBElement<String>(_SchemaPicture_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "CompanyName", scope = Schema.class)
    public JAXBElement<String> createSchemaCompanyName(String value) {
        return new JAXBElement<String>(_SchemaCompanyName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Recurrence", scope = Schema.class)
    public JAXBElement<String> createSchemaRecurrence(String value) {
        return new JAXBElement<String>(_SchemaRecurrence_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Department", scope = Schema.class)
    public JAXBElement<String> createSchemaDepartment(String value) {
        return new JAXBElement<String>(_SchemaDepartment_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressStreet", scope = Schema.class)
    public JAXBElement<String> createSchemaOtherAddressStreet(String value) {
        return new JAXBElement<String>(_SchemaOtherAddressStreet_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Timezone", scope = Schema.class)
    public JAXBElement<String> createSchemaTimezone(String value) {
        return new JAXBElement<String>(_SchemaTimezone_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "From", scope = Schema.class)
    public JAXBElement<String> createSchemaFrom(String value) {
        return new JAXBElement<String>(_SchemaFrom_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "RadioTelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaRadioTelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaRadioTelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "InternetCPID", scope = Schema.class)
    public JAXBElement<String> createSchemaInternetCPID(String value) {
        return new JAXBElement<String>(_SchemaInternetCPID_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts2", name = "ManagerName", scope = Schema.class)
    public JAXBElement<String> createSchemaManagerName(String value) {
        return new JAXBElement<String>(_SchemaManagerName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Home2TelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaHome2TelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaHome2TelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressPostalCode", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessAddressPostalCode(String value) {
        return new JAXBElement<String>(_SchemaBusinessAddressPostalCode_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Email", name = "Subject", scope = Schema.class)
    public JAXBElement<String> createSchemaSubject(String value) {
        return new JAXBElement<String>(_SchemaSubject_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "BusyStatus", scope = Schema.class)
    public JAXBElement<String> createSchemaBusyStatus(String value) {
        return new JAXBElement<String>(_SchemaBusyStatus_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "AllDayEvent", scope = Schema.class)
    public JAXBElement<String> createSchemaAllDayEvent(String value) {
        return new JAXBElement<String>(_SchemaAllDayEvent_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "Attendees", scope = Schema.class)
    public JAXBElement<String> createSchemaAttendees(String value) {
        return new JAXBElement<String>(_SchemaAttendees_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "DtStamp", scope = Schema.class)
    public JAXBElement<String> createSchemaDtStamp(String value) {
        return new JAXBElement<String>(_SchemaDtStamp_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Business2TelephoneNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaBusiness2TelephoneNumber(String value) {
        return new JAXBElement<String>(_SchemaBusiness2TelephoneNumber_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "FileAs", scope = Schema.class)
    public JAXBElement<String> createSchemaFileAs(String value) {
        return new JAXBElement<String>(_SchemaFileAs_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressCity", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessAddressCity(String value) {
        return new JAXBElement<String>(_SchemaBusinessAddressCity_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Birthday", scope = Schema.class)
    public JAXBElement<String> createSchemaBirthday(String value) {
        return new JAXBElement<String>(_SchemaBirthday_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressCity", scope = Schema.class)
    public JAXBElement<String> createSchemaHomeAddressCity(String value) {
        return new JAXBElement<String>(_SchemaHomeAddressCity_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Calendar", name = "OrganizerName", scope = Schema.class)
    public JAXBElement<String> createSchemaOrganizerName(String value) {
        return new JAXBElement<String>(_SchemaOrganizerName_QNAME, String.class, Schema.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessFaxNumber", scope = Schema.class)
    public JAXBElement<String> createSchemaBusinessFaxNumber(String value) {
        return new JAXBElement<String>(_SchemaBusinessFaxNumber_QNAME, String.class, Schema.class, value);
    }

}
