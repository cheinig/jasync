//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.contacts;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.request.contacts package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OfficeLocation_QNAME = new QName("Contacts", "OfficeLocation");
    private final static QName _FirstName_QNAME = new QName("Contacts", "FirstName");
    private final static QName _AssistantPhoneNumber_QNAME = new QName("Contacts", "AssistantPhoneNumber");
    private final static QName _YomiFirstName_QNAME = new QName("Contacts", "YomiFirstName");
    private final static QName _JobTitle_QNAME = new QName("Contacts", "JobTitle");
    private final static QName _PagerNumber_QNAME = new QName("Contacts", "PagerNumber");
    private final static QName _Business2PhoneNumber_QNAME = new QName("Contacts", "Business2PhoneNumber");
    private final static QName _Anniversary_QNAME = new QName("Contacts", "Anniversary");
    private final static QName _BusinessPhoneNumber_QNAME = new QName("Contacts", "BusinessPhoneNumber");
    private final static QName _HomeAddressStreet_QNAME = new QName("Contacts", "HomeAddressStreet");
    private final static QName _BusinessAddressStreet_QNAME = new QName("Contacts", "BusinessAddressStreet");
    private final static QName _Picture_QNAME = new QName("Contacts", "Picture");
    private final static QName _Department_QNAME = new QName("Contacts", "Department");
    private final static QName _BusinessAddressPostalCode_QNAME = new QName("Contacts", "BusinessAddressPostalCode");
    private final static QName _CarPhoneNumber_QNAME = new QName("Contacts", "CarPhoneNumber");
    private final static QName _Home2PhoneNumber_QNAME = new QName("Contacts", "Home2PhoneNumber");
    private final static QName _BusinessAddressCity_QNAME = new QName("Contacts", "BusinessAddressCity");
    private final static QName _HomeAddressCity_QNAME = new QName("Contacts", "HomeAddressCity");
    private final static QName _BusinessFaxNumber_QNAME = new QName("Contacts", "BusinessFaxNumber");
    private final static QName _HomeAddressPostalCode_QNAME = new QName("Contacts", "HomeAddressPostalCode");
    private final static QName _RadioPhoneNumber_QNAME = new QName("Contacts", "RadioPhoneNumber");
    private final static QName _Title_QNAME = new QName("Contacts", "Title");
    private final static QName _YomiCompanyName_QNAME = new QName("Contacts", "YomiCompanyName");
    private final static QName _OtherAddressCountry_QNAME = new QName("Contacts", "OtherAddressCountry");
    private final static QName _Spouse_QNAME = new QName("Contacts", "Spouse");
    private final static QName _HomeAddressState_QNAME = new QName("Contacts", "HomeAddressState");
    private final static QName _Email2Address_QNAME = new QName("Contacts", "Email2Address");
    private final static QName _LastName_QNAME = new QName("Contacts", "LastName");
    private final static QName _HomeAddressCountry_QNAME = new QName("Contacts", "HomeAddressCountry");
    private final static QName _BusinessAddressState_QNAME = new QName("Contacts", "BusinessAddressState");
    private final static QName _Email1Address_QNAME = new QName("Contacts", "Email1Address");
    private final static QName _OtherAddressState_QNAME = new QName("Contacts", "OtherAddressState");
    private final static QName _HomeFaxNumber_QNAME = new QName("Contacts", "HomeFaxNumber");
    private final static QName _OtherAddressPostalCode_QNAME = new QName("Contacts", "OtherAddressPostalCode");
    private final static QName _AssistantName_QNAME = new QName("Contacts", "AssistantName");
    private final static QName _Suffix_QNAME = new QName("Contacts", "Suffix");
    private final static QName _BodyTruncated_QNAME = new QName("Contacts", "BodyTruncated");
    private final static QName _OtherAddressStreet_QNAME = new QName("Contacts", "OtherAddressStreet");
    private final static QName _WeightedRank_QNAME = new QName("Contacts", "WeightedRank");
    private final static QName _Birthday_QNAME = new QName("Contacts", "Birthday");
    private final static QName _Alias_QNAME = new QName("Contacts", "Alias");
    private final static QName _Email3Address_QNAME = new QName("Contacts", "Email3Address");
    private final static QName _MiddleName_QNAME = new QName("Contacts", "MiddleName");
    private final static QName _HomePhoneNumber_QNAME = new QName("Contacts", "HomePhoneNumber");
    private final static QName _Body_QNAME = new QName("Contacts", "Body");
    private final static QName _WebPage_QNAME = new QName("Contacts", "WebPage");
    private final static QName _MobilePhoneNumber_QNAME = new QName("Contacts", "MobilePhoneNumber");
    private final static QName _OtherAddressCity_QNAME = new QName("Contacts", "OtherAddressCity");
    private final static QName _BodySize_QNAME = new QName("Contacts", "BodySize");
    private final static QName _BusinessAddressCountry_QNAME = new QName("Contacts", "BusinessAddressCountry");
    private final static QName _YomiLastName_QNAME = new QName("Contacts", "YomiLastName");
    private final static QName _CompanyName_QNAME = new QName("Contacts", "CompanyName");
    private final static QName _FileAs_QNAME = new QName("Contacts", "FileAs");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.request.contacts
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Categories }
     * 
     */
    public Categories createCategories() {
        return new Categories();
    }

    /**
     * Create an instance of {@link Children }
     * 
     */
    public Children createChildren() {
        return new Children();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OfficeLocation")
    public JAXBElement<String> createOfficeLocation(String value) {
        return new JAXBElement<String>(_OfficeLocation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "FirstName")
    public JAXBElement<String> createFirstName(String value) {
        return new JAXBElement<String>(_FirstName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistantPhoneNumber")
    public JAXBElement<String> createAssistantPhoneNumber(String value) {
        return new JAXBElement<String>(_AssistantPhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiFirstName")
    public JAXBElement<String> createYomiFirstName(String value) {
        return new JAXBElement<String>(_YomiFirstName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "JobTitle")
    public JAXBElement<String> createJobTitle(String value) {
        return new JAXBElement<String>(_JobTitle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "PagerNumber")
    public JAXBElement<String> createPagerNumber(String value) {
        return new JAXBElement<String>(_PagerNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Business2PhoneNumber")
    public JAXBElement<String> createBusiness2PhoneNumber(String value) {
        return new JAXBElement<String>(_Business2PhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Anniversary")
    public JAXBElement<XMLGregorianCalendar> createAnniversary(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_Anniversary_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessPhoneNumber")
    public JAXBElement<String> createBusinessPhoneNumber(String value) {
        return new JAXBElement<String>(_BusinessPhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressStreet")
    public JAXBElement<String> createHomeAddressStreet(String value) {
        return new JAXBElement<String>(_HomeAddressStreet_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressStreet")
    public JAXBElement<String> createBusinessAddressStreet(String value) {
        return new JAXBElement<String>(_BusinessAddressStreet_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Picture")
    public JAXBElement<String> createPicture(String value) {
        return new JAXBElement<String>(_Picture_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Department")
    public JAXBElement<String> createDepartment(String value) {
        return new JAXBElement<String>(_Department_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressPostalCode")
    public JAXBElement<String> createBusinessAddressPostalCode(String value) {
        return new JAXBElement<String>(_BusinessAddressPostalCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "CarPhoneNumber")
    public JAXBElement<String> createCarPhoneNumber(String value) {
        return new JAXBElement<String>(_CarPhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Home2PhoneNumber")
    public JAXBElement<String> createHome2PhoneNumber(String value) {
        return new JAXBElement<String>(_Home2PhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressCity")
    public JAXBElement<String> createBusinessAddressCity(String value) {
        return new JAXBElement<String>(_BusinessAddressCity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressCity")
    public JAXBElement<String> createHomeAddressCity(String value) {
        return new JAXBElement<String>(_HomeAddressCity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessFaxNumber")
    public JAXBElement<String> createBusinessFaxNumber(String value) {
        return new JAXBElement<String>(_BusinessFaxNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressPostalCode")
    public JAXBElement<String> createHomeAddressPostalCode(String value) {
        return new JAXBElement<String>(_HomeAddressPostalCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "RadioPhoneNumber")
    public JAXBElement<String> createRadioPhoneNumber(String value) {
        return new JAXBElement<String>(_RadioPhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Title")
    public JAXBElement<String> createTitle(String value) {
        return new JAXBElement<String>(_Title_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiCompanyName")
    public JAXBElement<String> createYomiCompanyName(String value) {
        return new JAXBElement<String>(_YomiCompanyName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressCountry")
    public JAXBElement<String> createOtherAddressCountry(String value) {
        return new JAXBElement<String>(_OtherAddressCountry_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Spouse")
    public JAXBElement<String> createSpouse(String value) {
        return new JAXBElement<String>(_Spouse_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressState")
    public JAXBElement<String> createHomeAddressState(String value) {
        return new JAXBElement<String>(_HomeAddressState_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email2Address")
    public JAXBElement<String> createEmail2Address(String value) {
        return new JAXBElement<String>(_Email2Address_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "LastName")
    public JAXBElement<String> createLastName(String value) {
        return new JAXBElement<String>(_LastName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeAddressCountry")
    public JAXBElement<String> createHomeAddressCountry(String value) {
        return new JAXBElement<String>(_HomeAddressCountry_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressState")
    public JAXBElement<String> createBusinessAddressState(String value) {
        return new JAXBElement<String>(_BusinessAddressState_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email1Address")
    public JAXBElement<String> createEmail1Address(String value) {
        return new JAXBElement<String>(_Email1Address_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressState")
    public JAXBElement<String> createOtherAddressState(String value) {
        return new JAXBElement<String>(_OtherAddressState_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomeFaxNumber")
    public JAXBElement<String> createHomeFaxNumber(String value) {
        return new JAXBElement<String>(_HomeFaxNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressPostalCode")
    public JAXBElement<String> createOtherAddressPostalCode(String value) {
        return new JAXBElement<String>(_OtherAddressPostalCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "AssistantName")
    public JAXBElement<String> createAssistantName(String value) {
        return new JAXBElement<String>(_AssistantName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Suffix")
    public JAXBElement<String> createSuffix(String value) {
        return new JAXBElement<String>(_Suffix_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BodyTruncated")
    public JAXBElement<Short> createBodyTruncated(Short value) {
        return new JAXBElement<Short>(_BodyTruncated_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressStreet")
    public JAXBElement<String> createOtherAddressStreet(String value) {
        return new JAXBElement<String>(_OtherAddressStreet_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "WeightedRank")
    public JAXBElement<Integer> createWeightedRank(Integer value) {
        return new JAXBElement<Integer>(_WeightedRank_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Birthday")
    public JAXBElement<XMLGregorianCalendar> createBirthday(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_Birthday_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Alias")
    public JAXBElement<String> createAlias(String value) {
        return new JAXBElement<String>(_Alias_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Email3Address")
    public JAXBElement<String> createEmail3Address(String value) {
        return new JAXBElement<String>(_Email3Address_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "MiddleName")
    public JAXBElement<String> createMiddleName(String value) {
        return new JAXBElement<String>(_MiddleName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "HomePhoneNumber")
    public JAXBElement<String> createHomePhoneNumber(String value) {
        return new JAXBElement<String>(_HomePhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "Body")
    public JAXBElement<String> createBody(String value) {
        return new JAXBElement<String>(_Body_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "WebPage")
    public JAXBElement<String> createWebPage(String value) {
        return new JAXBElement<String>(_WebPage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "MobilePhoneNumber")
    public JAXBElement<String> createMobilePhoneNumber(String value) {
        return new JAXBElement<String>(_MobilePhoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "OtherAddressCity")
    public JAXBElement<String> createOtherAddressCity(String value) {
        return new JAXBElement<String>(_OtherAddressCity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BodySize")
    public JAXBElement<BigInteger> createBodySize(BigInteger value) {
        return new JAXBElement<BigInteger>(_BodySize_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "BusinessAddressCountry")
    public JAXBElement<String> createBusinessAddressCountry(String value) {
        return new JAXBElement<String>(_BusinessAddressCountry_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "YomiLastName")
    public JAXBElement<String> createYomiLastName(String value) {
        return new JAXBElement<String>(_YomiLastName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "CompanyName")
    public JAXBElement<String> createCompanyName(String value) {
        return new JAXBElement<String>(_CompanyName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Contacts", name = "FileAs")
    public JAXBElement<String> createFileAs(String value) {
        return new JAXBElement<String>(_FileAs_QNAME, String.class, null, value);
    }

}
