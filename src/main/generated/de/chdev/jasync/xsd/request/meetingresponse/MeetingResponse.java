//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.meetingresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.request.airsyncbase.Body;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Request" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="UserResponse">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                         &lt;enumeration value="3"/>
 *                         &lt;enumeration value="1"/>
 *                         &lt;enumeration value="2"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="CollectionId" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="64"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="RequestId" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="64"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element ref="{Search}LongId" minOccurs="0"/>
 *                   &lt;element name="InstanceId" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="24"/>
 *                         &lt;maxLength value="24"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="SendResponse" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
 *                             &lt;element ref="{MeetingResponse}ProposedStartTime" minOccurs="0"/>
 *                             &lt;element ref="{MeetingResponse}ProposedEndTime" minOccurs="0"/>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request"
})
@XmlRootElement(name = "MeetingResponse")
public class MeetingResponse {

    @XmlElement(name = "Request", required = true)
    protected List<MeetingResponse.Request> request;

    /**
     * Gets the value of the request property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the request property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeetingResponse.Request }
     * 
     * 
     */
    public List<MeetingResponse.Request> getRequest() {
        if (request == null) {
            request = new ArrayList<MeetingResponse.Request>();
        }
        return this.request;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="UserResponse">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
     *               &lt;enumeration value="3"/>
     *               &lt;enumeration value="1"/>
     *               &lt;enumeration value="2"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="CollectionId" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="64"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="RequestId" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="64"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element ref="{Search}LongId" minOccurs="0"/>
     *         &lt;element name="InstanceId" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="24"/>
     *               &lt;maxLength value="24"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="SendResponse" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
     *                   &lt;element ref="{MeetingResponse}ProposedStartTime" minOccurs="0"/>
     *                   &lt;element ref="{MeetingResponse}ProposedEndTime" minOccurs="0"/>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Request {

        @XmlElement(name = "UserResponse")
        protected short userResponse;
        @XmlElement(name = "CollectionId")
        protected String collectionId;
        @XmlElement(name = "RequestId")
        protected String requestId;
        @XmlElement(name = "LongId", namespace = "Search")
        protected String longId;
        @XmlElement(name = "InstanceId")
        protected String instanceId;
        @XmlElement(name = "SendResponse")
        protected MeetingResponse.Request.SendResponse sendResponse;

        /**
         * Ruft den Wert der userResponse-Eigenschaft ab.
         * 
         */
        public short getUserResponse() {
            return userResponse;
        }

        /**
         * Legt den Wert der userResponse-Eigenschaft fest.
         * 
         */
        public void setUserResponse(short value) {
            this.userResponse = value;
        }

        /**
         * Ruft den Wert der collectionId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCollectionId() {
            return collectionId;
        }

        /**
         * Legt den Wert der collectionId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCollectionId(String value) {
            this.collectionId = value;
        }

        /**
         * Ruft den Wert der requestId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRequestId() {
            return requestId;
        }

        /**
         * Legt den Wert der requestId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRequestId(String value) {
            this.requestId = value;
        }

        /**
         * Ruft den Wert der longId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLongId() {
            return longId;
        }

        /**
         * Legt den Wert der longId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLongId(String value) {
            this.longId = value;
        }

        /**
         * Ruft den Wert der instanceId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstanceId() {
            return instanceId;
        }

        /**
         * Legt den Wert der instanceId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstanceId(String value) {
            this.instanceId = value;
        }

        /**
         * Ruft den Wert der sendResponse-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link MeetingResponse.Request.SendResponse }
         *     
         */
        public MeetingResponse.Request.SendResponse getSendResponse() {
            return sendResponse;
        }

        /**
         * Legt den Wert der sendResponse-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link MeetingResponse.Request.SendResponse }
         *     
         */
        public void setSendResponse(MeetingResponse.Request.SendResponse value) {
            this.sendResponse = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
         *         &lt;element ref="{MeetingResponse}ProposedStartTime" minOccurs="0"/>
         *         &lt;element ref="{MeetingResponse}ProposedEndTime" minOccurs="0"/>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class SendResponse {

            @XmlElement(name = "Body", namespace = "AirSyncBase")
            protected Body body;
            @XmlElement(name = "ProposedStartTime")
            protected String proposedStartTime;
            @XmlElement(name = "ProposedEndTime")
            protected String proposedEndTime;

            /**
             * Ruft den Wert der body-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Body }
             *     
             */
            public Body getBody() {
                return body;
            }

            /**
             * Legt den Wert der body-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Body }
             *     
             */
            public void setBody(Body value) {
                this.body = value;
            }

            /**
             * Ruft den Wert der proposedStartTime-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProposedStartTime() {
                return proposedStartTime;
            }

            /**
             * Legt den Wert der proposedStartTime-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProposedStartTime(String value) {
                this.proposedStartTime = value;
            }

            /**
             * Ruft den Wert der proposedEndTime-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProposedEndTime() {
                return proposedEndTime;
            }

            /**
             * Legt den Wert der proposedEndTime-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProposedEndTime(String value) {
                this.proposedEndTime = value;
            }

        }

    }

}
