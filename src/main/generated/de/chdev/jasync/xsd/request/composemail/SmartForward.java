//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.23 um 12:47:29 AM CEST 
//


package de.chdev.jasync.xsd.request.composemail;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.request.airsyncbase.Body;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element ref="{ComposeMail}ClientId"/>
 *         &lt;element ref="{ComposeMail}Source"/>
 *         &lt;element ref="{ComposeMail}AccountId" minOccurs="0"/>
 *         &lt;element ref="{ComposeMail}SaveInSentItems" minOccurs="0"/>
 *         &lt;element ref="{ComposeMail}ReplaceMime" minOccurs="0"/>
 *         &lt;element ref="{ComposeMail}Mime"/>
 *         &lt;element ref="{RightsManagement}TemplateID" minOccurs="0"/>
 *         &lt;element ref="{AirSyncBase}Body" minOccurs="0"/>
 *         &lt;element ref="{ComposeMail}Forwardees" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "SmartForward")
public class SmartForward {

    @XmlElement(name = "ClientId", required = true)
    protected String clientId;
    @XmlElement(name = "Source", required = true)
    protected Source source;
    @XmlElement(name = "AccountId")
    protected String accountId;
    @XmlElement(name = "SaveInSentItems")
    protected String saveInSentItems;
    @XmlElement(name = "ReplaceMime")
    protected String replaceMime;
    @XmlElement(name = "Mime", required = true)
    protected String mime;
    @XmlElement(name = "TemplateID", namespace = "RightsManagement")
    protected String templateID;
    @XmlElement(name = "Body", namespace = "AirSyncBase")
    protected Body body;
    @XmlElement(name = "Forwardees")
    protected Forwardees forwardees;

    /**
     * Ruft den Wert der clientId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Legt den Wert der clientId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Source }
     *     
     */
    public Source getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Source }
     *     
     */
    public void setSource(Source value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der accountId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Legt den Wert der accountId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Ruft den Wert der saveInSentItems-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaveInSentItems() {
        return saveInSentItems;
    }

    /**
     * Legt den Wert der saveInSentItems-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaveInSentItems(String value) {
        this.saveInSentItems = value;
    }

    /**
     * Ruft den Wert der replaceMime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplaceMime() {
        return replaceMime;
    }

    /**
     * Legt den Wert der replaceMime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplaceMime(String value) {
        this.replaceMime = value;
    }

    /**
     * Ruft den Wert der mime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMime() {
        return mime;
    }

    /**
     * Legt den Wert der mime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMime(String value) {
        this.mime = value;
    }

    /**
     * Ruft den Wert der templateID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateID() {
        return templateID;
    }

    /**
     * Legt den Wert der templateID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateID(String value) {
        this.templateID = value;
    }

    /**
     * Ruft den Wert der body-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Body }
     *     
     */
    public Body getBody() {
        return body;
    }

    /**
     * Legt den Wert der body-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Body }
     *     
     */
    public void setBody(Body value) {
        this.body = value;
    }

    /**
     * Ruft den Wert der forwardees-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Forwardees }
     *     
     */
    public Forwardees getForwardees() {
        return forwardees;
    }

    /**
     * Legt den Wert der forwardees-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Forwardees }
     *     
     */
    public void setForwardees(Forwardees value) {
        this.forwardees = value;
    }

}
