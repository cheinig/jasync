
package de.chdev.jasync.xsd.backend.carddav;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.chdev.jasync.xsd.backend.carddav package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Nresults_QNAME = new QName("urn:ietf:params:xml:ns:carddav", "nresults");
    private final static QName _SupportedCollation_QNAME = new QName("urn:ietf:params:xml:ns:carddav", "supported-collation");
    private final static QName _Allprop_QNAME = new QName("urn:ietf:params:xml:ns:carddav", "allprop");
    private final static QName _Prop_QNAME = new QName("urn:ietf:params:xml:ns:carddav", "prop");
    private final static QName _IsNotDefined_QNAME = new QName("urn:ietf:params:xml:ns:carddav", "is-not-defined");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.chdev.jasync.xsd.backend.carddav
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Addressbook }
     * 
     */
    public Addressbook createAddressbook() {
        return new Addressbook();
    }

    /**
     * Create an instance of {@link AddressbookMultiget }
     * 
     */
    public AddressbookMultiget createAddressbookMultiget() {
        return new AddressbookMultiget();
    }

    /**
     * Create an instance of {@link Filter }
     * 
     */
    public Filter createFilter() {
        return new Filter();
    }

    /**
     * Create an instance of {@link Propfilter }
     * 
     */
    public Propfilter createPropfilter() {
        return new Propfilter();
    }

    /**
     * Create an instance of {@link TextMatch }
     * 
     */
    public TextMatch createTextMatch() {
        return new TextMatch();
    }

    /**
     * Create an instance of {@link ParamFilter }
     * 
     */
    public ParamFilter createParamFilter() {
        return new ParamFilter();
    }

    /**
     * Create an instance of {@link AddressbookQuery }
     * 
     */
    public AddressbookQuery createAddressbookQuery() {
        return new AddressbookQuery();
    }

    /**
     * Create an instance of {@link Limit }
     * 
     */
    public Limit createLimit() {
        return new Limit();
    }

    /**
     * Create an instance of {@link Prop }
     * 
     */
    public Prop createProp() {
        return new Prop();
    }

    /**
     * Create an instance of {@link AddressData }
     * 
     */
    public AddressData createAddressData() {
        return new AddressData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ietf:params:xml:ns:carddav", name = "nresults")
    public JAXBElement<Long> createNresults(Long value) {
        return new JAXBElement<Long>(_Nresults_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ietf:params:xml:ns:carddav", name = "supported-collation")
    public JAXBElement<String> createSupportedCollation(String value) {
        return new JAXBElement<String>(_SupportedCollation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ietf:params:xml:ns:carddav", name = "allprop")
    public JAXBElement<Object> createAllprop(Object value) {
        return new JAXBElement<Object>(_Allprop_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Prop }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ietf:params:xml:ns:carddav", name = "prop")
    public JAXBElement<Prop> createProp(Prop value) {
        return new JAXBElement<Prop>(_Prop_QNAME, Prop.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ietf:params:xml:ns:carddav", name = "is-not-defined")
    public JAXBElement<Object> createIsNotDefined(Object value) {
        return new JAXBElement<Object>(_IsNotDefined_QNAME, Object.class, null, value);
    }

}
