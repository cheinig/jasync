
package de.chdev.jasync.xsd.backend.carddav;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}is-not-defined"/>
 *         &lt;sequence>
 *           &lt;element ref="{urn:ietf:params:xml:ns:carddav}text-match" maxOccurs="unbounded"/>
 *           &lt;element ref="{urn:ietf:params:xml:ns:carddav}param-filter" maxOccurs="unbounded"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="test" default="anyof">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="anyof"/>
 *             &lt;enumeration value="allof"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "isNotDefined",
    "textMatch",
    "paramFilter"
})
@XmlRootElement(name = "propfilter")
public class Propfilter {

    @XmlElementRef(name = "is-not-defined", namespace = "urn:ietf:params:xml:ns:carddav", type = JAXBElement.class, required = false)
    protected JAXBElement<Object> isNotDefined;
    @XmlElementRef(name = "text-match", namespace = "urn:ietf:params:xml:ns:carddav", type = TextMatch.class, required = false)
    protected List<TextMatch> textMatch;
    @XmlElementRef(name = "param-filter", namespace = "urn:ietf:params:xml:ns:carddav", type = ParamFilter.class, required = false)
    protected List<ParamFilter> paramFilter;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "test")
    protected String test;

    /**
     * Ruft den Wert der isNotDefined-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public JAXBElement<Object> getIsNotDefined() {
        return isNotDefined;
    }

    /**
     * Legt den Wert der isNotDefined-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void setIsNotDefined(JAXBElement<Object> value) {
        this.isNotDefined = value;
    }

    /**
     * Gets the value of the textMatch property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textMatch property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextMatch().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextMatch }
     * 
     * 
     */
    public List<TextMatch> getTextMatch() {
        if (textMatch == null) {
            textMatch = new ArrayList<TextMatch>();
        }
        return this.textMatch;
    }

    /**
     * Gets the value of the paramFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paramFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParamFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamFilter }
     * 
     * 
     */
    public List<ParamFilter> getParamFilter() {
        if (paramFilter == null) {
            paramFilter = new ArrayList<ParamFilter>();
        }
        return this.paramFilter;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der test-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTest() {
        if (test == null) {
            return "anyof";
        } else {
            return test;
        }
    }

    /**
     * Legt den Wert der test-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTest(String value) {
        this.test = value;
    }

}
