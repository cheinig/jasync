
package de.chdev.jasync.xsd.backend.webdav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{DAV:}lockscope"/>
 *         &lt;element ref="{DAV:}locktype"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lockscope",
    "locktype"
})
@XmlRootElement(name = "lockentry")
public class Lockentry {

    @XmlElementRef(name = "lockscope", namespace = "DAV:", type = Lockscope.class)
    protected Lockscope lockscope;
    @XmlElementRef(name = "locktype", namespace = "DAV:", type = Locktype.class)
    protected Locktype locktype;

    /**
     * Ruft den Wert der lockscope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Lockscope }
     *     
     */
    public Lockscope getLockscope() {
        return lockscope;
    }

    /**
     * Legt den Wert der lockscope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Lockscope }
     *     
     */
    public void setLockscope(Lockscope value) {
        this.lockscope = value;
    }

    /**
     * Ruft den Wert der locktype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Locktype }
     *     
     */
    public Locktype getLocktype() {
        return locktype;
    }

    /**
     * Legt den Wert der locktype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Locktype }
     *     
     */
    public void setLocktype(Locktype value) {
        this.locktype = value;
    }

}
