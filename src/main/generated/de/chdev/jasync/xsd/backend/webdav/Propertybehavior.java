
package de.chdev.jasync.xsd.backend.webdav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{DAV:}omit"/>
 *         &lt;element ref="{DAV:}keepalive"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "omit",
    "keepalive"
})
@XmlRootElement(name = "propertybehavior")
public class Propertybehavior {

    @XmlElementRef(name = "omit", namespace = "DAV:", type = Omit.class, required = false)
    protected Omit omit;
    @XmlElementRef(name = "keepalive", namespace = "DAV:", type = Keepalive.class, required = false)
    protected Keepalive keepalive;

    /**
     * Ruft den Wert der omit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Omit }
     *     
     */
    public Omit getOmit() {
        return omit;
    }

    /**
     * Legt den Wert der omit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Omit }
     *     
     */
    public void setOmit(Omit value) {
        this.omit = value;
    }

    /**
     * Ruft den Wert der keepalive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Keepalive }
     *     
     */
    public Keepalive getKeepalive() {
        return keepalive;
    }

    /**
     * Legt den Wert der keepalive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Keepalive }
     *     
     */
    public void setKeepalive(Keepalive value) {
        this.keepalive = value;
    }

}
