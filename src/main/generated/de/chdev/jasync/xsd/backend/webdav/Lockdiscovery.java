
package de.chdev.jasync.xsd.backend.webdav;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element ref="{DAV:}activelock"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "activelock"
})
@XmlRootElement(name = "lockdiscovery")
public class Lockdiscovery {

    @XmlElementRef(name = "activelock", namespace = "DAV:", type = Activelock.class, required = false)
    protected List<Activelock> activelock;

    /**
     * Gets the value of the activelock property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activelock property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivelock().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Activelock }
     * 
     * 
     */
    public List<Activelock> getActivelock() {
        if (activelock == null) {
            activelock = new ArrayList<Activelock>();
        }
        return this.activelock;
    }

}
