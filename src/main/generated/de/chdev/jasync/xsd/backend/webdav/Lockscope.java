
package de.chdev.jasync.xsd.backend.webdav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{DAV:}exclusive"/>
 *         &lt;element ref="{DAV:}shared"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exclusive",
    "shared"
})
@XmlRootElement(name = "lockscope")
public class Lockscope {

    @XmlElementRef(name = "exclusive", namespace = "DAV:", type = Exclusive.class, required = false)
    protected Exclusive exclusive;
    @XmlElementRef(name = "shared", namespace = "DAV:", type = Shared.class, required = false)
    protected Shared shared;

    /**
     * Ruft den Wert der exclusive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Exclusive }
     *     
     */
    public Exclusive getExclusive() {
        return exclusive;
    }

    /**
     * Legt den Wert der exclusive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Exclusive }
     *     
     */
    public void setExclusive(Exclusive value) {
        this.exclusive = value;
    }

    /**
     * Ruft den Wert der shared-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Shared }
     *     
     */
    public Shared getShared() {
        return shared;
    }

    /**
     * Legt den Wert der shared-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Shared }
     *     
     */
    public void setShared(Shared value) {
        this.shared = value;
    }

}
