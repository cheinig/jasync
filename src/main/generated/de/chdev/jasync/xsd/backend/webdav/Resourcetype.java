
package de.chdev.jasync.xsd.backend.webdav;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.backend.carddav.Addressbook;
import org.w3c.dom.Element;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{DAV:}collection" minOccurs="0"/>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}addressbook" minOccurs="0"/>
 *         &lt;any processContents='skip' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "collection",
    "addressbook",
    "any"
})
@XmlRootElement(name = "resourcetype")
public class Resourcetype {

    @XmlElementRef(name = "collection", namespace = "DAV:", type = Collection.class, required = false)
    protected Collection collection;
    @XmlElementRef(name = "addressbook", namespace = "urn:ietf:params:xml:ns:carddav", type = Addressbook.class, required = false)
    protected Addressbook addressbook;
    @XmlAnyElement
    protected List<Element> any;

    /**
     * Ruft den Wert der collection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Collection }
     *     
     */
    public Collection getCollection() {
        return collection;
    }

    /**
     * Legt den Wert der collection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Collection }
     *     
     */
    public void setCollection(Collection value) {
        this.collection = value;
    }

    /**
     * Ruft den Wert der addressbook-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Addressbook }
     *     
     */
    public Addressbook getAddressbook() {
        return addressbook;
    }

    /**
     * Legt den Wert der addressbook-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Addressbook }
     *     
     */
    public void setAddressbook(Addressbook value) {
        this.addressbook = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * 
     * 
     */
    public List<Element> getAny() {
        if (any == null) {
            any = new ArrayList<Element>();
        }
        return this.any;
    }

}
