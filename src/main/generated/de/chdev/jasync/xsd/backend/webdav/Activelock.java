
package de.chdev.jasync.xsd.backend.webdav;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{DAV:}lockscope"/>
 *         &lt;element ref="{DAV:}locktype"/>
 *         &lt;element ref="{DAV:}depth"/>
 *         &lt;element ref="{DAV:}owner" minOccurs="0"/>
 *         &lt;element ref="{DAV:}timeout" minOccurs="0"/>
 *         &lt;element ref="{DAV:}locktoken" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lockscope",
    "locktype",
    "depth",
    "owner",
    "timeout",
    "locktoken"
})
@XmlRootElement(name = "activelock")
public class Activelock {

    @XmlElementRef(name = "lockscope", namespace = "DAV:", type = Lockscope.class)
    protected Lockscope lockscope;
    @XmlElementRef(name = "locktype", namespace = "DAV:", type = Locktype.class)
    protected Locktype locktype;
    @XmlElementRef(name = "depth", namespace = "DAV:", type = JAXBElement.class)
    protected JAXBElement<String> depth;
    @XmlElementRef(name = "owner", namespace = "DAV:", type = Owner.class, required = false)
    protected Owner owner;
    @XmlElementRef(name = "timeout", namespace = "DAV:", type = JAXBElement.class, required = false)
    protected JAXBElement<String> timeout;
    @XmlElementRef(name = "locktoken", namespace = "DAV:", type = Locktoken.class, required = false)
    protected Locktoken locktoken;

    /**
     * Ruft den Wert der lockscope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Lockscope }
     *     
     */
    public Lockscope getLockscope() {
        return lockscope;
    }

    /**
     * Legt den Wert der lockscope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Lockscope }
     *     
     */
    public void setLockscope(Lockscope value) {
        this.lockscope = value;
    }

    /**
     * Ruft den Wert der locktype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Locktype }
     *     
     */
    public Locktype getLocktype() {
        return locktype;
    }

    /**
     * Legt den Wert der locktype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Locktype }
     *     
     */
    public void setLocktype(Locktype value) {
        this.locktype = value;
    }

    /**
     * Ruft den Wert der depth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepth() {
        return depth;
    }

    /**
     * Legt den Wert der depth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepth(JAXBElement<String> value) {
        this.depth = value;
    }

    /**
     * Ruft den Wert der owner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Owner }
     *     
     */
    public Owner getOwner() {
        return owner;
    }

    /**
     * Legt den Wert der owner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Owner }
     *     
     */
    public void setOwner(Owner value) {
        this.owner = value;
    }

    /**
     * Ruft den Wert der timeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTimeout() {
        return timeout;
    }

    /**
     * Legt den Wert der timeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTimeout(JAXBElement<String> value) {
        this.timeout = value;
    }

    /**
     * Ruft den Wert der locktoken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Locktoken }
     *     
     */
    public Locktoken getLocktoken() {
        return locktoken;
    }

    /**
     * Legt den Wert der locktoken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Locktoken }
     *     
     */
    public void setLocktoken(Locktoken value) {
        this.locktoken = value;
    }

}
