
package de.chdev.jasync.xsd.backend.webdav;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element ref="{DAV:}creationdate" minOccurs="0"/>
 *         &lt;element ref="{DAV:}displayname" minOccurs="0"/>
 *         &lt;element ref="{DAV:}getcontentlanguage" minOccurs="0"/>
 *         &lt;element ref="{DAV:}getcontentlength" minOccurs="0"/>
 *         &lt;element ref="{DAV:}getcontenttype" minOccurs="0"/>
 *         &lt;element ref="{DAV:}getetag" minOccurs="0"/>
 *         &lt;element ref="{DAV:}getlastmodified" minOccurs="0"/>
 *         &lt;element ref="{DAV:}lockdiscovery" minOccurs="0"/>
 *         &lt;element ref="{DAV:}resourcetype" minOccurs="0"/>
 *         &lt;element ref="{DAV:}supportedlock" minOccurs="0"/>
 *         &lt;element ref="{DAV:}supported-report-set" minOccurs="0"/>
 *         &lt;element ref="{DAV:}quota-available-bytes" minOccurs="0"/>
 *         &lt;element ref="{DAV:}quota-used-bytes" minOccurs="0"/>
 *         &lt;any processContents='skip' namespace='##other' maxOccurs="unbounded"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "prop")
public class Prop {

    @XmlElementRef(name = "creationdate", namespace = "DAV:", type = Creationdate.class, required = false)
    protected Creationdate creationdate;
    @XmlElementRef(name = "displayname", namespace = "DAV:", type = Displayname.class, required = false)
    protected Displayname displayname;
    @XmlElementRef(name = "getcontentlanguage", namespace = "DAV:", type = Getcontentlanguage.class, required = false)
    protected Getcontentlanguage getcontentlanguage;
    @XmlElementRef(name = "getcontentlength", namespace = "DAV:", type = Getcontentlength.class, required = false)
    protected Getcontentlength getcontentlength;
    @XmlElementRef(name = "getcontenttype", namespace = "DAV:", type = Getcontenttype.class, required = false)
    protected Getcontenttype getcontenttype;
    @XmlElementRef(name = "getetag", namespace = "DAV:", type = Getetag.class, required = false)
    protected Getetag getetag;
    @XmlElementRef(name = "getlastmodified", namespace = "DAV:", type = Getlastmodified.class, required = false)
    protected Getlastmodified getlastmodified;
    @XmlElementRef(name = "lockdiscovery", namespace = "DAV:", type = Lockdiscovery.class, required = false)
    protected Lockdiscovery lockdiscovery;
    @XmlElementRef(name = "resourcetype", namespace = "DAV:", type = Resourcetype.class, required = false)
    protected Resourcetype resourcetype;
    @XmlElementRef(name = "supportedlock", namespace = "DAV:", type = Supportedlock.class, required = false)
    protected Supportedlock supportedlock;
    @XmlElementRef(name = "supported-report-set", namespace = "DAV:", type = SupportedReportSet.class, required = false)
    protected SupportedReportSet supportedReportSet;
    @XmlElementRef(name = "quota-available-bytes", namespace = "DAV:", type = QuotaAvailableBytes.class, required = false)
    protected QuotaAvailableBytes quotaAvailableBytes;
    @XmlElementRef(name = "quota-used-bytes", namespace = "DAV:", type = QuotaUsedBytes.class, required = false)
    protected QuotaUsedBytes quotaUsedBytes;
    @XmlAnyElement
    protected List<Element> any;

    /**
     * Ruft den Wert der creationdate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Creationdate }
     *     
     */
    public Creationdate getCreationdate() {
        return creationdate;
    }

    /**
     * Legt den Wert der creationdate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Creationdate }
     *     
     */
    public void setCreationdate(Creationdate value) {
        this.creationdate = value;
    }

    /**
     * Ruft den Wert der displayname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Displayname }
     *     
     */
    public Displayname getDisplayname() {
        return displayname;
    }

    /**
     * Legt den Wert der displayname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Displayname }
     *     
     */
    public void setDisplayname(Displayname value) {
        this.displayname = value;
    }

    /**
     * Ruft den Wert der getcontentlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Getcontentlanguage }
     *     
     */
    public Getcontentlanguage getGetcontentlanguage() {
        return getcontentlanguage;
    }

    /**
     * Legt den Wert der getcontentlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Getcontentlanguage }
     *     
     */
    public void setGetcontentlanguage(Getcontentlanguage value) {
        this.getcontentlanguage = value;
    }

    /**
     * Ruft den Wert der getcontentlength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Getcontentlength }
     *     
     */
    public Getcontentlength getGetcontentlength() {
        return getcontentlength;
    }

    /**
     * Legt den Wert der getcontentlength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Getcontentlength }
     *     
     */
    public void setGetcontentlength(Getcontentlength value) {
        this.getcontentlength = value;
    }

    /**
     * Ruft den Wert der getcontenttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Getcontenttype }
     *     
     */
    public Getcontenttype getGetcontenttype() {
        return getcontenttype;
    }

    /**
     * Legt den Wert der getcontenttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Getcontenttype }
     *     
     */
    public void setGetcontenttype(Getcontenttype value) {
        this.getcontenttype = value;
    }

    /**
     * Ruft den Wert der getetag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Getetag }
     *     
     */
    public Getetag getGetetag() {
        return getetag;
    }

    /**
     * Legt den Wert der getetag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Getetag }
     *     
     */
    public void setGetetag(Getetag value) {
        this.getetag = value;
    }

    /**
     * Ruft den Wert der getlastmodified-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Getlastmodified }
     *     
     */
    public Getlastmodified getGetlastmodified() {
        return getlastmodified;
    }

    /**
     * Legt den Wert der getlastmodified-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Getlastmodified }
     *     
     */
    public void setGetlastmodified(Getlastmodified value) {
        this.getlastmodified = value;
    }

    /**
     * Ruft den Wert der lockdiscovery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Lockdiscovery }
     *     
     */
    public Lockdiscovery getLockdiscovery() {
        return lockdiscovery;
    }

    /**
     * Legt den Wert der lockdiscovery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Lockdiscovery }
     *     
     */
    public void setLockdiscovery(Lockdiscovery value) {
        this.lockdiscovery = value;
    }

    /**
     * Ruft den Wert der resourcetype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Resourcetype }
     *     
     */
    public Resourcetype getResourcetype() {
        return resourcetype;
    }

    /**
     * Legt den Wert der resourcetype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Resourcetype }
     *     
     */
    public void setResourcetype(Resourcetype value) {
        this.resourcetype = value;
    }

    /**
     * Ruft den Wert der supportedlock-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Supportedlock }
     *     
     */
    public Supportedlock getSupportedlock() {
        return supportedlock;
    }

    /**
     * Legt den Wert der supportedlock-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Supportedlock }
     *     
     */
    public void setSupportedlock(Supportedlock value) {
        this.supportedlock = value;
    }

    /**
     * Ruft den Wert der supportedReportSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SupportedReportSet }
     *     
     */
    public SupportedReportSet getSupportedReportSet() {
        return supportedReportSet;
    }

    /**
     * Legt den Wert der supportedReportSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SupportedReportSet }
     *     
     */
    public void setSupportedReportSet(SupportedReportSet value) {
        this.supportedReportSet = value;
    }

    /**
     * Ruft den Wert der quotaAvailableBytes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QuotaAvailableBytes }
     *     
     */
    public QuotaAvailableBytes getQuotaAvailableBytes() {
        return quotaAvailableBytes;
    }

    /**
     * Legt den Wert der quotaAvailableBytes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QuotaAvailableBytes }
     *     
     */
    public void setQuotaAvailableBytes(QuotaAvailableBytes value) {
        this.quotaAvailableBytes = value;
    }

    /**
     * Ruft den Wert der quotaUsedBytes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QuotaUsedBytes }
     *     
     */
    public QuotaUsedBytes getQuotaUsedBytes() {
        return quotaUsedBytes;
    }

    /**
     * Legt den Wert der quotaUsedBytes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QuotaUsedBytes }
     *     
     */
    public void setQuotaUsedBytes(QuotaUsedBytes value) {
        this.quotaUsedBytes = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * 
     * 
     */
    public List<Element> getAny() {
        if (any == null) {
            any = new ArrayList<Element>();
        }
        return this.any;
    }

}
