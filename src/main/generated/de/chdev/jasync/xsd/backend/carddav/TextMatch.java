
package de.chdev.jasync.xsd.backend.carddav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="collation" type="{http://www.w3.org/2001/XMLSchema}string" default="i;unicode-casemap" />
 *       &lt;attribute name="negate-condition" default="no">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="yes"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="match-type" default="contains">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="equals"/>
 *             &lt;enumeration value="contains"/>
 *             &lt;enumeration value="starts-with"/>
 *             &lt;enumeration value="ends-with"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "text-match")
public class TextMatch {

    @XmlAttribute(name = "collation")
    protected String collation;
    @XmlAttribute(name = "negate-condition")
    protected String negateCondition;
    @XmlAttribute(name = "match-type")
    protected String matchType;

    /**
     * Ruft den Wert der collation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollation() {
        if (collation == null) {
            return "i;unicode-casemap";
        } else {
            return collation;
        }
    }

    /**
     * Legt den Wert der collation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollation(String value) {
        this.collation = value;
    }

    /**
     * Ruft den Wert der negateCondition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegateCondition() {
        if (negateCondition == null) {
            return "no";
        } else {
            return negateCondition;
        }
    }

    /**
     * Legt den Wert der negateCondition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegateCondition(String value) {
        this.negateCondition = value;
    }

    /**
     * Ruft den Wert der matchType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchType() {
        if (matchType == null) {
            return "contains";
        } else {
            return matchType;
        }
    }

    /**
     * Legt den Wert der matchType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchType(String value) {
        this.matchType = value;
    }

}
