
package de.chdev.jasync.xsd.backend.carddav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}propfilter"/>
 *       &lt;/sequence>
 *       &lt;attribute name="test" default="anyof">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="anyof"/>
 *             &lt;enumeration value="allof"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "propfilter"
})
@XmlRootElement(name = "filter")
public class Filter {

    @XmlElementRef(name = "propfilter", namespace = "urn:ietf:params:xml:ns:carddav", type = Propfilter.class)
    protected Propfilter propfilter;
    @XmlAttribute(name = "test")
    protected String test;

    /**
     * Ruft den Wert der propfilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Propfilter }
     *     
     */
    public Propfilter getPropfilter() {
        return propfilter;
    }

    /**
     * Legt den Wert der propfilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Propfilter }
     *     
     */
    public void setPropfilter(Propfilter value) {
        this.propfilter = value;
    }

    /**
     * Ruft den Wert der test-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTest() {
        if (test == null) {
            return "anyof";
        } else {
            return test;
        }
    }

    /**
     * Legt den Wert der test-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTest(String value) {
        this.test = value;
    }

}
