
package de.chdev.jasync.xsd.backend.carddav;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}allprop"/>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}prop" maxOccurs="unbounded"/>
 *       &lt;/choice>
 *       &lt;attribute name="content-type" type="{http://www.w3.org/2001/XMLSchema}string" default="text/vcard" />
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" default="3.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "allprop",
    "prop"
})
@XmlRootElement(name = "address-data")
public class AddressData {

    @XmlElementRef(name = "allprop", namespace = "urn:ietf:params:xml:ns:carddav", type = JAXBElement.class, required = false)
    protected JAXBElement<Object> allprop;
    @XmlElementRef(name = "prop", namespace = "urn:ietf:params:xml:ns:carddav", type = JAXBElement.class, required = false)
    protected List<JAXBElement<Prop>> prop;
    @XmlAttribute(name = "content-type")
    protected String contentType;
    @XmlAttribute(name = "version")
    protected String version;

    /**
     * Ruft den Wert der allprop-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public JAXBElement<Object> getAllprop() {
        return allprop;
    }

    /**
     * Legt den Wert der allprop-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void setAllprop(JAXBElement<Object> value) {
        this.allprop = value;
    }

    /**
     * Gets the value of the prop property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prop property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProp().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Prop }{@code >}
     * 
     * 
     */
    public List<JAXBElement<Prop>> getProp() {
        if (prop == null) {
            prop = new ArrayList<JAXBElement<Prop>>();
        }
        return this.prop;
    }

    /**
     * Ruft den Wert der contentType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentType() {
        if (contentType == null) {
            return "text/vcard";
        } else {
            return contentType;
        }
    }

    /**
     * Legt den Wert der contentType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentType(String value) {
        this.contentType = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        if (version == null) {
            return "3.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
