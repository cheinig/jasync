
package de.chdev.jasync.xsd.backend.carddav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import de.chdev.jasync.xsd.backend.webdav.Allprop;
import de.chdev.jasync.xsd.backend.webdav.Prop;
import de.chdev.jasync.xsd.backend.webdav.Propname;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element ref="{DAV:}allprop"/>
 *           &lt;element ref="{DAV:}propname"/>
 *           &lt;element ref="{DAV:}prop"/>
 *         &lt;/choice>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}filter"/>
 *         &lt;element ref="{urn:ietf:params:xml:ns:carddav}limit"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "allprop",
    "propname",
    "prop",
    "filter",
    "limit"
})
@XmlRootElement(name = "addressbook-query")
public class AddressbookQuery {

    @XmlElementRef(name = "allprop", namespace = "DAV:", type = Allprop.class, required = false)
    protected Allprop allprop;
    @XmlElementRef(name = "propname", namespace = "DAV:", type = Propname.class, required = false)
    protected Propname propname;
    @XmlElementRef(name = "prop", namespace = "DAV:", type = Prop.class, required = false)
    protected Prop prop;
    @XmlElementRef(name = "filter", namespace = "urn:ietf:params:xml:ns:carddav", type = Filter.class)
    protected Filter filter;
    @XmlElementRef(name = "limit", namespace = "urn:ietf:params:xml:ns:carddav", type = Limit.class)
    protected Limit limit;

    /**
     * Ruft den Wert der allprop-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Allprop }
     *     
     */
    public Allprop getAllprop() {
        return allprop;
    }

    /**
     * Legt den Wert der allprop-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Allprop }
     *     
     */
    public void setAllprop(Allprop value) {
        this.allprop = value;
    }

    /**
     * Ruft den Wert der propname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Propname }
     *     
     */
    public Propname getPropname() {
        return propname;
    }

    /**
     * Legt den Wert der propname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Propname }
     *     
     */
    public void setPropname(Propname value) {
        this.propname = value;
    }

    /**
     * Ruft den Wert der prop-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Prop }
     *     
     */
    public Prop getProp() {
        return prop;
    }

    /**
     * Legt den Wert der prop-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Prop }
     *     
     */
    public void setProp(Prop value) {
        this.prop = value;
    }

    /**
     * Ruft den Wert der filter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Filter }
     *     
     */
    public Filter getFilter() {
        return filter;
    }

    /**
     * Legt den Wert der filter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Filter }
     *     
     */
    public void setFilter(Filter value) {
        this.filter = value;
    }

    /**
     * Ruft den Wert der limit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Limit }
     *     
     */
    public Limit getLimit() {
        return limit;
    }

    /**
     * Legt den Wert der limit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Limit }
     *     
     */
    public void setLimit(Limit value) {
        this.limit = value;
    }

}
