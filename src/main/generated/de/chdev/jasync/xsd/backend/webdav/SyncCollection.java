
package de.chdev.jasync.xsd.backend.webdav;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{DAV:}sync-token"/>
 *         &lt;element ref="{DAV:}sync-level"/>
 *         &lt;element ref="{DAV:}limit" minOccurs="0"/>
 *         &lt;element ref="{DAV:}prop"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "syncToken",
    "syncLevel",
    "limit",
    "prop"
})
@XmlRootElement(name = "sync-collection")
public class SyncCollection {

    @XmlElementRef(name = "sync-token", namespace = "DAV:", type = JAXBElement.class)
    protected JAXBElement<String> syncToken;
    @XmlElementRef(name = "sync-level", namespace = "DAV:", type = JAXBElement.class)
    protected JAXBElement<String> syncLevel;
    @XmlElementRef(name = "limit", namespace = "DAV:", type = Limit.class, required = false)
    protected Limit limit;
    @XmlElementRef(name = "prop", namespace = "DAV:", type = Prop.class)
    protected Prop prop;

    /**
     * Ruft den Wert der syncToken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSyncToken() {
        return syncToken;
    }

    /**
     * Legt den Wert der syncToken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSyncToken(JAXBElement<String> value) {
        this.syncToken = value;
    }

    /**
     * Ruft den Wert der syncLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSyncLevel() {
        return syncLevel;
    }

    /**
     * Legt den Wert der syncLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSyncLevel(JAXBElement<String> value) {
        this.syncLevel = value;
    }

    /**
     * Ruft den Wert der limit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Limit }
     *     
     */
    public Limit getLimit() {
        return limit;
    }

    /**
     * Legt den Wert der limit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Limit }
     *     
     */
    public void setLimit(Limit value) {
        this.limit = value;
    }

    /**
     * Ruft den Wert der prop-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Prop }
     *     
     */
    public Prop getProp() {
        return prop;
    }

    /**
     * Legt den Wert der prop-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Prop }
     *     
     */
    public void setProp(Prop value) {
        this.prop = value;
    }

}
