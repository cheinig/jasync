package exceptions;

public class JasyncException extends RuntimeException {

    public JasyncException(String message){
        super(message);
    }

    public JasyncException(){}
}
