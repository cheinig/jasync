package exceptions;

public class ConfigurationException extends JasyncException {

    public ConfigurationException(String message) {
        super(message);
    }
}
