package de.chdev.jasync.model;

import java.util.HashMap;
import java.util.Map;

public class HttpTransfer {

    public enum Content { XML, WBXML }

    private String method;
    private String query;
    private Map<String, String> headers = new HashMap<>();
    private byte[] body;
    private Content contentType = Content.WBXML;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public Content getContentType() {
        return contentType;
    }

    public void setContentType(Content contentType) {
        this.contentType = contentType;
    }
}
