package de.chdev.jasync.model;

import java.util.List;

public class Response {

    private Request relatedRequest;
    private List<String> supportedVersions;
    private String serverVersion;
    private List<String> supportedCommands;
    private Status globalStatus;
    private Object objectData;
    private String xmlData;

    public Response(Request relatedRequest){
        this.relatedRequest = relatedRequest;
        globalStatus = new Status();
    }

    public List<String> getSupportedVersions() {
        return supportedVersions;
    }

    public void setSupportedVersions(List<String> supportedVersions) {
        this.supportedVersions = supportedVersions;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public List<String> getSupportedCommands() {
        return supportedCommands;
    }

    public void setSupportedCommands(List<String> supportedCommands) {
        this.supportedCommands = supportedCommands;
    }

    public Request getRelatedRequest() {
        return relatedRequest;
    }

    public void setRelatedRequest(Request relatedRequest) {
        this.relatedRequest = relatedRequest;
    }

    public Status getGlobalStatus() {
        return globalStatus;
    }

    public void setGlobalStatus(Status globalStatus) {
        this.globalStatus = globalStatus;
    }

    public Object getObjectData() {
        return objectData;
    }

    public void setObjectData(Object objectData) {
        this.objectData = objectData;
    }

    public String getXmlData() {
        return xmlData;
    }

    public void setXmlData(String xmlData) {
        this.xmlData = xmlData;
    }
}
