package de.chdev.jasync.model;

public class Status {

    // Constants
    public static final int OK = 0;
    public static final int ErrorInvalidXML = 103;
    public static final int ErrorServerError = 110;
    public static final int ErrorCommandNotSupported = 137;
    public static final int ErrorVersionNotSupported = 138;

    // Status values
    private Integer intValue=0;


    // getters and setters
    public Integer getIntValue() {
        return intValue;
    }

    public void setIntValue(Integer intValue) {
        this.intValue = intValue;
    }
}
