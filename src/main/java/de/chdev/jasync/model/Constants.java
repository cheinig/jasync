package de.chdev.jasync.model;

import org.apache.commons.collections.MapUtils;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String[] SUPPORTED_COMMANDS = {
            "Sync",
            "FolderSync"
    };

    public static final String[] SUPPORTED_VERSIONS = {
            "14.0",
            "14.1",
            "16.0",
            "16.1"
    };

    public static final String SERVER_VERSION = "16.0";

    public static final Map<String, String> COMMAND_NAMESPACE_MAP = MapUtils.putAll(new HashMap<String, String>(), new String[][] {
            {"sync", "AirSync"},
            {"foldersync", "FolderHierarchy"}
    });

    public static final Integer FOLDERTYPE_GENERIC = 1;
    public static final Integer FOLDERTYPE_INBOX = 2;
    public static final Integer FOLDERTYPE_DRAFTS = 3;
    public static final Integer FOLDERTYPE_DELETEDITEMS = 4;
    public static final Integer FOLDERTYPE_DEFAULT_CONTACT = 9;
    public static final Integer FOLDERTYPE_CUSTOM_CONTACT = 14;
}
