package de.chdev.jasync.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("base")
public class Configuration {

    String greeting;


    // GETTER AND SETTER
    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }



    // SINGLETON PATTERN
    private static Configuration instance;

    private Configuration(){
        instance = this;
    }

    public static Configuration getInstance() {
        return instance;
    }
}
