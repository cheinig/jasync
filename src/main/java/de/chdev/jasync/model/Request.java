package de.chdev.jasync.model;

import de.chdev.jasync.persistence.UserDevice;

import java.util.HashMap;
import java.util.Map;

public class Request {

    public enum Method { options, request, unknown }

    private Response relatedResponse;
    private UserDevice userDevice;
    private Method method;
    private String command;
    private String username;
    private String deviceId;
    private String deviceType;
    private Map<String, String> uriParameters = new HashMap<>();
    private String version;
    private Object objectData;
    private String xmlData;

    // Constructor
    public Request(){
        relatedResponse = new Response(this);
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Response getRelatedResponse() {
        return relatedResponse;
    }

    public void setRelatedResponse(Response relatedResponse) {
        this.relatedResponse = relatedResponse;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public Object getObjectData() {
        return objectData;
    }

    public void setObjectData(Object objectData) {
        this.objectData = objectData;
    }

    public String getXmlData() {
        return xmlData;
    }

    public void setXmlData(String xmlData) {
        this.xmlData = xmlData;
    }

    public String getCommandNamespace() {
        return Constants.COMMAND_NAMESPACE_MAP.get(command.toLowerCase());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Map<String, String> getUriParameters() {
        return uriParameters;
    }

    public void setUriParameters(Map<String, String> uriParameters) {
        this.uriParameters = uriParameters;
    }

    public UserDevice getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(UserDevice userDevice) {
        this.userDevice = userDevice;
    }
}
