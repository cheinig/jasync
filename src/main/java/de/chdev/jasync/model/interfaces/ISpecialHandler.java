package de.chdev.jasync.model.interfaces;

import de.chdev.jasync.model.Request;

public interface ISpecialHandler {

    Request handleRequest(Request request);
}
