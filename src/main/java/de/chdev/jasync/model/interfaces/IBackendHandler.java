package de.chdev.jasync.model.interfaces;

import de.chdev.jasync.persistence.UserDevice;

public interface IBackendHandler {
    void validateCredentials(UserDevice userDevice);
}
