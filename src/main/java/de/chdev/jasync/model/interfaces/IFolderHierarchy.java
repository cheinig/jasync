package de.chdev.jasync.model.interfaces;

import de.chdev.jasync.persistence.UserDevice;
import de.chdev.jasync.xsd.response.folderhierarchy.FolderSync;

public interface IFolderHierarchy {
    FolderSync.Changes getFolderChanges(Integer syncKey, UserDevice userDevice);
}
