package de.chdev.jasync.model.interfaces;

public interface IBackendInitializer {

    IBackendHandler init(String id);
}
