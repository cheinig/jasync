package de.chdev.jasync.backend.carddav;

import de.chdev.jasync.xsd.backend.webdav.Multistatus;
import de.chdev.jasync.xsd.backend.webdav.ObjectFactory;
import de.chdev.jasync.xsd.backend.webdav.Response;
import exceptions.AuthenticationFailedException;
import exceptions.ConfigurationException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.apache.tomcat.util.codec.binary.Base64;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class CardDavRESTFunctions {

    Logger log = Logger.getLogger(CardDavRESTFunctions.class);

    public static final int MAX_REDIRECTS=5;
    private CloseableHttpClient httpclient;

    public CardDavRESTFunctions() {
        httpclient = HttpClients.createDefault();
    }


    public URI autodiscoverUserDavURI(URI baseUri, String username, String password) {
        URI discoveredUri = autodiscoverUserDavURI(baseUri, username, password, 0);
        if (discoveredUri == null)
            throw new ConfigurationException("carddav uri cannot be found");
        return discoveredUri;
    }

    private URI autodiscoverUserDavURI(URI baseUri, String username, String password, int redirects) {
        String path;
        String requestBody = "<d:propfind xmlns:d=\"DAV:\"><d:prop><d:current-user-principal /></d:prop></d:propfind>";
        String depth = "0";
        String xpathExpression = "//*[local-name()='current-user-principal']/*[local-name()='href']";

        CloseableHttpResponse response = null;

        try {
            // Create Request
            HttpUriRequest propfindRequest = createPropfindRequest(baseUri, username, password, requestBody, depth);
            // Send request
            response = httpclient.execute(propfindRequest);

            switch (response.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_MOVED_PERMANENTLY:
                case 308:
                    // Follow redirects
                    if (redirects<MAX_REDIRECTS){
                        URI redirectedURI = URI.create(response.getFirstHeader("Location").getValue());
                        response.close();
                        return autodiscoverUserDavURI(redirectedURI, username, password, redirects+1);
                    }
                    break;
                case HttpStatus.SC_OK:
                case HttpStatus.SC_MULTI_STATUS:
                    String responseXml = EntityUtils.toString(response.getEntity());
                    path = getValueFromXMLByXpath(responseXml, xpathExpression);
                    return createNewURIWithNewPath(baseUri, path);
                case HttpStatus.SC_UNAUTHORIZED:
                    throw new AuthenticationFailedException();
                default:
                    // If a base URL is set without subpath, and an error occured, try to search for well-known carddav location
                    if (baseUri.getPath().isEmpty() || baseUri.getPath().equals("/")) {
                        URI redirectedURI = createNewURIWithNewPath(baseUri, "/.well-known/carddav");
                        response.close();
                        return autodiscoverUserDavURI(redirectedURI, username, password, redirects+1);
                    }
            }

        } catch (IOException | XPathExpressionException e) {
            e.printStackTrace();
        }finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private String getValueFromXMLByXpath(String responseXml, String xpathExpression) throws XPathExpressionException {
        InputSource source = new InputSource(new StringReader(responseXml));
        XPath xpath = XPathFactory.newInstance().newXPath();
        return xpath.compile(xpathExpression).evaluate(source);
    }

    private HttpUriRequest createPropfindRequest(URI davURI, String username, String password, String body, String depth) {
        // Create request
        RequestBuilder requestBuilder = RequestBuilder.create("PROPFIND");
        requestBuilder.setUri(davURI);

        // Set header
        requestBuilder.addHeader("Content-Type", "application/xml");
        requestBuilder.addHeader("Depth", depth);
        requestBuilder.addHeader("Authorization", createBasisAuthenticationString(username, password));

        // Set body
        HttpEntity entity = new StringEntity(body, "UTF-8");
        requestBuilder.setEntity(entity);

        return requestBuilder.build();
    }

    private String createBasisAuthenticationString(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));
        return "Basic " + new String(encodedAuth);
    }

    private URI createNewURIWithNewPath(URI baseURI, String path) {
        return URI.create(baseURI.getScheme()+"://"+ baseURI.getHost()+(baseURI.getPort()>0?":"+ baseURI.getPort():"")+path);
    }

    public List<Addressbook> getAddressbookList(URI uri, String username, String password) {
        List<Addressbook> addressbookList = new ArrayList<>();

        String requestBody = "<d:propfind xmlns:d=\"DAV:\" xmlns:cs=\"http://calendarserver.org/ns/\"><d:prop><d:resourcetype /><d:displayname /><cs:getctag /></d:prop></d:propfind>";
        String depth = "1";

        try {
            // Create Request
            HttpUriRequest propfindRequest = createPropfindRequest(uri, username, password, requestBody, depth);
            // Send request
            CloseableHttpResponse response = httpclient.execute(propfindRequest);

            switch (response.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_OK:
                case HttpStatus.SC_MULTI_STATUS:
                    // unmarshal response
                    Unmarshaller unmarshaller = JAXBContext.newInstance(ObjectFactory.class).createUnmarshaller();
                    Multistatus extractedResponse = (Multistatus)unmarshaller.unmarshal(response.getEntity().getContent());
                    // Handle response
                    addressbookList.addAll(getAddressbookListFromMultistatusResponse(extractedResponse));
                    break;
                case HttpStatus.SC_UNAUTHORIZED:
                    throw new AuthenticationFailedException();

            }

        } catch (IOException | JAXBException e) {
            e.printStackTrace();
        }

        return addressbookList;
    }

    protected List<Addressbook> getAddressbookListFromMultistatusResponse(Multistatus extractedResponse) {
        List<Addressbook> addressbookList = new ArrayList<>();
        for (Response multistatusResponse : extractedResponse.getResponse()){
            if (multistatusResponse.getPropstat().get(0).getProp().getResourcetype().getAddressbook() != null){
                Addressbook addressbook = new Addressbook();
                addressbook.url = multistatusResponse.getHref().get(0).getValue();
                addressbook.id = addressbook.url;
                addressbook.name = multistatusResponse.getPropstat().get(0).getProp().getDisplayname().getContent().get(0);
                addressbookList.add(addressbook);
            }
        }
        return addressbookList;
    }

    public URI autodiscoverUserHomeURI(URI userUri, String username, String password) {
        return autodiscoverUserHomeURI(userUri,username,password,0);
    }

    private URI autodiscoverUserHomeURI(URI userUri, String username, String password, int redirects) {
        String path;
        String requestBody = "" +
                "<d:propfind xmlns:d=\"DAV:\" xmlns:card=\"urn:ietf:params:xml:ns:carddav\">\n" +
                "  <d:prop>\n" +
                "     <card:addressbook-home-set />\n" +
                "  </d:prop>\n" +
                "</d:propfind>";
        String depth = "1";
        String xpathExpression = "//*[local-name()='addressbook-home-set']/*[local-name()='href']";

        try {
            // Create Request
            HttpUriRequest propfindRequest = createPropfindRequest(userUri, username, password, requestBody, depth);
            // Send request
            CloseableHttpResponse response = httpclient.execute(propfindRequest);


            // If a base URL is set without subpath, and an error occured, try to search for well-known carddav location
            // Follow redirects

            switch (response.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_MOVED_PERMANENTLY:
                    if (redirects<MAX_REDIRECTS) {
                        URI redirectedURI = URI.create(response.getFirstHeader("Location").getValue());
                        response.close();
                        return autodiscoverUserHomeURI(redirectedURI, username, password, redirects+1);
                    }
                    break;
                case HttpStatus.SC_UNAUTHORIZED:
                    throw new AuthenticationFailedException();
                default:
                    String responseXml = EntityUtils.toString(response.getEntity());
                    path = getValueFromXMLByXpath(responseXml, xpathExpression);
                    return createNewURIWithNewPath(userUri, path);
            }

        } catch (IOException | XPathExpressionException e) {
            e.printStackTrace();
        }

        return null;
    }

    class Addressbook {
        String id;
        String name;
        String url;
    }
}
