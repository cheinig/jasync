package de.chdev.jasync.backend.carddav;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.model.interfaces.IBackendHandler;
import de.chdev.jasync.model.interfaces.IFolderHierarchy;
import de.chdev.jasync.persistence.UserDevice;
import de.chdev.jasync.persistence.UserSetting;
import de.chdev.jasync.persistence.UserSettingRepository;
import de.chdev.jasync.xsd.response.folderhierarchy.FolderSync;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.List;

@Component
public class CardDavBackendImpl implements IBackendHandler, IFolderHierarchy {

    protected CardDavRESTFunctions restFunctions;
    private String backendId = "carddav";


    private enum userSettingsParameter { HOME };

    @Autowired
    UserSettingRepository userSettingRepository;

    public CardDavBackendImpl(){
        restFunctions = new CardDavRESTFunctions();
    }

    @PostConstruct
    private void register(){
        BackendRegistry.getInstance().addBackend(backendId, this);
    }

    @Override
    public void validateCredentials(UserDevice userDevice) {
        String davUrl = CardDavConfiguration.getInstance().getServer();
        URI autodiscoverUserDavURI = restFunctions.autodiscoverUserDavURI(URI.create(davUrl), userDevice.getUsername(), userDevice.getPassword());
        restFunctions.autodiscoverUserHomeURI(autodiscoverUserDavURI, userDevice.getUsername(), userDevice.getPassword());
    }

    @Override
    public FolderSync.Changes getFolderChanges(Integer syncKey, UserDevice userDevice) {
        FolderSync.Changes changes = new FolderSync.Changes();

        List<CardDavRESTFunctions.Addressbook> addressbookList = restFunctions.getAddressbookList(getAddressbookUri(userDevice), userDevice.getUsername(), userDevice.getPassword());

        for (CardDavRESTFunctions.Addressbook addressbook : addressbookList){
            changes.getAdd().add(createAddFromAddressbook(addressbook));
        }

        return changes;
    }

    protected FolderSync.Changes.Add createAddFromAddressbook(CardDavRESTFunctions.Addressbook addressbook) {
        FolderSync.Changes.Add add = new FolderSync.Changes.Add();
        add.setServerId(addressbook.id);
        add.setDisplayName(addressbook.name);
        return add;
    }

    protected URI getAddressbookUri(UserDevice userDevice) {
        List<UserSetting> homeSetting = userSettingRepository.getUserSettingsByUserDeviceAndBackendIdAndParameterName(userDevice, backendId, userSettingsParameter.HOME.name());
        if (homeSetting.size()>0){
            return URI.create(homeSetting.get(0).getParameterValue());
        } else {
            String davUrl = CardDavConfiguration.getInstance().getServer();
            URI autodiscoverUserDavURI = restFunctions.autodiscoverUserDavURI(URI.create(davUrl), userDevice.getUsername(), userDevice.getPassword());
            URI autodiscoverUserHomeURI = restFunctions.autodiscoverUserHomeURI(autodiscoverUserDavURI, userDevice.getUsername(), userDevice.getPassword());
            createUserSetting(userDevice, autodiscoverUserHomeURI, userSettingsParameter.HOME.name());
            return autodiscoverUserHomeURI;
        }
    }

    private void createUserSetting(UserDevice userDevice, URI autodiscoverUserHomeURI, String parameterName) {
        UserSetting userSetting = new UserSetting();
        userSetting.setUserDevice(userDevice);
        userSetting.setBackendId(backendId);
        userSetting.setParameterName(parameterName);
        userSetting.setParameterValue(autodiscoverUserHomeURI.toString());
        userSettingRepository.save(userSetting);
    }
}
