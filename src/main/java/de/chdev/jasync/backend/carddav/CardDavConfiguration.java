package de.chdev.jasync.backend.carddav;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("backend.carddav")
public class CardDavConfiguration {

    String id;
    String server;


    // GETTER AND SETTER
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }


    // SINGLETON PATTERN
    private static CardDavConfiguration instance;

    private CardDavConfiguration(){
        instance = this;
    }

    public static CardDavConfiguration getInstance() {
        if (instance==null){
            new CardDavConfiguration();
        }
        return instance;
    }

}
