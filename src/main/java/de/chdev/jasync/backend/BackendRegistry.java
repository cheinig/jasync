package de.chdev.jasync.backend;

import de.chdev.jasync.model.interfaces.IBackendHandler;
import de.chdev.jasync.model.interfaces.IBackendInitializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BackendRegistry {

    private static BackendRegistry instance;

    private Map<String, IBackendHandler> backendHandlerList = new HashMap<>();

    private BackendRegistry(){
    }

    public static BackendRegistry getInstance() {
        if (instance==null) {
            instance = new BackendRegistry();
        }
        return instance;
    }

    public void addBackend(String backendId, IBackendHandler backendHandler){
        backendHandlerList.put(backendId, backendHandler);
    }

    public void removeBackend(String backendId){
        backendHandlerList.remove(backendId);
    }

    public void removeAllBackends(){
        backendHandlerList.clear();
    }

    public List<IBackendHandler> getBackendHandlerList(){
        List<IBackendHandler> backendHandlerListCopy = new ArrayList<>();
        backendHandlerListCopy.addAll(backendHandlerList.values());
        return backendHandlerListCopy;
    }

    public List<IBackendHandler> getBackendHandlerList(Class clazz){
        List<IBackendHandler> backendHandlerListCopy = new ArrayList<>();
        for (Object backendHandler : backendHandlerList.values()) {
            if (clazz.isAssignableFrom(backendHandler.getClass())){
                backendHandlerListCopy.add((IBackendHandler)backendHandler);
            }
        }
        return backendHandlerListCopy;
    }
}
