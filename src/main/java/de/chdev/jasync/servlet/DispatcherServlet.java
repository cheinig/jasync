package de.chdev.jasync.servlet;

import de.chdev.jasync.workflow.IncomingRequestHandler;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DispatcherServlet extends HttpServlet {

    private Logger log = Logger.getLogger(this.getClass());

    private IncomingRequestHandler requestHandler;

    public DispatcherServlet(IncomingRequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("options");
        requestHandler.handleIncomingRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("post");
        requestHandler.handleIncomingRequest(req, resp);
    }


}
