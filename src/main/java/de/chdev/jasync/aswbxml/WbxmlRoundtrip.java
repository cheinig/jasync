package de.chdev.jasync.aswbxml;


import java.io.*;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.kxml2.io.*;
import org.kxml2.wap.*;
import org.xmlpull.v1.*;

/*
 * Created on 25.09.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/**
 * @author haustein
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WbxmlRoundtrip {

    public static void main(String[] argv) throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        FileInputStream fileInputStream = new FileInputStream(argv[0]);
        FileOutputStream fileOutputStream = new FileOutputStream(argv[1]);

        if (argv[0].endsWith(".xml")){
            String xmlString = org.apache.commons.io.IOUtils.toString(fileInputStream, "UTF-8");
            ActiveSyncWbxmlConverter converter = new ActiveSyncWbxmlConverter(xmlString);
            IOUtils.write(converter.getWbxml(), fileOutputStream);
        } else if (argv[0].endsWith(".wbxml")){
            byte[] bytes = org.apache.commons.io.IOUtils.toByteArray(fileInputStream);
            ActiveSyncWbxmlConverter converter = new ActiveSyncWbxmlConverter(bytes);
            IOUtils.write(converter.getXmlString(), fileOutputStream, Charset.forName("UTF-8"));
        }
    }

}