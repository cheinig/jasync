package de.chdev.jasync.aswbxml;

import org.apache.log4j.Logger;
import org.kxml2.wap.Wbxml;
import org.xmlpull.v1.XmlSerializer;

import java.io.*;
import java.util.*;


public class ActiveSyncSerializer implements XmlSerializer {

    Logger log = Logger.getLogger(this.getClass());

    // Constants
    final Integer WBXMLVERSION = 3;
    final Integer DEFAULTPUBLICIDENTIFIER = 1;

    // Variables
    Hashtable stringTable = new Hashtable();
    OutputStream out;
    ByteArrayOutputStream mainBuffer = new ByteArrayOutputStream();
    ByteArrayOutputStream stringTableBuf = new ByteArrayOutputStream();

    String encoding;
    Integer currentDepth=0;
    String pendingTagName;
    Integer attrPage = 0;
    Integer currentTagPage = 0;
    Boolean useInlineStrings = true;

    Stack<String> targetNamespaceStack = new Stack<>();
    Stack<Integer> targetNamespaceDepthStack = new Stack<>();
    Map<Integer, Map<String, String>> namespaceMap = new HashMap<>();
    Map<String, Integer> namespaceTagPageIndex = new HashMap<>();

    Map<Integer, ArrayList<String>> tagPageMap = new HashMap<>();


    // Init methods
    /**
     * Sets the tag table for a given page.
     * The first string in the array defines tag 5, the second tag 6 etc.
     */

    public void setTagTable(int page, ArrayList<String> tagTable, String namespace) {
        tagPageMap.put(page, tagTable);
        if (namespace!=null)
            namespaceTagPageIndex.put(namespace, page);
    }


    // Interface Implementation
    @Override
    public void setFeature(String s, boolean b) throws IllegalArgumentException, IllegalStateException {
        log.trace("called setFeature");
    }

    @Override
    public boolean getFeature(String s) {
        log.trace("called getFeature");
        return false;
    }

    @Override
    public void setProperty(String s, Object o) throws IllegalArgumentException, IllegalStateException {
        log.trace("called setProperty");
    }

    @Override
    public Object getProperty(String s) {
        log.trace("called getProperty");
        return null;
    }

    @Override
    public void setOutput(OutputStream out, String encoding) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called setOutput (stream");
        this.encoding = encoding == null ? "UTF-8" : encoding;
        this.out = out;

        mainBuffer = new ByteArrayOutputStream();
        stringTableBuf = new ByteArrayOutputStream();
    }

    @Override
    public void setOutput(Writer writer) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called setFeature (writer)");
        throw new RuntimeException ("Wbxml requires an OutputStream!");
    }

    @Override
    public void startDocument(String s, Boolean aBoolean) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called startDocument");

        out.write(WBXMLVERSION); // version 1.3
        // http://www.openmobilealliance.org/tech/omna/omna-wbxml-public-docid.htm
        out.write(DEFAULTPUBLICIDENTIFIER); // unknown or missing public identifier

        // default encoding is UTF-8

        if(s != null){
            encoding = s;
        }

        out.write(getIanaMibNumberForEncodingString(encoding));
    }

    @Override
    public void endDocument() throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called endDocument");
        out.write(getWbxmlInt(stringTableBuf.size()));

        // write StringTable

        out.write(stringTableBuf.toByteArray());

        // write mainBuffer

        out.write(mainBuffer.toByteArray());

        // ready!

        out.flush();
    }

    @Override
    public void setPrefix(String s, String s1) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called setPrefix");
    }

    @Override
    public String getPrefix(String s, boolean b) throws IllegalArgumentException {
        log.trace("called getPrefix");
        return null;
    }

    @Override
    public int getDepth() {
        log.trace("called getDepth");
        return 0;
    }

    @Override
    public String getNamespace() {
        log.trace("called getNamespacePrefix");
        return null;
    }

    @Override
    public String getName() {
        log.trace("called getName");
        return null;
    }

    @Override
    public XmlSerializer startTag(String s, String tagName) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called startTag: "+s+" / "+tagName);

        checkPending(false);
        currentDepth++;
        pendingTagName = tagName;

        return this;
    }

    @Override
    public XmlSerializer attribute(String s, String attributeKey, String attributeValue) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called attribute: "+s+" / "+attributeKey+" / "+attributeValue);
        getNamespaceFromAttribute(attributeKey, attributeValue);

        // TODO handle other attributes

        return this;
    }

    @Override
    public XmlSerializer endTag(String s, String s1) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called endTag");

        if (pendingTagName != null)
            checkPending(true);
        else
            mainBuffer.write(Wbxml.END);

        currentDepth--;

        // Check if namespacePrefix has changed
        if (!targetNamespaceDepthStack.isEmpty() && targetNamespaceDepthStack.peek()<currentDepth){
            targetNamespaceDepthStack.pop();
            targetNamespaceStack.pop();
        }
        if (namespaceMap.containsKey(currentDepth+1)){
            namespaceMap.remove(currentDepth+1);
        }

        return this;
    }

    @Override
    public XmlSerializer text(String text) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called text (String)");

        checkPending(false);

        mainBuffer.write(getByteArrayFromString(text.trim()));

        return this;
    }

    @Override
    public XmlSerializer text(char[] chars, int start, int length) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called text (chars)");
        checkPending(false);

        mainBuffer.write(getByteArrayFromString(new String(chars, start, length)));

        return this;
    }

    @Override
    public void cdsect(String s) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called cdsect");

    }

    @Override
    public void entityRef(String s) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called entityRef");
    }

    @Override
    public void processingInstruction(String s) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called processingInstruction");
    }

    @Override
    public void comment(String s) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called comment");
    }

    @Override
    public void docdecl(String s) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called docdecl");
    }

    @Override
    public void ignorableWhitespace(String s) throws IOException, IllegalArgumentException, IllegalStateException {
        log.trace("called ignorableWhitespace");
        // will be ignored
    }

    @Override
    public void flush() throws IOException {
        log.trace("called flush");
    }



    // Own implementations

    protected byte getIanaMibNumberForEncodingString(String encoding) throws UnsupportedEncodingException {

        Map<String, Integer> mibEncodingMap = new HashMap<>();
        mibEncodingMap.put("UTF-8", 106);
        mibEncodingMap.put("ISO-8859-1", 4);

        if (mibEncodingMap.containsKey(encoding.toUpperCase())){
            return mibEncodingMap.get(encoding.toUpperCase()).byteValue();
        } else {
            throw new UnsupportedEncodingException(encoding);
        }
    }

    protected void getNamespaceFromAttribute(String attributeKey, String attributeValue) {
        if (attributeKey.equalsIgnoreCase("xmlns")){
            targetNamespaceStack.push(attributeValue);
            targetNamespaceDepthStack.push(currentDepth);
        } else if (attributeKey.startsWith("xmlns:")){
            Map<String, String> namespaceDepthMap = namespaceMap.get(currentDepth);
            if (namespaceDepthMap==null) {
                namespaceDepthMap = new HashMap<>();
                namespaceMap.put(currentDepth, namespaceDepthMap);
            }
            namespaceDepthMap.put(attributeKey.substring(attributeKey.indexOf(':')+1), attributeValue);
        }
    }

    protected byte[] getWbxmlInt(int i) throws IOException {

        byte[] resultArray;
        byte[] tempBuffer = new byte[5];
        int idx = 0;

        do {
            tempBuffer[idx++] = (byte) (i & 0x7f);
            i = i >> 7;
        } while (i != 0);

        resultArray = new byte[idx];
        while (idx > 1) {
            idx--;
            resultArray[resultArray.length-idx-1] = (byte)(tempBuffer[idx] | 0x80);
        }
        resultArray[resultArray.length-1] = tempBuffer[0];

        return resultArray;
    }

    public void checkPending(boolean degenerated) throws IOException {
        if (pendingTagName == null)
            return;

        String usedTagName = pendingTagName.contains(":")?pendingTagName.substring(pendingTagName.indexOf(':')+1):pendingTagName;
        String usedNamespace = pendingTagName.contains(":")?pendingTagName.substring(0, pendingTagName.indexOf(':')):"";
        Integer usedTagPage = getTagPageIdByNamespace(usedNamespace);


        String[] test = new String[]{"1","2"};
        int idx = tagPageMap.get(usedTagPage).indexOf(usedTagName);

        if (idx<0){
            mainBuffer.write(degenerated ? Wbxml.LITERAL : Wbxml.LITERAL_C);
            mainBuffer.write(getStringTableBytes(usedTagName, false));
        } else {
            if(usedTagPage != currentTagPage){
                currentTagPage=usedTagPage;
                mainBuffer.write(Wbxml.SWITCH_PAGE);
                mainBuffer.write(currentTagPage);
            }

            mainBuffer.write(
                    (degenerated ? idx+5 : idx+5 | 64));
        }

        pendingTagName = null;
    }

    private Integer getTagPageIdByNamespace(String namespace) {
        Integer tagPageId=null;

        if (!namespace.isEmpty()){
            for (int depth=currentDepth; depth>=0; depth--){
                if (namespaceMap.containsKey(depth)){
                    String tagPageName = namespaceMap.get(depth).get(namespace);
                    if (tagPageName!=null){
                        tagPageId = namespaceTagPageIndex.get(tagPageName);
                        break;
                    }
                }
            }
        } else if (!targetNamespaceStack.empty() && namespaceTagPageIndex.get(targetNamespaceStack.peek()) != null) {
            tagPageId = namespaceTagPageIndex.get(targetNamespaceStack.peek());
        }

        if (tagPageId==null){
            tagPageId = currentTagPage;
        }

        return tagPageId;
    }

    protected byte[] getStringTableBytes(String s, boolean mayPrependSpace) throws IOException {

        Integer idx = (Integer) stringTable.get(s);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        if (idx != null) {
            bos.write(getWbxmlInt(idx.intValue()));
        }
        else{
            int i = stringTableBuf.size();
            if(s.charAt(0) >= '0' && mayPrependSpace){
                s = ' ' + s;
                bos.write(getWbxmlInt(i+1));
            }
            else{
                bos.write(getWbxmlInt(i));
            }

            stringTable.put(s, new Integer(i));
            if(s.charAt(0) == ' '){
                stringTable.put(s.substring(1), new Integer(i+1));
            }
            int j = s.lastIndexOf(' ');
            if(j > 1){
                stringTable.put(s.substring(j), new Integer(i+j));
                stringTable.put(s.substring(j+1), new Integer(i+j+1));
            }

            stringTableBuf.write(getStringInlineBytes(s));
            stringTableBuf.flush();
        }

        return bos.toByteArray();
    }

    protected byte[] getStringInlineBytes(String s) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(s.getBytes(encoding));
        bos.write(0);
        return bos.toByteArray();
    }

    /** Used in text() and attribute() to write text */

    protected byte[] getByteArrayFromString(String text) throws IOException{
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        int p0 = 0;
        int lastCut = 0;
        int len = text.length();

        while(p0 < len){
            while(p0 < len && text.charAt(p0) < 'A' ){ // skip interpunctation
                p0++;
            }
            int p1 = p0;
            while(p1 < len && text.charAt(p1) >= 'A'){
                p1++;
            }

            if(p1 - p0 > 10) {

                if(p0 > lastCut && text.charAt(p0-1) == ' '
                        && stringTable.get(text.substring(p0, p1)) == null){

                    writeTextToStream(bos,text.substring(lastCut, p1));
                }
                else {

                    if(p0 > lastCut && text.charAt(p0-1) == ' '){
                        p0--;
                    }

                    if(p0 > lastCut){
                        writeTextToStream(bos,text.substring(lastCut, p0));
                    }

                    writeTextToStream(bos, text.substring(p0, p1));
                }
                lastCut = p1;
            }
            p0 = p1;
        }

        if(lastCut < len){
            writeTextToStream(bos, text.substring(lastCut, len));
        }

        return bos.toByteArray();
    }

    private void writeTextToStream(ByteArrayOutputStream bos, String textToBytes) throws IOException {
        if (useInlineStrings) {
            bos.write(Wbxml.STR_I);
            bos.write(getStringInlineBytes(textToBytes));
        } else {
            bos.write(Wbxml.STR_T);
            bos.write(getStringTableBytes(textToBytes, false));
        }
    }
}
