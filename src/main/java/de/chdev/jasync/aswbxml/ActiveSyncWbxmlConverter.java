package de.chdev.jasync.aswbxml;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.kxml2.io.KXmlParser;
import org.kxml2.io.KXmlSerializer;
import org.kxml2.wap.WbxmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class ActiveSyncWbxmlConverter {

    private Logger log = Logger.getLogger(this.getClass());

    private byte[] wbxml;
    private String xml;
    protected ActiveSyncParser parser = ActiveSyncWbxml.createParser();
    protected ActiveSyncSerializer serializer = ActiveSyncWbxml.createSerializer();

    public ActiveSyncWbxmlConverter(byte[] wbxml) {
        this.wbxml = wbxml;
    }

    public ActiveSyncWbxmlConverter(String xml) {
        this.xml = xml;
    }

    public String getXmlString(){
        if (xml==null && wbxml != null) {
            try {
                convertToXml();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return xml;
    }

    public byte[] getWbxml() {
        if (wbxml==null && xml != null) {
            try {
                convertToWbxml();
            } catch (XmlPullParserException e) {
                log.error("Cannot convert wbxml to xml", e);
            } catch (IOException e) {
                log.error("Cannot convert wbxml to xml", e);
            }
        }
        return wbxml;
    }

    private void convertToWbxml() throws XmlPullParserException, IOException {
        // Guard
        if (xml == null) {
            wbxml = new byte[]{};
            return;
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        XmlPullParser xp = new KXmlParser();
        xp.setInput(IOUtils.toInputStream(xml, Charset.forName("UTF-8")), null);
        serializer.setOutput(bos, null);

        new Roundtrip(xp, serializer).roundTrip();

        wbxml = bos.toByteArray();

    }

    private void convertToXml() throws XmlPullParserException, IOException {
        // Guard
        if (wbxml == null) {
            xml = "";
            return;
        }

        ByteArrayInputStream bis = new ByteArrayInputStream(wbxml);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        parser.setInput(bis, null);

        XmlSerializer xs = new KXmlSerializer();
        xs.setOutput(bos,  "UTF-8");

        new Roundtrip(parser, xs).roundTrip();

        xml = bos.toString("UTF-8");
        xml = xml.replaceAll("(<\\?xml version='1\\.0' encoding='UTF-8' \\?><\\w*)>", "$1"+parser.getNamespaceAttributeString()+">");
    }

}
