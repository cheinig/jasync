package de.chdev.jasync.aswbxml;

import org.kxml2.wap.WbxmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class ActiveSyncWbxml {

    /** Creates a WbxmlParser with the ActiveSync code pages set */

    public static ActiveSyncParser createParser() {
        ActiveSyncParser p = new ActiveSyncParser();
//        WbxmlParser p = new WbxmlParser();
        p.setTagTable(0, TAG_TABLE_0, "AirSync");
        p.setTagTable(1, TAG_TABLE_1, "Contacts");
        p.setTagTable(2, TAG_TABLE_2, "Email");
        p.setTagTable(4, TAG_TABLE_4, "Calendar");
        p.setTagTable(5, TAG_TABLE_5, "Move");
        p.setTagTable(6, TAG_TABLE_6, "GetItemEstimate");
        p.setTagTable(7, TAG_TABLE_7, "FolderHierarchy");
        p.setTagTable(8, TAG_TABLE_8, "MeetingResponse");
        p.setTagTable(9, TAG_TABLE_9, "Tasks");
        p.setTagTable(10, TAG_TABLE_10, "ResolveRecipients");
        p.setTagTable(11, TAG_TABLE_11, "ValidateCert");
        p.setTagTable(12, TAG_TABLE_12, "Contacts2");
        p.setTagTable(13, TAG_TABLE_13, "Ping");
        p.setTagTable(14, TAG_TABLE_14, "Provision");
        p.setTagTable(15, TAG_TABLE_15, "Search");
        p.setTagTable(16, TAG_TABLE_16, "GAL");
        p.setTagTable(17, TAG_TABLE_17, "AirSyncBase");
        p.setTagTable(18, TAG_TABLE_18, "Settings");
        p.setTagTable(19, TAG_TABLE_19, "DocumentLibrary");
        p.setTagTable(20, TAG_TABLE_20, "ItemOperations");
        p.setTagTable(21, TAG_TABLE_21, "ComposeMail");
        p.setTagTable(22, TAG_TABLE_22, "Email2");
        p.setTagTable(23, TAG_TABLE_23, "Notes");
        p.setTagTable(24, TAG_TABLE_24, "RightsManagement");
        p.setTagTable(25, TAG_TABLE_25, "Find");
        return p;
    }

    public static ActiveSyncSerializer createSerializer() {
        ActiveSyncSerializer s = new ActiveSyncSerializer();
        s.setTagTable(0, new ArrayList<>(Arrays.asList(TAG_TABLE_0)), "AirSync");
        s.setTagTable(1, new ArrayList<>(Arrays.asList(TAG_TABLE_1)), "Contacts");
        s.setTagTable(2, new ArrayList<>(Arrays.asList(TAG_TABLE_2)), "Email");
        s.setTagTable(4, new ArrayList<>(Arrays.asList(TAG_TABLE_4)), "Calendar");
        s.setTagTable(5, new ArrayList<>(Arrays.asList(TAG_TABLE_5)), "Move");
        s.setTagTable(6, new ArrayList<>(Arrays.asList(TAG_TABLE_6)), "GetItemEstimate");
        s.setTagTable(7, new ArrayList<>(Arrays.asList(TAG_TABLE_7)), "FolderHierarchy");
        s.setTagTable(8, new ArrayList<>(Arrays.asList(TAG_TABLE_8)), "MeetingResponse");
        s.setTagTable(9, new ArrayList<>(Arrays.asList(TAG_TABLE_9)), "Tasks");
        s.setTagTable(10, new ArrayList<>(Arrays.asList(TAG_TABLE_10)), "ResolveRecipients");
        s.setTagTable(11, new ArrayList<>(Arrays.asList(TAG_TABLE_11)), "ValidateCert");
        s.setTagTable(12, new ArrayList<>(Arrays.asList(TAG_TABLE_12)), "Contacts2");
        s.setTagTable(13, new ArrayList<>(Arrays.asList(TAG_TABLE_13)), "Ping");
        s.setTagTable(14, new ArrayList<>(Arrays.asList(TAG_TABLE_14)), "Provision");
        s.setTagTable(15, new ArrayList<>(Arrays.asList(TAG_TABLE_15)), "Search");
        s.setTagTable(16, new ArrayList<>(Arrays.asList(TAG_TABLE_16)), "GAL");
        s.setTagTable(17, new ArrayList<>(Arrays.asList(TAG_TABLE_17)), "AirSyncBase");
        s.setTagTable(18, new ArrayList<>(Arrays.asList(TAG_TABLE_18)), "Settings");
        s.setTagTable(19, new ArrayList<>(Arrays.asList(TAG_TABLE_19)), "DocumentLibrary");
        s.setTagTable(20, new ArrayList<>(Arrays.asList(TAG_TABLE_20)), "ItemOperations");
        s.setTagTable(21, new ArrayList<>(Arrays.asList(TAG_TABLE_21)), "ComposeMail");
        s.setTagTable(22, new ArrayList<>(Arrays.asList(TAG_TABLE_22)), "Email2");
        s.setTagTable(23, new ArrayList<>(Arrays.asList(TAG_TABLE_23)), "Notes");
        s.setTagTable(24, new ArrayList<>(Arrays.asList(TAG_TABLE_24)), "RightsManagement");
        s.setTagTable(25, new ArrayList<>(Arrays.asList(TAG_TABLE_25)), "Find");
        return s;
    }

    public static final String [] TAG_TABLE_0 = {
            "Sync", // 05
            "Responses", // 06
            "Add", // 07
            "Change", // 08
            "Delete", // 09
            "Fetch", // 0A
            "SyncKey", // 0B
            "ClientId", // 0C
            "ServerId", // 0D
            "Status", // 0E
            "Collection", // 0F

            "Class", // 10
            null, // 11
            "CollectionId", // 12
            "GetChanges", // 13
            "MoreAvailable", // 14
            "WindowSize", // 15
            "Commands", // 16
            "Options", // 17
            "FilterType", // 18
            "Truncation", // 19
            null, // 1A
            "Conflict", // 1B
            "Collections",  // 1C
            "ApplicationData", // 1D
            "DeletesAsMoves", // 1E
            null, // 1F

            "Supported", // 20
            "SoftDelete", // 21
            "MIMESupport", // 22
            "MIMETruncation", // 23
            "Wait", // 24
            "Limit", // 25
            "Partial", // 26
            "ConversationMode", // 27
            "MaxItems", // 28
            "HeartbeatInterval" // 29
    };

    public static final String [] TAG_TABLE_1 = {
            "Anniversary",              // 05
            "AssistantName",            // 06
            "AssistantPhoneNumber",     // 07
            "Birthday",                 // 08
            "Body",                     // 09
            "BodySize",                 // 0A
            "BodyTruncated",            // 0B
            "Business2PhoneNumber",     // 0C
            "BusinessAddressCity",      // 0D
            "BusinessAddressCountry",   // 0E
            "BusinessAddressPostalCode",// 0F
            "BusinessAddressState",     // 10
            "BusinessAddressStreet",    // 11
            "BusinessFaxNumber",        // 12
            "BusinessPhoneNumber",      // 13
            "CarPhoneNumber",           // 14
            "Categories",               // 15
            "Category",                 // 16
            "Children",                 // 17
            "Child",                    // 18
            "CompanyName",              // 19
            "Department",               // 1A
            "Email1Address",            // 1B
            "Email2Address",            // 1C
            "Email3Address",            // 1D
            "FileAs",                   // 1E
            "FirstName",                // 1F
            "Home2PhoneNumber",         // 20
            "HomeAddressCity",          // 21
            "HomeAddressCountry",       // 22
            "HomeAddressPostalCode",    // 23
            "HomeAddressState",         // 24
            "HomeAddressStreet",        // 25
            "HomeFaxNumber",            // 26
            "HomePhoneNumber",          // 27
            "JobTitle",                 // 28
            "LastName",                 // 29
            "MiddleName",               // 2A
            "MobilePhoneNumber",        // 2B
            "OfficeLocation",           // 2C
            "OtherAddressCity",         // 2D
            "OtherAddressCountry",      // 2E
            "OtherAddressPostalCode",   // 2F
            "OtherAddressState",        // 30
            "OtherAddressStreet",       // 31
            "PagerNumber",              // 32
            "RadioPhoneNumber",         // 33
            "Spouse",                   // 34
            "Suffix",                   // 35
            "Title",                    // 36
            "WebPage",                  // 37
            "YomiCompanyName",          // 38
            "YomiFirstName",            // 39
            "YomiLastName",             // 3A
            null,                       // 3B
            "Picture",                  // 3C
            "Alias",                    // 3D
            "WeightedRank"              // 3E
    };

    public static final String [] TAG_TABLE_2 = {
            "Attachment",               // 05
            "Attachments",              // 06
            "AttName",                  // 07
            "AttSize",                  // 08
            "Att0id",                   // 09
            "AttMethod",                // 0A
            null,                       // 0B
            "Body",                     // 0C
            "BodySize",                 // 0D
            "BodyTruncated",            // 0E
            "DateReceived",             // 0F
            "DisplayName",              // 10
            "DisplayTo",                // 11
            "Importance",               // 12
            "MessageClass",             // 13
            "Subject",                  // 14
            "Read",                     // 15
            "To",                       // 16
            "Cc",                       // 17
            "From",                     // 18
            "ReplyTo",                  // 19
            "AllDayEvent",              // 1A
            "Categories",               // 1B
            "Category",                 // 1C
            "DtStamp",                  // 1D
            "EndTime",                  // 1E
            "InstanceType",             // 1F
            "BusyStatus",               // 20
            "Location",                 // 21
            "MeetingRequest",           // 22
            "Organizer",                // 23
            "RecurrenceId",             // 24
            "Reminder",                 // 25
            "ResponseRequested",        // 26
            "Recurrences",              // 27
            "Recurrence",               // 28
            "Type",                     // 29
            "Until",                    // 2A
            "Occurrences",              // 2B
            "Interval",                 // 2C
            "DayOfWeek",                // 2D
            "DayOfMonth",               // 2E
            "WeekOfMonth",              // 2F
            "MonthOfYear",              // 30
            "StartTime",                // 31
            "Sensitivity",              // 32
            "TimeZone",                 // 33
            "GlobalObjId",              // 34
            "ThreadTopic",              // 35
            "MIMEData",                 // 36
            "MIMETruncated",            // 37
            "MIMESize",                 // 38
            "InternetCPID",             // 39
            "Flag",                     // 3A
            "Status",                   // 3B
            "ContentClass",             // 3C
            "FlagType",                 // 3D
            "CompleteTime",             // 3E
            "DisallowNewTimeProposal"   // 3F
    };

    public static final String [] TAG_TABLE_4 = {
            "Timezone",                 // 05
            "AllDayEvent",              // 06
            "Attendees",                // 07
            "Attendee",                 // 08
            "Email",                    // 09
            "Name",                     // 0A
            "Body",                     // 0B
            "BodyTruncated",            // 0C
            "BusyStatus",               // 0D
            "Categories",               // 0E
            "Category",                 // 0F
            null,                       // 10
            "DtStamp",                  // 11
            "EndTime",                  // 12
            "Exception",                // 13
            "Exceptions",               // 14
            "Deleted",                  // 15
            "ExceptionStartTime",       // 16
            "Location",                 // 17
            "MeetingStatus",            // 18
            "OrganizerEmail",           // 19
            "OrganizerName",            // 1A
            "Recurrence",               // 1B
            "Type",                     // 1C
            "Until",                    // 1D
            "Occurrences",              // 1E
            "Interval",                 // 1F
            "DayOfWeek",                // 20
            "DayOfMonth",               // 21
            "WeekOfMonth",              // 22
            "MonthOfYear",              // 23
            "Reminder",                 // 24
            "Sensitivity",              // 25
            "Subject",                  // 26
            "StartTime",                // 27
            "UID",                      // 28
            "AttendeeStatus",           // 29
            "AttendeeType",             // 2A
            null,                       // 2B
            null,                       // 2C
            null,                       // 2D
            null,                       // 2E
            null,                       // 2F
            null,                       // 30
            null,                       // 31
            null,                       // 32
            "DisallowNewTimeProposal",  // 33
            "ResponseRequested",        // 34
            "AppointmentReplyTime",     // 35
            "ResponseType",             // 36
            "CalendarType",             // 37
            "IsLeapMonth",              // 38
            "FirstDayOfWeek",           // 39
            "OnlineMeetingConfLink",    // 3A
            "OnlineMeetingExternalLink",// 3B
            "ClientUid"                 // 3C
    };

    public static final String [] TAG_TABLE_5 = {
            "MoveItems",                // 05
            "Move",                     // 06
            "SrcMsgId",                 // 07
            "SrcFldId",                 // 08
            "DstFldId",                 // 09
            "Response",                 // 0A
            "Status",                   // 0B
            "DstMsgId"                  // 0C
    };

    public static final String [] TAG_TABLE_6 = {
            "GetItemEstimate", // 05
            null, // 06
            "Collections", // 07
            "Collection", // 08
            "Class", // 09
            "CollectionId", // 0A
            null, // 0B
            "Estimate", // 0C
            "Response", // 0D
            "Status" // 0E
    };

    public static final String [] TAG_TABLE_7 = {
            "Folders", // 05
            "Folder", // 06
            "DisplayName", // 07
            "ServerId", // 08
            "ParentId", // 09
            "Type", // 0A
            null, // 0B
            "Status", // 0C
            null, // 0D
            "Changes", // 0E
            "Add", // 0F
            "Delete", // 10
            "Update", // 11
            "SyncKey", // 12
            "FolderCreate", // 13
            "FolderDelete", // 14
            "FolderUpdate", // 15
            "FolderSync", // 16
            "Count" // 17
    };

    public static final String [] TAG_TABLE_8 = {
            "CalendarId",               // 05
            "CollectionId",             // 06
            "MeetingResponse",          // 07
            "RequestId",                // 08
            "Request",                  // 09
            "Result",                   // 0A
            "Status",                   // 0B
            "UserResponse",             // 0C
            null,                       // 0D
            "InstanceId",               // 0E
            null,                       // 0F
            "ProposedStartTime",        // 10
            "ProposedEndTime",          // 11
            "SendResponse"              // 12
    };

    public static final String [] TAG_TABLE_9 = {
            "Body",                     // 05
            "BodySize",                 // 06
            "BodyTruncated",            // 07
            "Categories",               // 08
            "Category",                 // 09
            "Complete",                 // 0A
            "DateCompleted",            // 0B
            "DueDate",                  // 0C
            "UtcDueDate",               // 0D
            "Importance",               // 0E
            "Recurrence",               // 0F
            "Type",                     // 10
            "Start",                    // 11
            "Until",                    // 12
            "Occurrences",              // 13
            "Interval",                 // 14
            "DayOfMonth",               // 15
            "DayOfWeek",                // 16
            "WeekOfMonth",              // 17
            "MonthOfYear",              // 18
            "Regenerate",               // 19
            "DeadOccur",                // 1A
            "ReminderSet",              // 1B
            "ReminderTime",             // 1C
            "Sensitivity",              // 1D
            "StartDate",                // 1E
            "UtcStartDate",             // 1F
            "Subject",                  // 20
            null,                       // 21
            "OrdinalDate",              // 22
            "SubOrdinalDate",           // 23
            "CalendarType",             // 24
            "IsLeapMonth",              // 25
            "FirstDayOfWeek"            // 26
    };

    public static final String [] TAG_TABLE_10 = {
            "ResolveRecipients",        // 05
            "Response",                 // 06
            "Status",                   // 07
            "Type",                     // 08
            "Recipient",                // 09
            "DisplayName",              // 0A
            "EmailAddress",             // 0B
            "Certificates",             // 0C
            "Certificate",              // 0D
            "MiniCertificate",          // 0E
            "Options",                  // 0F
            "To",                       // 10
            "CertificateRetrieval",     // 11
            "RecipientCount",           // 12
            "MaxCertificates",          // 13
            "MaxAmbiguousRecipients",   // 14
            "CertificateCount",         // 15
            "Availability",             // 16
            "StartTime",                // 17
            "EndTime",                  // 18
            "MergedFreeBusy",           // 19
            "Picture",                  // 1A
            "MaxSize",                  // 1B
            "Data",                     // 1C
            "MaxPictures"               // 1D
    };

    public static final String [] TAG_TABLE_11 = {
            "ValidateCert",             // 05
            "Certificates",             // 06
            "Certificate",              // 07
            "CertificateChain",         // 08
            "CheckCRL",                 // 09
            "Status"                    // 0A
    };

    public static final String [] TAG_TABLE_12 = {
            "CustomerId",               // 05
            "GovernmentId",             // 06
            "IMAddress",                // 07
            "IMAddress2",               // 08
            "IMAddress3",               // 09
            "ManagerName",              // 0A
            "CompanyMainPhone",         // 0B
            "AccountName",              // 0C
            "NickName",                 // 0D
            "MMS"                       // 0E
    };

    public static final String [] TAG_TABLE_13 = {
            "Ping",                     // 05
            null,                       // 06
            "Status",                   // 07
            "HeartbeatInterval",        // 08
            "Folders",                  // 09
            "Folder",                   // 0A
            "Id",                       // 0B
            "Class",                    // 0C
            "MaxFolders"                // 0D
    };

    public static final String [] TAG_TABLE_14 = {
            "Provision",                                  // 05
            "Policies",                                   // 06
            "Policy",                                     // 07
            "PolicyType",                                 // 08
            "PolicyKey",                                  // 09
            "Data",                                       // 0A
            "Status",                                     // 0B
            "RemoteWipe",                                 // 0C
            "EASProvisionDoc",                            // 0D
            "DevicePasswordEnabled",                      // 0E
            "AlphanumericDevicePasswordRequired",         // 0F
            "RequireStorageCardEncryption",               // 10
            "PasswordRecoveryEnabled",                    // 11
            null,                                         // 12
            "AttachmentsEnabled",                         // 13
            "MinDevicePasswordLength",                    // 14
            "MaxInactivityTimeDeviceLock",                // 15
            "MaxDevicePasswordFailedAttempts",            // 16
            "MaxAttachmentSize",                          // 17
            "AllowSimpleDevicePassword",                  // 18
            "DevicePasswordExpiration",                   // 19
            "DevicePasswordHistory",                      // 1A
            "AllowStorageCard",                           // 1B
            "AllowCamera",                                // 1C
            "RequireDeviceEncryption",                    // 1D
            "AllowUnsignedApplications",                  // 1E
            "AllowUnsignedInstallationPackages",          // 1F
            "MinDevicePasswordComplexCharacters",         // 20
            "AllowWiFi",                                  // 21
            "AllowTextMessaging",                         // 22
            "AllowPOPIMAPEmail",                          // 23
            "AllowBluetooth",                             // 24
            "AllowIrDA",                                  // 25
            "RequireManualSyncWhenRoaming",               // 26
            "AllowDesktopSync",                           // 27
            "MaxCalendarAgeFilter",                       // 28
            "AllowHTMLEmail",                             // 29
            "MaxEmailAgeFilter",                          // 2A
            "MaxEmailBodyTruncationSize",                 // 2B
            "MaxEmailHTMLBodyTruncationSize",             // 2C
            "RequireSignedSMIMEMessages",                 // 2D
            "RequireEncryptedSMIMEMessages",              // 2E
            "RequireSignedSMIMEAlgorithm",                // 2F
            "RequireEncryptionSMIMEAlgorithm",            // 30
            "AllowSMIMEEncryptionAlgorithmNegotiation",   // 31
            "AllowSMIMESoftCerts",                        // 32
            "AllowBrowser",                               // 33
            "AllowConsumerEmail",                         // 34
            "AllowRemoteDesktop",                         // 35
            "AllowInternetSharing",                       // 36
            "UnapprovedInROMApplicationList",             // 37
            "ApplicationName",                            // 38
            "ApprovedApplicationList",                    // 39
            "Hash",                                       // 3A
            "AccountOnlyRemoteWipe"                       // 3B
    };

    public static final String [] TAG_TABLE_15 = {
            "Search",                   // 05
            null,                       // 06
            "Store",                    // 07
            "Name",                     // 08
            "Query",                    // 09
            "Options",                  // 0A
            "Range",                    // 0B
            "Status",                   // 0C
            "Response",                 // 0D
            "Result",                   // 0E
            "Properties",               // 0F
            "Total",                    // 10
            "EqualTo",                  // 11
            "Value",                    // 12
            "And",                      // 13
            "Or",                       // 14
            "FreeText",                 // 15
            null,                       // 16
            "DeepTraversal",            // 17
            "LongId",                   // 18
            "RebuildResults",           // 19
            "LessThan",                 // 1A
            "GreaterThan",              // 1B
            null,                       // 1C
            null,                       // 1D
            "UserName",                 // 1E
            "Password",                 // 1F
            "ConversationId",           // 20
            "Picture",                  // 21
            "MaxSize",                  // 22
            "MaxPictures"               // 23
    };                                  

    public static final String [] TAG_TABLE_16 = {
            "DisplayName",              // 05
            "Phone",                    // 06
            "Office",                   // 07
            "Title",                    // 08
            "Company",                  // 09
            "Alias",                    // 0A
            "FirstName",                // 0B
            "LastName",                 // 0C
            "HomePhone",                // 0D
            "MobilePhone",              // 0E
            "EmailAddress",             // 0F
            "Picture",                  // 10
            "Status",                   // 11
            "Data"                      // 12
    };

    public static final String [] TAG_TABLE_17 = {
            "BodyPreference",           // 05
            "Type",                     // 06
            "TruncationSize",           // 07
            "AllOrNone",                // 08
            null,                       // 09
            "Body",                     // 0A
            "Data",                     // 0B
            "EstimatedDataSize",        // 0C
            "Truncated",                // 0D
            "Attachments",              // 0E
            "Attachment",               // 0F
            "DisplayName",              // 10
            "FileReference",            // 11
            "Method",                   // 12
            "ContentId",                // 13
            "ContentLocation",          // 14
            "IsInline",                 // 15
            "NativeBodyType",           // 16
            "ContentType",              // 17
            "Preview",                  // 18
            "BodyPartPreference",       // 19
            "BodyPart",                 // 1A
            "Status",                   // 1B
            "Add",                      // 1C
            "Delete",                   // 1D
            "ClientId",                 // 1E
            "Content",                  // 1F
            "Location",                 // 20
            "Annotation",               // 21
            "Street",                   // 22
            "City",                     // 23
            "State",                    // 24
            "Country",                  // 25
            "PostalCode",               // 26
            "Latitude",                 // 27
            "Longitude",                // 28
            "Accuracy",                 // 29
            "Altitude",                 // 2A
            "AltitudeAccuracy",         // 2B
            "LocationUri",              // 2C
            "InstanceId"                // 2D
    };

    public static final String [] TAG_TABLE_18 = {
            "Settings",                   // 05
            "Status",                     // 06
            "Get",                        // 07
            "Set",                        // 08
            "Oof",                        // 09
            "OofState",                   // 0A
            "StartTime",                  // 0B
            "EndTime",                    // 0C
            "OofMessage",                 // 0D
            "AppliesToInternal",          // 0E
            "AppliesToExternalKnown",     // 0F
            "AppliesToExternalUnknown",   // 10
            "Enabled",                    // 11
            "ReplyMessage",               // 12
            "BodyType",                   // 13
            "DevicePassword",             // 14
            "Password",                   // 15
            "DeviceInformation",          // 16
            "Model",                      // 17
            "IMEI",                       // 18
            "FriendlyName",               // 19
            "OS",                         // 1A
            "OSLanguage",                 // 1B
            "PhoneNumber",                // 1C
            "UserInformation",            // 1D
            "EmailAddresses",             // 1E
            "SMTPAddress",                // 1F
            "UserAgent",                  // 20
            "EnableOutboundSMS",          // 21
            "MobileOperator",             // 22
            "PrimarySmtpAddress",         // 23
            "Accounts",                   // 24
            "Account",                    // 25
            "AccountId",                  // 26
            "AccountName",                // 27
            "UserDisplayName",            // 28
            "SendDisabled",               // 29
            null,                         // 2A
            "RightsManagementInformation" // 2B
    };

    public static final String [] TAG_TABLE_19 = {
            "LinkId",                     // 05
            "DisplayName",                // 06
            "IsFolder",                   // 07
            "CreationDate",               // 08
            "LastModifiedDate",           // 09
            "IsHidden",                   // 0A
            "ContentLength",              // 0B
            "ContentType"                 // 0C
    };

    public static final String [] TAG_TABLE_20 = {
            "ItemOperations",             // 05
            "Fetch",                      // 06
            "Store",                      // 07
            "Options",                    // 08
            "Range",                      // 09
            "Total",                      // 0A
            "Properties",                 // 0B
            "Data",                       // 0C
            "Status",                     // 0D
            "Response",                   // 0E
            "Version",                    // 0F
            "Schema",                     // 10
            "Part",                       // 11
            "EmptyFolderContents",        // 12
            "DeleteSubFolders",           // 13
            "UserName",                   // 14
            "Password",                   // 15
            "Move",                       // 16
            "DstFldId",                   // 17
            "ConversationId",             // 18
            "MoveAlways"                  // 19
    };

    public static final String [] TAG_TABLE_21 = {
            "SendMail",                   // 05
            "SmartForward",               // 06
            "SmartReply",                 // 07
            "SaveInSentItems",            // 08
            "ReplaceMime",                // 09
            null,                         // 0A
            "Source",                     // 0B
            "FolderId",                   // 0C
            "ItemId",                     // 0D
            "LongId",                     // 0E
            "InstanceId",                 // 0F
            "Mime",                       // 10
            "ClientId",                   // 11
            "Status",                     // 12
            "AccountId",                  // 13
            null,                         // 14
            "Forwardees",                 // 15
            "Forwardee",                  // 16
            "ForwardeeName",              // 17
            "ForwardeeEmail"              // 18
    };

    public static final String [] TAG_TABLE_22 = {
            "UmCallerID",                 // 05
            "UmUserNotes",                // 06
            "UmAttDuration",              // 07
            "UmAttOrder",                 // 08
            "ConversationId",             // 09
            "ConversationIndex",          // 0A
            "LastVerbExecuted",           // 0B
            "LastVerbExecutionTime",      // 0C
            "ReceivedAsBcc",              // 0D
            "Sender",                     // 0E
            "CalendarType",               // 0F
            "IsLeapMonth",                // 10
            "AccountId",                  // 11
            "FirstDayOfWeek",             // 12
            "MeetingMessageType",         // 13
            null,                         // 14
            "IsDraft",                    // 15
            "Bcc",                        // 16
            "Send"                        // 17
    };

    public static final String [] TAG_TABLE_23 = {
            "Subject",                    // 05
            "MessageClass",               // 06
            "LastModifiedDate",           // 07
            "Categories",                 // 08
            "Category"                    // 09
    };

    public static final String [] TAG_TABLE_24 = {
            "RightsManagementSupport",         // 05
            "RightsManagementTemplates",       // 06
            "RightsManagementTemplate",        // 07
            "RightsManagementLicense",         // 08
            "EditAllowed",                     // 09
            "ReplyAllowed",                    // 0A
            "ReplyAllAllowed",                 // 0B
            "ForwardAllowed",                  // 0C
            "ModifyRecipientsAllowed",         // 0D
            "ExtractAllowed",                  // 0E
            "PrintAllowed",                    // 0F
            "ExportAllowed",                   // 10
            "ProgrammaticAccessAllowed",       // 11
            "Owner",                           // 12
            "ContentExpiryDate",               // 13
            "TemplateID",                      // 14
            "TemplateName",                    // 15
            "TemplateDescription",             // 16
            "ContentOwner",                    // 17
            "RemoveRightsManagementProtection" // 18
    };

    public static final String [] TAG_TABLE_25 = {
            "Find",                            // 05
            "SearchId",                        // 06
            "ExecuteSearch",                   // 07
            "MailBoxSearchCriterion",          // 08
            "Query",                           // 09
            "Status",                          // 0A
            "FreeText",                        // 0B
            "Options",                         // 0C
            "Range",                           // 0D
            "DeepTraversal",                   // 0E
            null,                              // 0F
            null,                              // 10
            "Response",                        // 11
            "Result",                          // 12
            "Properties",                      // 13
            "Preview",                         // 14
            "HasAttachments",                  // 15
            "Total",                           // 16
            "DisplayCc",                       // 17
            "DisplayBcc",                      // 18
            "GalSearchCriterion"               // 19
    };
}
