package de.chdev.jasync.aswbxml;

import org.apache.log4j.Logger;
import org.kxml2.wap.Wbxml;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.*;
import java.util.*;

public class ActiveSyncParser implements XmlPullParser {

    Logger log = Logger.getLogger(this.getClass());

    // Constants
    public static final int WAP_EXTENSION = 64;

    // Static information
    private InputStream inputStream;
    private String encoding;
    private int version;
    private byte[] stringTable;

    Map<Integer, String[]> pageTagMap = new HashMap<>();
    Map<Integer, String> pageNamespaceMap = new HashMap<>();
    ArrayList<Integer> usedPagesList = new ArrayList<>();


    // Dynamic information
    private int type = XmlPullParser.START_DOCUMENT;
    private String[] currentPage;
    private int currentPageId = 0;
    //    private int depth = 0;
    private Stack<XmlEntity> xmlEntityStack = new Stack<>();

    private Object wapExtensionData;
    private int wapCode;


    // Init methods

    /**
     * Sets the tag table for a given page.
     * The first string in the array defines tag 5, the second tag 6 etc.
     */
    public void setTagTable(int page, String[] tagTable, String namespace) {
        pageTagMap.put(page, tagTable);
        if (namespace != null)
            pageNamespaceMap.put(page, namespace);
        if (page == currentPageId) {
            currentPage = tagTable;
        }
    }


    // Interface implementation
    @Override
    public void setFeature(String s, boolean b) throws XmlPullParserException {
        log.debug("Called setFeature: " + s + "/ " + b);
    }

    @Override
    public boolean getFeature(String s) {
        log.debug("Called getFeature: " + s);
        return false;
    }

    @Override
    public void setProperty(String s, Object o) throws XmlPullParserException {
        log.debug("Called setProperty: " + s + "/ " + o);
    }

    @Override
    public Object getProperty(String s) {
        log.debug("Called getProperty: " + s);
        return null;
    }

    @Override
    public void setInput(Reader reader) throws XmlPullParserException {
        log.debug("Called setInput (reader): " + reader);
        throw new XmlPullParserException("InputStream required", this, null);
    }

    @Override
    public void setInput(InputStream inputStream, String encoding) throws XmlPullParserException {
        log.debug("Called setInput (stream): " + inputStream + " / " + encoding);

        this.inputStream = inputStream;
        this.encoding = encoding;

        try {
            version = readByte();
            int publicIdentifierId = readInt();

            if (publicIdentifierId == 0)
                readInt();

            int charset = readInt();

            if (encoding == null) {
                switch (charset) {
                    case 4:
                        this.encoding = "ISO-8859-1";
                        break;
                    case 106:
                        this.encoding = "UTF-8";
                        break;
                    // add more if you need them
                    // http://www.iana.org/assignments/character-sets
                    // case MIBenum: encoding = Name  break;
                    default:
                        throw new UnsupportedEncodingException("" + charset);
                }
            }

            int stringTableSize = readInt();
            stringTable = new byte[stringTableSize];

            int offset = 0;
            inputStream.read(stringTable, offset, stringTableSize - offset);
        } catch (IOException e) {
            throw new XmlPullParserException("Illegal input format", this, null);
        }

    }

    @Override
    public String getInputEncoding() {
        log.debug("Called getInputEncoding");
        return null;
    }

    @Override
    public void defineEntityReplacementText(String s, String s1) throws XmlPullParserException {
        log.debug("Called defineEntityReplacementText: " + s + " / " + s1);
    }

    @Override
    public int getNamespaceCount(int i) {
        log.debug("Called getNamespaceCount: " + i);
        return 0;
    }

    @Override
    public String getNamespacePrefix(int i) {
        log.debug("Called getNamespacePrefix: " + i);
        return null;
    }

    @Override
    public String getNamespaceUri(int i) {
        log.debug("Called getNamespaceUri: " + i);
        return null;
    }

    @Override
    public String getNamespace(String s) {
        log.debug("Called getNamespace (String): " + s);
        return null;
    }

    @Override
    public int getDepth() {
        log.debug("Called getDepth");
        return xmlEntityStack.size();
    }

    @Override
    public String getPositionDescription() {
        log.debug("Called getPositionDescription");
        return null;
    }

    @Override
    public int getLineNumber() {
        log.debug("Called getLineNumber");
        return 0;
    }

    @Override
    public int getColumnNumber() {
        log.debug("Called getColumnNumber");
        return 0;
    }

    @Override
    public boolean isWhitespace() throws XmlPullParserException {
        log.debug("Called isWhitespace");
        return false;
    }

    @Override
    public String getText() {
        log.debug("Called getText");
        return xmlEntityStack.peek().getText();
    }

    @Override
    public char[] getTextCharacters(int[] ints) {
        log.debug("Called getTextCharacters: " + ints);
        return null;
    }

    @Override
    public String getNamespace() {
        log.debug("Called getNamespace (without)");
        return null;
    }

    @Override
    public String getName() {
        log.debug("Called getName");
        String namespace = null;
        // Avoid prefix if default namespace
        if (usedPagesList.get(0) != xmlEntityStack.peek().getPageId())
            namespace = pageNamespaceMap.get(xmlEntityStack.peek().getPageId());
        String name = xmlEntityStack.peek().getName();
        return namespace == null ? name : namespace.toLowerCase() + ":" + name;
    }

    @Override
    public String getPrefix() {
        log.debug("Called getPrefix");
        return null;
    }

    @Override
    public boolean isEmptyElementTag() throws XmlPullParserException {
        log.debug("Called isEmptyElementTag");
        return false;
    }

    @Override
    public int getAttributeCount() {
        log.debug("Called getAttributeCount");
        return 0;
    }

    @Override
    public String getAttributeNamespace(int i) {
        log.debug("Called getAttributeNamespace: " + i);
        return null;
    }

    @Override
    public String getAttributeName(int i) {
        log.debug("Called getAttributeName: " + i);
        return null;
    }

    @Override
    public String getAttributePrefix(int i) {
        log.debug("Called getAttributePrefix: " + i);
        return null;
    }

    @Override
    public String getAttributeType(int i) {
        log.debug("Called getAttributeType: " + i);
        return null;
    }

    @Override
    public boolean isAttributeDefault(int i) {
        log.debug("Called isAttributeDefault: " + i);
        return false;
    }

    @Override
    public String getAttributeValue(int i) {
        log.debug("Called getAttributeValue (int): " + i);
        return null;
    }

    @Override
    public String getAttributeValue(String s, String s1) {
        log.debug("Called getAttributeValue: " + s + " / " + s1);
        return null;
    }

    @Override
    public int getEventType() throws XmlPullParserException {
        log.debug("Called getEventType");
        return type;
    }

    @Override
    public int next() throws XmlPullParserException, IOException {
        log.debug("Called next");
        return nextToken();
    }

    @Override
    public int nextToken() throws XmlPullParserException, IOException {
        log.debug("Called nextToken");
        nextImpl();
        return type;
    }

    @Override
    public void require(int i, String s, String s1) throws XmlPullParserException, IOException {
        log.debug("Called setInput (reader): " + i + " / " + s + " / " + s1);
    }

    @Override
    public String nextText() throws XmlPullParserException, IOException {
        log.debug("Called nextText");
        return null;
    }

    @Override
    public int nextTag() throws XmlPullParserException, IOException {
        log.debug("Called nextTag");
        return 0;
    }


    // Custom methods
    protected int readByte() throws IOException {
        int i = inputStream.read();
        return i;
    }

    protected int readInt() throws IOException {
        int result = 0;
        int i;

        do {
            i = readByte();
            result = (result << 7) | (i & 0x7f);
        }
        while ((i & 0x80) != 0);

        return result;
    }

    public ArrayList<Integer> getUsedPageIds() {
        return usedPagesList;
    }

    public String getNamespaceAttributeString() {
        StringBuilder stringBuilder = new StringBuilder();
        boolean defaultNamespace = true;
        if (usedPagesList.isEmpty()) usedPagesList.add(currentPageId);
        for (Integer usedPageId : usedPagesList) {
            if (defaultNamespace) {
                stringBuilder.append(" xmlns=\"");
                if (pageNamespaceMap.containsKey(usedPageId)) {
                    stringBuilder.append(pageNamespaceMap.get(usedPageId));
                } else {
                    stringBuilder.append("unknown");
                }
            } else if (pageNamespaceMap.containsKey(usedPageId)) {
                stringBuilder.append(" xmlns:" + pageNamespaceMap.get(usedPageId).toLowerCase() + "=\"");
                stringBuilder.append(pageNamespaceMap.get(usedPageId));
            } else {
                stringBuilder.append("xmlns:" + usedPageId + "=\"");
                stringBuilder.append("unknown");
            }
            stringBuilder.append("\"");
            defaultNamespace = false;
        }
        return stringBuilder.toString();
    }

    protected void nextImpl() throws IOException, XmlPullParserException {

        if (type == END_TAG) {
            xmlEntityStack.pop();
        }

        if (!xmlEntityStack.isEmpty() && xmlEntityStack.peek().isEmpty()) {
            type = XmlPullParser.END_TAG;
            return;
        }

        int id = readByte();

        while (id == Wbxml.SWITCH_PAGE) {
            id = readByte();
            switchPage(id);
            id = readByte();
        }

        if (!usedPagesList.contains(currentPageId)) {
            usedPagesList.add(currentPageId);
        }

        switch (id) {
            case -1:
                type = XmlPullParser.END_DOCUMENT;
                break;

            case Wbxml.END: {

                type = END_TAG;
            }
            break;

//            case Wbxml.ENTITY :
//            {
//                type = ENTITY_REF;
//                char c = (char) readInt();
//                text = "" + c;
//                name = "#" + ((int) c);
//            }
//            break;

            case Wbxml.STR_I:
                type = TEXT;
                xmlEntityStack.peek().setText(readInlineString());
                break;

            case Wbxml.EXT_I_0:
            case Wbxml.EXT_I_1:
            case Wbxml.EXT_I_2:
            case Wbxml.EXT_T_0:
            case Wbxml.EXT_T_1:
            case Wbxml.EXT_T_2:
            case Wbxml.EXT_0:
            case Wbxml.EXT_1:
            case Wbxml.EXT_2:
            case Wbxml.OPAQUE:

                type = WAP_EXTENSION;
                wapCode = id;
                wapExtensionData = parseWapExtension(id);
                break;

            case Wbxml.PI:
                throw new RuntimeException("PI curr. not supp.");
                // readPI;
                // break;

            case Wbxml.STR_T: {
                type = TEXT;
                xmlEntityStack.peek().setText(readTableString());
            }
            break;

            default:
                parseElement(id);
        }
    }

    protected void switchPage(int pageNumber) throws XmlPullParserException {

        if (!pageTagMap.containsKey(pageNumber))
            exception("Code Page " + pageNumber + " undefined!");

        currentPage = pageTagMap.get(pageNumber);
        currentPageId = pageNumber;
    }

    protected void exception(String message) throws XmlPullParserException {
        throw new XmlPullParserException(message, this, null);
    }

    protected void parseElement(int id)
            throws IOException, XmlPullParserException {

        type = START_TAG;
        XmlEntity entity = new XmlEntity();
        xmlEntityStack.push(entity);
        entity.setName(resolveId(currentPage, id & 0x03f));
        entity.setPageId(currentPageId);

        // if content bit is not set, an empty node will be created
        entity.setEmpty((id & 64) == 0);

    }

    String resolveId(String[] tab, int id) throws IOException {
        int idx = (id & 0x07f) - 5;
        if (idx == -1) {
            wapCode = -1;
            return readTableString();
        }
        if (idx < 0
                || tab == null
                || idx >= tab.length
                || tab[idx] == null)
            throw new IOException("id " + id + " undef.");

        wapCode = idx + 5;

        return tab[idx];
    }

    public Object parseWapExtension(int id) throws IOException, XmlPullParserException {

        switch (id) {
            case Wbxml.EXT_I_0:
            case Wbxml.EXT_I_1:
            case Wbxml.EXT_I_2:
                return readInlineString();

            case Wbxml.EXT_T_0:
            case Wbxml.EXT_T_1:
            case Wbxml.EXT_T_2:
                return new Integer(readInt());

            case Wbxml.EXT_0:
            case Wbxml.EXT_1:
            case Wbxml.EXT_2:
                return null;

            case Wbxml.OPAQUE: {
                int count = readInt();
                byte[] buf = new byte[count];

                while (count > 0) {
                    count -= inputStream.read(buf, buf.length - count, count);
                }

                return buf;
            } // case OPAQUE


            default:
                exception("illegal id: " + id);
                return null; // dead code
        } // SWITCH
    }

    String readInlineString() throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        boolean onlyWhitespaceCharacters = true;

        int currentCharacter = inputStream.read();

        while (currentCharacter > 0) {
            if (currentCharacter > 32) {
                onlyWhitespaceCharacters = false;
            }
            buffer.write(currentCharacter);
            currentCharacter = inputStream.read();
        }
//        isWhitespace = wsp;
        String result = new String(buffer.toByteArray(), encoding);
        buffer.close();
        return result;
    }

    String readTableString() throws IOException {
        Integer tableStartPosition = readInt();

        int tableEndPosition = tableStartPosition;
        while (tableEndPosition < stringTable.length && stringTable[tableEndPosition] != '\0') {
            tableEndPosition++;
        }
        String resultText = new String(stringTable, tableStartPosition, tableEndPosition - tableStartPosition, encoding);

        return resultText;
    }


    // Inner classes
    class XmlEntity {
        String name;
        Integer pageId;
        String text;
        boolean empty;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }

        public Integer getPageId() {
            return pageId;
        }

        public void setPageId(Integer pageId) {
            this.pageId = pageId;
        }
    }
}
