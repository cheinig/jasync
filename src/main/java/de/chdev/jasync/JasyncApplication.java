package de.chdev.jasync;

import de.chdev.jasync.servlet.DispatcherServlet;
import de.chdev.jasync.workflow.IncomingRequestHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JasyncApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasyncApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean mainServletBean(IncomingRequestHandler requestHandler) {
		ServletRegistrationBean bean = new ServletRegistrationBean(
				new DispatcherServlet(requestHandler), "/*");
		bean.setLoadOnStartup(1);
		return bean;
	}
}
