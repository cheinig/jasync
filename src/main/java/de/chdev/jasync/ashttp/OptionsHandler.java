package de.chdev.jasync.ashttp;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.model.Constants;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Response;
import de.chdev.jasync.model.interfaces.IBackendHandler;
import exceptions.AuthenticationFailedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class OptionsHandler {

    public Response getOptions(Request request){

        validateCredentials(request);

        Response response = request.getRelatedResponse();

        List<String> supportedVersionList = Arrays.asList(Constants.SUPPORTED_VERSIONS);
        response.setSupportedVersions(supportedVersionList);
        response.setServerVersion(Constants.SERVER_VERSION);
        List<String> supportedCommandList = Arrays.asList(Constants.SUPPORTED_COMMANDS);
        response.setSupportedCommands(supportedCommandList);

        return response;
    }

    private boolean validateCredentials(Request request) {

        if (request == null ||
                request.getUserDevice() == null ||
                request.getUserDevice().getUsername()==null ||
                request.getUserDevice().getUsername().isEmpty())
            throw new AuthenticationFailedException();

        for (IBackendHandler backendHandler : BackendRegistry.getInstance().getBackendHandlerList()) {
            backendHandler.validateCredentials(request.getUserDevice());
        }

        return true;
    }
}
