package de.chdev.jasync.ashttp;

import de.chdev.jasync.aswbxml.ActiveSyncWbxmlConverter;
import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.model.HttpTransfer;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Response;
import de.chdev.jasync.model.interfaces.IBackendHandler;
import de.chdev.jasync.persistence.UserDevice;
import exceptions.AuthenticationFailedException;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class HttpParser {

    public Request getASRequestFromHttpDataAndValidate(HttpTransfer transfer) {
        Request request = new Request();
        request = mapTransferToRequestAndValidateUser(transfer);
        return request;
    }

    protected Request mapTransferToRequestAndValidateUser(HttpTransfer transfer) {
        Request request = new Request();
        request.setUserDevice(getUserDeviceFromTransfer(transfer, request));

        validateCredentials(request);

        if (transfer.getMethod().equalsIgnoreCase("options")) {
            request.setMethod(Request.Method.options);
        } else if (transfer.getMethod().equalsIgnoreCase("post")) {
            request.setMethod(Request.Method.request);
            mapAndConvertBodyData(transfer, request);
        } else {
            request.setMethod(Request.Method.unknown);
        }
        mapQueryData(transfer, request);
        request.setVersion(transfer.getHeaders().get("ms-asprotocolversion"));
        return request;
    }

    private UserDevice getUserDeviceFromTransfer(HttpTransfer transfer, Request request) {
        String loginUsername = null;
        String loginPassword = null;

        if (transfer.getHeaders().get("authorization")!=null) {
            String authenticationString = transfer.getHeaders().get("authorization").replaceAll("BASIC", "").replaceAll("Basic", "").trim();
            String decodedAuthenticationString = new String(Base64.getDecoder().decode(authenticationString));
            loginUsername = decodedAuthenticationString.substring(0, decodedAuthenticationString.indexOf(':'));
            loginPassword = decodedAuthenticationString.substring(decodedAuthenticationString.indexOf(':') + 1);
        }

        // Set UserDevice
        UserDevice userDevice = new UserDevice(loginUsername, request.getDeviceId());
        userDevice.setPassword(loginPassword);
        return userDevice;
    }

    private void mapAndConvertBodyData(HttpTransfer transfer, Request request) {
        if (transfer.getBody()!=null) {
            if (transfer.getContentType() == HttpTransfer.Content.WBXML) {
                ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(transfer.getBody());
                request.setXmlData(activeSyncWbxmlConverter.getXmlString());
            } else {
                request.setXmlData(new String(transfer.getBody()));
            }
        }
    }

    private void mapQueryData(HttpTransfer transfer, Request request) {
        if (transfer.getQuery()!=null) {
            String[] queries = transfer.getQuery().split("&");
            for (String query : queries) {
                String[] splittedQuery = query.split("=");
                switch (splittedQuery[0].toLowerCase()){
                    case "cmd":
                        request.setCommand(splittedQuery[1]);
                        break;
                    case "user":
                        request.setUsername(splittedQuery[1]);
                        break;
                    case "deviceid":
                        request.setDeviceId(splittedQuery[1]);
                        break;
                    case "devicetype":
                        request.setDeviceType(splittedQuery[1]);
                        break;
                    default:
                        request.getUriParameters().put(splittedQuery[0], splittedQuery[1]);
                }
            }
        }
    }


    public HttpTransfer getASHttpDataFromResponse(Response response, HttpTransfer httpTransfer) {
        HttpTransfer transfer = mapResponseToTransfer(response, httpTransfer);

        return transfer;
    }

    private HttpTransfer mapResponseToTransfer(Response response, HttpTransfer transfer) {

        if (response.getServerVersion()!=null) transfer.getHeaders().put("ms-server-activesync", response.getServerVersion());
        if (response.getSupportedVersions()!=null) transfer.getHeaders().put("ms-asprotocolversions", String.join(",",response.getSupportedVersions()));
        if (response.getSupportedCommands()!=null) transfer.getHeaders().put("ms-asprotocolcommands", String.join(",", response.getSupportedCommands()));

        // Add additional options header
        if (response.getRelatedRequest()!=null && response.getRelatedRequest().getMethod()== Request.Method.options){
            transfer.getHeaders().put("allow", "OPTIONS,POST");
            transfer.getHeaders().put("public", "OPTIONS,POST");
        }

        // Add body
        if (response.getObjectData()!=null){
            if (transfer.getContentType() == HttpTransfer.Content.WBXML) {
                ActiveSyncWbxmlConverter activeSyncWbxmlConverter = new ActiveSyncWbxmlConverter(response.getXmlData());
                transfer.setBody(activeSyncWbxmlConverter.getWbxml());
                transfer.getHeaders().put("content-type", "application/vnd.ms-sync.wbxml");
            } else {
                transfer.setBody(response.getXmlData().getBytes());
                transfer.getHeaders().put("content-type", "application/xml");
            }

        }

        return transfer;
    }

    private boolean validateCredentials(Request request) {

        if (request == null ||
                request.getUserDevice() == null ||
                request.getUserDevice().getUsername()==null ||
                request.getUserDevice().getUsername().isEmpty())
            throw new AuthenticationFailedException();

        for (IBackendHandler backendHandler : BackendRegistry.getInstance().getBackendHandlerList()) {
            backendHandler.validateCredentials(request.getUserDevice());
        }

        return true;
    }
}
