package de.chdev.jasync.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserSettingRepository extends CrudRepository<UserSetting, Long> {

    List<UserSetting> getUserSettingsByUserDeviceAndBackendIdAndParameterName(UserDevice userDevice, String backendId, String parameterName);
}
