package de.chdev.jasync.persistence;

import javax.persistence.*;

@Entity
public class Collection {

    @Id
    @GeneratedValue
    private Long id;

    private String backendId;

    private String backendCollectionId;

    private String displayName;

    private String typeId;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="parentid")
    private Collection parent;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="userdeviceid")
    private UserDevice userDevice;

    public Collection() {
    }

    public Collection(String backendId, String backendCollectionId, String displayName, String typeId, Collection parent, UserDevice userDevice) {
        this.backendId = backendId;
        this.backendCollectionId = backendCollectionId;
        this.displayName = displayName;
        this.typeId = typeId;
        this.parent = parent;
        this.userDevice = userDevice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBackendId() {
        return backendId;
    }

    public void setBackendId(String backendId) {
        this.backendId = backendId;
    }

    public String getBackendCollectionId() {
        return backendCollectionId;
    }

    public void setBackendCollectionId(String backendCollectionId) {
        this.backendCollectionId = backendCollectionId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public Collection getParent() {
        return parent;
    }

    public void setParent(Collection parent) {
        this.parent = parent;
    }

    public UserDevice getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(UserDevice userDevice) {
        this.userDevice = userDevice;
    }
}
