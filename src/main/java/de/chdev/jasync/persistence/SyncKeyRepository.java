package de.chdev.jasync.persistence;

import org.springframework.data.repository.CrudRepository;

public interface SyncKeyRepository extends CrudRepository<SyncKey, Long> {

}
