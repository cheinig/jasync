package de.chdev.jasync.persistence;

import javax.persistence.*;

@Entity
public class SyncKey {

    @Id
    @GeneratedValue
    private Long id;

    private Long iteration;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="collectionid")
    private Collection collection;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="userdeviceid")
    private UserDevice userDevice;

    public SyncKey() {
    }

    public SyncKey(Long iteration, Collection collection, UserDevice userDevice) {
        this.iteration = iteration;
        this.collection = collection;
        this.userDevice = userDevice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIteration() {
        return iteration;
    }

    public void setIteration(Long iteration) {
        this.iteration = iteration;
    }

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    public UserDevice getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(UserDevice userDevice) {
        this.userDevice = userDevice;
    }
}
