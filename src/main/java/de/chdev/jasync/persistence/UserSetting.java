package de.chdev.jasync.persistence;

import javax.persistence.*;

@Entity
public class UserSetting {

    @Id
    @GeneratedValue
    private Long id;

    private String backendId;

    private String parameterName;

    private String parameterValue;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="userdeviceid")
    private UserDevice userDevice;

    public UserSetting(){

    }

    public UserSetting(String parameterName, String parameterValue, String backendId, UserDevice userDevice){
        this.parameterName = parameterName;
        this.parameterValue = parameterValue;
        this.backendId = backendId;
        this.userDevice = userDevice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBackendId() {
        return backendId;
    }

    public void setBackendId(String backendId) {
        this.backendId = backendId;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public UserDevice getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(UserDevice userDevice) {
        this.userDevice = userDevice;
    }
}
