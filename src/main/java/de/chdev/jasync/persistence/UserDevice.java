package de.chdev.jasync.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class UserDevice implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    @Transient
    private String password;

    private String deviceId;

    @OneToMany(mappedBy="userDevice", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserSetting> settings;

    @OneToMany(mappedBy="userDevice", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SyncKey> syncKeys;

    @OneToMany(mappedBy="userDevice", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Collection> collections;

    protected UserDevice(){

    }

    public UserDevice(String username, String deviceId){
        this.username=username;
        this.deviceId = deviceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<UserSetting> getSettings() {
        return settings;
    }

    public void setSettings(List<UserSetting> settings) {
        this.settings = settings;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<SyncKey> getSyncKeys() {
        return syncKeys;
    }

    public void setSyncKeys(List<SyncKey> syncKeys) {
        this.syncKeys = syncKeys;
    }

    public List<Collection> getCollections() {
        return collections;
    }

    public void setCollections(List<Collection> collections) {
        this.collections = collections;
    }

    @Transient
    public String getPassword() {
        return password;
    }

    @Transient
    public void setPassword(String password) {
        this.password = password;
    }
}
