package de.chdev.jasync.persistence;

import org.springframework.data.repository.CrudRepository;

public interface UserDeviceRepository extends CrudRepository<UserDevice, Long> {

    UserDevice getFirstByUsernameAndDeviceId(String username, String deviceId);
}
