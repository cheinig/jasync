package de.chdev.jasync.workflow;

import de.chdev.jasync.ascmd.CommandHandler;
import de.chdev.jasync.ashttp.HttpParser;
import de.chdev.jasync.ashttp.OptionsHandler;
import de.chdev.jasync.model.HttpTransfer;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Response;
import exceptions.AuthenticationFailedException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;

@Component
public class IncomingRequestHandler {

    private Logger log = Logger.getLogger(this.getClass());

    @Autowired
    HttpParser httpParser;
    @Autowired
    OptionsHandler optionsHandler;
    @Autowired
    CommandHandler commandHandler;

    public HttpServletResponse handleIncomingRequest(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {

        try {
            // Map httpRequest to transferRequest
            HttpTransfer transferRequest = mapHttpRequestToTransfer(httpRequest);

            // Parse http transferRequest
            Request request = null;
            request = httpParser.getASRequestFromHttpDataAndValidate(transferRequest);

            // Process asRequest
            Response response = null;
            if (request.getMethod().equals(Request.Method.options)) {
                response = optionsHandler.getOptions(request);
            } else {

                response = commandHandler.handleRequest(request).getRelatedResponse();
            }


            if (response != null) {
                HttpTransfer responseTransfer = new HttpTransfer();
                responseTransfer.setContentType(transferRequest.getContentType());
                // Parse asResponse to transferResponse
                responseTransfer = httpParser.getASHttpDataFromResponse(response, responseTransfer);
                // Map transferResponse to httpResponse
                httpResponse = mapResponseTransferToHttpResponse(responseTransfer, httpResponse);
            } else {
                // Catch all
                httpResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        } catch (AuthenticationFailedException afe) {
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }


        return httpResponse;
    }



    private HttpTransfer mapHttpRequestToTransfer(HttpServletRequest httpRequest) throws IOException {
        HttpTransfer transfer = new HttpTransfer();

        transfer.setMethod(httpRequest.getMethod());

        transfer.setQuery(httpRequest.getQueryString());

        // Set Body
        ServletInputStream inputStream = httpRequest.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (inputStream!=null) {
            int singleByte = inputStream.read();
            while (singleByte != -1) {
                byteArrayOutputStream.write(singleByte);
                singleByte = inputStream.read();
            }
            transfer.setBody(byteArrayOutputStream.toByteArray());
        }

        // Header
        Enumeration<String> headerNames = httpRequest.getHeaderNames();
        while (headerNames != null && headerNames.hasMoreElements()){
            String headerName = headerNames.nextElement();
            transfer.getHeaders().put(headerName, httpRequest.getHeader(headerName));
        }
        if (httpRequest.getContentType()!=null &&
                httpRequest.getContentType().equalsIgnoreCase("application/xml"))
            transfer.setContentType(HttpTransfer.Content.XML);
        return transfer;
    }

    public HttpServletResponse mapResponseTransferToHttpResponse(HttpTransfer transfer, HttpServletResponse httpResponse) {

        // Map header data
        for (String key : transfer.getHeaders().keySet()){
            httpResponse.setHeader(key, transfer.getHeaders().get(key));
        }

        // Map body data
        if (transfer.getBody() != null) {
            try {
                httpResponse.getOutputStream().write(transfer.getBody());
            } catch (IOException e) {
                log.debug("Body byte array cannot be written to http response");
            }
        }

        httpResponse.setStatus(HttpServletResponse.SC_OK);

        return httpResponse;
    }
}
