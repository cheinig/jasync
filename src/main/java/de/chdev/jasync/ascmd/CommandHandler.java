package de.chdev.jasync.ascmd;

import de.chdev.jasync.ascmd.folderhierarchy.FolderHierarchyHandler;
import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.model.interfaces.IBackendHandler;
import de.chdev.jasync.model.interfaces.ISpecialHandler;
import de.chdev.jasync.model.Constants;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Response;
import de.chdev.jasync.model.Status;
import de.chdev.jasync.persistence.UserDevice;
import de.chdev.jasync.persistence.UserDeviceRepository;
import exceptions.AuthenticationFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;

@Component
public class CommandHandler {

    FolderHierarchyHandler folderHierarchyHandler = new FolderHierarchyHandler();

    @Autowired
    protected UserDeviceRepository userDeviceRepository;

    public Request handleRequest(Request request) {
        validateRequest(request);
        request.setUserDevice(persistUserDevice(request.getUserDevice()));
        ISpecialHandler specialHandler = getSpecialHandler(request);
        if (specialHandler!=null) {
            handleSpecialRequest(request, specialHandler);
        } else {
            request.getRelatedResponse().getGlobalStatus().setIntValue(Status.ErrorCommandNotSupported);
        }
        return request;
    }

    private void handleSpecialRequest(Request request, ISpecialHandler specialHandler) {
        request.setObjectData(unmarshalRequestXml(request));
        specialHandler.handleRequest(request);
        request.getRelatedResponse().setXmlData(marshalResponseXml(request));
    }

    protected ISpecialHandler getSpecialHandler(Request request) {
        ISpecialHandler specialHandler = null;
        if (request!= null && request.getCommand()!=null) {
            switch (request.getCommand().toLowerCase()) {
                case "foldersync":
                    specialHandler = folderHierarchyHandler;
                    break;
                default:

            }
        }
        return specialHandler;
    }


    protected Request validateRequest(Request request) {
        validateCredentials(request);

        Response response = request.getRelatedResponse();
        if (response.getGlobalStatus().getIntValue()==0 && !isCommandSupported(request.getCommand()))
            response.getGlobalStatus().setIntValue(Status.ErrorCommandNotSupported);
        if (response.getGlobalStatus().getIntValue()==0 && !isVersionSupported(request.getVersion()))
            response.getGlobalStatus().setIntValue(Status.ErrorVersionNotSupported);
        return request;
    }

    private boolean validateCredentials(Request request) {

        if (request == null ||
                request.getUserDevice() == null ||
                request.getUserDevice().getUsername()==null ||
                request.getUserDevice().getUsername().isEmpty())
            throw new AuthenticationFailedException();

        for (IBackendHandler backendHandler : BackendRegistry.getInstance().getBackendHandlerList()) {
            backendHandler.validateCredentials(request.getUserDevice());
        }

        return true;
    }

    private boolean isCommandSupported(String command){
        boolean result = false;

        if (command==null) return false;
        if (Arrays.asList(Constants.SUPPORTED_COMMANDS).contains(command)) result = true;

        return result;
    }

    private boolean isVersionSupported(String version){
        boolean result = false;

        if (version==null) return false;
        if (Arrays.asList(Constants.SUPPORTED_VERSIONS).contains(version)) result = true;

        return result;
    }

    private UserDevice persistUserDevice(UserDevice userDevice){
        UserDevice persistedUserDevice = userDeviceRepository.getFirstByUsernameAndDeviceId(userDevice.getUsername(), userDevice.getDeviceId());
        if (persistedUserDevice==null){
            userDeviceRepository.save(userDevice);
            persistedUserDevice = userDevice;
        }
        return persistedUserDevice;
    }

    // XML Handling
    public Object unmarshalRequestXml(Request request) {
        Object objectData = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance("de.chdev.jasync.xsd.request." + request.getCommandNamespace().toLowerCase());
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            objectData = unmarshaller.unmarshal(new StringReader(request.getXmlData()));
        } catch (Exception e) {
            if (request.getRelatedResponse().getGlobalStatus().getIntValue()==0)
                request.getRelatedResponse().getGlobalStatus().setIntValue(Status.ErrorInvalidXML);
        }
        return objectData;
    }

    public Object unmarshalResponseXml(Request request) throws JAXBException {
        JAXBContext   jaxbContext   = JAXBContext.newInstance( "de.chdev.jasync.xsd.response."+request.getCommandNamespace().toLowerCase() );
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Object objectData = unmarshaller.unmarshal(new StringReader(request.getRelatedResponse().getXmlData()));
        return objectData;
    }

    public String marshalRequestXml(Request request) throws JAXBException {
        JAXBContext   jaxbContext   = JAXBContext.newInstance( "de.chdev.jasync.xsd.request."+request.getCommandNamespace().toLowerCase() );
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(request.getObjectData(), stringWriter);
        return stringWriter.toString();
    }

    public String marshalResponseXml(Request request) {
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance("de.chdev.jasync.xsd.response." + request.getCommandNamespace().toLowerCase());
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(request.getRelatedResponse().getObjectData(), stringWriter);
        } catch (Exception e) {
            if (request.getRelatedResponse().getGlobalStatus().getIntValue()==0)
                request.getRelatedResponse().getGlobalStatus().setIntValue(Status.ErrorServerError);
        }
        return stringWriter.toString();
    }

}
