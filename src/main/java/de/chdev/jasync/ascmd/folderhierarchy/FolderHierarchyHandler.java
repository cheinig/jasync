package de.chdev.jasync.ascmd.folderhierarchy;

import de.chdev.jasync.backend.BackendRegistry;
import de.chdev.jasync.model.interfaces.IBackendHandler;
import de.chdev.jasync.model.interfaces.IFolderHierarchy;
import de.chdev.jasync.model.interfaces.ISpecialHandler;
import de.chdev.jasync.model.Request;
import de.chdev.jasync.model.Status;
import de.chdev.jasync.xsd.response.folderhierarchy.FolderSync;

import java.math.BigInteger;

public class FolderHierarchyHandler implements ISpecialHandler {

    public static final Integer STATUS_SUCCESS = 1;
    public static final Integer STATUS_SERVER_ERROR = 6;
    public static final Integer STATUS_SYNCKEY = 9;

    public Request handleRequest(Request request){
        switch (request.getCommand().toLowerCase()) {
            case "foldersync":
                handleFolderSync(request);
        }
        return request;
    }

    private Request handleFolderSync(Request request) {

        FolderSync folderSync = new FolderSync();
        if (request.getRelatedResponse().getGlobalStatus().getIntValue()== Status.OK) {

            folderSync.setChanges(new FolderSync.Changes());

            for (IBackendHandler backendHandler : BackendRegistry.getInstance().getBackendHandlerList(IFolderHierarchy.class)) {
                IFolderHierarchy folderHierarchy = (IFolderHierarchy) backendHandler;
                FolderSync.Changes backendChanges = folderHierarchy.getFolderChanges(0, request.getUserDevice());
                folderSync.getChanges().getAdd().addAll(backendChanges.getAdd());
                folderSync.getChanges().getDelete().addAll(backendChanges.getDelete());
                folderSync.getChanges().getUpdate().addAll(backendChanges.getUpdate());
            }

            Long count = new Long(folderSync.getChanges().getAdd().size() + folderSync.getChanges().getDelete().size() + folderSync.getChanges().getUpdate().size());
            folderSync.getChanges().setCount(count);

            folderSync.setStatus(BigInteger.valueOf(STATUS_SUCCESS));

            // Set dummy synckey
            folderSync.setSyncKey("1");

        } else {
            folderSync.setStatus(BigInteger.valueOf(request.getRelatedResponse().getGlobalStatus().getIntValue()));
        }

        request.getRelatedResponse().setObjectData(folderSync);

        return request;
    }
}
