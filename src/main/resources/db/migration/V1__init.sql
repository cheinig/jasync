create table UserDevice
(
  id bigint(20) not null auto_increment,
  username varchar(255) not null,
  deviceId varchar(255) not null,
  PRIMARY KEY (id)
);

create table UserSetting
(
  id bigint(20) not null auto_increment,
  userdevice_id bigint not null,
  backendId varchar(255) not null,
  parameter varchar(255),
  value varchar(2048),
  PRIMARY KEY (id)
);

create table Collection
(
  id bigint(20) not null auto_increment,
  userdevice_id bigint not null,
  backendId varchar(255) not null,
  backendCollectionId varchar(1024),
  parentCollection_id bigint,
  displayName varchar(1024),
  typeId integer,
  PRIMARY KEY (id)
);

create table SyncKey
(
  id bigint(20) not null auto_increment,
  userdevice_id bigint not null,
  collection_id bigint,
  iteration bigint,
  PRIMARY KEY (id)
);

ALTER TABLE UserSetting ADD CONSTRAINT fk_UserSetting_UserDevice FOREIGN KEY (userdevice_id) REFERENCES UserDevice(id);

ALTER TABLE Collection ADD CONSTRAINT fk_Collection_UserDevice FOREIGN KEY (userdevice_id) REFERENCES UserDevice(id);
ALTER TABLE Collection ADD CONSTRAINT fk_Collection_Parent FOREIGN KEY (parentCollection_id) REFERENCES Collection(id);

ALTER TABLE SyncKey ADD CONSTRAINT fk_SyncKey_UserDevice FOREIGN KEY (userdevice_id) REFERENCES UserDevice(id);
ALTER TABLE SyncKey ADD CONSTRAINT fk_SyncKey_Collection FOREIGN KEY (collection_id) REFERENCES Collection(id);
