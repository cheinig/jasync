#!/usr/bin/env bash

xjc -d src/main/generated -no-header -b src/main/resources/xsd/requestBinding.xjb src/main/resources/xsd/request/*.xsd
xjc -d src/main/generated -no-header -b src/main/resources/xsd/responseBinding.xjb src/main/resources/xsd/response/*.xsd
xjc -d src/main/generated -no-header -b src/main/resources/xsd/backend/dav/davbinding.xjb src/main/resources/xsd/backend/dav/*.xsd -nv